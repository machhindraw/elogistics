<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <button type="button" class="btn btn-success btn-icon left-icon" onclick="printAllReports('print_opening_balance');"><i class="fa fa-print"></i><span> Print</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            
                            <div class="clearfix"></div><br>                          
                            <div id="print_opening_balance">
                            <div class="print-opening-balance">
                                    <div class="row1">
                                        <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>

                                        <div class="report-bill">Opening Balance</div>
                                        <div class="clearfix"></div>
                                        <div class="report-bill-outer-section">
                                            <div class="table-wrap1">
                                                <table class="table table-bordered padding-td-none padding-th-none">
                                                    <thead>
                                                        <tr>
                                                            <th>Particulars</th>
                                                            <th width="12%" class="text-center">Dr</th>
                                                            <th width="12%" class="text-center">Cr</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3" class="border-top-bold"><strong>Liability</strong></td>
                                                        </tr> 
                                                        <tr>
                                                            <td class="border-bottom-none">Conveyance Deed Fund </td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=""></td>
                                                            <td class="border-top-bold text-right">0.00</td>
                                                            <td class="border-top-bold text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="border-none"><strong>Current Liabilities & Provision</strong></td>
                                                        </tr> 
                                                        <tr>
                                                            <td class="border-bottom-none">Audit Fees Payable</td>
                                                            <td class="text-right border-bottom-none"></td>
                                                            <td class="text-right border-bottom-none">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Educational Fund</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right">12500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=""></td>
                                                            <td class="border-top-bold text-right">0.00</td>
                                                            <td class="border-top-bold text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"><strong>Liability Group Total</strong></td>
                                                            <td class="border-top-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bold text-right font-weight-bold">437500</td>
                                                        </tr>


                                                        <tr>
                                                            <td colspan="3" class="border-top-bold"><strong>Assets</strong></td>
                                                        </tr> 
                                                        <tr>
                                                            <td class="border-bottom-none">Fixed Assets</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=""></td>
                                                            <td class="border-top-bold text-right">0.00</td>
                                                            <td class="border-top-bold text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="border-top-bold"><strong>Current Assets</strong></td>
                                                        </tr> 
                                                        <tr>
                                                            <td class="border-bottom-none">Cash</td>
                                                            <td class="text-right border-bottom-none"></td>
                                                            <td class="text-right border-bottom-none">12500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Bank</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right">12500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=""></td>
                                                            <td class="border-top-bold text-right">0.00</td>
                                                            <td class="border-top-bold text-right">437500</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"><strong>Assets Group Total</strong></td>
                                                            <td class="border-top-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bold text-right font-weight-bold">437500</td>
                                                        </tr>
                                                    </tbody>  
                                                    <br><br>
                                                </table> 
                                            </div>
                                        </div>  
                                    </div> 
                            </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

