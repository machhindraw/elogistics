<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ExcelsReportController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('PhpExcel', 'Session', 'Util', 'SocietyBill');
    public $helpers = array('PhpExcel');
    public $societyloginID = '';

    //public $uses = array('Society');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
        $this->societyloginID = $this->Session->read('Auth.User.id');
        $this->loadModel('User');
        $this->loadModel('Member');
        $this->loadModel('Society');
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberBillGenerate');
        $this->loadModel('MemberPayment');
        $this->loadModel('SocietyPayment');
        $this->loadModel('CashWithdraw');
        $this->loadAllNavigationModals();
    }

    function loadAllNavigationModals() {
        $this->SocietyBill->getSocietyBuildingLists();
        $this->SocietyBill->getSocietyLedgerHeadsDetails();
        $this->SocietyBill->navigateSocietyMemberDetails();
        $this->SocietyBill->loadAllNavigationModalsDropdowns();
    }

    public function download_account_bill_register() {
        $this->layout = false;
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $filename = isset($societyDetails['Society']['society_name']) ? 'Bill Register' . $societyDetails['Society']['society_name'] . '_' . $this->Util->getDateTime() : 'Bill Register -' . $this->Util->getDateTime();
        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array(base64_decode($this->request->query['bill_generated_date']), base64_decode($this->request->query['bill_generated_date_to']));
        } else if (isset($this->request->query['bill_generated_date_to']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date_to']);
        } else if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date']);
        }
        $billRegisterData = array();
        $billSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_generated_date' => 'asc', 'MemberBillSummary.bill_no' => 'asc')));
        $uniqueTariffsInBills = array();

        if (!empty($billSummaryDetails)) {
            foreach ($billSummaryDetails as $billDetails) {
                $memberBillDetails = $billDetails['MemberBillSummary'];
                $billRegisterData[$memberBillDetails['id']]['MemberBillSummary'] = $billDetails['MemberBillSummary'];
                $billRegisterData[$memberBillDetails['id']]['Member'] = $billDetails['Member'];
                $billTariffDetails = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.member_id' => $memberBillDetails['member_id'], 'MemberBillGenerate.bill_number' => $memberBillDetails['bill_no'])));
                if (!empty($billTariffDetails)) {
                    foreach ($billTariffDetails as $tariff) {
                        $memberBillTariff = $tariff['MemberBillGenerate'];
                        $billRegisterData[$memberBillDetails['id']]['MemberBillGenerate'][$memberBillTariff['ledger_head_id']] = $memberBillTariff['amount'];
                        if (!array_key_exists($tariff['MemberBillGenerate']['ledger_head_id'], $uniqueTariffsInBills)) {
                            $uniqueTariffsInBills[$tariff['MemberBillGenerate']['ledger_head_id']] = $tariff['SocietyLedgerHeads']['title'];
                        }
                    }
                }
            }
        }

        $sheetCount = 0;
        $excelCounter = 0;

        $tarrifVerticalTotal = array();
        $billSummaryFooterData = array();
        $monthlyAmtTotal = $inteAmtTotal = $billAmtTotal = $prinArrTotal = $inteArrTotal = $taxArrTotal = $payAmtTotal = $paidAmtTotal = $adjuAmtTotal = $balanceAmtTotal = 0.00;
        $finalBillRegisterArr[$excelCounter++] = array('#', 'Unit No', 'Member', 'For', 'Bill No', 'Bill Date');
        foreach ($uniqueTariffsInBills as $billLedgerTitle) {
            array_push($finalBillRegisterArr[0], $billLedgerTitle);
        }
        array_push($finalBillRegisterArr[0], "Amount");
        array_push($finalBillRegisterArr[0], "Interest");
        array_push($finalBillRegisterArr[0], "Bill Amt");
        array_push($finalBillRegisterArr[0], "Principal Arrear");
        array_push($finalBillRegisterArr[0], "Interest Arrear");
        array_push($finalBillRegisterArr[0], "Tax Arrear");
        array_push($finalBillRegisterArr[0], "Payable Amount");
        array_push($finalBillRegisterArr[0], "Amount Paid");
        array_push($finalBillRegisterArr[0], "Adjust");
        array_push($finalBillRegisterArr[0], "Balance Amount");
        $sr = 1;
        if (isset($billRegisterData) && count($billRegisterData) > 0) {
            foreach ($billRegisterData as $billDetails) {
                $monthlyAmtTotal +=$billDetails['MemberBillSummary']['monthly_amount'];
                $inteAmtTotal +=$billDetails['MemberBillSummary']['interest_on_due_amount'];
                $billAmtTotal +=$billDetails['MemberBillSummary']['monthly_bill_amount'];
                $prinArrTotal +=$billDetails['MemberBillSummary']['op_principal_arrears'];
                $inteArrTotal +=$billDetails['MemberBillSummary']['op_interest_arrears'];
                $taxArrTotal +=$billDetails['MemberBillSummary']['op_tax_arrears'];
                $payAmtTotal +=$billDetails['MemberBillSummary']['amount_payable'];
                $balanceAmtTotal +=$billDetails['MemberBillSummary']['balance_amount'];

                $paidAmtTotal += ($billDetails['MemberBillSummary']['principal_paid'] + $billDetails['MemberBillSummary']['interest_paid'] + $billDetails['MemberBillSummary']['tax_paid']);
                $adjuAmtTotal +=($billDetails['MemberBillSummary']['interest_adjusted'] + $billDetails['MemberBillSummary']['tax_adjusted'] + $billDetails['MemberBillSummary']['principal_adjusted']);

                $finalBillRegisterArr[$excelCounter]['id'] = $sr;
                $finalBillRegisterArr[$excelCounter]['flat_no'] = $billDetails['Member']['flat_no'];
                $finalBillRegisterArr[$excelCounter]['member_name'] = $billDetails['Member']['member_name'];
                $finalBillRegisterArr[$excelCounter]['month'] = $billDetails['MemberBillSummary']['month'];
                $finalBillRegisterArr[$excelCounter]['bill_no'] = $billDetails['MemberBillSummary']['bill_no'];
                $finalBillRegisterArr[$excelCounter]['bill_generated_date'] = $this->Util->getFormatDate($billDetails['MemberBillSummary']['bill_generated_date'], 'd/m/Y');
                /* monthly tarrif amount */
                foreach ($uniqueTariffsInBills as $billLedgerId => $billLedgerTitle) {
                    $amount = 0.00;
                    if (isset($billDetails['MemberBillGenerate'][$billLedgerId])) {
                        $finalBillRegisterArr[$excelCounter][$billLedgerTitle] = $billDetails['MemberBillGenerate'][$billLedgerId];
                        $amount = $billDetails['MemberBillGenerate'][$billLedgerId];
                        $tarrifVerticalTotal[$billLedgerId]['amount'] = isset($tarrifVerticalTotal[$billLedgerId]['amount']) ? $tarrifVerticalTotal[$billLedgerId]['amount'] + $amount : $amount;
                    } else {
                        $finalBillRegisterArr[$excelCounter][$billLedgerTitle] = $amount;
                    }
                }
                $finalBillRegisterArr[$excelCounter]['monthly_amount'] = $billDetails['MemberBillSummary']['monthly_amount'];
                $finalBillRegisterArr[$excelCounter]['interest_on_due_amount'] = $billDetails['MemberBillSummary']['interest_on_due_amount'];
                $finalBillRegisterArr[$excelCounter]['monthly_bill_amount'] = $billDetails['MemberBillSummary']['monthly_bill_amount'];
                $finalBillRegisterArr[$excelCounter]['op_principal_arrears'] = $billDetails['MemberBillSummary']['op_principal_arrears'];
                $finalBillRegisterArr[$excelCounter]['op_interest_arrears'] = $billDetails['MemberBillSummary']['op_interest_arrears'];
                $finalBillRegisterArr[$excelCounter]['op_tax_arrears'] = $billDetails['MemberBillSummary']['op_tax_arrears'];
                $finalBillRegisterArr[$excelCounter]['amount_payable'] = $billDetails['MemberBillSummary']['amount_payable'];
                $finalBillRegisterArr[$excelCounter]['Paid'] = $billDetails['MemberBillSummary']['principal_paid'] + $billDetails['MemberBillSummary']['interest_paid'] + $billDetails['MemberBillSummary']['tax_paid'];
                $finalBillRegisterArr[$excelCounter]['Adjust'] = $billDetails['MemberBillSummary']['interest_adjusted'] + $billDetails['MemberBillSummary']['tax_adjusted'] + $billDetails['MemberBillSummary']['principal_adjusted'];
                $finalBillRegisterArr[$excelCounter]['Bal'] = $billDetails['MemberBillSummary']['balance_amount'];
                $sr++;
                $excelCounter++;
            }
            unset($billRegisterData);
        }
        /* bill summary footer */

        array_push($billSummaryFooterData, " ");
        array_push($billSummaryFooterData, " ");
        array_push($billSummaryFooterData, " ");
        array_push($billSummaryFooterData, " ");
        array_push($billSummaryFooterData, " ");
        array_push($billSummaryFooterData, "Grand Total:");
        foreach ($tarrifVerticalTotal as $billLedgerId => $amountTariff) {
            array_push($billSummaryFooterData, $amountTariff['amount']);
        }
        array_push($billSummaryFooterData, $monthlyAmtTotal);
        array_push($billSummaryFooterData, $inteAmtTotal);
        array_push($billSummaryFooterData, $billAmtTotal);
        array_push($billSummaryFooterData, $prinArrTotal);
        array_push($billSummaryFooterData, $inteArrTotal);
        array_push($billSummaryFooterData, $taxArrTotal);
        array_push($billSummaryFooterData, $payAmtTotal);
        array_push($billSummaryFooterData, $paidAmtTotal);
        array_push($billSummaryFooterData, $adjuAmtTotal);
        array_push($billSummaryFooterData, $balanceAmtTotal);

        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $lastRowCounter = "";

        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->setTitle('Master');
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Bill Register');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 0;
        foreach ($finalBillRegisterArr as $billRegData) {
            foreach ($billRegData as $billData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                if ($numberFormatCounter > 4) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $billData);
                $lastRowCounter = $innerrowCounter;
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }
        $this->PhpExcel->setRow($innerColumnCounter);
        $this->PhpExcel->addTableRow($billSummaryFooterData);
        $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $lastRowCounter . $innerColumnCounter)->getFont()->setBold(true);
        $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $lastRowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $lastRowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $lastRowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }

    public function download_account_bill_half() {
        $this->layout = false;
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $filename = isset($societyDetails['Society']['society_name']) ? 'Bill Half Page' . $societyDetails['Society']['society_name'] . '_' . $this->Util->getDateTime() : 'Bill Register -' . $this->Util->getDateTime();
        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);

        if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['OR']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array(base64_decode($this->request->query['bill_generated_date']), base64_decode($this->request->query['bill_generated_date_to']));
        } else if (isset($this->request->query['bill_generated_date_to']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date_to']);
        } else if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date']);
        }

        if (isset($this->request->query['month']) && !empty($this->request->query['month']) && !empty($this->request->query['month_to'])) {
            $options['OR']['MemberBillSummary.month BETWEEN ? and ?'] = array(base64_decode($this->request->query['month']), base64_decode($this->request->query['month_to']));
        } else if (isset($this->request->query['month']) && !empty($this->request->query['month'])) {
            $options['AND']['MemberBillSummary.month'] = base64_decode($this->request->query['month']);
        } else if (isset($this->request->query['month_to']) && !empty($this->request->query['month_to'])) {
            $options['AND']['MemberBillSummary.month'] = base64_decode($this->request->query['month_to']);
        }

        if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no']) && !empty($this->request->query['flat_no_to'])) {
            $options['OR']['MemberBillSummary.flat_no BETWEEN ? and ?'] = array(base64_decode($this->request->query['flat_no']), base64_decode($this->request->query['flat_no_to']));
        } else if (isset($this->request->query['flat_no_to']) && !empty($this->request->query['flat_no_to'])) {
            $options['AND']['MemberBillSummary.flat_no'] = base64_decode($this->request->query['flat_no_to']);
        } else if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no'])) {
            $options['AND']['MemberBillSummary.flat_no'] = base64_decode($this->request->query['flat_no']);
        }

        if (isset($this->request->query['bill_no']) && !empty($this->request->query['bill_no']) && !empty($this->request->query['bill_no_to'])) {
            $options['OR']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array(base64_decode($this->request->query['bill_no']), base64_decode($this->request->query['bill_no_to']));
        } else if (isset($this->request->query['bill_no']) && !empty($this->request->query['bill_no'])) {
            $options['AND']['MemberBillSummary.bill_no'] = base64_decode($this->request->query['bill_no']);
        } else if (isset($this->request->query['bill_no_to']) && !empty($this->request->query['bill_no_to'])) {
            $options['AND']['MemberBillSummary.bill_no'] = base64_decode($this->request->query['bill_no_to']);
        }

        $monthlyBillsSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_no' => 'asc')));
        if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
            $summaryCounter = 0;
            foreach ($monthlyBillsSummaryDetails as $monthlyData) {
                $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['month']);
                $tariffDetails = $this->MemberBillGenerate->find('all', array('fields' => array('MemberBillGenerate.ledger_head_id', 'MemberBillGenerate.amount', 'SocietyLedgerHeads.title'), 'conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.month' => $monthlyData['MemberBillSummary']['month'], 'MemberBillGenerate.member_id' => $monthlyData['MemberBillSummary']['member_id'], 'MemberBillGenerate.amount !=' => 0.00)));
                if (isset($tariffDetails) && !empty($tariffDetails)) {
                    foreach ($tariffDetails as $key => $tariffData) {
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    }
                }
                $summaryCounter++;
            }
        }

        //print_r($monthlyBillsSummaryDetails);die;
        /* Excel Bill half page Started */
        $sheetCount = 0;
        $nexttblStart = $borderStart = $borderEnd = 0;
        
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->setTitle('Master');
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        //$this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $this->PhpExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $this->PhpExcel->getActiveSheet()->setTitle('Bill Register');
        $this->PhpExcel->getActiveSheet()->setTitle('Bill half page report');

        if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
            foreach ($monthlyBillsSummaryDetails as $memberbillReport) {
                $societyName = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Society']['society_name']) ? $memberbillReport['Society']['society_name'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false));
                $registrationNumber = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Registration No :"), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Society']['registration_no']) ? $memberbillReport['Society']['registration_no'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false));
                $societyAddress = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Address :"), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Society']['address']) ? $memberbillReport['Society']['address'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false));
                $unitNo = array(array('label' => __("Unit No: "), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Member']['flat_no']) ? $memberbillReport['Member']['flat_no'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" Unit Area : "), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Member']['area']) ? $memberbillReport['Member']['area'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __("Unit type :"), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Member']['unit_type']) ? $memberbillReport['Member']['unit_type'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __("Bill Due Date : "), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['MemberBillSummary']['bill_generated_date']) ? $memberbillReport['MemberBillSummary']['bill_generated_date'] : ''), 'width' => 'auto', 'filter' => false));
                $billFor = array(array('label' => __("Bill For :"), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['MemberBillSummary']['month']) ? $memberbillReport['MemberBillSummary']['month'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" Unit Area : "), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['Member']['area']) ? $memberbillReport['Member']['area'] : ''), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Due Date :"), 'width' => 'auto', 'filter' => false), array('label' => __(isset($memberbillReport['MemberBillSummary']['bill_due_date']) ? $memberbillReport['MemberBillSummary']['bill_due_date'] : ''), 'width' => 'auto', 'filter' => false));
                $tariffHeader = array(array('label' => __("Sr."), 'width' => 'auto', 'filter' => false), array('label' => __("PARTICULAR OF CHARGES"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Amount"), 'width' => 'auto', 'filter' => false));
                //$this->PhpExcel->getActiveSheet()->getStyle("A$borderStart:K$borderEnd")->getBorders()->applyFromArray(array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000'))));
                $blankArr = Array();
                
                array_push($blankArr, " ");
                $this->PhpExcel->addTableRow($blankArr);
                $nexttblStart++;
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($societyName, array('name' => 'Arial', 'bold' => true, 'size' => 15));
                $nexttblStart++;
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($registrationNumber, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($societyAddress, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($unitNo, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($billFor, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("B" . $nexttblStart . ":K" . $nexttblStart);
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($tariffHeader, array('name' => 'Arial', 'bold' => true, 'size' => 10));
                $this->PhpExcel->getActiveSheet()->getStyle("A" . $nexttblStart . ":K" . $nexttblStart)->getBorders()->applyFromArray(array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000'))));
                $this->PhpExcel->getActiveSheet()->getStyle('A' . $nexttblStart . ":" . 'K' . $nexttblStart)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'd6d6c2'))));
                $nexttblStart++;
                $tarriSumTotal = 0;
                if (isset($memberbillReport['MemberTariff']) && count($memberbillReport['MemberTariff']) > 0) {
                    $srCounter = 1;
                    foreach ($memberbillReport['MemberTariff'] as $key => $tariffData) {
                        $tarriSumTotal += $tariffData['amount'];
                        $this->PhpExcel->getActiveSheet()->mergeCells("B" . $nexttblStart . ":K" . $nexttblStart);
                        $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                        $tariffData = array(array('label' => __($srCounter), 'width' => 'auto', 'filter' => false), array('label' => __($tariffData['title']), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __($tariffData['amount']), 'width' => 'auto', 'filter' => false));
                        $this->PhpExcel->setRow($nexttblStart);
                        $this->PhpExcel->addTableHeader($tariffData, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                        $nexttblStart++;
                        $srCounter++;
                    }
                }
                
                $AmountInWord = $this->Util->convertToWords(abs($memberbillReport['MemberBillSummary']['balance_amount']));
                $totalTariffAmt = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Total"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("$tarriSumTotal"), 'width' => 'auto', 'filter' => false));
                $AddInterestAmt = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Add : Interest"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("$tarriSumTotal"), 'width' => 'auto', 'filter' => false));
                $totalAdjustmentAmt = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Less : Adjustment"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("$tarriSumTotal"), 'width' => 'auto', 'filter' => false));
                $totalPrincipalArrearsAmt = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Principal Arrears"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("$tarriSumTotal"), 'width' => 'auto', 'filter' => false));
                $totalInterestArrearsAmt = array(array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Interest Arrears"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("$tarriSumTotal"), 'width' => 'auto', 'filter' => false));
                $AmtPayableFooter = array(array('label' => __($AmountInWord), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("  "), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __("Total Due & Payable Amount"), 'width' => 'auto', 'filter' => false), array('label' => __(" "), 'width' => 'auto', 'filter' => false), array('label' => __($memberbillReport['MemberBillSummary']['balance_amount']), 'width' => 'auto', 'filter' => false));
                
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->mergeCells("J" . $nexttblStart . ":K" . $nexttblStart);
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->addTableHeader($totalTariffAmt, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->mergeCells("J" . $nexttblStart . ":K" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($AddInterestAmt, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->mergeCells("J" . $nexttblStart . ":K" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($totalAdjustmentAmt, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($totalPrincipalArrearsAmt, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($totalInterestArrearsAmt, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $nexttblStart++;
                $this->PhpExcel->getActiveSheet()->mergeCells("A" . $nexttblStart . ":I" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $this->PhpExcel->setRow($nexttblStart);
                $this->PhpExcel->addTableHeader($AmtPayableFooter, array('name' => 'Arial', 'bold' => false, 'size' => 10));
                $this->PhpExcel->getActiveSheet()->getStyle("J" . $nexttblStart . ":K" . $nexttblStart)->getFont()->setBold(true);
                $this->PhpExcel->getActiveSheet()->mergeCells("J" . $nexttblStart . ":K" . $nexttblStart);
                $this->PhpExcel->getActiveSheet()->getStyle("L$nexttblStart:L$nexttblStart")->getNumberFormat()->setFormatCode("0.00");
                $nexttblStart++;
                $this->PhpExcel->addTableRow($blankArr);
                $nexttblStart++;
            }
        }
        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
        
    }
    
    public function download_member_collection_register(){
        $this->layout = false;
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyBankBalanceHeadsLists = $this->SocietyBill->societyBankBalanceHeadsLists();
        $societyCashBalanceHeadsLists = $this->SocietyBill->societyCashBalanceHeadsLists();
        $filename = isset($societyDetails['Society']['society_name']) ? 'Bill Register' . $societyDetails['Society']['society_name'] . '_' . $this->Util->getDateTime() : 'Member Collection Register -' . $this->Util->getDateTime();
        $options['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));

        $conditionPayment = '';
        if (isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) && !empty($this->request->query['payment_date_to'])) {
            $conditionPayment['AND']['MemberPayment.payment_date BETWEEN ? and ?'] = array(base64_decode($this->request->query['payment_date']), base64_decode($this->request->query['payment_date_to']));
        } else if (isset($this->request->query['payment_date_to']) && !empty($this->request->query['payment_date_to'])) {
            $conditionPayment['AND']['MemberPayment.payment_date'] = base64_decode($this->request->query['payment_date_to']);
        } else if (isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date'])) {
            $conditionPayment['AND']['MemberPayment.payment_date'] = base64_decode($this->request->query['payment_date']);
        }

        if (isset($this->request->query['receipt_id']) && !empty($this->request->query['receipt_id']) && !empty($this->request->query['receipt_id_to'])) {
            $conditionPayment['AND']['MemberPayment.receipt_id BETWEEN ? and ?'] = array(base64_decode($this->request->query['receipt_id']), base64_decode($this->request->query['receipt_id_to']));
        } else if (isset($this->request->query['receipt_id_to']) && !empty($this->request->query['receipt_id_to'])) {
            $conditionPayment['AND']['MemberPayment.receipt_id'] = base64_decode($this->request->query['receipt_id_to']);
        } else if (isset($this->request->query['receipt_id']) && !empty($this->request->query['receipt_id'])) {
            $conditionPayment['AND']['MemberPayment.receipt_id'] = base64_decode($this->request->query['receipt_id']);
        }
        
        if (isset($this->request->query['building_id']) && !empty($this->request->query['building_id'])) {
            $options['AND']['Member.building_id'] = base64_decode($this->request->query['building_id']);
        }
        if (isset($this->request->query['wing_id']) && !empty($this->request->query['wing_id'])) {
            $options['AND']['Member.wing_id'] = base64_decode($this->request->query['wing_id']);
        }

        if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no']) && !empty($this->request->query['flat_no_to'])) {
            $options['AND']['Member.flat_no BETWEEN ? and ?'] = array(base64_decode($this->request->query['flat_no']), base64_decode($this->request->query['flat_no_to']));
        } else if (isset($this->request->query['flat_no_to']) && !empty($this->request->query['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = base64_decode($this->request->query['flat_no_to']);
        } else if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no'])) {
            $options['AND']['Member.flat_no'] = base64_decode($this->request->query['flat_no']);
        }
        
        $this->Member->Behaviors->attach('Containable');
        $societyMemberPaymentCollectionData = $this->Member->find('all', array('contain' => array('Society','Building','Wing','MemberPayment' => array('conditions' => $conditionPayment)), 'conditions' => $options,'order' => array('Member.id' => 'asc')));
        
        $sheetCount = 0;
        $excelCounter = 0;
        $memberReceiptCollectionData[$excelCounter++] = array('#', 'MONTH', 'BUILDING', 'WING', 'UNIT NO', 'RECEIPT DATE', 'RECEIPT NO.', 'MEMBER NAME', 'AMOUNT', 'CHQNO', 'CHQDATE', 'BANK NAME', 'BRANCH NAME');
        if (isset($societyMemberPaymentCollectionData) && count($societyMemberPaymentCollectionData) > 0) {
            $idCount = 1;
            foreach ($societyMemberPaymentCollectionData as $collectionPaymentData) {
                if (isset($collectionPaymentData['MemberPayment']) && count($collectionPaymentData['MemberPayment']) > 0) {
                    foreach ($collectionPaymentData['MemberPayment'] as $memberPaymentData) {
                        $memberReceiptCollectionData[$excelCounter]['id'] = $idCount;
                        $memberReceiptCollectionData[$excelCounter]['bill_month'] = isset($memberPaymentData['bill_month']) ? $memberPaymentData['bill_month'] : '';
                        $memberReceiptCollectionData[$excelCounter]['Building'] = isset($collectionPaymentData['Building']['building_name']) ? $collectionPaymentData['Building']['building_name'] : '';
                        $memberReceiptCollectionData[$excelCounter]['Wing'] = isset($collectionPaymentData['Wing']['building_name']) ? $collectionPaymentData['Wing']['building_name'] : '';
                        $memberReceiptCollectionData[$excelCounter]['flat_no'] = isset($collectionPaymentData['Member']['flat_no']) ? $collectionPaymentData['Member']['flat_no'] : '';
                        $memberReceiptCollectionData[$excelCounter]['payment_date'] = isset($memberPaymentData['payment_date']) ? $this->Util->getFormatDate($memberPaymentData['payment_date'], 'd/m/Y') : '';
                        $memberReceiptCollectionData[$excelCounter]['receipt_id'] = isset($memberPaymentData['receipt_id']) ? $memberPaymentData['receipt_id'] : '';
                        $memberReceiptCollectionData[$excelCounter]['member_name'] = isset($collectionPaymentData['Member']['member_name']) ? $collectionPaymentData['Member']['member_name'] : '';
                        $memberReceiptCollectionData[$excelCounter]['amount_paid'] = isset($memberPaymentData['amount_paid']) ? $memberPaymentData['amount_paid'] : '';
                        $memberReceiptCollectionData[$excelCounter]['cheque_reference_number'] = isset($memberPaymentData['cheque_reference_number']) ? $memberPaymentData['cheque_reference_number'] : '';
                        $memberReceiptCollectionData[$excelCounter]['entry_date'] = isset($memberPaymentData['entry_date']) ? $memberPaymentData['entry_date'] : '';
                        if (isset($memberPaymentData['payment_mode']) && $memberPaymentData['payment_mode'] == Configure::read('PaymentMode.Cash')) {
                            $memberReceiptCollectionData[$excelCounter]['society_bank_id'] = isset($societyCashBalanceHeadsLists[$memberPaymentData['society_bank_id']]) ? $societyCashBalanceHeadsLists[$memberPaymentData['society_bank_id']] : '';
                        } else {
                            $memberReceiptCollectionData[$excelCounter]['society_bank_id'] = isset($societyBankBalanceHeadsLists[$memberPaymentData['society_bank_id']]) ? $societyBankBalanceHeadsLists[$memberPaymentData['society_bank_id']] : '';
                        }
                        $memberReceiptCollectionData[$excelCounter]['member_bank_branch'] = isset($memberPaymentData['member_bank_branch']) ? $memberPaymentData['member_bank_branch'] : '-';
                        $excelCounter++;
                        $idCount++;
                    }
                }
            }
            unset($societyMemberPaymentCollectionData);
        }
        
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $lastRowCounter = "";
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Member Collection Register');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 0;
        foreach ($memberReceiptCollectionData as $memberPaymentReceiptData) {
            foreach ($memberPaymentReceiptData as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $lastRowCounter = $innerrowCounter;
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }
        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }
    
    
    public function download_account_gst_register(){
        $this->layout = false;
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $filename = isset($societyDetails['Society']['society_name']) ? 'Gst Register' . $societyDetails['Society']['society_name'] . '_' . $this->Util->getDateTime() : 'Gst Register -' . $this->Util->getDateTime();
        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array(base64_decode($this->request->query['bill_generated_date']), base64_decode($this->request->query['bill_generated_date_to']));
        } else if (isset($this->request->query['bill_generated_date_to']) && !empty($this->request->query['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date_to']);
        } else if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = base64_decode($this->request->query['bill_generated_date']);
        }

        if (isset($this->request->query['building_id']) && !empty($this->request->query['building_id'])) {
            $options['AND']['Member.building_id'] = base64_decode($this->request->query['building_id']);
        }
        if (isset($this->request->query['wing_id']) && !empty($this->request->query['wing_id'])) {
            $options['AND']['Member.wing_id'] = base64_decode($this->request->query['wing_id']);
        }

        if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no']) && !empty($this->request->query['flat_no_to'])) {
            $options['AND']['Member.flat_no BETWEEN ? and ?'] = array(base64_decode($this->request->query['flat_no']), base64_decode($this->request->query['flat_no_to']));
        } else if (isset($this->request->query['flat_no_to']) && !empty($this->request->query['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = base64_decode($this->request->query['flat_no_to']);
        } else if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no'])) {
            $options['AND']['Member.flat_no'] = base64_decode($this->request->query['flat_no']);
        }

        $billSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_generated_date' => 'asc', 'MemberBillSummary.bill_no' => 'asc')));

        $gstRegisterData = array();
        $uniqueTariffsInBills = array();
        if (!empty($billSummaryDetails)) {
            foreach ($billSummaryDetails as $billDetails) {
                $memberBillDetails = $billDetails['MemberBillSummary'];

                $gstRegisterData[$memberBillDetails['id']]['MemberBillSummary'] = array('bill_no' => $billDetails['MemberBillSummary']['bill_no'], 'bill_date' => $billDetails['MemberBillSummary']['bill_generated_date'], 'member_id' => $billDetails['MemberBillSummary']['member_id'], 'gstin_no' => $billDetails['Society']['gstin_no']);
                $gstRegisterData[$memberBillDetails['id']]['Member'] = $billDetails['Member'];

                $billTariffDetails = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.member_id' => $memberBillDetails['member_id'], 'MemberBillGenerate.bill_number' => $memberBillDetails['bill_no'], 'SocietyLedgerHeads.is_tax_applicable' => 1)));
                if (!empty($billTariffDetails)) {
                    foreach ($billTariffDetails as $tariff) {
                        $memberBillTariff = $tariff['MemberBillGenerate'];
                        $gstRegisterData[$memberBillDetails['id']]['MemberBillGenerate'][$memberBillTariff['ledger_head_id']] = array('amount' => $memberBillTariff['amount'], 'cgst' => $memberBillTariff['cgst_total'], 'sgst' => $memberBillTariff['sgst_total']);

                        if (!array_key_exists($tariff['MemberBillGenerate']['ledger_head_id'], $uniqueTariffsInBills)) {
                            $uniqueTariffsInBills[$tariff['MemberBillGenerate']['ledger_head_id']] = $tariff['SocietyLedgerHeads']['title'];
                        }
                    }
                }
            }
        }

        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;
        $billAmount = $sgstTotal = $cgstTotal = 0.00;
        $billAmtTotal = $cgstAmtTotal = $sgstAmtTotal = $totalTaxAmtTotal = 0.00;
        $gstRegisterFinalData[$excelCounter++] = array('#', 'Unit No', 'Bill No', 'MEMBER NAME', 'BILL DATE');
        foreach ($uniqueTariffsInBills as $billLedgerTitle) {
            array_push($gstRegisterFinalData[0], $billLedgerTitle);
        }
        array_push($gstRegisterFinalData[0], "Bill Amount");
        array_push($gstRegisterFinalData[0], "SGST @9%");
        array_push($gstRegisterFinalData[0], "CGST @9%");
        array_push($gstRegisterFinalData[0], "NET TAX AMOUNT");
        array_push($gstRegisterFinalData[0], "GST NO");
        $tarrifVerticalTotal = array();
        $srCouner = 1;
        foreach ($gstRegisterData as $billDetails) {
            $gstRegisterFinalData[$excelCounter]['sr'] = $srCouner;
            $gstRegisterFinalData[$excelCounter]['flat_no'] = $billDetails['Member']['flat_no'];
            $gstRegisterFinalData[$excelCounter]['bill_no'] = $billDetails['MemberBillSummary']['bill_no'];
            $gstRegisterFinalData[$excelCounter]['member_name'] = $billDetails['Member']['member_name'];
            $gstRegisterFinalData[$excelCounter]['bill_date'] = isset($billDetails['MemberBillSummary']['bill_date']) ? $this->Util->getFormatDate($billDetails['MemberBillSummary']['bill_date'], 'd/m/Y') : '';
            foreach ($uniqueTariffsInBills as $billLedgerId => $billLedgerTitle) {
                $amount = 0.00;
                if (isset($billDetails['MemberBillGenerate'][$billLedgerId])) {
                    $amount = $billDetails['MemberBillGenerate'][$billLedgerId]['amount'];
                    $sgstTotal += $billDetails['MemberBillGenerate'][$billLedgerId]['sgst'];
                    $cgstTotal += $billDetails['MemberBillGenerate'][$billLedgerId]['cgst'];
                    $billAmount += $amount;
                    $tarrifVerticalTotal[$billLedgerId]['amount'] = isset($tarrifVerticalTotal[$billLedgerId]['amount']) ? $tarrifVerticalTotal[$billLedgerId]['amount'] + $amount : $amount;
                }
                $gstRegisterFinalData[$excelCounter][$billLedgerId.'_amount'] = $amount;
            }
            $billAmtTotal += $billAmount;
            $cgstAmtTotal += $cgstTotal;
            $sgstAmtTotal += $sgstTotal;
            $totalTaxAmtTotal = $sgstAmtTotal + $cgstAmtTotal;
            $gstRegisterFinalData[$excelCounter]['billAmtTotal'] = $billAmtTotal;
            $gstRegisterFinalData[$excelCounter]['cgstAmtTotal'] = $cgstAmtTotal;
            $gstRegisterFinalData[$excelCounter]['sgstAmtTotal'] = $sgstAmtTotal;
            $gstRegisterFinalData[$excelCounter]['totalTaxAmtTotal'] = $totalTaxAmtTotal;
            $gstRegisterFinalData[$excelCounter]['gstin_no'] = isset($billDetails['Member']['gstin_no']) && !empty($billDetails['Member']['gstin_no']) ? $billDetails['Member']['gstin_no'] : '-';
            $excelCounter++;
            $srCouner++;
        }
        //print_r($gstRegisterFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Gst Register');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $gstCounter = 0;
        foreach ($gstRegisterFinalData as $gstRegisterDetails) {
            foreach ($gstRegisterDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                if ($gstCounter > 4) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $gstCounter++;
            }
            $gstCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }
    
    public function download_bank_book_sheet(){
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('CashWithdraw');
        $this->loadModel('SocietyOtherIncome');
        $this->loadModel('SocietyPayment');
        $this->layout = false;
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d'); 
        $filename = $societyDetails['Society']['society_name'].'_'.$reQuestedDate.'_Bank Book sheet';
        $operator = '=';       
        
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);
        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $optionsCashWithdraw['AND'] = array('CashWithdraw.society_id' => $societyLoginID);
        $optionsContraReceived['AND'] = array('CashWithdraw.society_id' => $societyLoginID);
        
        
        if (isset($this->request->query['society_bank_id']) && !empty($this->request->query['society_bank_id'])) {
            $options['AND']['MemberPayment.society_bank_id'] = base64_decode($this->request->query['society_bank_id']);
            $optionsCashWithdraw['AND']['CashWithdraw.bank_ledger_head_id'] = base64_decode($this->request->query['society_bank_id']);
            $optionsContraReceived['AND']['CashWithdraw.bank_to_ledger_head_id'] = base64_decode($this->request->query['society_bank_id']);
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = base64_decode($this->request->query['society_bank_id']);
            //For Ledger head opening balance
            $ledgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => base64_decode($this->request->query['society_bank_id']), 'SocietyLedgerHeads.society_id' => $societyLoginID)));
        }
        if (isset($this->request->query['operator']) && !empty($this->request->query['operator'])) {
            $operator = base64_decode($this->request->query['operator']);
        }
        
        if (isset($this->request->query['amount']) && !empty($this->request->query['amount'])) {
            $options['AND']['MemberPayment.amount ' . $operator . ' '] = base64_decode($this->request->query['amount']);
            $optionsCashWithdraw['AND']['CashWithdraw.amount ' . $operator . ' '] = base64_decode($this->request->query['amount']);
            $optionsContraReceived['AND']['CashWithdraw.amount ' . $operator . ' '] = base64_decode($this->request->query['amount']);
            $optionsSociety['AND']['SocietyPayment.amount ' . $operator . ' '] = base64_decode($this->request->query['amount']);
        }

        if (isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) && !empty($this->request->query['payment_date_to'])) {
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array(base64_decode($this->request->query['payment_date']), base64_decode($this->request->query['payment_date_to']));
            $optionsCashWithdraw['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->query['payment_date'], base64_decode($this->request->query['payment_date_to']));
            $optionsContraReceived['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->query['payment_date'], base64_decode($this->request->query['payment_date_to']));
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->query['payment_date'],base64_decode($this->request->query['payment_date_to']));
        } else if (isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date'])) {
            $options['AND']['MemberPayment.payment_date'] = base64_decode($this->request->query['payment_date']);
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = base64_decode($this->request->query['payment_date']);
            $optionsContraReceived['AND']['CashWithdraw.payment_date'] = $this->request->query['payment_date'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = base64_decode($this->request->query['payment_date']);
        } else if (isset($this->request->query['payment_date_to']) && !empty($this->request->query['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date'] = base64_decode($this->request->query['payment_date']);
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = base64_decode($this->request->query['payment_date_to']);
            $optionsContraReceived['AND']['CashWithdraw.payment_date'] = base64_decode($this->request->query['payment_date_to']);
            $optionsSociety['AND']['SocietyPayment.payment_date'] = base64_decode($this->request->query['payment_date_to']);
        }       
        
        $bankBookData = array();
        $societyBankBbookDetails = array();
        $memberBankBbookDetails = array();
        $cashWithdrawDetails = array();
        $SocietyOtherIncomeDetails = array();

       // if ($this->request->query['report_type']) {           
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            if (isset($this->request->query['report_type']) && !empty($this->request->query['report_type']) && base64_decode($this->request->query['report_type']) == "Deposit") {
                $memberBankBbookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'deposit';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $SocietyOtherIncomeDetails = $this->SocietyOtherIncome->find('all', array('conditions' => array('SocietyOtherIncome.payment_mode'=>'Bank','SocietyOtherIncome.society_bank_id'=>base64_decode($this->request->query['society_bank_id'])),'order' => array('SocietyOtherIncome.payment_date' => 'asc')));
            } else if (isset($this->request->query['report_type']) && !empty($this->request->query['report_type']) && base64_decode($this->request->query['report_type']) == "Withdrawal") {
                $societyBankBbookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                //print_r($SocietyOtherIncomeDetails);
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'withdraw';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else if (isset($this->request->query['report_type']) && !empty($this->request->query['report_type']) && base64_decode($this->request->query['report_type']) == "Contra") {
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'contra';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $cashContraReceivedDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsContraReceived, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else {                
                $memberBankBbookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $societyBankBbookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $cashContraReceivedDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsContraReceived, 'order' => array('CashWithdraw.payment_date' => 'asc')));
           }
           
          if (isset($societyBankBbookDetails) && !empty($societyBankBbookDetails)) {
                $sCounter = 0;
                foreach ($societyBankBbookDetails as $societyBankBbookData) {
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . ' &' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                    unset($societyBankBbookData);
                    $sCounter++;
                }
            }
            
            if (isset($memberBankBbookDetails) && !empty($memberBankBbookDetails)) {
                $mCounter = 0;
                foreach ($memberBankBbookDetails as $memberBankBbookData) {
                    // echo '<pre>'; print_r($memberBankBbookData);
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['flat_no'] . ' - ' . $memberBankBbookData['Member']['member_name'] . '<br>' . 'Being Maintenance received';
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                    unset($memberBankBbookData);
                    $mCounter++;
                }
            }
            $cashCounter = 0;
            if (isset($cashWithdrawDetails) && !empty($cashWithdrawDetails)) {
                foreach ($cashWithdrawDetails as $cashWithdrawData) {
                    $note = '';
                    switch ($cashWithdrawData['CashWithdraw']['txn_type']) {
                        case 'contra' :
                            $flag = 'Contra';
                            $note = 'Contra Trasferred';
                            break;
                        case 'deposit' :
                            $flag = 'Deposit';
                            $note = 'Deposited in Bank';
                            break;
                        case 'withdraw' :
                            $flag = 'Withdrawal';
                            $note = 'Withdrawn from Bank';
                            break;
                    }
                    //echo '<pre>'; print_r($cashWithdrawData);
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $cashWithdrawData['CashWithdraw']['payment_date'];
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $cashWithdrawData['CashWithdraw']['particulars'] . '<br>' . $note;
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $cashWithdrawData['CashWithdraw']['cheque_no'];

                    if ($flag == 'Contra' || $flag == 'Withdrawal') {
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = $cashWithdrawData['CashWithdraw']['amount'];
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = '0.00';
                    } else if ($flag == 'Deposit') {
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $cashWithdrawData['CashWithdraw']['amount'];
                    }

                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = $flag;
                    unset($cashWithdrawData);
                    $cashCounter++;
                }
                
            }            
            //society other income bank general receipts only for withdraw data
            
            if (isset($SocietyOtherIncomeDetails) && !empty($SocietyOtherIncomeDetails)) {
                $otherIncomeCounter = $cashCounter;
                $flag = "Deposit";
                foreach ($SocietyOtherIncomeDetails as $otherIncomeData) {
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_date'] = $otherIncomeData['SocietyOtherIncome']['payment_date'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['particulars'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['withdrawal'] = "0.00";
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['deposit'] = $otherIncomeData['SocietyOtherIncome']['amount_paid'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['cheque_number'] = $otherIncomeData['SocietyOtherIncome']['cheque_no'];                    
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_flag'] = 'Deposit';
                    unset($otherIncomeData);
                    $otherIncomeCounter++;
                }
            }

            if (isset($cashContraReceivedDetails) && !empty($cashContraReceivedDetails)) {
                $contraCounter = 0;
                foreach ($cashContraReceivedDetails as $contraData) {

                    if ($contraData['CashWithdraw']['txn_type'] != 'contra') {
                        continue;
                    }

                    $note = 'Contra Received';
                    //echo '<pre>'; print_r($cashWithdrawData);
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $contraData['CashWithdraw']['payment_date'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $contraData['CashWithdraw']['particulars'] . '<br>' . $note;
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $contraData['CashWithdraw']['cheque_no'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $contraData['CashWithdraw']['amount'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'Contra';
                    unset($contraData);
                    $contraCounter++;
                }
            }
            
            ksort($bankBookData); //sorting society and member payment by payment 
           //echo '<pre>';print_r($otherIncomeData);die;
        //}    
        
        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;

        $bankBookFinalData = array();
        $bankBookFinalData[$excelCounter++] = array('#', 'DATE', 'TYPE','PARTICULAR', 'CHEQUE NO', 'DEPOSIT', 'WITHDRAWAL', 'BALANCE');
        if (isset($bankBookData) && count($bankBookData) > 0) {
            $idCount = 1;$balancePerRow = 0;
            foreach ($bankBookData as $bankDatails) {
                foreach ($bankDatails as $flagType => $finalData) {
                    foreach($finalData as $bankData){ 
                        $balancePerRow = $balancePerRow + ($bankData['deposit'] - $bankData['withdrawal']);
                        $bankBookFinalData[$excelCounter]['sr'] = $idCount;
                        $bankBookFinalData[$excelCounter]['payment_date'] = isset($bankData['payment_date']) ? $bankData['payment_date'] : '';
                        $bankBookFinalData[$excelCounter]['payment_flag'] = isset($bankData['payment_flag']) ? $bankData['payment_flag'] : '';
                        $bankBookFinalData[$excelCounter]['cheque_number'] = isset($bankData['cheque_number']) ? $bankData['cheque_number'] : '';
                        $bankBookFinalData[$excelCounter]['particulars'] = isset($bankData['particulars']) ? $bankData['particulars'] : '';
                        $bankBookFinalData[$excelCounter]['cheque_number'] = isset($bankData['cheque_number']) ? $bankData['cheque_number'] : '';
                        $bankBookFinalData[$excelCounter]['deposit'] = isset($bankData['deposit']) ? $bankData['deposit'] : '';
                        $bankBookFinalData[$excelCounter]['withdrawal'] = isset($bankData['withdrawal']) ? $bankData['withdrawal'] : '';
                        $bankBookFinalData[$excelCounter]['balance'] = $balancePerRow;
                    }
                   
                    $excelCounter++;
                    $idCount++;
                }
            }
        }
        //print_r($bankBookFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Bank Book Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($bankBookFinalData as $bankBookFinalDetails) {
            foreach ($bankBookFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }
    
    function download_cash_book_sheet(){
        $this->loadModel('SocietyLedgerHeads');

        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $accountCashBook = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';

        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);
        $optionsCashWithdraw['AND'] = array('CashWithdraw.society_id' => $societyLoginID);
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d'); 
        $filename = $societyDetails['Society']['society_name'].'_'.$reQuestedDate.'_Cash Book sheet';

        if (isset($this->request->data->query['cash_ledger_head_id']) && !empty($this->request->data->query['cash_ledger_head_id'])) {
            $options['AND']['MemberPayment.society_bank_id'] = $this->request->data->query['cash_ledger_head_id'];
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = $this->request->data->query['cash_ledger_head_id'];

            //For Ledger head opening balance
            $ledgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $this->request->data->query['cash_ledger_head_id'], 'SocietyLedgerHeads.society_id' => $societyLoginID)));
        }

        if (isset($this->request->data->query['operator']) && !empty($this->request->data->query['operator'])) {
            $operator = $this->request->data->query['operator'];
        }

        if (isset($this->request->data->query['amount']) && !empty($this->request->data->query['amount'])) {
            $options['AND']['MemberPayment.amount ' . $operator . ' '] = $this->request->data->query['amount'];
            $optionsSociety['AND']['SocietyPayment.amount ' . $operator . ' '] = $this->request->data->query['amount'];
            $optionsCashWithdraw['AND']['CashWithdraw.amount ' . $operator . ' '] = $this->request->data->query['amount'];
        }

        if (isset($this->request->data->query['payment_date']) && !empty($this->request->data->query['payment_date']) && !empty($this->request->data->query['payment_date_to'])) {
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data->query['payment_date'], $this->request->data->query['payment_date_to']);
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data->query['payment_date'], $this->request->data->query['payment_date_to']);
            $optionsCashWithdraw['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->data->query['payment_date'], $this->request->data->query['payment_date_to']);
        } else if (isset($this->request->data->query['payment_date']) && !empty($this->request->data->query['payment_date'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data->query['payment_date'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data->query['payment_date'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data->query['payment_date'];
        } else if (isset($this->request->data->query['payment_date_to']) && !empty($this->request->data->query['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data->query['payment_date_to'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data->query['payment_date_to'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data->query['payment_date_to'];
        }
        $societyCashBookDetails = array();
        $memberCashBookDetails = array();
        $cashWithdrawDetails = array();
        $cashBookData = array();
            $SocietyOtherIncomeDetails = array();
		
        //if ($this->request->is(array('post'))) {
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            if (isset($this->request->data->query['report_type']) && !empty($this->request->data->query['report_type']) && $this->request->data->query['report_type'] == "Receipt") {
                $memberCashBookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'withdraw';
                $SocietyOtherIncomeDetails = $this->SocietyOtherIncome->find('all', array('conditions' => array('SocietyOtherIncome.payment_mode'=>'Cash','SocietyOtherIncome.society_bank_id'=>$this->request->data->query['cash_ledger_head_id']),'order' => array('SocietyOtherIncome.payment_date' => 'asc')));
                //print_r($SocietyOtherIncomeDetails);die;
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else if (isset($this->request->data->query['report_type']) && !empty($this->request->data->query['report_type']) && $this->request->data->query['report_type'] == "Payment") {
                $societyCashBookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'deposit';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else {
                $memberCashBookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $societyCashBookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            }
        //}

        if (isset($societyCashBookDetails) && !empty($societyCashBookDetails)) {
            $sCounter = 0;
            foreach ($societyCashBookDetails as $societyBankBbookData) {
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . '&' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                unset($societyBankBbookData);
                $sCounter++;
            }
        }

        if (isset($memberCashBookDetails) && !empty($memberCashBookDetails)) {
            $mCounter = 0;
            foreach ($memberCashBookDetails as $memberBankBbookData) {
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['member_name'] . ' Maint received';
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                unset($memberBankBbookData);
                $mCounter++;
            }
        }
        //echo '<pre>'; print_r($cashWithdrawDetails); exit;

            $cashCounter = 0;
        if (isset($cashWithdrawDetails) && !empty($cashWithdrawDetails)) {
            foreach ($cashWithdrawDetails as $cashWithdrawData) {
                $note = '';

                switch ($cashWithdrawData['CashWithdraw']['txn_type']) {
                    case 'deposit' :
                        $flag = 'Payment';
                        $note = 'Deposited in Bank';
                        break;
                    case 'withdraw' :
                        $flag = 'Receipt';
                        $note = 'Withdrawn from Bank';
                        break;
                }
                //echo '<pre>'; print_r($cashWithdrawData);die;
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $cashWithdrawData['CashWithdraw']['payment_date'];
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $cashWithdrawData['CashWithdraw']['particulars'] . '<br>' . $note;
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $cashWithdrawData['CashWithdraw']['cheque_no'];

                if ($flag == 'Payment') {
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = $cashWithdrawData['CashWithdraw']['amount'];
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = '0.00';
                } else if ($flag == 'Receipt') {
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $cashWithdrawData['CashWithdraw']['amount'];
                }

                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'Contra';
                unset($cashWithdrawData);
                $cashCounter++;
            }
            
            if (isset($SocietyOtherIncomeDetails) && !empty($SocietyOtherIncomeDetails)) {
                $otherIncomeCounter = $cashCounter;
                $flag = 'Receipt';
                foreach ($SocietyOtherIncomeDetails as $otherIncomeData) {
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_date'] = $otherIncomeData['SocietyOtherIncome']['payment_date'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['particulars'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['withdrawal'] = "0.00";
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['deposit'] = $otherIncomeData['SocietyOtherIncome']['amount_paid'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['cheque_number'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'General Receipt';
                    unset($otherIncomeData);
                    $otherIncomeCounter++;
                }
            }
        }

        ksort($cashBookData);
       // print_r($cashBookData);die;
        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;

        $cashBookFinalData = array();
        $cashBookFinalData[$excelCounter++] = array('#', 'DATE', 'TYPE', 'PARTICULAR', 'RECEIPT', 'PAYMENT', 'BALANCE');
        if (isset($cashBookData) && count($cashBookData) > 0) {
            $idCount = 1;
            $balancePerRow = 0;
            foreach ($cashBookData as $cashDetails) {
                foreach ($cashDetails as $flagType => $finalData) {
                    foreach ($finalData as $cashData) {
                        $balancePerRow = $balancePerRow + ($cashData['deposit'] - $cashData['withdrawal']);
                        $cashBookFinalData[$excelCounter]['sr'] = $idCount;
                        $cashBookFinalData[$excelCounter]['payment_date'] = isset($cashData['payment_date']) ? $cashData['payment_date'] : '';
                        $cashBookFinalData[$excelCounter]['payment_flag'] = isset($cashData['payment_flag']) ? $cashData['payment_flag'] : '';
                        $cashBookFinalData[$excelCounter]['particulars'] = isset($cashData['particulars']) ? $cashData['particulars'] : '';
                        $cashBookFinalData[$excelCounter]['deposit'] = isset($cashData['deposit']) ? $cashData['deposit'] : '';
                        $cashBookFinalData[$excelCounter]['withdrawal'] = isset($cashData['withdrawal']) ? $cashData['withdrawal'] : '';
                        $cashBookFinalData[$excelCounter]['balance'] = $balancePerRow;
                    }

                    $excelCounter++;
                    $idCount++;
                }
            }
        }
        //print_r($cashBookFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Cash Book Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($cashBookFinalData as $cashBookFinalDetails) {
            foreach ($cashBookFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }
    
    function download_dues_from_member() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d');
        $filename = $societyDetails['Society']['society_name'] . '_' . $reQuestedDate . '_Dues From Member Sheet';
        $this->Member->Behaviors->attach('Containable');

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        $optionsMember['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));

        if (isset($this->request->data['MemberPayment']['operator']) && !empty($this->request->data['MemberPayment']['operator'])) {
            $operator = $this->request->data['MemberPayment']['operator'];
        }
        if (isset($this->request->data['MemberPayment']['amount']) && !empty($this->request->data['MemberPayment']['amount'])) {
            //$options['AND']['MemberBillSummary.balance_amount'.$operator.' '] = $this->request->data['MemberPayment']['amount'];
        }
        if (isset($this->request->data['MemberPayment']['report_type']) && !empty($this->request->data['MemberPayment']['report_type'])) {
            //$options['AND']['MemberPayment.report_type'] = $this->request->data['MemberPayment']['report_type'];
        }
        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $optionsMember['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $optionsMember['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        $societyMemberListsForDuesReport = $this->Member->find('all', array('contain' => array('Society', 'Building', 'Wing'), 'conditions' => $optionsMember, 'order' => array('Member.id' => 'asc')));

        $accountDuesMemberSummary = array();
        //if ($this->request->is(array('post'))) {
        $mDuesCounter = 0;
        foreach ($societyMemberListsForDuesReport as $memberDuesListData) {
            $options['AND'] = array('MemberBillSummary.member_id' => $memberDuesListData['Member']['id'], 'MemberBillSummary.balance_amount >=' => 0);
            $accountDuesMemberSummaryData = $this->MemberBillSummary->find('first', array('conditions' => $options, 'fields' => array('MemberBillSummary.balance_amount'), 'order' => array('MemberBillSummary.bill_no' => 'desc')));
            $accountDuesMemberSummary[$mDuesCounter]['total_dues_amount'] = isset($accountDuesMemberSummaryData['MemberBillSummary']['balance_amount']) ? $accountDuesMemberSummaryData['MemberBillSummary']['balance_amount'] : 0.00;
            $accountDuesMemberSummary[$mDuesCounter]['flat_no'] = $memberDuesListData['Member']['flat_no'];
            $accountDuesMemberSummary[$mDuesCounter]['member_prefix'] = $memberDuesListData['Member']['member_prefix'];
            $accountDuesMemberSummary[$mDuesCounter]['member_name'] = $memberDuesListData['Member']['member_name'];
            $mDuesCounter ++;
        }
        //}
        //print_r($societyMemberListsForDuesReport);die;

        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;

        $accountDuesMemberFinalData = array();
        $accountDuesMemberFinalData[$excelCounter++] = array('#', 'BUILDING', '	UNIT NO', 'MEMBER NAME', 'DUES');
        if (isset($accountDuesMemberSummary) && count($accountDuesMemberSummary) > 0) {
            $idCount = 1;
            foreach ($accountDuesMemberSummary as $duesMemberDatails) {
                $accountDuesMemberFinalData[$excelCounter]['sr'] = $idCount;
                $accountDuesMemberFinalData[$excelCounter]['society_name'] = isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : '';
                $accountDuesMemberFinalData[$excelCounter]['flat_no'] = isset($duesMemberDatails['flat_no']) ? $duesMemberDatails['flat_no'] : '';
                $accountDuesMemberFinalData[$excelCounter]['member_name'] = isset($duesMemberDatails['member_name']) ? $duesMemberDatails['member_name'] : '';
                $accountDuesMemberFinalData[$excelCounter]['total_dues_amount'] = isset($duesMemberDatails['total_dues_amount']) ? $duesMemberDatails['total_dues_amount'] : '';

                $excelCounter++;
                $idCount++;
            }
        }
        //print_r($accountDuesMemberFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Dues From Member Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($accountDuesMemberFinalData as $accountDuesMemberFinalDetails) {
            foreach ($accountDuesMemberFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }

    function download_dues_advance_from_member() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d');
        $filename = $societyDetails['Society']['society_name'] . '_' . $reQuestedDate . '_Dues Advance From Member Sheet';
        $this->Member->Behaviors->attach('Containable');

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        $optionsMember['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));

        if (isset($this->request->data['MemberPayment']['operator']) && !empty($this->request->data['MemberPayment']['operator'])) {
            $operator = $this->request->data['MemberPayment']['operator'];
        }
        if (isset($this->request->data['MemberPayment']['amount']) && !empty($this->request->data['MemberPayment']['amount'])) {
            //$options['AND']['MemberBillSummary.balance_amount'.$operator.' '] = $this->request->data['MemberPayment']['amount'];
        }
        if (isset($this->request->data['MemberPayment']['report_type']) && !empty($this->request->data['MemberPayment']['report_type'])) {
            //$options['AND']['MemberPayment.report_type'] = $this->request->data['MemberPayment']['report_type'];
        }
        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $optionsMember['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $optionsMember['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        $societyMemberListsForDuesAdvanceReport = $this->Member->find('all', array('contain' => array('Society', 'Building', 'Wing'), 'conditions' => $optionsMember, 'order' => array('Member.id' => 'asc')));

        $accountDuesAdvanceMemberSummary = array();
        //if ($this->request->is(array('post'))) {
        $mDuesCounter = 0;
        foreach ($societyMemberListsForDuesAdvanceReport as $memberDuesListData) {
            $options['AND'] = array('MemberBillSummary.member_id' => $memberDuesListData['Member']['id'], 'MemberBillSummary.balance_amount <=' => 0);
            $accountDuesMemberSummaryData = $this->MemberBillSummary->find('first', array('conditions' => $options, 'fields' => array('MemberBillSummary.balance_amount'), 'order' => array('MemberBillSummary.bill_no' => 'desc')));
            $accountDuesAdvanceMemberSummary[$mDuesCounter]['total_dues_amount'] = isset($accountDuesMemberSummaryData['MemberBillSummary']['balance_amount']) ? $accountDuesMemberSummaryData['MemberBillSummary']['balance_amount'] : 0.00;
            $accountDuesAdvanceMemberSummary[$mDuesCounter]['flat_no'] = $memberDuesListData['Member']['flat_no'];
            $accountDuesAdvanceMemberSummary[$mDuesCounter]['member_prefix'] = $memberDuesListData['Member']['member_prefix'];
            $accountDuesAdvanceMemberSummary[$mDuesCounter]['member_name'] = $memberDuesListData['Member']['member_name'];
            $mDuesCounter ++;
        }
        //}
        //print_r($societyMemberListsForDuesAdvanceReport);die;

        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;

        $accountDuesAdvanceMemberFinalData = array();
        $accountDuesAdvanceMemberFinalData[$excelCounter++] = array('#', 'BUILDING', 'UNIT NO', 'MEMBER NAME', 'Advance');
        if (isset($accountDuesAdvanceMemberSummary) && count($accountDuesAdvanceMemberSummary) > 0) {
            $idCount = 1;
            foreach ($accountDuesAdvanceMemberSummary as $duesAdvanceMemberDatails) {
                $accountDuesAdvanceMemberFinalData[$excelCounter]['sr'] = $idCount;
                $accountDuesAdvanceMemberFinalData[$excelCounter]['society_name'] = isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : '';
                $accountDuesAdvanceMemberFinalData[$excelCounter]['flat_no'] = isset($duesAdvanceMemberDatails['flat_no']) ? $duesAdvanceMemberDatails['flat_no'] : '';
                $accountDuesAdvanceMemberFinalData[$excelCounter]['member_name'] = isset($duesAdvanceMemberDatails['member_name']) ? $duesAdvanceMemberDatails['member_name'] : '';
                $accountDuesAdvanceMemberFinalData[$excelCounter]['total_dues_amount'] = isset($duesAdvanceMemberDatails['total_dues_amount']) ? $duesAdvanceMemberDatails['total_dues_amount'] : '';

                $excelCounter++;
                $idCount++;
            }
        }
        //print_r($accountDuesMemberFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Dues From Member Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($accountDuesAdvanceMemberFinalData as $accountDuesMemberFinalDetails) {
            foreach ($accountDuesMemberFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }

    function download_account_general_ledger() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberName = $this->SocietyBill->getSocietyMemberList();
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d');
        $filename = $societyDetails['Society']['society_name'] . '_' . $reQuestedDate . '_General Ledger Sheet';
        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);

        if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
        } else if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date'])) {
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        } else if (isset($this->request->data['SocietyPayment']['payment_date_to']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        }

        $societyLedgerData = array();
        $memberLedgerDetails = array();
        $societyLedgerDetails = array();

        //Cash in hand ledger id
        $cashInHandLedgerDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.title LIKE' => "Cash in hand"), 'order' => array('SocietyLedgerHeads.status' => 'asc')));
        if (isset($cashInHandLedgerDetails['SocietyLedgerHeads']) && !empty($cashInHandLedgerDetails['SocietyLedgerHeads'])) {
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = isset($cashInHandLedgerDetails['SocietyLedgerHeads']['id']) ? $cashInHandLedgerDetails['SocietyLedgerHeads']['id'] : 0;
        }

        // if ($this->request->is(array('post'))) {
        $this->MemberPayment->Behaviors->attach('Containable');
        $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
        $memberLedgerDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
        $societyLedgerDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
        //}

        if (isset($societyLedgerDetails) && !empty($societyLedgerDetails)) {
            $sCounter = 0;
            foreach ($societyLedgerDetails as $societyBankBbookData) {
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . '&' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_type'] = $societyBankBbookData['SocietyPayment']['payment_type'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                unset($societyBankBbookData);
                $sCounter++;
            }
        }

        if (isset($memberLedgerDetails) && !empty($memberLedgerDetails)) {
            $mCounter = 0;
            foreach ($memberLedgerDetails as $memberBankBbookData) {
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['member_name'] . ' Maint received';
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                unset($memberBankBbookData);
                $mCounter++;
            }
        }
        ksort($societyLedgerData);
        //print_r($societyLedgerData);die;
        
        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;
        $balancePerRow = '';

        $accountGeneralLedgerFinalData = array();
        $accountGeneralLedgerFinalData[$excelCounter++] = array('DATE', 'TYPE', 'NO', 'CHEQUE TYPE', 'CHEQUE NO', 'DEBIT', 'CREDIT', 'BALANCE');
        if (isset($societyLedgerData) && count($societyLedgerData) > 0) {
            $idCount = 1;
            foreach ($societyLedgerData as $ledgerDetails) {
                foreach ($ledgerDetails as $flagType => $finalData) {
                    foreach ($finalData as $ledgerData) {
                        $balancePerRow = $balancePerRow + ($ledgerData['deposit'] - $ledgerData['withdrawal']);
                        $accountGeneralLedgerFinalData[$excelCounter]['payment_date'] = isset($ledgerData['payment_date']) ? $ledgerData['payment_date'] : '';
                        $accountGeneralLedgerFinalData[$excelCounter]['particulars'] = isset($ledgerData['particulars']) ? $ledgerData['particulars'] : '';
                        $accountGeneralLedgerFinalData[$excelCounter]['sr'] = $idCount;
                        $accountGeneralLedgerFinalData[$excelCounter]['payment_type'] = isset($ledgerData['payment_type']) ? $ledgerData['payment_type'] : '';
                        $accountGeneralLedgerFinalData[$excelCounter]['cheque_number'] = isset($ledgerData['cheque_number']) ? $ledgerData['cheque_number'] : '-';
                        $accountGeneralLedgerFinalData[$excelCounter]['deposit'] = isset($ledgerData['deposit']) ? $ledgerData['deposit'] : '';
                        $accountGeneralLedgerFinalData[$excelCounter]['withdrawal'] = isset($ledgerData['withdrawal']) ? $ledgerData['withdrawal'] : '';
                        $accountGeneralLedgerFinalData[$excelCounter]['balance'] = $balancePerRow;
                    }
                    $excelCounter++;
                    $idCount++;
                }
            }

            $excelCounter++;
            $idCount++;
            //print_r($accountGeneralLedgerFinalData);die;
        }
        //print_r($accountGeneralLedgerFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Geeral Report Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($accountGeneralLedgerFinalData as $accountGeneralLedgerFinalDetails) {
            foreach ($accountGeneralLedgerFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
    }

    function download_account_payment_register() {    
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyPayment');
        $postData = $this->request->data;
        $paymentRegisterData = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyBankLists = $this->SocietyBill->getSocietyBankNameList();
        $reQuestedDate = isset($this->request->query['payment_date']) && !empty($this->request->query['payment_date']) ? $this->request->query['payment_date'] : date('Y-m-d');
        $filename = $societyDetails['Society']['society_name'] . '_' . $reQuestedDate . '_Payment Register Sheet';
        //$societyMemberDetails = $this->SocietyBill->navigateSocietyMemberDetails();
        $options['AND'] = array('SocietyPayment.society_id' => $societyLoginID);

        if (isset($this->request->data['SocietyPayment']['payment_type']) && !empty($this->request->data['SocietyPayment']['payment_type']) && $this->request->data['SocietyPayment']['payment_type'] != "Both") {
            $options['AND']['SocietyPayment.payment_type'] = $this->request->data['SocietyPayment']['payment_type'];
        }

        if (isset($this->request->data['SocietyPayment']['payment_type']) && !empty($this->request->data['SocietyPayment']['payment_type']) && $this->request->data['SocietyPayment']['payment_type'] == "Both") {
            $options['OR'][]['SocietyPayment.payment_type'] = "Cash";
            $options['OR'][]['SocietyPayment.payment_type'] = "Bank";
        }

        if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $options['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
        } else if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date'])) {
            $options['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        } else if (isset($this->request->data['SocietyPayment']['payment_date_to']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $options['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        }

        //if ($this->request->is(array('post'))) {
            $paymentRegisterData = $this->SocietyPayment->find('all', array('conditions' => $options));
        //}
        
      
        $sheetCount = 0;
        $innerrowCounter = 'A';
        $innerColumnCounter = '1';
        $excelCounter = 0;

        $paymentRegisterFinalData = array();
        $paymentRegisterFinalData[$excelCounter++] = array('VNO', 'VDATE', 'BY', 'CHEQUE NO', 'CHEQUE DATE', 'AMOUNT', 'ACCOUNT NAME', 'RS','PARTICULAR','REMARK');
        if (isset($paymentRegisterData) && count($paymentRegisterData) > 0) {
            $idCount = 1;
            foreach($paymentRegisterData as $paymentData) { 
                        $paymentRegisterFinalData[$excelCounter]['bill_voucher_number'] = isset($paymentData['SocietyPayment']['bill_voucher_number']) ? $paymentData['SocietyPayment']['bill_voucher_number'] : '';
                        $paymentRegisterFinalData[$excelCounter]['payment_date'] = isset($paymentData['SocietyPayment']['payment_date']) ? $paymentData['SocietyPayment']['payment_date'] : '';
                        $paymentRegisterFinalData[$excelCounter]['payment_by_ledger_id'] = isset($societyBankLists[$paymentData['SocietyPayment']['payment_by_ledger_id']]) ? $societyBankLists[$paymentData['SocietyPayment']['payment_by_ledger_id']]: 'Cash'; 
                        $paymentRegisterFinalData[$excelCounter]['cheque_reference_number'] = isset($paymentData['SocietyPayment']['cheque_reference_number']) ? $paymentData['SocietyPayment']['cheque_reference_number'] : '-';
                        $paymentRegisterFinalData[$excelCounter]['cheque_date'] = isset($paymentData['SocietyPayment']['cheque_date']) ? $paymentData['SocietyPayment']['cheque_date'] : '';
                        $paymentRegisterFinalData[$excelCounter]['amount'] = isset($paymentData['SocietyPayment']['amount']) ? $paymentData['SocietyPayment']['amount'] : '';
                        $paymentRegisterFinalData[$excelCounter]['title'] = isset($paymentData['SocietyLedgerHeads']['title']) ? $paymentData['SocietyLedgerHeads']['title'] : '';
                        $paymentRegisterFinalData[$excelCounter]['amount'] = isset($paymentData['SocietyPayment']['amount']) ? $paymentData['SocietyPayment']['amount'] : '';
                        $paymentRegisterFinalData[$excelCounter]['particulars'] = isset($paymentData['SocietyPayment']['particulars']) ? $paymentData['SocietyPayment']['particulars'] : '';
                        $paymentRegisterFinalData[$excelCounter]['notes'] = isset($paymentData['SocietyPayment']['notes']) ? $paymentData['SocietyPayment']['notes'] : '';
                       $excelCounter++;
                       $idCount++; 
                    }
            
        }
        
        //print_r($paymentRegisterFinalData);die; 
        //print_r($accountGeneralLedgerFinalData);die;
        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->removeSheetByIndex(0);
        $this->PhpExcel->createSheet($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex($sheetCount);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->getSheetView($sheetCount)->setZoomScale(100);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Geeral Report Sheet');
        $this->PhpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $numberFormatCounter = 1;
        foreach ($paymentRegisterFinalData as $accountGeneralLedgerFinalDetails) {
            foreach ($accountGeneralLedgerFinalDetails as $ReceiptData) {
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . '1')->getFont()->setBold(true);
                if ($numberFormatCounter > 3) {
                    $this->PhpExcel->getActiveSheet()->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getNumberFormat()->setFormatCode("0.00");
                }
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->PhpExcel->getActiveSheet($sheetCount)->getStyle($innerrowCounter . $innerColumnCounter . ":" . $innerrowCounter . $innerColumnCounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->PhpExcel->setActiveSheetIndex($sheetCount)->setCellValue($innerrowCounter . $innerColumnCounter, $ReceiptData);
                $innerrowCounter++;
                $numberFormatCounter++;
            }
            $numberFormatCounter = 0;
            $innerrowCounter = 'A';
            $innerColumnCounter++;
        }

        $this->PhpExcel->output($filename . ".xlsx");
        $this->PhpExcel->exit();
        
    }
    function download_account_member_ledger_default(){
        
    }

}
