function showLoader(centerY){
    $.blockUI({
        message: '<span class="p-10  b-c-gainsboro b-r-1"><img src="'+webrootUrl+'img/loader_please_wait.gif" height="80" align=""><span> &nbsp;Please wait...</span>',
        centerY: centerY != undefined ? centerY : true,
        baseZ: 99999,
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.15,
            cursor: 'wait'
        }
    });
}


function redirectToActionController(URL) {
    if (URL !== "undefined" && URL !== '') {
        window.location.href = URL;
    }
}

function hideLoader(){
    $.unblockUI({
        onUnblock: function () {
            $.removeAttr("style");
        }
    });
}

function showActiveHeader(id){
    $('.header_outer').removeClass('active');
    $('#'+id).addClass('active');
}

function goToPage(url){
    window.location = url;
}

function resetSrNo(id) {
    $.each($(id).children("tr"), function(ind, obj) {
        $(obj).children("td").eq(0).html(ind + 1);
    });
}

function resetSrNo2(id) {
    $.each($(id).children("tr"), function(ind, obj) {
        $(obj).children("td").eq(1).html(ind + 1);
    });
}


function checkemail(email) {
    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (filter.test(email)) {
        return 1;
    }
    else {
        return 0;
    }
}

function goToByScroll(id){
	$('html,body').animate({scrollTop: $(id).offset().top - 100},'slow');
}

function openModal(id) {
    $('#' + id).modal('show');
}

function closeModal(id) {
    $('#' + id).modal('hide');
}

function showNotifyAlert(jsonResponse,displayDivId){
    var error_display = '';
    var error_flag = jsonResponse.error ? jsonResponse.error : 0;
    var error_msg = jsonResponse.error_message ? jsonResponse.error_message : 'Opps! Somthing went wrong.';
    //alert(error_flag);
    if (error_flag !== "undefined" && error_flag === 0) {
        error_display = "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + error_msg + "</div>";
    } else if (error_flag !== "undefined" && error_flag === 1) {
        error_display = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + error_msg + "</div>";
    }
    if (error_display !== '') {
        $('#'+displayDivId).show();
        $('#'+displayDivId).html(error_display);
    }
}

function showNotificationToastr(toasterType, customMsg, positionTostr) {
    var toasterIcon = toasterType ? toasterType : 'success';
    var positionTost = positionTostr ? positionTostr : 'top-center';
    var headingTostr = customMsg ? customMsg : 'Welcome to Elogistics';
    $.toast({
        heading: headingTostr,
        text: '',
        position: positionTost,
        loaderBg: '#f2b701',
        icon: toasterIcon,
        hideAfter: 3500,
        stack: 6
    });
}

$(function() {
$(".select2").select2();
//$('input[type="date"]').val(financialStartEndDate);    
$('.numeric').on('blur', function () {
    if ($(this).val() === "") {
        $(this).val(0);
    }
});
$('.numeric').each(function (index) {
    if ($(this).val() === '') {
        var readOnlyCheck = $(this).attr('readonly');
        if (readOnlyCheck !== 'true') {
            $(this).val(0);
        }
    }
});
$('.numeric').on('click', function () {
    //alert();
    if ($(this).val() === 0) {
        var readOnlyCheck = $(this).attr('readonly');
        if (readOnlyCheck !== true) {
            $(this).val('');
        }
    }
});
});

function checkNegativePositiveValue(amount){
    if(amount){
        if(parseInt(amount) > 0){
            return 'Dr';
        }else{
            return 'Cr';
        }
    }
    return '';
}