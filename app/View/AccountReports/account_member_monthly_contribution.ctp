<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Member Monthly Contribution</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form class="" method="post" id="memberMonthlyControFrm" name="memberMonthlyControFrm" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="col-md-12 member-tariff-left-box"> 
                                    <div class="row">                                            
                                        <div class="col-md-3 nopadding">  
                                            <div class="account-report-form-field-box">     
                                                <div class="account-report-form-box-label">Building &nbsp;</div>  
                                                <select class="account-report-form-box-field-dropdown-80" id="" name="data[MemberPayment][building_id]" onchange="getAllBuildingWings(this.value, '#member_monthly_contro');">
                                                    <option value="">Select building</option>
                                                    <?php if(isset($societyBuildingLists) && count($societyBuildingLists) > 0) {
                                                    foreach($societyBuildingLists as $buildingID => $buildingName){?>
                                                    <?php if(isset($postData['MemberPayment']['building_id']) && $postData['MemberPayment']['building_id'] == $buildingID){?>
                                                    <option value="<?php echo $buildingID; ?>" selected><?php echo $buildingName; ?></option>
                                                    <?php }else {?>
                                                    <option value="<?php echo $buildingID; ?>"><?php echo $buildingName; ?></option>
                                                    <?php }?>
                                                    <?php } }?> 
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-3 no-padding-right"> 
                                            <div class="account-report-form-field-box">   
                                                <div class="account-report-form-box-label">Wing &nbsp;</div>  
                                                <select class="account-report-form-box-field-dropdown-80" id="member_monthly_contro"  name="data[MemberPayment][wing_id]">
                                                    <option value="">Select</option>  
                                                </select> 
                                            </div> 
                                        </div>
                                        <div class="col-md-4 no-padding-right"> 
                                            <div class="account-report-form-field-box">   
                                                <div class="account-report-form-box-label">Member &nbsp;</div>  
                                                <select class="account-report-form-box-field-dropdown-80" name="data[MemberPayment][member_id]" id="member_monthly_contribution_member_name" onchange="selectMemberFlatDropdownByMemberFlatID(this.value, '#member_monthly_contribution_flat_no', 0);" name="[Member][member_name]">
                                                    <option value="">Select</option>  
                                                    <?php if(isset($societyMemberName) && count($societyMemberName) > 0) {
                                                    foreach($societyMemberName as $memberID => $memberName){?>
                                                    <?php if(isset($postData['MemberPayment']['member_name']) && $postData['MemberPayment']['member_name'] == $memberID){?>
                                                    <option value="<?php echo $memberID; ?>" selected><?php echo $memberName; ?></option>
                                                    <?php }else {?>
                                                    <option value="<?php echo $memberID; ?>"><?php echo $memberName; ?></option>
                                                    <?php }?>
                                                    <?php } }?> 
                                                </select> 
                                            </div> 
                                        </div> 
                                        <div class="col-md-2 no-padding-right"> 
                                            <div class="account-report-form-field-box">   
                                                <div class="account-report-form-box-label">Unit No &nbsp;</div>  
                                                <select class="account-report-form-box-field-dropdown" id="member_monthly_contribution_flat_no" onchange="selectMemberFlatDropdownByMemberFlatID(this.value, '#member_monthly_contribution_member_name', 1);" name="data[MemberPayment][flat_no]">
                                                    <option value="">Select</option>  
                                                     <?php if(isset($societyMemberFlatlList) && count($societyMemberFlatlList) > 0) {
                                                    foreach($societyMemberFlatlList as $flatNO => $memberID){?>
                                                    <?php if(isset($postData['MemberPayment']['flat_no']) && $postData['MemberPayment']['flat_no'] == $flatNO){?>
                                                    <option value="<?php echo $flatNO; ?>" selected><?php echo $flatNO; ?></option>
                                                    <?php }else {?>
                                                    <option value="<?php echo $flatNO; ?>"><?php echo $flatNO; ?></option>
                                                    <?php }?>
                                                    <?php } }?> 
                                                </select> 
                                            </div> 
                                        </div>                                                         
                                    </div>  
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">                                
                                            <div class="form-group">
                                                <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-sm btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button> 
                                                <a type="button" data-toggle="tooltip" data-original-title="Print Friendly" class="btn btn-success btn-anim  btn-sm" href="javascript:void(0);" onclick="printAllReports('print_member_monthly_cotribution');"><i class="fa fa-print"></i><span class="btn-text">Print Friendly</span></a>                                                                            
                                                <a data-toggle="tooltip" data-original-title="Export to PDF" class="btn btn-success btn-anim btn-sm" href="javascript:void(0);" onclick=""><i class="fa fa-file-excel-o"></i><span class="btn-text">Export to PDF</span></a>                                             
                                                <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-sm btn-anim" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a>                                                                            
                                            </div>
                                        </div>    
                                    </div>
                                </div> 
                            </form> 
                            <div class="clearfix"></div>
                            <br><br>
                            <div id="print_member_monthly_cotribution">
                            <div class="print-member-monthly-cotribution">
                                <div class="row">
                                    <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>
                                </div>
                                <div style="padding:15px;">                            
                                    <table id="" class="table table-bordered padding-td-none padding-th-none">
                                        <thead>
                                            <tr>
                                                <th>Particulars</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="">
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>   
                                    </table> 
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>