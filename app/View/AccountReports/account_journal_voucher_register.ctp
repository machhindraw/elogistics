<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Journal Voucher</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form class="" method="post" id="" name="" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="col-md-12 left-section"> 
                                    <div class="row">
                                        <div class="col-md-3 no-padding-right">  
                                            <div class="account-report-form-field-box">  
                                                <div class="account-report-form-box-label">Type &nbsp;</div>                                                                     
                                                <select class="account-report-form-box-field-dropdown-70" id="" name="">
                                                    <option value="">Journal Voucher</option>                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 no-padding-right">  
                                            <div class="account-report-form-field-box">  
                                                <div class="account-report-form-box-label">Sub Type &nbsp;</div>                                                                     
                                                <select class="account-report-form-box-field-dropdown-65" id="" name="">
                                                    <option value="">General</option>                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 nopadding">   
                                            <div class="account-report-form-field-box"> 
                                                <div class="account-report-form-box-label">Voucher Date &nbsp;</div>                                                                     
                                                <input type="date" class="account-report-form-box-field-date" id="" name="" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-1 nopadding">  
                                            <div class="account-report-form-field-box"> 
                                                <div class="account-report-form-box-label">V.No &nbsp;</div>                                                                     
                                                <input type="text" class="account-report-form-box-field-date" id="" name="" value="">
                                            </div>  
                                        </div>                                                            
                                        <div class="col-md-1 nopadding">  
                                            <div class="account-report-form-field-box"> 
                                                <div class="account-report-form-box-label">Sr. &nbsp;</div>                                                                     
                                                <input type="text" class="account-report-form-box-field-date" id="" name="" value="">
                                            </div>  
                                        </div>                                                            
                                        <div class="col-md-1 nopadding"></div>  
                                        <div class="clearfix"></div><br>
                                        <div class="col-md-12 account-report-form"> 
                                            <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btns"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button> 
                                            <button type="button" class="btn btn-success btn-icon btns" onclick="printAllReports('society_journal_voucher');"><i class="fa fa-print"></i><span> Print</span></button>
                                            <a data-toggle="tooltip" data-original-title="Export to PDF" class="btn btn-success btn-anim btns" href="javascript:void(0);" onclick=""><i class="fa fa-file-excel-o"></i><span class="btn-text">Export to PDF</span></a>                                        
                                            <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btns" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a> 
                                        </div>                                                                       
                                    </div> 
                                </div>

                                <!--tab section -->
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="simple-border">   
                                            <div class="tab-struct custom-tab-2 mt-40">
                                                <ul role="tablist" class="nav nav-tabs" id="myTabs_15">
                                                    <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_15" href="#jv_details">Details</a></li>
                                                    <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_15" role="tab" href="#jv_other_adjustment" aria-expanded="false">Other Adjustment</a></li>                                                       
                                                </ul>
                                                <div class="tab-content" id="myTabContent_15">
                                                    <div  id="jv_details" class="tab-pane fade active in" role="tabpanel">
                                                        <div class="table-wrap">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered padding-td-none padding-th-none">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center">Sr</th>
                                                                            <th class="text-center">Type</th>
                                                                            <th class="text-center">Account Name</th>
                                                                            <th class="text-center">Dr. Amount</th>
                                                                            <th class="text-center">Cr. Amount</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="text-center" style="width:5%;">1</td>
                                                                            <td class="text-center" style="width:5%;">
                                                                                <select class="form-control1" id="" name="">
                                                                                    <option value="">Debit</option>                                                   
                                                                                    <option value="">Credit</option>                                                   
                                                                                </select>
                                                                            </td>
                                                                            <td>Repair Fund</td>
                                                                            <td class="text-right" style="width:12%;">10000.00</td>
                                                                            <td class="text-right" style="width:12%;">0.00</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-center" style="width:5%;">2</td>
                                                                            <td class="text-center" style="width:5%;">
                                                                                <select class="form-control1" id="" name="">
                                                                                    <option value="">Debit</option>                                                   
                                                                                    <option value="">Credit</option>                                                   
                                                                                </select>
                                                                            </td>
                                                                            <td>Last B/S Repair Fund</td>
                                                                            <td class="text-right" style="width:12%;">0.00</td>
                                                                            <td class="text-right" style="width:12%;">10000.00</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- second tab -->
                                                    <div id="jv_other_adjustment" class="tab-pane fade" role="tabpanel"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /tab section -->
                               
                                <div class="simple-border" style="overflow: hidden;">
                                     <br>
                                    <div class="col-md-3 no-padding-right">  
                                        <div class="account-report-form-field-box">  
                                            <div class="account-report-form-box-label">Type &nbsp;</div>                                                                     
                                            <select class="account-report-form-box-field-dropdown-70" id="" name="">
                                                <option value="">Journal Voucher</option>                                                   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9 no-padding-right">  
                                        <div class="account-report-form-field-box">  
                                            <div class="account-report-form-box-label" style="float:left !important;">Narration &nbsp;</div>                                                                     
                                            <textarea rows="3" class="" id="" name="" style="float:left !important;width:400px;"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </form>


                            <div class="clearfix"></div><br><br>                            
                            <div id="society_journal_voucher">
                                <div class="print_journal_voucher"> 
                                    <div class="bill-outer-border">
                                        <div class="row"><br>
                                            <h5 class="text-center">Jogani Industrial Estate Premises</h5>
                                            <div class="address-heading">Registration No. BOM / GEN / 832 1975 Dated:</div>
                                            <div class="address-heading">541. SENAPTI BAPAT MARG. DADAR (W) MUMBAI-400028</div>
                                        </div>
                                        <br>
                                        <div class="row1">
                                            <div class="bill">Journal Voucher Register</div>
                                            <div class="text-center"></div>

                                            <div class="row1">
                                                <div style="padding: 0px;">
                                                    <div class="table-wrap">
                                                        <div class="table-responsive1">
                                                            <table id="datable_1" class="table padding-td-none padding-th-none table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="15%" class="text-center">Date</th>
                                                                        <th>Prticulars</th>
                                                                        <th class="text-center" width="8%">Dr.</th>
                                                                        <th class="text-center" width="8%">Cr.</th>
                                                                    </tr>
                                                                </thead>
                                                                <tr id="">
                                                                    <td colspan="4" class="border-none">07/07/2016</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td class="border-none">Voucher No: </td>
                                                                    <td colspan="3" class="border-none">1</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Kamlesh .R. Shah / Mittal .K. Shah / Maniben .R. Shah</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Water charges Payable.</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Transfer From Transfer Preimium</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td colspan="4" class="border-none">07/07/2016</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td class="border-none">Voucher No: </td>
                                                                    <td colspan="3" class="border-none">2</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Kamlesh .R. Shah / Mittal .K. Shah / Maniben .R. Shah</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Water charges Payable.</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td></td>
                                                                    <td>Transfer From Transfer Preimium</td>
                                                                    <td class="text-right">1,196.80</td>
                                                                    <td class="text-right">0</td>
                                                                </tr>
                                                                 <tfoot id=""> 
                                                                    <tr>                                                                       
                                                                        <td colspan="2" style="font-weight:bold;">Grand Total</td>
                                                                        <td class="text-right" style="font-weight:bold;">5,819,257.20</td>
                                                                        <td class="text-right" style="font-weight:bold;">5,819,257.20</td>                                                                        
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br><br><br>
                                            <div class="row">
                                                <div class="col-md-3 col-xs-1"></div>  
                                                <div class="col-md-3 col-xs-5">Cashier's Signature</div> 
                                                <div class="col-md-3 col-xs-1"></div>  
                                                <div class="col-md-3 col-xs-5">Authorised Signatory</div> 
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>