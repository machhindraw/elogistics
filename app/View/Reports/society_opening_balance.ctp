<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Opening Balance</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form class="" method="post" id="societyReportBillFrm" name="societyReportBillFrm" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="col-md-12 member-tariff-left-box">                                     
                                    <div class="form-group society-report-form">
                                        <?php echo $this->Html->link('<i class="fa fa-print" style="color:#fff;font-size:15px;"></i>&nbsp; Print Opening Balance', array('controller' => 'Reports','action' => 'print_opening_balance'), array('class' => 'btn btn-primary','data-toggle' => 'tooltip', 'data-original-title' => 'Print Opening Balance','style'=>array('margin-right:10px;'),'escape' => false)); ?>
                                        <?php echo $this->Html->link('<i class="fa fa-print" style="color:#fff;font-size:15px;"></i>&nbsp; Member Opening Balance', array('controller' => 'Reports','action' => 'member_opening_balance'), array('class' => 'btn btn-primary','data-toggle' => 'tooltip', 'data-original-title' => 'Member Opening Balance','style'=>array('margin-right:10px;'),'escape' => false)); ?>                                       
                                    </div>
                                </div> 
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
