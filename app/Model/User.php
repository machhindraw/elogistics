<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property County $County
 * @property Parcel $Parcel
 * @property RequestComment $RequestComment
 * @property Request $Request
 * @property UserLogin $UserLogin
 */
class User extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    /**
     * hasMany associations
     *
     * @var array
     */

    public $hasMany = array(
        'UserLogin' => array(
            'className' => 'UserLogin',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Society' => array(
            'className' => 'Society',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    function isUniqueUsername($check) {
        
        $username = $this->find(
            'first', array(
            'fields' => array(
                'User.id',
                'User.username'
            ),
            'conditions' => array(
                'User.username' => $check['username']
            )
                )
        );
        if(!empty($username)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Before isUniqueEmail
     * @param array $options
     * @return boolean
     */
    function isUniqueEmail($check) {
        $email = $this->find(
            'first', array(
            'fields' => array(
                'User.id'
            ),
            'conditions' => array(
                'User.email' => $check['email']
            )
                )
        );

        if(!empty($email)) {
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $email['User']['id']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function alphaNumericDashUnderscore($check) {
        // $data array is passed using the form field name as the key
        // have to extract the value to make the function generic
        $value = array_values($check);
        $value = $value[0];

        return preg_match('/^[a-zA-Z0-9_ \-\.]*$/', $value);
    }

    public function equaltofield($check, $otherfield) {
        //get name of field
        $fname = '';
        foreach ($check as $key => $value) {
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    }
    /**
     * Before Save
     * @param array $options
     * @return boolean
     */
    public function beforeSave($options = array()) {
        
        // hash our password
        if(isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        
        // if we get a new password, hash it
       if(isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
        }

        // fallback to our parent
        return parent::beforeSave($options);
    }

    function checkcurrentpasswords()   // to check current password
    {
      //  $this->id = $this->data['Users']['id'];
        $this->id = AuthComponent::user('id');
        $user_data = $this->field('password');

         return($this->Auth->password($this->data['User']['old_password']) == $user_data);
    }
    
    function identifyUser($postData){
        if(isset($postData[$this->alias]['username']) && isset($postData[$this->alias]['password'])) {
            $userInfo = $this->find('first', array('conditions' => array('User.username' =>$postData[$this->alias]['username'],'User.password' =>$postData[$this->alias]['password']),'recursive'=>-1));
            return $userInfo;
        }
        return false;
    }

}
