<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */




class SocietysAjaxController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler','Util','SocietyBill');
    public $helpers = array('Js');
    //public $uses = array('Society');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('Society');
        $this->loadModel('Wing');
        $this->loadModel('Society');
    }
    
    public function get_all_buildingwings() {
        if ($this->RequestHandler->isAjax()) {
            //$this->autoRender = false;
            $buildingId = isset($this->request->data['building_id']) ? $this->request->data['building_id'] : 0;
            $wingLists = $this->Wing->find('list', array('fields' => 'id,wing_name', 'conditions' => array('Wing.building_id' =>$buildingId,'Wing.society_id' => $this->Auth->user('id'),'Wing.status' =>Configure::read('Active')),'order' => array('wing_name' => 'asc')));
            $this->set(compact('wingLists'));
        }
    }
    
    public function get_account_heads(){
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('AccountHead');
            $accountId = isset($this->request->data['accountId']) ? $this->request->data['accountId'] : 0;
            $accountHeadLists = $this->AccountHead->find('list', array('fields' => 'id,title','conditions' => array('AccountHead.account_category_id' =>$accountId,'AccountHead.status' =>Configure::read('Active')),'order' => array('AccountHead.title' => 'asc')));
            $this->set(compact('accountHeadLists'));
        }
    }
    
    public function get_account_categories(){
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('SocietyHeadSubCategory');
            $this->SocietyHeadSubCategory->recursive = 1;
            $headSubCategoryId = isset($this->request->data['headSubCategoryId']) ? $this->request->data['headSubCategoryId'] : 0;
            $accountHeadCategoryLists = $this->SocietyHeadSubCategory->find('all',array('fields' =>array('AccountCategory.id,AccountCategory.title'),'conditions' => array('SocietyHeadSubCategory.id' =>$headSubCategoryId,'SocietyHeadSubCategory.status' =>Configure::read('Active')),'order' => array('SocietyHeadSubCategory.title' => 'asc')));
            $this->set(compact('accountHeadCategoryLists'));
        }
    }
    
    public function account_heads_by_categoryids(){
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('SocietyHeadSubCategory');
            $this->SocietyHeadSubCategory->recursive = 1;
            $headSubCategoryId = isset($this->request->data['headSubCategoryId']) ? $this->request->data['headSubCategoryId'] : 0;
            $accountHeadsLists = $this->SocietyHeadSubCategory->find('all',array('fields' =>array('AccountHead.id,AccountHead.title'),'conditions' => array('SocietyHeadSubCategory.id' =>$headSubCategoryId,'SocietyHeadSubCategory.status' =>Configure::read('Active')),'order' => array('SocietyHeadSubCategory.title' => 'asc')));
            $this->set(compact('accountHeadsLists'));
        }
    }
    
    public function societyMemberPaymentDetails($societyMemberId = null) {
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('Member');
            $requestData = $this->request->data;
            $societyMemberId = isset($societyMemberId) ? $societyMemberId : 0;
            if ($this->Member->MemberPayment->hasAny(array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId))) {
                $resultArr = $this->getSocietyMemberPaymentDataListing($requestData, $societyMemberId);
                $totalFiltered = isset($resultArr['total_filtered']) ? $resultArr['total_filtered'] : count($resultArr);
                $data = $resultArr['data'];
                $json_data = array(
                    "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                    "recordsTotal" => intval(count($resultArr)), // total number of records
                    "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data" => $data   // total data array
                );
            } else {
                $json_data = array(
                    "draw" => intval(0), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                    "recordsTotal" => intval(0), // total number of records
                    "recordsFiltered" => intval(0), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data" => array()   // total data array
                );
            }
            return new CakeResponse(array('body' => json_encode($json_data), 'status' => 200));
        }
    }
    
    public function getSocietyMemberPaymentDataListing($requestData, $societyMemberId) {
        $returnArr = array();
        $this->loadModel('MemberPayment');
        if (!empty($requestData)) {
            $societyBankBalanceHeadsLists = $this->SocietyBill->societyBankBalanceHeadsLists();
            $societyCashBalanceHeadsLists = $this->SocietyBill->societyCashBalanceHeadsLists();
            $PaymentModeLists = $this->SocietyBill->getSocietyPaymentModeLists();
            //print_r($societyBankLists);die;
            $data = array();
            $columns = array(
                0 => 'id',
                1 => 'bill_generated_id',
                2 => 'payment_date',
                3 => 'amount_paid',
                4 => 'society_bank_id',
                5 => 'payment_mode',
                6 => 'cheque_reference_number',
                7 => 'bill_type',
                8 => 'action_edit',
                9 => 'action_delete'
            );
            $requestData['start'] = isset($requestData['start']) ? $requestData['start'] : 0;
            $requestData['length'] = isset($requestData['length']) ? $requestData['length'] : 5;

            if (isset($requestData['search']['value']) && $requestData['search']['value'] != '') {
                $options['OR']['MemberPayment.payment_date LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.amount_paid LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.payment_mode LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.cheque_reference_number LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.society_bank_id LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.bill_generated_id LIKE'] = '%' . $requestData['search']['value'] . '%';
                $options['OR']['MemberPayment.bill_type LIKE'] = '%' . $requestData['search']['value'] . '%';
            }
            $options[] = array('AND' => array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId));

            if (isset($requestData['order'][0]['column']) && isset($requestData['order'][0]['dir'])) {
                $order[] = array('MemberPayment.' . $columns[$requestData['order'][0]['column']] => $requestData['order'][0]['dir']);
            } else {
                $order[] = array('MemberPayment.id' => 'asc');
            }

            if (isset($requestData['start']) && isset($requestData['length'])) {
                $limit = $requestData['length'];
            } else {
                $limit = 10;
            }

            $memberPaymentArr = $this->MemberPayment->find('all', array('conditions' => $options, 'limit' => $limit, 'order' => $order));
            if (count($memberPaymentArr) > 0) {
                $count = 1;
                foreach ($memberPaymentArr as $memberData) {
                    $nestedData = array();
                    $actionEditString = "<td class='text-nowrap'><a href='javascript:void(0);' class='mr-25' data-toggle='tooltip' data-original-title='Edit' onclick='editMemberPaymentDetails(".$memberData['MemberPayment']['id'].");'><i class='fa fa-pencil text-inverse m-r-10'></i></a></td>";
                    //$actionDeleteString = "<td class='text-nowrap'><a href='javascript:void(0);' class='mr-25' data-toggle='tooltip' data-original-title='Edit' onclick='deleteMemberPaymentDetails(".$memberData['MemberPayment']['id'].");'><i class='fa fa-close text-danger'></i></a></td>";
                    $actionDeleteString = "<td class='text-nowrap'><a href='javascript:void(0);' class='mr-25' data-toggle='tooltip' data-original-title='Edit'><i class='fa fa-close text-danger'></i></a></td>";
                    $nestedData[] = $count;
                    $nestedData[] = isset($memberData['MemberPayment']['bill_generated_id']) ? $memberData['MemberPayment']['bill_generated_id'] : '';
                    $nestedData[] = isset($memberData['MemberPayment']['payment_date']) ? $memberData['MemberPayment']['payment_date'] : '';
                    $nestedData[] = isset($memberData['MemberPayment']['amount_paid']) ? $memberData['MemberPayment']['amount_paid'] : '';
                    if($memberData['MemberPayment']['payment_mode'] == Configure::read('PaymentMode.Cash')){
                        $nestedData[] = isset($societyCashBalanceHeadsLists[$memberData['MemberPayment']['society_bank_id']]) ? $societyCashBalanceHeadsLists[$memberData['MemberPayment']['society_bank_id']] : '';
                    }else{
                        $nestedData[] = isset($societyBankBalanceHeadsLists[$memberData['MemberPayment']['society_bank_id']]) ? $societyBankBalanceHeadsLists[$memberData['MemberPayment']['society_bank_id']] : '';
                    }
                    $nestedData[] = isset($PaymentModeLists[$memberData['MemberPayment']['payment_mode']]) ? $PaymentModeLists[$memberData['MemberPayment']['payment_mode']] : '';
                    $nestedData[] = isset($memberData['MemberPayment']['cheque_reference_number']) ? $memberData['MemberPayment']['cheque_reference_number'] : '';
                    $nestedData[] = isset($memberData['MemberPayment']['bill_type']) ? $memberData['MemberPayment']['bill_type'] : '';
                    $nestedData[] = $actionEditString;
                    $nestedData[] = $actionDeleteString;
                    $data[] = $nestedData;
                    $count++;
                }
            }
            $returnArr['total_filtered'] = count($memberPaymentArr);
            $returnArr['data'] = $data;
        }
        return $returnArr;
    }

    public function society_member_tariff_details() {
        $dataArry = array();
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('Member');
            $this->loadModel('MemberTariff');
            $this->loadModel('MemberTariffDetails');
            $societyMemberId = isset($this->request->data['societyMemberId']) ? $this->request->data['societyMemberId'] : 0;
            $societyMemberTariff = $this->MemberTariff->find('all', array('conditions' => array('MemberTariff.member_id' => $societyMemberId, 'MemberTariff.society_id' => $this->Session->read('Auth.User.id'))));
            $societyMemberTariffDetails = $this->MemberTariffDetails->find('first', array('conditions' => array('MemberTariffDetails.member_id' => $societyMemberId)));

            $societyMemberDetails = $this->Member->find('first', array('conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'),'Member.id' => $societyMemberId), 'fields' => array('Member.flat_no')));

            $tariffSetData['tariff_effective_since'] = empty($societyMemberTariffDetails['MemberTariffDetails']['tariff_effective_since']) ? '' : $societyMemberTariffDetails['MemberTariffDetails']['tariff_effective_since'];

            $tariffSetData['flat_no'] = empty($societyMemberDetails['Member']['flat_no']) ? '' : $societyMemberDetails['Member']['flat_no'];

            $tariffSetData['remark'] = empty($societyMemberTariffDetails['MemberTariffDetails']['remark']) ? '' : $societyMemberTariffDetails['MemberTariffDetails']['remark'];
            $tariffSetData['updated_date'] = empty($societyMemberTariffDetails['MemberTariffDetails']['updated_date']) ? '' : $societyMemberTariffDetails['MemberTariffDetails']['updated_date'];
            $dataArry['MemberTariffDetails'] = $tariffSetData;

            if (isset($societyMemberTariff) && count($societyMemberTariff) > 0) {
                $tariffCount = 0;
                foreach ($societyMemberTariff as $tariffData) {
                    $memberTariffData[$tariffCount]['ledger_head_id'] = isset($tariffData['MemberTariff']['ledger_head_id']) ? $tariffData['MemberTariff']['ledger_head_id'] : '';
                    $memberTariffData[$tariffCount]['amount'] = isset($tariffData['MemberTariff']['amount']) ? $tariffData['MemberTariff']['amount'] : '';
                    $memberTariffData[$tariffCount]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    $tariffCount++;
                }
                $dataArry['MemberTariff'] = $memberTariffData;
            }
        }
        return new CakeResponse(array('body' => json_encode($dataArry), 'status' => 200));
    }

    public function society_building_tariff_details() {
        $dataArry = array();
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('Building');
            $this->loadModel('SocietyTariffBuildings');           
            
            $buildingId = isset($this->request->data['buildingId']) ? $this->request->data['buildingId'] : 0;
            
            $societyBuildingTariff = $this->SocietyTariffBuildings->find('all', array('conditions' => array('SocietyTariffBuildings.building_id' => $buildingId, 'SocietyTariffBuildings.society_id' => $this->Session->read('Auth.User.id'))));
            
            $buildingTariffData = array();
            
            if (isset($societyBuildingTariff) && count($societyBuildingTariff) > 0) {
                $tariffCount = 0;
                foreach ($societyBuildingTariff as $tariffData) {
                    $buildingTariffData[$tariffCount]['ledger_head_id'] = isset($tariffData['SocietyTariffBuildings']['ledger_head_id']) ? $tariffData['SocietyTariffBuildings']['ledger_head_id'] : '';
                    $buildingTariffData[$tariffCount]['amount'] = isset($tariffData['SocietyTariffBuildings']['amount']) ? $tariffData['SocietyTariffBuildings']['amount'] : '';
                    $buildingTariffData[$tariffCount]['effective_since'] = isset($tariffData['SocietyTariffBuildings']['effective_since']) ? $tariffData['SocietyTariffBuildings']['effective_since'] : '';
                    $buildingTariffData[$tariffCount]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    $tariffCount++;
                }
                
                $dataArry['BuildingTariff'] = $buildingTariffData;
            }
        }
        return new CakeResponse(array('body' => json_encode($dataArry), 'status' => 200));
    }
    
    public function society_wing_tariff_details() {
        $dataArry = array();
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('SocietyTariffWings');           
            
            $buildingId = isset($this->request->data['buildingId']) ? $this->request->data['buildingId'] : 0;
            $wingId = isset($this->request->data['wingId']) ? $this->request->data['wingId'] : 0;
            
            $societyWingTariff = $this->SocietyTariffWings->find('all', array('conditions' => array('SocietyTariffWings.building_id' => $buildingId,'SocietyTariffWings.wing_id' => $wingId, 'SocietyTariffWings.society_id' => $this->Session->read('Auth.User.id'))));
            
            $wingTariffData = array();
            
            if (isset($societyWingTariff) && count($societyWingTariff) > 0) {
                $tariffCount = 0;
                foreach ($societyWingTariff as $tariffData) {
                    $wingTariffData[$tariffCount]['ledger_head_id'] = isset($tariffData['SocietyTariffWings']['ledger_head_id']) ? $tariffData['SocietyTariffWings']['ledger_head_id'] : '';
                    $wingTariffData[$tariffCount]['amount'] = isset($tariffData['SocietyTariffWings']['amount']) ? $tariffData['SocietyTariffWings']['amount'] : '';
                    $wingTariffData[$tariffCount]['effective_since'] = isset($tariffData['SocietyTariffWings']['effective_since']) ? $tariffData['SocietyTariffWings']['effective_since'] : '';
                    $wingTariffData[$tariffCount]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    $tariffCount++;
                }
                
                $dataArry['WingTariff'] = $wingTariffData;
            }
        }
        return new CakeResponse(array('body' => json_encode($dataArry), 'status' => 200));
    }
    
    public function society_unit_type_tariff_details() {
        $dataArry = array();
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('SocietyTariffUnitTypes');           
            
            $unitType = isset($this->request->data['unitType']) ? $this->request->data['unitType'] : '';
            
            $societyUnitTypeTariff = $this->SocietyTariffUnitTypes->find('all', array('conditions' => array('SocietyTariffUnitTypes.unit_type' => $unitType, 'SocietyTariffUnitTypes.society_id' => $this->Session->read('Auth.User.id'))));
            
            $unitTypeTariffData = array();
            
            if (isset($societyUnitTypeTariff) && count($societyUnitTypeTariff) > 0) {
                $tariffCount = 0;
                foreach ($societyUnitTypeTariff as $tariffData) {
                    $unitTypeTariffData[$tariffCount]['ledger_head_id'] = isset($tariffData['SocietyTariffUnitTypes']['ledger_head_id']) ? $tariffData['SocietyTariffUnitTypes']['ledger_head_id'] : '';
                    $unitTypeTariffData[$tariffCount]['amount'] = isset($tariffData['SocietyTariffUnitTypes']['amount']) ? $tariffData['SocietyTariffUnitTypes']['amount'] : '';
                    $unitTypeTariffData[$tariffCount]['effective_since'] = isset($tariffData['SocietyTariffUnitTypes']['effective_since']) ? $tariffData['SocietyTariffUnitTypes']['effective_since'] : '';
                    $unitTypeTariffData[$tariffCount]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    $tariffCount++;
                }
                
                $dataArry['UnitTypeTariff'] = $unitTypeTariffData;
            }
        }
        return new CakeResponse(array('body' => json_encode($dataArry), 'status' => 200));
    }
    
    public function sortMultiDimentionalArray($a,$b){
        return (strtotime($a["formatted_date"]) <= strtotime($b["formatted_date"])) ? -1 : 1;
    }
    
    public function society_ledger_head_details(){
        $societyLedgerHeadDetails = array('SocietyLedgerHeads'=>array('status'=> 1));
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('SocietyLedgerHeads');
            //$this->SocietyLedgerHeads->recursive = -1;
            $societyLedgerHeadId = isset($this->request->data['societyLedgerheadId']) ? $this->request->data['societyLedgerheadId'] : 0;
            $societyLedgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $societyLedgerHeadId, 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.status' => Configure::read('Active'))));            
            if (isset($societyLedgerHeadDetails) && count($societyLedgerHeadDetails) > 0) {
                $societyLedgerHeadsNextId = $this->SocietyLedgerHeads->find('first', array('fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.title'), 'conditions' => array('SocietyLedgerHeads.id >' => $societyLedgerHeadDetails['SocietyLedgerHeads']['id'], 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.status' => Configure::read('Active')), 'order' => array('SocietyLedgerHeads.id' => 'asc')));
                $societyLedgerHeadsPreviousId = $this->SocietyLedgerHeads->find('first', array('fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.title'), 'conditions' => array('SocietyLedgerHeads.id <' => $societyLedgerHeadDetails['SocietyLedgerHeads']['id'], 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.status' => Configure::read('Active')), 'order' => array('SocietyLedgerHeads.id' => 'desc')));
                $societyLedgerHeadDetails['SocietyLedgerHeads']['next'] = 0;
                $societyLedgerHeadDetails['SocietyLedgerHeads']['previous'] = 0;
                $societyLedgerHeadDetails['SocietyLedgerHeads']['previous_id'] = (isset($societyLedgerHeadsPreviousId['SocietyLedgerHeads']['id']) && $societyLedgerHeadsPreviousId['SocietyLedgerHeads']['id'] != '') ? $societyLedgerHeadsPreviousId['SocietyLedgerHeads']['id'] : 0;
                $societyLedgerHeadDetails['SocietyLedgerHeads']['next_id'] = (isset($societyLedgerHeadsNextId['SocietyLedgerHeads']['id']) && $societyLedgerHeadsNextId['SocietyLedgerHeads']['id'] != '') ? $societyLedgerHeadsNextId['SocietyLedgerHeads']['id'] : 0;
                if (empty($societyLedgerHeadsPreviousId)) {
                    $societyLedgerHeadDetails['SocietyLedgerHeads']['previous'] = 1;
                }
                if (empty($societyLedgerHeadsNextId)) {
                    $societyLedgerHeadDetails['SocietyLedgerHeads']['next'] = 1;
                }
                $societyLedgerHeadDetails['SocietyLedgerHeads']['status'] = 1;
                
                //Get society ledger head expenses and payments
                $txnType = $societyLedgerHeadDetails['AccountHead']['transaction_type'];
                if(stripos($txnType,'credit') !== false) {
                   $txnType = 'credit'; 
                } else if(stripos($txnType,'debit') !== false) {
                   $txnType = 'debit'; 
                }
                
                $isInBill = $societyLedgerHeadDetails['SocietyLedgerHeads']['is_in_bill_charges'];
                
                $societyPaymentData = array();
                
                if($isInBill == 0) {
                    // get records from society_payments
                    $this->loadModel('SocietyPayment');
                    $societyPayments = $this->SocietyPayment->find('all', array('conditions' => array('SocietyPayment.ledger_head_id' => $societyLedgerHeadId, 'SocietyPayment.society_id' => $this->Session->read('Auth.User.id'))));
                    if (!empty($societyPayments)) {
                        foreach ($societyPayments as $paymentDetails) {
                            $paymentData = $paymentDetails['SocietyPayment'];
                            $paymentDate = date('d/m/Y', strtotime($paymentData['payment_date']));
                            $formattedDate = date('Y-m-d', strtotime($paymentData['bill_generated_date']));
                            $societyPaymentData[] = array('payment_date' => $paymentDate, 'formatted_date' => $formattedDate, 'amount' => $paymentData['total_amount'], 'txn_type' => $txnType);
                        }
                    }
                } else {
                    // get records from member bills
                    $this->loadModel('MemberBillGenerate');
                    $memberBills = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.ledger_head_id' => $societyLedgerHeadId, 'MemberBillGenerate.society_id' => $this->Session->read('Auth.User.id'))));
                    if (!empty($memberBills)) {
                        foreach ($memberBills as $paymentDetails) {
                            $paymentData = $paymentDetails['MemberBillGenerate'];
                            $paymentDate = date('d/m/Y', strtotime($paymentData['bill_generated_date']));
                            $formattedDate = date('Y-m-d', strtotime($paymentData['bill_generated_date']));
                            $societyPaymentData[] = array('payment_date' => $paymentDate, 'formatted_date' => $formattedDate, 'amount' => $paymentData['amount'], 'txn_type' => $txnType);
                        }
                    }
                }

                $journalVoucherLedgerPaymentData = $this->SocietyBill->getJournalVoucherCreaditDebitDataByLedgerHeadId($societyLedgerHeadId);
                if (!empty($journalVoucherLedgerPaymentData)) {
                    foreach ($journalVoucherLedgerPaymentData as $jvData) {
                        $txnJvType = '';
                        $jvAmt = 0;
                        $JVPARTICULAR = "";
                        if ($jvData['JournalVoucher']['jv_debit_ledger_head_id'] != '') {
                            $txnJvType = "debit";
                            $jvAmt = $jvData['JournalVoucher']['jv_amount_debited'];
                            $JVPARTICULAR = "JV Debited. V.No ".$jvData['JournalVoucher']['voucher_no'];
                        }

                        if ($jvData['JournalVoucher']['jv_credit_ledger_head_id'] != '') {
                            $txnJvType = "credit";
                            $jvAmt = $jvData['JournalVoucher']['jv_amount_credited'];
                            $JVPARTICULAR = "JV Credited. V.No ".$jvData['JournalVoucher']['voucher_no'];
                        }

                        $paymentDate = isset($jvData['JournalVoucher']['voucher_date']) ? date('d/m/Y', strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                        $formattedDate = isset($jvData['JournalVoucher']['voucher_date']) ? date('Y-m-d', strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                        $societyPaymentData[] = array('particularNote' => $JVPARTICULAR, 'payment_date' => $paymentDate, 'formatted_date' => $formattedDate, 'amount' => $jvAmt, 'txn_type' => $txnJvType);
                    }
                }
                usort($societyPaymentData,array($this,"sortMultiDimentionalArray"));
                
                $societyLedgerHeadDetails['ledgerPaymentData'] = $societyPaymentData;
                $societyLedgerHeadDetails['txn_type'] = $txnType;
            }
        }
        return new CakeResponse(array('body' => json_encode($societyLedgerHeadDetails), 'status' => 200));
    }
    
    public function add_society_ledger_heads() {
        $this->layout = false; //Update add Society/Login 
        $response['error'] = 1;
        $response['error_message'] = 'Opps! Somthing went wrong.';
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $this->loadModel('SocietyLedgerHeads');
            $societyLoginID = $this->Auth->user('id');
            $societyLedgerId = isset($this->request->data['SocietyLedgerHeads']['id']) ? $this->request->data['SocietyLedgerHeads']['id'] : '';
            $societyLedgerTitle = isset($this->request->data['SocietyLedgerHeads']['title']) ? $this->request->data['SocietyLedgerHeads']['title'] : '';
            $societyLedgerSubCategory = isset($this->request->data['SocietyLedgerHeads']['society_head_sub_category_id']) ? $this->request->data['SocietyLedgerHeads']['society_head_sub_category_id'] : '';
            $societyLedgerAccountHeadId = isset($this->request->data['SocietyLedgerHeads']['account_head_id']) ? $this->request->data['SocietyLedgerHeads']['account_head_id'] : '';
            $societyLedgerAccountCategoryId = isset($this->request->data['SocietyLedgerHeads']['account_category_id']) ? $this->request->data['SocietyLedgerHeads']['account_category_id'] : '';
            $societyLedgerShortCode = isset($this->request->data['SocietyLedgerHeads']['short_code']) ? $this->request->data['SocietyLedgerHeads']['short_code'] : '';
            if ($this->SocietyLedgerHeads->updateAll(array('SocietyLedgerHeads.title' =>"'$societyLedgerTitle'",'SocietyLedgerHeads.society_head_sub_category_id' =>"'$societyLedgerSubCategory'", 'SocietyLedgerHeads.account_head_id' => "'$societyLedgerAccountHeadId'", 'SocietyLedgerHeads.account_category_id' => "'$societyLedgerAccountCategoryId'", 'SocietyLedgerHeads.short_code' => "'$societyLedgerShortCode'"),array('SocietyLedgerHeads.id'=>$societyLedgerId,'SocietyLedgerHeads.society_id'=>$societyLoginID))) {
                $response['error'] = 0;
                $response['error_message'] = 'Society ledger head data updated successfully.';
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }
    
    public function getSocietyMemberDetails() {
        $societyMemberDetails = array('Member' => array('status' => 1));
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('Member');
            $this->loadModel('MemberBillSummary');
            //$this->Member->recursive = -1;
            $societyMemberId = isset($this->request->data['societyMemberId']) ? $this->request->data['societyMemberId'] : 0;
            $societyMemberDetails = $this->Member->find('first', array('conditions' => array('Member.id' => $societyMemberId, 'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'))));
            $condition = array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id'), 'MemberBillSummary.member_id' => $societyMemberId);
            $societyMemberBillPaymentData = array();
            $memberBillSummary = $this->Member->MemberBillSummary->find('all', array('conditions' => $condition));

            if (isset($memberBillSummary) && count($memberBillSummary) > 0) {
                $summaryCounter = 0;
                foreach ($memberBillSummary as $billData) {
                    if (isset($billData['MemberBillSummary'])) {
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary'] = $billData['MemberBillSummary'];
                        
                        //Change date format
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date'] = date('d/m/Y',strtotime($societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date']));
                        $monthName = date("F", mktime(0, 0, 0, $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month'], 10));
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month']);
                        
                        $paymentMadeBillNo = isset($billData['MemberBillSummary']['bill_no']) ? $billData['MemberBillSummary']['bill_no'] : '';
                        $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId, 'MemberPayment.bill_generated_id' => $paymentMadeBillNo);
                        if ($this->Member->MemberPayment->hasAny($conditionPayment)) {
                            $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('conditions' => $conditionPayment));
                            if (isset($memberPaymentDetails) && count($memberPaymentDetails) > 0) {
                                $paymentCounter = 0;
                                foreach ($memberPaymentDetails as $paymentData) {
                                    $paymentData['MemberPayment']['payment_date'] = date('d/m/Y',strtotime($paymentData['MemberPayment']['payment_date']));
                                    $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter] = $paymentData['MemberPayment'];
                                    $paymentCounter++;
                                }
                            }
                        }
                    }
                    $summaryCounter++;
                }
            }

            if (isset($societyMemberDetails) && count($societyMemberDetails) > 0) {
                $societyMemberNextId = $this->Member->find('first', array('fields' => array('Member.id'), 'conditions' => array('Member.id >' => $societyMemberDetails['Member']['id'], 'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'order' => array('Member.id' => 'asc')));
                $societyMemberPreviousId = $this->Member->find('first', array('fields' => array('Member.id'),'conditions' => array('Member.id <' => $societyMemberDetails['Member']['id'], 'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'order' => array('Member.id' => 'desc')));
                $societyMemberDetails['Member']['next'] = 0;
                $societyMemberDetails['Member']['previous'] = 0;
                $societyMemberDetails['Member']['previous_id'] = (isset($societyMemberPreviousId['Member']['id']) && $societyMemberPreviousId['Member']['id'] != '') ? $societyMemberPreviousId['Member']['id'] : 0;
                $societyMemberDetails['Member']['next_id'] = (isset($societyMemberNextId['Member']['id']) && $societyMemberNextId['Member']['id'] != '') ? $societyMemberNextId['Member']['id'] : 0;
                if (empty($societyMemberPreviousId)) {
                    $societyMemberDetails['Member']['previous'] = 1;
                }
                if (empty($societyMemberNextId)) {
                    $societyMemberDetails['Member']['next'] = 1;
                }
                $societyMemberDetails['Member']['status'] = 1;
            }
            
            $journalVoucherMemberPaymentData = $this->SocietyBill->getJournalVoucherCreaditDebitDataByMemberId($societyMemberId);
            
            
            if (!empty($journalVoucherMemberPaymentData)){
				$paymentCounter = 0;
                foreach ($journalVoucherMemberPaymentData as $jvData){
                    if ($jvData['JournalVoucher']['jv_credit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_credit_member_head_id'] == $societyMemberId) {
						$societyMemberBillPaymentData[0]['MemberPayment'][$paymentCounter]['amount_paid'] = isset($jvData['JournalVoucher']['jv_amount_credited']) ? $jvData['JournalVoucher']['jv_amount_credited'] : 0.00;
                        $societyMemberBillPaymentData[0]['MemberPayment'][$paymentCounter]['payment_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $this->Util->mysqlToDate($jvData['JournalVoucher']['voucher_date'], '/') : "";
                        $societyMemberBillPaymentData[0]['MemberPayment'][$paymentCounter]['cheque_reference_number'] = "JV Credited. V.No ".$jvData['JournalVoucher']['voucher_no'];
						$societyMemberBillPaymentData[0]['MemberPayment'][$paymentCounter]['flag'] = "jv";
						$paymentCounter++;
					}

                    if ($jvData['JournalVoucher']['jv_debit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_debit_member_head_id'] == $societyMemberId) {
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['id'] = "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_no'] = "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_principal_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_bill_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $this->Util->mysqlToDate($jvData['JournalVoucher']['voucher_date'], '/') : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthName'] = isset($jvData['JournalVoucher']['voucher_date']) ? "JV Debited. V.No " . $jvData['JournalVoucher']['voucher_no'] : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month'] = isset($jvData['JournalVoucher']['voucher_date']) ? date('m', strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_principal_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
						$societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['flag'] = "jv";
                        $summaryCounter++;
                    }
                }
            }
            
            $societyMemberDetails['memberBillPayment'] = $societyMemberBillPaymentData;
        }
        //echo '<pre>';print_r($societyMemberDetails);
        return new CakeResponse(array('body' => json_encode($societyMemberDetails), 'status' => 200));
    }
    
    public function getMemberDetailsByFlatNo() {
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('Member');
            $societyMemberDeta = array();
            if($this->request->data['wing_id'] > 0) {
                $societyMemberDetails = $this->Member->find('first', array('conditions' => array('Member.flat_no' => $this->request->data['flat_no'], 'Member.building_id' => $this->request->data['building_id'],'Member.wing_id' => $this->request->data['wing_id'],'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'))));            
            } else {
               $societyMemberDetails = $this->Member->find('first', array('conditions' => array('Member.flat_no' => $this->request->data['flat_no'], 'Member.building_id' => $this->request->data['building_id'],'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'))));             
            }
            if(isset($societyMemberDetails['Member']) && count($societyMemberDetails['Member']) > 0){
                $societyMemberDeta = $societyMemberDetails['Member'];
            }
        }
        return new CakeResponse(array('body' => json_encode($societyMemberDeta), 'status' => 200));
    }
    
    public function updateSocietyMemberDetails() {
        $this->layout = false; //Update add Society/Login 
        $response['error'] = 1;
        $response['error_message'] = 'Member data could not saved.';
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->loadModel('Member');
            if (isset($this->request->data['Member']) && count($this->request->data['Member']) > 0) {
                if ($this->Member->save($this->request->data['Member'])) {
                    $response['error'] = 0;
                    $response['error_message'] = 'Details of "'.$this->request->data['Member']['member_name'].'" has been updated successfully.';
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function updateSocietyLedgerOrder(){
        $this->layout = false; //Update add Society/Login 
        $response['error'] = 1;
        $response['error_message'] = 'Society tariff order data could not saved.';
        $societyLoginID = $this->Auth->user('id');
        $this->loadModel('SocietyTariffOrder');
        if ($this->request->is('ajax')) {
            if(isset($this->request->data['sort_order'])){
                $societyLedgerIdArray = explode(",",$this->request->data["sort_order"]);
                $serialNo=1;
                foreach ($societyLedgerIdArray as $key => $societyLedgerId) {
                    $postData[$serialNo]['society_id'] = $societyLoginID; 
                    $postData[$serialNo]['ledger_head_id'] = $societyLedgerId;
                    $postData[$serialNo]['tariff_serial'] = $serialNo;
                    $serialNo++;
                }
                if($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id'=>$societyLoginID))){
                     $this->SocietyTariffOrder->deleteAll(array('SocietyTariffOrder.society_id'=>$societyLoginID));
                }
                if($this->SocietyTariffOrder->saveAll($postData)){
                    $response['error'] = 0;
                    $response['error_message'] = 'Society tariff order set successfully.';
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }
    
    public function getMembersOutstandingPaymentsBalance($societyMemberId=null) {
        $jsonData = array();
        $this->layout = false; 
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('Member');
            $conditions = array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id'), 'MemberBillSummary.member_id' => $societyMemberId);
             if ($this->Member->MemberBillSummary->hasAny($conditions)) {
                $memberPaymentBalance = $this->Member->MemberBillSummary->find('first', array('conditions' =>$conditions,'order' => array('MemberBillSummary.month' => 'desc')));
                if(count($memberPaymentBalance) > 0){
                    $jsonData = $memberPaymentBalance;
                }
            }
            return new CakeResponse(array('body' => json_encode($jsonData), 'status' => 200));
        }
    }
    
    //To Do For Search
    
    public function getMemberDetailsByMemberID(){
        $this->layout = false;
        $jsonData = array();
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('SocietyHeadSubCategory');
            $this->SocietyHeadSubCategory->recursive = 1;
            $memberId = isset($this->request->data['member_id']) ? $this->request->data['member_id'] : 0;
            $jsonData = $this->SocietyBill->getMemberDetailsByMemberIDAndFlatNo($memberId);
            return new CakeResponse(array('body' => json_encode($jsonData), 'status' => 200));
        }
    }
    public function searchSocietyMemberDetailsByFlat() {
        $societyMemberDetails = array('Member' => array('status' => 1));
        if ($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->loadModel('Member');
            $this->loadModel('MemberBillSummary');
            //$this->Member->recursive = -1;
            $societyMemberId = isset($this->request->data['societyMemberId']) ? $this->request->data['societyMemberId'] : 0;
            $societyMemberFlatId = isset($this->request->data['$societyMemberFlatId']) ? $this->request->data['$societyMemberFlatId'] : 0;
            $societyMemberDetails = $this->Member->find('first', array('conditions' => array('Member.id' => $societyMemberId,'Member.flat_no' => $societyMemberFlatId, 'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'))));
            $condition = array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id'), 'MemberBillSummary.member_id' => $societyMemberId);
            $societyMemberBillPaymentData = array();
            $memberBillSummary = $this->Member->MemberBillSummary->find('all', array('conditions' => $condition));

           

            $societyMemberDetails['memberBillPayment'] = $societyMemberBillPaymentData;
        }
        return new CakeResponse(array('body' => json_encode($societyMemberDetails), 'status' => 200));
    }
    
    public function getSocietyLedgerHeadsList(){
        $this->layout = false;
        $jsonData = array();
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('SocietyLedgerHeads');
            $this->loadModel('SocietyTariffOrder');
            $this->SocietyLedgerHeads->recursive = -1;
            if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id')))) {
                $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id')), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
            } else {
                $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
            }
            return new CakeResponse(array('body' => json_encode($societyLedgerHeadList),'status' => 200));
        }
    }
    
    public function getCashInHandSocietyLedgerHeadId(){
        $this->layout = false;
        $jsonData = array();
        if ($this->RequestHandler->isAjax()) {
            $jsonData = $this->SocietyBill->getCashInHandSocietyLedgerHeadId();
        }
        return new CakeResponse(array('body' => json_encode($jsonData),'status' => 200));
    }
    
    public function getCashAndBankSocietyLedgerHeadDropDown(){
        $this->layout = false;
        $ledgerData = array();
        if ($this->RequestHandler->isAjax()) {
            $paymentMode = isset($this->request->data['paymentMode']) ? $this->request->data['paymentMode'] : "Bank";
            if($paymentMode == "Bank"){
                $ledgerData = $this->SocietyBill->societyBankBalanceHeadsLists();
            }else{
                 $ledgerData = $this->SocietyBill->societyCashBalanceHeadsLists();
            }
            
            echo "<option value=\"\">Select Balance Ledger</option>";
            if(isset($ledgerData) && count($ledgerData) > 0){
                foreach($ledgerData as $societyLedgerHeadsId => $societyLedgerHeadsTitle){
                    echo "<option value=\"$societyLedgerHeadsId\">$societyLedgerHeadsTitle</option>";
                }
            }
        }
        return new CakeResponse(array('body' =>'','status' => 200));
    }
    
    public function getBillDateBilldueDateFromFinancialYearDate(){
        $this->layout = false;
        $jsonData = array();
        if ($this->RequestHandler->isAjax()){
            $checkYear = isset($this->request->data['financialBillYear']) ? substr($this->request->data['financialBillYear'],0,4) : date('Y');
            $checkMonth = isset($this->request->data['financialMonth']) ? $this->request->data['financialMonth'] : date('m');
            $jsonData = $this->Util->getBillDateBilldueDateFromFinancialYearDate($checkYear,$checkMonth);  
        }
        return new CakeResponse(array('body' => json_encode($jsonData),'status' => 200));
    }
    
    public function getAllMemberTariffDetails(){
        ini_set('memory_limit', -1);
        set_time_limit(0); // make sure we don't time out
        $this->loadModel('MemberTariff');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');
        $memberTariffHeader  = array();
        $memberTariffDataArra = array();
        if ($this->RequestHandler->isAjax()){
            if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
                $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
            } else {
                $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'),'order' => array('SocietyLedgerHeads.title' => 'asc'),));
            }
            $orConditions = '';
            if(isset($this->request->data['building_id']) && !empty($this->request->data['building_id'])){
                $orConditions  = array('OR'=>array('Member.building_id'=>$this->request->data['building_id']));
            }
            
            $societyMemberListArr = $this->SocietyBill->getSocietyMemberList();
           
            $wingID = isset($this->request->data['wing_id']) ? $this->request->data['wing_id'] :'';
            $memberTariffHeader['sr_no'] = 'SR.';
            $memberTariffHeader['Member_Name'] = 'MEMBER NAME';
            $memberTariffHeader['flat_no'] = 'Flat No';
            if(isset($societyLedgerHeadList) && !empty($societyLedgerHeadList)){
                foreach($societyMemberListArr as $memberID => $memberName){
                foreach($societyLedgerHeadList as $ledgerData){
                        $memberTariffDetails = $this->MemberTariff->find('all', array('conditions' => array('MemberTariff.society_id' => $societyLoginID,'MemberTariff.member_id' => $memberID,'MemberTariff.ledger_head_id' => $ledgerData['SocietyLedgerHeads']['id'],'Member.status' => Configure::read('Active'),$orConditions)));
                        if(isset($memberTariffDetails) && count($memberTariffDetails) > 0){
                            foreach($memberTariffDetails as $memberTarrifData){
                                $memberTariffDataArra[$memberTarrifData['MemberTariff']['member_id']]['member_name'] = $memberName;
                                $memberTariffDataArra[$memberTarrifData['MemberTariff']['member_id']]['flat_no'] = $memberTarrifData['Member']['flat_no'];
                                $memberTariffDataArra[$memberTarrifData['MemberTariff']['member_id']]['ledgerData'][$memberTarrifData['MemberTariff']['ledger_head_id']][$memberTarrifData['MemberTariff']['ledger_head_id']] = $memberTarrifData['MemberTariff']['amount'];
                                $memberTariffHeader[$memberTarrifData['SocietyLedgerHeads']['title']] = $memberTarrifData['SocietyLedgerHeads']['title'];
                            }
                        }else{
                            $memberTariffDataArra[$memberID]['member_name'] = $memberName;
                            $memberTariffDataArra[$memberID]['flat_no'] = "";
                            $memberTariffDataArra[$memberID]['ledgerData'][$ledgerData['SocietyLedgerHeads']['id']][$ledgerData['SocietyLedgerHeads']['id']] = "0.00";
                            $memberTariffHeader[$ledgerData['SocietyLedgerHeads']['title']] = $ledgerData['SocietyLedgerHeads']['title'];
                        }
                    }
                }
                $memberTariffHeader['total'] = 'Total';
            }
        }
        $this->set(compact('memberTariffHeader','memberTariffDataArra'));
    }
    
    public function updateAllMemberTariffDetails(){
        $this->layout = false;
        ini_set('memory_limit', -1);
        set_time_limit(0); // make sure we don't time out
        $jsonData = array();
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        if ($this->RequestHandler->isAjax()) {
            $societyLoginID = $this->Session->read('Auth.User.id');
            $postMemberTariff = array();
            if ($this->request->is('post', 'put')) {
                if (isset($this->request->data['MemberTariff']) && count($this->request->data['MemberTariff']) > 0) {
                    foreach ($this->request->data['MemberTariff'] as $MemberID => $mTariffData) {
                        foreach ($mTariffData as $ledgerHeadId => $fData) {
                            $this->MemberTariff->create();
                            $societyMemberExists = $this->MemberTariff->find('first', array('conditions' => array('MemberTariff.member_id' => $MemberID, 'MemberTariff.ledger_head_id' => $ledgerHeadId, 'MemberTariff.society_id' => $societyLoginID), 'recursive' => -1));
                            if (!empty($societyMemberExists)) {
                                $this->MemberTariff->id = $societyMemberExists['MemberTariff']['id'];
                            }
                            $postMemberTariff['society_id'] = $societyLoginID;
                            $postMemberTariff['member_id'] = isset($MemberID) ? $MemberID : '';
                            $postMemberTariff['amount'] = isset($fData[$ledgerHeadId]) ? $fData[$ledgerHeadId] : 0;
                            $postMemberTariff['updated_date'] = $this->Util->getDateTime();
                            $postMemberTariff['ledger_head_id'] = isset($ledgerHeadId) ? $ledgerHeadId : null;
                            $serialOrderDetails = $this->SocietyTariffOrder->find('first',array('conditions' => array('SocietyTariffOrder.society_id' => $societyLoginID, 'SocietyTariffOrder.ledger_head_id' => $ledgerHeadId),'recursive' => -1));
                            $postMemberTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                            $postMemberTariff['updated_date'] = $this->Util->getDateTime();
                            if ($this->MemberTariff->save($postMemberTariff)) {
                                $jsonData['error'] = 0;
                                $jsonData['error_message'] = 'The member tariff has been Updated.';
                                unset($postMemberTariff);
                            } else {
                                $jsonData['error'] = 1;
                                $jsonData['error_message'] = 'The member tariff could not be saved. Please, try again.';
                            }
                        }
                    }
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($jsonData), 'status' => 200));
    }

}
