<?php

App::uses('Component', 'Controller');

class MenuComponent extends Component {

    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    function menuItems($accessLevel = null, $url = null) {
        $menuArray = null;

        if ($accessLevel == 'Admin') {
            $menuArray = array('menu' => array(
                    'Dashboard' => array(
                        'link' => Router::url(array('controller' => 'admin', 'action' => 'dashboard'), true),
                        'icon' => '<i class="zmdi zmdi-landscape mr-20"></i>'
                    ),
                    'Manage Societies' => array('icon' => '<i class="zmdi zmdi-edit mr-20"></i></i>', 'sublink' => Router::url(array('controller' => 'admin', 'action' => 'dashboard'), true),
                        'submenu' => array(
                            Router::url(array('controller' => 'admin', 'action' => 'add_societys'), true) => 'Add Society',
                            Router::url(array('controller' => 'admin', 'action' => 'assign_societies'), true) => 'Reseller Societies',
                            Router::url(array('controller' => 'admin', 'action' => 'view_societys'), true) => 'Societies List',
                        ),
                    ),
                    'Society Parameters' => array(
                        'link' => Router::url(array('controller' => 'admin', 'action' => 'society_parameter'), true),
                        'icon' => '<i class="zmdi zmdi-google-pages mr-20"></i></i>',
                    //'submenu' => array(
                    // Router::url(array('controller' => 'admin', 'action' => 'society_parameter'), true) => 'Bill Frequency',
                    // Router::url(array('controller' => 'Requests', 'action' => 'index'), true) => 'Interest Types',
                    // Router::url(array('controller' => 'Requests', 'action' => 'project_tracker'), true) => 'Tariff Types',
                    //),
                    //'link' => Router::url(array('controller' => 'Batches', 'action' => 'index'), true)
                    )
                /* 'Ledger Management' => array(
                  'icon' => '<i class="zmdi zmdi-book mr-20"></i>',
                  'submenu' => array(
                  Router::url(array('controller' => 'Users', 'action' => 'index'), true) => 'Ledger Categories Management',
                  Router::url(array('controller' => 'Requests', 'action' => 'index'), true) => 'Ledger Groups Management',
                  Router::url(array('controller' => 'Requests', 'action' => 'index'), true) => 'Ledger Sub Groups Management',
                  ),
                  //'link' => Router::url(array('controller' => 'Batches', 'action' => 'index'), true)
                  ),
                  'Reports' => array(
                  'link' => Router::url(array('controller' => 'Requests', 'action' => 'report'), true),
                  'icon' => '<i class="zmdi zmdi-chart-donut mr-20"></i>'
                  ), */
                )
            );
        } else if ($accessLevel == 'Reseller') {
            $menuArray = array('menu' => array(
                    'Dashboard' => array(
                        'link' => Router::url(array('controller' => 'resellers', 'action' => 'dashboard'), true),
                        'icon' => '<i class="zmdi zmdi-landscape mr-20"></i>'
                    ),
                    'Personal Identity' => array(
                        'link' => Router::url(array('controller' => 'resellers', 'action' => 'update_profiles'), true),
                        'icon' => '<i class="zmdi zmdi-edit mr-20"></i>',
                    //'submenu' => array(
                    // Router::url(array('controller' => 'societys', 'action' => 'update_assign_society'), true) => 'Update Society',
                    //Router::url(array('controller' => 'society', 'action' => 'add_update_societies'), true) => 'Add/Edit/Delete Societies',
                    //Router::url(array('controller' => 'societys', 'action' => 'view_societys',AuthComponent::user('id')), true) => 'View Society',
                    //)
                    ),
                    'My Societies' => array(
                        'link' => Router::url(array('controller' => 'resellers', 'action' => 'assigned_societys'), true),
                        'icon' => '<i class="zmdi zmdi-google-pages mr-20"></i>'
                    )
            ));
        } else if ($accessLevel == 'Society') {
            $menuArray = array('menu' => array(
                    'Dashboard' => array(
                        'link' => Router::url(array('controller' => 'societys', 'action' => 'dashboard'), true),
                        'icon' => '<i class="zmdi zmdi-landscape mr-20"></i>'
                    ),
                    'Society' => array('icon' => '<i class="zmdi zmdi-city-alt mr-20"></i></i>', 'sublink' => Router::url(array('controller' => 'society', 'action' => 'dashboard'), true),
                        'submenu' => array(
                            Router::url(array('controller' => 'societys', 'action' => 'update_societys', AuthComponent::user('id')), true) => 'Society Identity',
                            Router::url(array('controller' => 'societys', 'action' => 'society_parameter'), true) => 'Society Parameters',
                            //Router::url(array('controller' => 'societys', 'action' => 'tarrif_defination'), true) => 'Tariff Defination',                            
                            Router::url(array('controller' => 'societys', 'action' => 'society_head_sub_categories'), true) => 'Society Head Sub Group',
                            Router::url(array('controller' => 'societys', 'action' => 'society_ledger_heads'), true) => 'Society Ledger Heads ',
                            Router::url(array('controller' => 'societys_members', 'action' => 'society_tariffs'), true) => 'Society Tariffs',
                            Router::url(array('controller' => 'societys', 'action' => 'society_tariff_orders'), true) => 'Society Tariff Order',
                            Router::url(array('controller' => 'societys', 'action' => 'society_payments'), true) => 'Society Payments',
                            Router::url(array('controller' => 'societys', 'action' => 'cheque_clear_date'), true) => 'Bank Reconciliation',
                            Router::url(array('controller' => 'societys', 'action' => 'cash_withdraws'), true) => 'Society Cash Contra',
                            Router::url(array('controller' => 'societys', 'action' => 'general_receipt'), true) => 'General Receipt'
                        )
                    ),
                    'Member' => array('icon' => '<i class="zmdi zmdi-account mr-20"></i></i>', 'sublink' => Router::url(array('controller' => 'society', 'action' => 'dashboard'), true),
                        'submenu' => array(
                            Router::url(array('controller' => 'societys', 'action' => 'building_identitys'), true) => 'Building Identity',
                            Router::url(array('controller' => 'societys', 'action' => 'wing_identitys'), true) => 'Wing Identity',
                            Router::url(array('controller' => 'societys', 'action' => 'member_identitys'), true) => 'Member Identity',
                            Router::url(array('controller' => 'societys_members', 'action' => 'member_tariffs'), true) => 'Member Tariff',
                            Router::url(array('controller' => 'societys', 'action' => 'tenant_member_identitys'), true) => 'Tenant Member Identity',
                            Router::url(array('controller' => 'societys_members', 'action' => 'member_payments'), true) => 'Member Payments',
                            Router::url(array('controller' => 'society_bills', 'action' => 'society_generated_bills'), true) => 'All Generated Bills'
                        )
                    ),
                    'Employee' => array('icon' => '<i class="zmdi zmdi-accounts-alt mr-20"></i></i>', 'sublink' => Router::url(array('controller' => 'society', 'action' => 'dashboard'), true),
                        'submenu' => array(
                            Router::url(array('controller' => 'societys_employees', 'action' => 'employee_categories'), true) => 'Employee Category',
                            Router::url(array('controller' => 'societys_employees', 'action' => 'employee_sub_categories'), true) => 'Employee Sub Category',
                            Router::url(array('controller' => 'societys_employees', 'action' => 'employee_details'), true) => 'Employee Details',
                        )
                    ),
                    'Reports' => array('icon' => '<i class="zmdi zmdi-account mr-20"></i></i>', 'sublink' => Router::url(array('controller' => 'societys', 'action' => 'dashboard'), true),
                        'submenu' => array(
                            Router::url(array('controller' => 'account_reports', 'action' => 'account'), true) => 'Accounts',
                            Router::url(array('controller' => 'reports', 'action' => 'society'), true) => 'Society',
                            Router::url(array('controller' => 'reports', 'action' => 'journal_vouchers'), true) => 'Journal Voucher'
                        )
                    ),					
                    /*'PrintBill' => array(
                        'link' => Router::url(array('controller' => 'societybills', 'action' => 'print_member_bills'), true),
                        'icon' => '<i class="fa fa-print mr-20"></i> '
                    )*/
            ));
        }
        return $menuArray;
    }
}

?>