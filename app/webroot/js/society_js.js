/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *Society releated all event 
 */

if(societyMemberPaymentID && societyMemberPaymentID !== ''){
    getSocietyMemberPaymentDetails(societyMemberPaymentID);
}


function getAllBuildingWings(buildingId,showWingDropDown) {
    if (buildingId) {
        showLoader();
        var postData ='building_id='+buildingId;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/get_all_buildingwings',
            data: postData,
            dataType: 'html',
            async: false,
            success: function (resp) {
                hideLoader();
                if (resp) {
                    $(showWingDropDown).html(resp);
                }
            }
        });
    }
    return false;
}

function selectMemberFlatDropdownByMemberFlatID(MemberID,dropDownId,flagDisplay){
    if (MemberID) {
        showLoader();
        var postData ='member_id='+MemberID;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/getMemberDetailsByMemberID',
            data: postData,
            dataType: 'json',
            async: false,
            success: function (resp) {
                hideLoader();
                if (resp) {
                    if(flagDisplay === 0){
                        $(dropDownId).val(resp[0].Member.flat_no);
                    }else if(flagDisplay === 1){
                        $(dropDownId).val(resp[0].Member.id);
                    }
                }
            }
        });
    }
    return false;
}

function getAccountHeads(accountId,accountHeadDropDownID){
    if (accountId) {
        showLoader();
        var postData ='accountId='+accountId;
        
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/get_account_heads',
            data: postData,
            dataType: 'html',
            async: false,
            success: function (resp) {
                hideLoader();
                if (resp) {
                    $(accountHeadDropDownID).html(resp);
                }
            }
        });
    }
    return false;
}

function getAccountCategory(headSubCategoryId) {
    if (headSubCategoryId) {
        showLoader();
        var postData = 'headSubCategoryId='+headSubCategoryId;
        
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/get_account_categories',
            data: postData,
            dataType: 'html',
            async: false,
            success: function (resp) {
                hideLoader();
                if (resp) {
                    getAccountHeadsBySubCategoryId(headSubCategoryId);
                    $('#account_category_id').html(resp);
                }
            }
        });
    }
    return false;
}

function societyEmployeeShortCode(shortCode) {
    if (shortCode != '') {
        var shortCodeArr = shortCode.split(" ");
        var code1 = shortCodeArr[0] ? shortCodeArr[0] : '';
        var code2 = shortCodeArr[1] ? shortCodeArr[1] : '';
        //alert(shortCodeArr.length);
        if (shortCodeArr.length > 1) {
            var code = code1.substr(0, 2) + '_' + code2.substr(0, 2);
        } else {
            var code = code1.substr(0, 3);
        }
        $('#emp_code').val(code.toUpperCase());
    }
}

function getAccountHeadsBySubCategoryId(headSubCategoryId){
    if (headSubCategoryId) {
        showLoader();
        var postData = 'headSubCategoryId='+headSubCategoryId;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/account_heads_by_categoryids',
            data: postData,
            dataType: 'html',
            async: false,
            success: function (resp) {
                hideLoader();
                if (resp) {
                    $('#account_head_id').html(resp);
                }
            }
        });
    }
    return false;
}

function getSocietyMemberPaymentDetails(societyMemberId){
    if (societyMemberId) {
        createDataTableMemberPayments(0,societyMemberId);
        getSocietyMembersPaymentsBalance(societyMemberId);
    }
    return false;
}

function getSocietyMembersPaymentsBalance(societyMemberId){
     if (societyMemberId) {
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/getMembersOutstandingPaymentsBalance/'+societyMemberId,
            dataType: 'json',
            async: false,
            success: function (paymentBalance) {
                if (paymentBalance.MemberBillSummary) {
                    $('#member_outstanding_payment').val(paymentBalance.MemberBillSummary.balance_amount);
                    $('#member_interest').val(paymentBalance.MemberBillSummary.interest_balance);
                    $('#member_principle').val(paymentBalance.MemberBillSummary.principal_balance);
                }
            }
        });
    }
    return false;
}

function loadMemberTarrifDetails(societyMemberId) {
    if (societyMemberId) {
        showLoader();
        $("input[type='text']").val(0);
        var postData = 'societyMemberId=' + societyMemberId;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/society_member_tariff_details',
            data: postData,
            dataType: 'json',
            async: false,
            success: function (tariff_array) {
                hideLoader();
                var totalAmt = 0;
                $('#t_member_tariff_details').empty();
                if (tariff_array.MemberTariffDetails) {
                    $('#effective_date').val(tariff_array.MemberTariffDetails.tariff_effective_since);
                    $('#tariff_remark').val(tariff_array.MemberTariffDetails.remark);
                    $('#member_tariff_flat_no').val(tariff_array.MemberTariffDetails.flat_no);
                }
                if (tariff_array.MemberTariff && tariff_array.MemberTariff.length > 0) {
                    $.each(tariff_array.MemberTariff, function (index, itemData) {
                        if (itemData.amount > 0) {
                            $('#ledger_head_id_' + itemData.ledger_head_id).val(itemData.amount);
                            totalAmt = totalAmt + parseInt(itemData.amount);
                            $('#t_member_tariff_details').append('<tr><td>' + itemData.title + '</td><td class="text-right">' + itemData.amount + '</td></tr>');
                        }
                    });
                    $('#t_member_tariff_details').append('<tr style="font-weight:bold;"><td>Total</td><td class="text-right">' + totalAmt + '</td></tr>');

                } else {
                    $("input[type='text']").val(0);
                    $('#member_tariff_flat_no').val(tariff_array.MemberTariffDetails.flat_no);
                }
                $('#total_member_tariff').val(totalAmt);
            }
        });
    }
    return false;
}


function loadBuildingTarrifDetails(buildingId) {
    if (buildingId) {
        showLoader();
        $("input[type='text']").val(0);
        var postData = 'buildingId=' + buildingId;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/society_building_tariff_details',
            data: postData,
            dataType: 'json',
            async: false,
            success: function (tariff_array) {
                hideLoader();
                var totalAmt = 0;
                var effectiveSince = '';
                $('#t_building_tariff_details').empty();
               
                if (tariff_array.BuildingTariff && tariff_array.BuildingTariff.length > 0) {
                    $.each(tariff_array.BuildingTariff, function (index, itemData) {
                        if (itemData.amount > 0) {
                            $('#ledger_head_id_' + itemData.ledger_head_id).val(itemData.amount);
                            totalAmt = totalAmt + parseInt(itemData.amount);
                            $('#t_building_tariff_details').append('<tr><td>' + itemData.title + '</td><td>' + itemData.amount + '</td></tr>');
                            
                            effectiveSince = itemData.effective_since;
                        }
                    });
                    
                    $('#effective_date').val(effectiveSince);
                    
                    $('#t_building_tariff_details').append('<tr style="font-weight:bold;"><td>Total</td><td>' + totalAmt + '</td></tr>');

                } else {
                    $("input[type='text']").val(0);
                }
                $('#total_building_tariff').val(totalAmt);
            }
        });
    }
    return false;
}


function loadWingTarrifDetails(wingId) {
    var buildingId = $('#building_id').val();
    
    if (buildingId && wingId) {
        showLoader();
        $("input[type='text']").val(0);
        var postData = 'buildingId=' + buildingId+'&wingId=' + wingId;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/society_wing_tariff_details',
            data: postData,
            dataType: 'json',
            async: false,
            success: function (tariff_array) {
                hideLoader();
                var totalAmt = 0;
                var effectiveSince = '';
                $('#t_wing_tariff_details').empty();
               
                if (tariff_array.WingTariff && tariff_array.WingTariff.length > 0) {
                    $.each(tariff_array.WingTariff, function (index, itemData) {
                        if (itemData.amount > 0) {
                            $('#ledger_head_id_' + itemData.ledger_head_id).val(itemData.amount);
                            totalAmt = totalAmt + parseInt(itemData.amount);
                            $('#t_wing_tariff_details').append('<tr><td>' + itemData.title + '</td><td>' + itemData.amount + '</td></tr>');
                            
                            effectiveSince = itemData.effective_since;
                        }
                    });
                    
                    $('#effective_date').val(effectiveSince);
                    
                    $('#t_wing_tariff_details').append('<tr style="font-weight:bold;"><td>Total</td><td>' + totalAmt + '</td></tr>');

                } else {
                    $("input[type='text']").val(0);
                }
                $('#total_wing_tariff').val(totalAmt);
            }
        });
    }
    return false;
}

function loadUnitTypeTarrifDetails(unitType) {
    if (unitType) {
        showLoader();
        $("input[type='text']").val(0);
        var postData = 'unitType=' + unitType;
        $.ajax({
            type: 'POST',
            url: webrootUrl+'societys_ajax/society_unit_type_tariff_details',
            data: postData,
            dataType: 'json',
            async: false,
            success: function (tariff_array) {
                hideLoader();
                var totalAmt = 0;
                var effectiveSince = '';
                $('#t_unit_type_tariff_details').empty();
               
                if (tariff_array.UnitTypeTariff && tariff_array.UnitTypeTariff.length > 0) {
                    $.each(tariff_array.UnitTypeTariff, function (index, itemData) {
                        if (itemData.amount > 0) {
                            $('#ledger_head_id_' + itemData.ledger_head_id).val(itemData.amount);
                            totalAmt = totalAmt + parseInt(itemData.amount);
                            $('#t_unit_type_tariff_details').append('<tr><td>' + itemData.title + '</td><td>' + itemData.amount + '</td></tr>');
                            
                            effectiveSince = itemData.effective_since;
                        }
                    });
                    
                    $('#effective_date').val(effectiveSince);
                    
                    $('#t_unit_type_tariff_details').append('<tr style="font-weight:bold;"><td>Total</td><td>' + totalAmt + '</td></tr>');

                } else {
                    $("input[type='text']").val(0);
                }
                $('#total_unit_type_tariff').val(totalAmt);
            }
        });
    }
    return false;
}

 function addUpdateSocietyLedgerHeads(){
    var validate = $('#societyAddLedgerHeadsForm').parsley().validate();
    if (validate) {
        showLoader();
        var postStr = $('#societyAddLedgerHeadsForm').serialize();
        $.ajax({
            type: "POST",
            url: webrootUrl+'societys_ajax/add_society_ledger_heads',
            data: postStr,
            dataType: 'json',
            success: function(resp) {
                hideLoader();
                showNotifyAlert(resp,'modal_ledger_notify_error');
                //setTimeout(function(){ $('#modal_ledger_notify_error').hide(); },5000);
            }
        });
    }
}

function updateSocietyMemberDetails(){
    var validate = $('#societyMemberDetailsForm').parsley().validate();
    if (validate) {
        showLoader();
        var postStr = $('#societyMemberDetailsForm').serialize();
        $.ajax({
            type: "POST",
            url: webrootUrl+'societys_ajax/updateSocietyMemberDetails',
            data: postStr,
            dataType: 'json',
            success: function(resp) {
                hideLoader();
                showNotifyAlert(resp,'modal_member_notify_error');
                //setTimeout(function(){ $('#modal_ledger_notify_error').hide(); },5000);
            }
        });
    }
}

if(typeof(societyTariffOrder) != "undefined" && societyTariffOrder !== null){
$(function() {
    $("#sortable").sortable({
    placeholder: "ui-state-highlight",
    //update: function( event, ui ) {
   // updateSocietyLedgerOrder();
    //}
    });
    $("#sortable").disableSelection();
  });
}


function updateSocietyLedgerOrder() {
    var societyLedgerArr = new Array();
    $('ul#sortable li').each(function() {
        //alert($(this).attr("id"));
        societyLedgerArr.push($(this).attr("id"));
    });

    var postStr = 'sort_order=' + societyLedgerArr;
    $.ajax({
        type: "POST",
        url: webrootUrl+'societys_ajax/updateSocietyLedgerOrder',
        data: postStr,
        dataType: 'json',
        cache: false,
        success: function(resp) {
            //hideLoader();
            showNotifyAlert(resp, 'modal_society_ledger_order');
            setTimeout(function() {
                $('#modal_society_ledger_order').hide();
            }, 5000);
        }
    });
}

    
    
    $('.memberTariffCount').blur(function () {
        var memberTariffBillTotal = 0;
        $('.memberTariffCount').each(function (index, element) {
            memberTariffBillTotal = memberTariffBillTotal + parseInt($(this).val());
            
        });
        $('#total_member_tariff').val(memberTariffBillTotal);
    });

    function hideChequeInputFields(paymentMode,dropdownValue){
    if (paymentMode != 3 && paymentMode != 2) {
        paymentMode = 'Cash';
        $('.cheque_hide').hide();
        $('#cheque_reference_number').val('');
        $('#credited_date').val('');
        $('#member-bank-details').val('');
        $('#member_bank_id').val('');
        $('#member_bank_ifsc').val('');
        $('#member_bank_branch').val('');
    } else {
        paymentMode = 'Bank';
        $('.cheque_hide').show();
    }
    changeDropDownByPaymentMode(paymentMode,dropdownValue);
}
    
   function hideSocietyPaymentChequeInputFields(paymentMode){
        if(paymentMode == 'Cash'){
            $('.so-check').hide();
            $('#cheque_reference_number').val('');
            $('#so_cheque_date').val('');
        }else{
            $('.so-check').show();
        }
        changeDropDownByPaymentMode(paymentMode,'bank_payment_by_ledger_id');
    }
    
    function changeDropDownByPaymentMode(paymentMode,dropDownValue){
        var postStr = 'paymentMode='+paymentMode;
        $.ajax({
            type: "POST",
            url: webrootUrl+'societys_ajax/getCashAndBankSocietyLedgerHeadDropDown',
            dataType: 'html',
            data: postStr,
            cache: false,
            success: function (resp) {
                  $('#'+dropDownValue).html(resp);
            }
        });
    }
    
    function editMemberPaymentDetails(societyMemberID){
        redirectToActionController(webrootUrl+'societys_members/add_member_payment/'+societyMemberID);
    }
    
    function deleteMemberPaymentDetails(societyMemberID){
        var validate = confirm('Are you sure you want to delete this particular record?');
        if (validate) {
            redirectToActionController(webrootUrl + 'societys_members/delete_member_payments/' + societyMemberID);
        }
    }

function createDataTableMemberPayments(destroyFlag,societyMemberId) {
    if (destroyFlag == 0) {
        $('#t_member_payment').dataTable().fnDestroy();
    }
    var url = webrootUrl+'societys_ajax/societyMemberPaymentDetails/'+societyMemberId;
    
    var dataTable = $('#t_member_payment').DataTable({
        "responsive": true,
        "processing": false, //loader
        "serverSide": true,
        "orderClasses": false,
        "bLengthChange": false, // page drop down
        "aoColumnDefs": [{className: "dt-right", "aTargets": [-1]}, {"bSortable": false, "aTargets": [-1]}],
        "order": [[0, 'asc']],
        "pageLength": 10, //per page records
        "ajax": {
            beforeSend: function () {
                showLoader();
            },
            url: url,
            type: "post",
            complete: function () {
                hideLoader();
            }
        }
    });
}

function uploadMemberIdentitysCSV() {
    $("#member_identitys_upload_csv").submit();
}

$(function() {
    $("#member_identitys_upload_csv").submit(function(e)
    {
        var validated = $("#member_identitys_upload_csv").parsley().validate();
        if (validated) {
            showLoader();
            $.ajax({
                method: 'POST',
                url: webrootUrl+'societys_members/uploadSocietyMemberListCsv',
                data: new FormData(this),
                contentType: false, // The content type used when sending data to the server.  
                cache: false, // To unable request pages to be cached  
                processData: false,
                dataType: 'json',
                success: function(resp) {
                    hideLoader();
                    if (resp.error != 1) {
                        showNotificationToastr('success',resp.error_message);
                        hideLoader();
                        redirectToActionController(webrootUrl+'societys/member_identitys');
                    } else {
                        showNotificationToastr('error',resp.error_message);
                    }
                }
            });
        }
        return false;
    });
});
function uploadMemberTariffIdentitysCSV() {
     //alert();
    $("#member_tariff_upload_csv").submit();
}

$(function() {
    $("#member_tariff_upload_csv").submit(function(e)
    {
        var validated = $("#member_tariff_upload_csv").parsley().validate();
        if (validated) {
            showLoader();
            $.ajax({
                method: 'POST',
                url: webrootUrl+'societys_members/uploadSocietyMemberTariffData',
                data: new FormData(this),
                contentType: false, // The content type used when sending data to the server.  
                cache: false, // To unable request pages to be cached  
                processData: false,
                dataType: 'json',
                success: function(resp) {
                    hideLoader();
                    if (resp.error != 1) {
                        showNotificationToastr('success',resp.error_message);
                        hideLoader();
                        redirectToActionController(webrootUrl+'societys_members/member_tariffs');
                    } else {
                        showNotificationToastr('error',resp.error_message);
                    }
                }
            });
        }
        return false;
    });
});


function downloadMemberTariffIdentitysCSV(){
    $("#member_tariff_download_csv_file").submit();
}

$(function() {
    $("#member_tariff_download_csv_file").submit(function(e)
    {
        var validated = $("#member_tariff_download_csv_file").parsley().validate();
        if (validated) {
            redirectToActionController(webrootUrl+'societys_members/downloadSocietyMemberTariffData');
        }
        return false;
    });
});

    function hideChequeDetailsFields(paymentMode){
        if(paymentMode != 'Bank'){
            $('.hide_content').hide();
            $('#cheque_reference_number').val('');
            $('#credited_date').val('');
        }else{
            $('.hide_content').show();
        }
        changeDropDownByPaymentMode(paymentMode,'society_payment_by_ledger');
    }
    
    function hidePaymentRegisterInputFields(payment_type){
        if(payment_type == 'Cash'){
            $('.so-check').hide();
            $('#payment_by_ledger_id').val('');
            $('#cheque_reference_number').val('');
            $('#cheque_date').val('');
        }else{
            $('.so-check').show();
        }
    }