<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Bank Slip</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form method="post" id="societyBankslipFrm" name="societyBankslipFrm" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="col-md-9 member-tariff-left-box"> 
                                    <div class="row">
                                        <div class="col-md-8">    
                                            <div class="society-report-form-box-label">Bank Name &nbsp;&nbsp;</div>                                                                     
                                            <select class="society-report-form-box-field-dropdown" id="" name="data[][]">
                                                <option value="">Select Month</option>
                                            </select> 
                                        </div>                                                    
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-12">                       
                                            <div class="col-md-4 nopadding">
                                                <div class="society-report-form-field-box">
                                                    <div class="society-report-form-box-label">From Date &nbsp;&nbsp;&nbsp;</div>                                   
                                                    <input type="date" class="society-report-form-box-field-date-70" id="" name="data[][]" value="">
                                                </div>
                                            </div>  
                                            <div class="col-md-4 nopadding">
                                                <div class="society-report-form-field-box">
                                                    <div class="society-report-form-box-label">To &nbsp;&nbsp;</div>                                    
                                                    <input type="date" class="society-report-form-box-field-70" id="" name="data[][]" value="">
                                                </div>
                                            </div> 
                                            <div class="clearfix"></div>   
                                            <div class="col-md-4 nopadding">
                                                <div class="society-report-form-field-box">
                                                    <div class="society-report-form-box-label">Slip No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>                                   
                                                    <input type="text" class="society-report-form-box-field-70" id="" name="data[][]" value="">
                                                </div>
                                            </div>  
                                            <div class="col-md-4 nopadding">
                                                <div class="society-report-form-field-box">
                                                    <div class="society-report-form-box-label">To &nbsp;&nbsp;</div>                                    
                                                    <input type="text" class="society-report-form-box-field-70" id="" name="data[][]" value="">
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">  
                                            <div class="form-group society-report-form">
                                                <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btns"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>                                        
                                                <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btns" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a>
                                                <a type="button" data-toggle="tooltip" data-original-title="Print Report" class="btn btn-success btn-anim btns" onclick="printAllReports('print_society_bank_slip');"><i class="fa fa-print"></i><span class="btn-text">Print</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3 right-section">
                                    <div class="row">   
                                        <div class="right-title">Select if required </div> 
                                        <div class="col-md-12">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">Building &nbsp;</div>                                    
                                                <select name="data[MemberBillSummary][building_id]" id="wingBuildingName" class="society-report-form-box-field-dropdown" onchange="getAllBuildingWings(this.value, '#society_bill_report');">
                                                    <option value="">Select building</option>
                                                    <?php if(isset($societyBuildingLists) && count($societyBuildingLists) > 0) {
                                                    foreach($societyBuildingLists as $buildingID => $buildingName){?>
                                                    <?php if(isset($this->request->data['MemberBillSummary']['building_id']) && $this->request->data['MemberBillSummary']['building_id'] == $buildingID){?>
                                                    <option value="<?php echo $buildingID; ?>" selected><?php echo $buildingName; ?></option>
                                                    <?php }else {?>
                                                    <option value="<?php echo $buildingID; ?>"><?php echo $buildingName; ?></option>
                                                    <?php }?>
                                                    <?php } }?>
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="col-md-12">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">Wing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>                                    
                                                <select class="society-report-form-box-field-dropdown" id="society_bill_report" name="data[MemberBillSummary][wing_id]">
                                                    <option value="">Select Wing</option>  
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">Unit No &nbsp;</div>                                   
                                                <input type="text" class="society-report-form-box-field-50" id="" name="data[MemberBillSummary][flat_no]" value="">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">To &nbsp;&nbsp;</div>                                    
                                                <input type="text" class="society-report-form-box-field-50" id="" name="data[MemberBillSummary][flat_no_to]" value="">
                                            </div>
                                        </div> 
                                        <div class="clearfix"></div><br>
                                    </div>    
                                </div> 
                            </form>
                            <div class="clearfix"></div><br>
                            <div id="print_society_bank_slip">
                                <div class="print-society-bank-slip">
                                    <div class="row1">
                                        <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>

                                        <div class="report-bill">Bank Slip</div>
                                        <div style="border-bottom:1px solid #ccc;margin: 10px;"></div>
                                        <div class="col-md-9 col-xs-9">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Bank Name : <strong><?php echo 'Bank of India'; ?></strong></div>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Branch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong><?php ?></strong></div>
                                                </div> 
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">A/c No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong><?php ?></strong></div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-3">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Slip No &nbsp;&nbsp;: &nbsp;&nbsp; <?php ?></div>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; <?php echo '11/04/2018'; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="report-bill-outer-section">
                                            <div class="table-wrap1">
                                                <table class="table table-bordered padding-td-none padding-th-none" style="margin-top: -22px;">
                                                    <thead>
                                                        <tr>
                                                            <th width="8%" class="text-center">Sr.</th>
                                                            <th width="8%" class="text-center">Unit No</th>
                                                            <th width="8%" class="text-center">Voucher No</th>
                                                            <th width="8%" class="text-center">Cheque No</th>
                                                            <th width="8%" class="text-center">Cheque Date</th>
                                                            <th width="8%" class="text-center">Bank Slip</th>
                                                            <th class="text-center">Drawee Bank</th>
                                                            <th class="text-center">Branch</th>
                                                            <th width="10%" class="text-center">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody> 
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">A-302</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-center">895645</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>Axis Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">10000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">A-304</td>
                                                            <td class="text-center">2</td>
                                                            <td class="text-center">895688</td>
                                                            <td class="text-center">12/04/2018</td>
                                                            <td class="text-center">0</td>
                                                            <td>ICICI Bank</td>
                                                            <td>Kamothe</td>
                                                            <td class="text-right">12000.00</td>
                                                        </tr>
                                                    </tbody>  
                                                    <br><br>
                                                </table> 
                                            </div>
                                        </div>  
                                    </div>    
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>