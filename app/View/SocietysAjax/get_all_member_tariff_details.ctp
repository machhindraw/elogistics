<table id="datable_4" class="dataTable table table-hover table-bordered padding-th-none padding-td-none">
    <thead>
        <tr>
            <?php if(isset($memberTariffHeader) && !empty($memberTariffHeader)){
                foreach($memberTariffHeader as $headerValue){ ?>
                <th><?php echo  $headerValue; ?></th>
            <?php  } }?>
        </tr>
    </thead>
    <tbody id="member_tarrif_data">
        <?php 
            if (isset($memberTariffDataArra) && count($memberTariffDataArra) > 0) :
            $s = 1;
            $totalTarrifAmount = 0;
            foreach ($memberTariffDataArra as $MemberID => $mTariffData) {
        ?>
        <tr>
            <td><?php echo  $s; ?></td>
            <td><?php echo  $mTariffData['member_name']; ?></td>
            <td><?php echo  $mTariffData['flat_no']; ?></td>
                    <?php foreach($mTariffData['ledgerData'] as $ledgerHeadId =>$fData){
                        $ledgerAmount = isset($fData[$ledgerHeadId]) ? $fData[$ledgerHeadId] : 0;
                        $totalTarrifAmount += $ledgerAmount;
                        echo "<td><input type=\"text\" id=\"member_tariff_$MemberID$ledgerHeadId\" name=\"data[MemberTariff][$MemberID][$ledgerHeadId][$ledgerHeadId]\" class=\"memberTariffCount text-right\" value=\"$ledgerAmount\"></td>";
                    }?>
            <td><input type="text" id="ledger_head_id_<?php echo $MemberID;?>" name="" class="memberTariffCount text-right" value="<?php echo $totalTarrifAmount; ?>" style=""></td>
        </tr>                                                       
            <?php $s++; $totalTarrifAmount = 0;}endif;?>
    </tbody>
</table>