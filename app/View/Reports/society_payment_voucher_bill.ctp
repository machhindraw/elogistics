<div class="container-fluid">       
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark"></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                             <form action="" name="" id="" method="">
                                <div class="row">
                                    <h5 class="text-center">AMBIKA SHRUSTI C.H.S. LTD</h5>
                                    <div class="report-address-heading">Registration No. N.B.O/CIDCO/HHG(OH)/2858/JTR/YEAR 2008-2009 Dated: 19/01/2009</div>
                                    <div class="report-address-heading">PLOT NO.92 SECTOR-44 KARVE SEAWOODS NERUL NAVI MUMBAI 4000706</div>
                                </div>
                                <br>
                                <div class="row1">
                                    <div class="report-bill">Bank Payment Voucher</div>
                                    <div class="report-bill-outer-section">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="">Cheque No : 101</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="">Paid to : Sandesh Y Ghorpade</div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="">No : 136</div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="">Date : 11/01/2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div style="padding: 0px;">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table id="datable_1" class="table padding-td-none padding-th-none table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Particular</th>
                                                                    <th class="text-center">Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr id="">
                                                                    <td>Being New CCTV DVR Purchase</td>
                                                                    <td rowspan="2" class="text-right">1520.00</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td>CCTV Expenses <span style="float:right;">A/c... &nbsp;&nbsp;&nbsp;</span></td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td>(Rupees  Ten Thousand Five Hundred Only)<span style="float:right;">Total &nbsp;&nbsp;&nbsp;</span></td>
                                                                    <td class="text-right">100.00</td>
                                                                </tr>
                                                            </tbody>
                                                        </table> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                Paid by :
                                            </div> 
                                        </div> 
                                        <br><br><br>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Prepared By
                                            </div> 
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Authorised By
                                            </div> 
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Receiver's Signature
                                            </div> 
                                        </div>
                                    </div>
                                </div>                 
                            </form>
                            <hr>
                            <form action="" name="" id="" method="">
                                <div class="row1">
                                    <h5 class="text-center">AMBIKA SHRUSTI C.H.S. LTD</h5>
                                    <div class="report-address-heading">Registration No. N.B.O/CIDCO/HHG(OH)/2858/JTR/YEAR 2008-2009 Dated: 19/01/2009</div>
                                    <div class="report-address-heading">PLOT NO.92 SECTOR-44 KARVE SEAWOODS NERUL NAVI MUMBAI 4000706</div>
                                </div>
                                <br>
                                <div class="row1">
                                    <div class="report-bill">Cash Payment Voucher</div>
                                    <div class="report-bill-outer-section">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="">Cheque No : 101</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="">Paid to : Sandesh Y Ghorpade</div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="">No : 136</div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="">Date : 11/01/2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div style="padding: 0px;">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table id="datable_1" class="table padding-td-none padding-th-none table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Particular</th>
                                                                    <th class="text-center">Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr id="">
                                                                    <td>Being New CCTV DVR Purchase</td>
                                                                    <td rowspan="2" class="text-right">1520.00</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td>CCTV Expenses <span style="float:right;">A/c... &nbsp;&nbsp;&nbsp;</span></td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td>(Rupees  Ten Thousand Five Hundred Only)<span style="float:right;">Total &nbsp;&nbsp;&nbsp;</span></td>
                                                                    <td class="text-right">100.00</td>
                                                                </tr>
                                                            </tbody>
                                                        </table> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                Paid by :
                                            </div> 
                                        </div> 
                                        <br><br><br>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Prepared By
                                            </div> 
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Authorised By
                                            </div> 
                                            <div class="col-md-4 col-sm-4 text-center">
                                                Receiver's Signature
                                            </div> 
                                        </div>
                                    </div>
                                </div>                 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
