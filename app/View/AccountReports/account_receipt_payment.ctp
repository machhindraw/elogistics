<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Receipt & Payment</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form class="" method="post" id="societyReportBillFrm" name="societyReportBillFrm" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="col-md-12 member-tariff-left-box"> 
                                    <div class="row">
                                        <div class="col-md-4">    
                                            <div class="account-report-form-field-box"> 
                                                <div class="account-report-form-box-label">From date &nbsp;&nbsp;</div>                                                                     
                                                <input type="date" class="account-report-form-box-field-date" id="" name="data[MemberPayment][payment_date]" value="<?php echo isset($postData['MemberPayment']['payment_date']) ? $postData['MemberPayment']['payment_date'] :'' ; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">    
                                            <div class="account-report-form-field-box"> 
                                                <div class="account-report-form-box-label">To &nbsp;&nbsp;</div>                                                                     
                                                <input type="date" class="account-report-form-box-field-date-75" id="" name="data[MemberPayment][payment_date_to]" value="<?php echo isset($postData['MemberPayment']['payment_date_to']) ? $postData['MemberPayment']['payment_date_to'] :'' ; ?>">
                                            </div>  
                                        </div>  
                                        <div class="col-md-5">    
                                            <div class="form-group society-report-form">
                                                <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btns"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>                                        
                                                <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btns" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a>
                                                <button type="button" class="btn btn-success btn-anim btns" onclick="printAllReports('print_receipt_payment');"><i class="fa fa-print"></i><span> Print</span></button>
                                                <button type="button" class="btn btn-success btn-anim btns" onclick=""><span> Export to Excel</span></button>
                                            </div>
                                        </div>  
                                    </div> 
                                    <br>
                                </div>
                            </form> 
                            <div class="clearfix"></div><br><br>
                            <div id="print_receipt_payment">
                                <div class="print-receipt-payment">
                                    <div class="row">
                                        <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>
                                    </div>
                                    <br>
                                    <div class="report-bill">Receipt & Payment</div>
                                    <div class="report-bill"></div>  
                                    <div class="report-bill-outer-section">
                                        <div class="table-wrap1">
                                            <table id="" class="table table-bordered padding-td-none padding-th-none">
                                                <thead>
                                                    <tr>
                                                        <?php if (isset($recieptPaymentHeader) && count($recieptPaymentHeader) > 0) {
                                                            foreach($recieptPaymentHeader as $headerValue){
                                                                echo "<th>$headerValue</th>";
                                                            }
                                                        }?>                  
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (isset($recieptPaymentDetails) && count($recieptPaymentDetails) > 0) {
                                                        
                                                        foreach($recieptPaymentDetails as $paymentRecieptData){
                                                            echo "<tr>";
                                                            $Counter = 1;
                                                            foreach($paymentRecieptData as $value){
                                                                if (is_numeric($value)) {
                                                                    if($Counter > 1){
                                                                        echo "<td>".$utilObj->CreditDebitAmountCheck($value)."</td>";
                                                                    }else{
                                                                        echo "<td>$value</td>";
                                                                    }
                                                  
                                                                }  else {
                                                                    echo "<td>$value</td>";
                                                                }
                                                              $Counter++;   
                                                            }
                                                            echo "</tr>";
                                                        }
                                                    }?> 
                                                </tbody>   
                                            </table> 
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <br>
                            <center>

                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

