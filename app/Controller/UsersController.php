<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */




class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler');
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->autoRedirect = false;
        $this->Auth->allow('login','forgot_password','change_password','home');
    }

    public function login() {
        //$this->autoRender = false;
        $this->Session->write('Auth.reseller_Flag', 0);
        if ($this->Session->read('Auth.User.role') == 'Admin') {
            $this->redirect(array('controller' => 'admin', 'action' => 'dashboard'));
        } else if ($this->Session->read('Auth.User.role') == 'Society') {
            $this->redirect(array('controller' => 'societys', 'action' => 'dashboard'));
        } else if ($this->Session->read('Auth.User.role') == 'Reseller') {
            $this->redirect(array('controller' => 'resellers', 'action' => 'dashboard'));
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $loginInfo = array('user_id' => $this->Auth->user('id'), 'ipaddress' => $this->request->clientIp(), 'time' => date('Y-m-d H:i:s'));
                $this->User->UserLogin->save($loginInfo);
                //echo $this->Auth->User('role');die;
                $this->Session->setFlash(__('Welcome, ' . $this->Auth->user('username')), 'info');
                if ($this->Auth->User('role') == "Admin") {
                    $this->redirect($this->Auth->redirectUrl(array('controller' => 'admin', 'action' => 'dashboard')));
                } else if ($this->Auth->User('role') == "Society") {
                    $this->redirect($this->Auth->redirectUrl(array('controller' => 'societys', 'action' => 'dashboard')));
                } else if ($this->Auth->User('role') == "Reseller") {
                    $this->redirect(array('controller' => 'resellers', 'action' => 'dashboard'));
                } else {
                    $this->redirect($this->Auth->redirectUrl());
                }
            } else {
                $this->Session->setFlash(__('Invalid username or password'), 'error');
                $this->redirect(array("controller" => "users", "action" => "home"));
                $this->set('error', 'Invalid username or password');
            }
        }
        $this->redirect(array("controller" => "users", "action" => "home"));
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }
    
    public function home() {
        $this->layout = 'home';
    }
    
    public function aboutus() {
        $this->layout = 'aboutus';
    }
    
    public function setMenu(){
        $selectedID = '';
        if($this->RequestHandler->isAjax()){
            $this->autoRender = false;
            $selectedID = $this->request->query('selected');
            $this->Session->write('Auth.User.selectedMenu','#menu-'.$selectedID);
        }
        die;
    }
    
    function getMenu() {
        $this->layout = 'ajax';

        if($this->request->is('ajax')) {
            $selectedMenu = $this->Session->read('Auth.User.selectedMenu');

            if(empty($selectedMenu)) {
                $selectedMenu = '#menu-1';
            }
            echo json_encode($selectedMenu);
        }
        die;
    }
    
    public function auto_login() {
        $this->autoRender = false;
        $this->User->id = AuthComponent::user('id');
        $this->Session->write('Auth.reseller_username', AuthComponent::user('username'));
        $this->Session->write('Auth.reseller_password', $this->User->field('password'));
        $this->Session->write('Auth.reseller_Flag', 1);
        if ($this->Auth->login()) {
            $this->Session->delete('Auth.User');
        }
        if ($this->request->is('post')) {
            if (isset($this->request->data['societyAutoLogin']['loginFlag'])) {
                if ($this->request->data['societyAutoLogin']['loginFlag'] == 1) {
                    $this->Session->write('Auth.reseller_Flag', 0);
                }
            }
            if (isset($this->request->data['User']['username']) && !empty($this->request->data['User']['username'])) {
                $userLoginInfo = $this->User->identifyUser($this->request->data);
                if ($this->Auth->login($userLoginInfo['User'])) {
                    if ($this->Auth->User('role') == "Admin") {
                        $this->redirect($this->Auth->redirectUrl(array('controller' => 'admin', 'action' => 'dashboard')));
                    } else if ($this->Auth->User('role') == 'Society') {
                        $this->redirect($this->Auth->redirectUrl(array('controller' => 'societys', 'action' => 'dashboard')));
                    } else if ($this->Auth->User('role') == 'Reseller') {
                        $this->redirect(array('controller' => 'resellers', 'action' => 'assigned_societys'));
                    } else {
                        $this->redirect($this->Auth->redirectUrl());
                    }
                }
            }
        }
    }
}
