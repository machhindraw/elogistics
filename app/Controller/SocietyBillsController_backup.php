<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SocietyBillsController extends AppController {

    public $components = array('Session', 'RequestHandler', 'Util', 'SocietyBill');
    public $helpers = array('Js');
    public $societyloginID = '';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
        $this->set("utilObj", $this->Util);
        $this->set("societyObj", $this->SocietyBill);
        $this->loadModel('Member');
        $this->loadModel('MemberBillGenerate');
        $this->loadModel('Society');
        $this->loadModel('MemberBillSummary');
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');
        $this->societyloginID = $this->Session->read('Auth.User.id');
        $this->loadAllNavigationModals();
    }

    function loadAllNavigationModals(){
        $this->SocietyBill->getSocietyBuildingLists();
        $this->SocietyBill->getSocietyLedgerHeadsDetails();
        $this->SocietyBill->navigateSocietyMemberDetails();
        $this->SocietyBill->loadAllNavigationModalsDropdowns();
    }

    public function memberBillGenerate() {
        $this->layout = false;
        $response = array();
        $response['error'] = 1;
        $response['error_message'] = 'Member Tariff has not set.';
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $this->Member->Behaviors->attach('Containable');
            if (isset($this->request->data['MemberBillGenerate']['bill_generated_date']) && !empty($this->request->data['MemberBillGenerate']['bill_generated_date'])) {
                $societyMemberLists = $this->Member->find('all', array('fields' => array('Member.id', 'Member.building_id', 'Member.wing_id', 'Member.unit_type', 'Member.area', 'Member.flat_no', 'Member.op_principal', 'Member.op_interest', 'Member.op_bill_due_date', 'Member.op_bill_date'), 'conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'recursive' => -1));
                $billLedgerHeadSettings = $this->memberRegularBillLedgerHeadSettings(); //check if tax is applicable
                //print_r($billLedgerHeadSettings);die;
                //Based on society Parameters get tariffs for member
                $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('conditions'=>array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id'))));
                
                if (count($societyMemberLists) > 0) {
                    foreach ($societyMemberLists as $details) {
                        $memberDetails = $details['Member'];
                        $memberId = $memberDetails['id'];
                        $memberTariffInfo = $this->getMemberTariffFromDiffrentTariffTypes($societyTariffParameterCheck, $memberDetails);
                        $billNo = $this->SocietyBill->memberBillsUniqueNumber();
                        if (isset($memberTariffInfo['MemberTariff']) && !empty($memberTariffInfo['MemberTariff'])) {
                            $responseArray = $this->calculateMembersRegularBillByTariff($memberId, $memberTariffInfo, $billNo, $this->request->data, $billLedgerHeadSettings, $memberDetails);
                            if (!empty($responseArray)) {
                                $response = $responseArray;
                            }
                        }
                    }
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function getMemberTariffFromDiffrentTariffTypes($societyTariffParameterCheck, $memberDetails) {
        switch ($societyTariffParameterCheck['SocietyParameter']['tariff_id']) {
            case Configure::read('TariffType.Building') :
                $this->loadModel('SocietyTariffBuildings');
            case Configure::read('TariffType.Wing') :
                $this->loadModel('SocietyTariffWings');
            case Configure::read('TariffType.UnitType') :
                $this->loadModel('SocietyTariffUnitTypes');
            case Configure::read('TariffType.TotalArea') :
                $this->loadModel('SocietyTariffTotalAreas');
            case Configure::read('TariffType.PerUnitArea') :
                $this->loadModel('SocietyTariffPerAreas');
                break;
        }
        $memberId = $memberDetails['id'];
        $memberTariffInfo = array();
        $memberTariffInfo['Member']['flat_no'] = $memberDetails['flat_no'];
        if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.Building')) {
            //Building
            $buildingTariffInfo = $this->SocietyTariffBuildings->find('all', array('conditions' => array('SocietyTariffBuildings.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffBuildings.building_id' => $memberDetails['building_id'])));
            if (isset($buildingTariffInfo) && !empty($buildingTariffInfo)) {
                foreach ($buildingTariffInfo as $tariffDetails) {
                    $memberTariffInfo['MemberTariff'][] = array('society_id' => $tariffDetails['SocietyTariffBuildings']['society_id'],
                        'member_id' => $memberId,
                        'ledger_head_id' => $tariffDetails['SocietyTariffBuildings']['ledger_head_id'],
                        'amount' => $tariffDetails['SocietyTariffBuildings']['amount'],
                        'tariff_serial' => $tariffDetails['SocietyTariffBuildings']['tariff_serial'],);
                }
            }
        } else if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.Wing')) {
            //Wing
            $wingTariffInfo = $this->SocietyTariffWings->find('all', array('conditions' => array('SocietyTariffWings.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffWings.building_id' => $memberDetails['building_id'], 'SocietyTariffWings.wing_id' => $memberDetails['wing_id'])));
            if (isset($wingTariffInfo) && !empty($wingTariffInfo)) {
                foreach ($wingTariffInfo as $tariffDetails) {
                    $memberTariffInfo['MemberTariff'][] = array('society_id' => $tariffDetails['SocietyTariffWings']['society_id'],
                        'member_id' => $memberId,
                        'ledger_head_id' => $tariffDetails['SocietyTariffWings']['ledger_head_id'],
                        'amount' => $tariffDetails['SocietyTariffWings']['amount'],
                        'tariff_serial' => $tariffDetails['SocietyTariffWings']['tariff_serial'],);
                }
            }
        } else if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.UnitNo') || $societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.PerPerson')) {
            //Per Person, Unit No
            $this->Member->Behaviors->attach('Containable');
            $memberTariffInfo = $this->Member->find('first', array('conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.id' => $memberDetails['id']), 'contain' => array('MemberTariff' => array('conditions' => array('MemberTariff.amount >' => 0)))));
        } else if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.UnitType')) {
            //Unit Type
            $unitType = '';
            if ($memberDetails['unit_type'] == 'Residencial' || $memberDetails['unit_type'] == 'R' || $memberDetails['unit_type'] == 1) {
                $unitType = 'R';
            } else if ($memberDetails['unit_type'] == 'Commercial' || $memberDetails['unit_type'] == 'C' || $memberDetails['unit_type'] == 2) {
                $unitType = 'C';
            } else if ($memberDetails['unit_type'] == 'Both' || $memberDetails['unit_type'] == 'B' || $memberDetails['unit_type'] == 3) {
                $unitType = 'B';
            }
            $unitTypeTariffInfo = $this->SocietyTariffUnitTypes->find('all', array('conditions' => array('SocietyTariffUnitTypes.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffUnitTypes.unit_type' => "$unitType")));
            if (!empty($unitTypeTariffInfo)) {
                foreach ($unitTypeTariffInfo as $tariffDetails) {
                    $memberTariffInfo['MemberTariff'][] = array('society_id' => $tariffDetails['SocietyTariffUnitTypes']['society_id'],
                        'member_id' => $memberId,
                        'ledger_head_id' => $tariffDetails['SocietyTariffUnitTypes']['ledger_head_id'],
                        'amount' => $tariffDetails['SocietyTariffUnitTypes']['amount'],
                        'tariff_serial' => $tariffDetails['SocietyTariffUnitTypes']['tariff_serial'],);
                }
            }
        } else if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.TotalArea')) {
            //Total Area
            $totalAreaTariffInfo = $this->SocietyTariffTotalAreas->find('all', array('conditions' => array('SocietyTariffTotalAreas.society_id' => $this->Session->read('Auth.User.id'))));
            if (!empty($totalAreaTariffInfo)) {
                foreach ($totalAreaTariffInfo as $tariffDetails) {
                    $memberTariffInfo['MemberTariff'][] = array('society_id' => $tariffDetails['SocietyTariffTotalAreas']['society_id'],
                        'member_id' => $memberId,
                        'ledger_head_id' => $tariffDetails['SocietyTariffTotalAreas']['ledger_head_id'],
                        'amount' => $tariffDetails['SocietyTariffTotalAreas']['amount'],
                        'tariff_serial' => $tariffDetails['SocietyTariffTotalAreas']['tariff_serial'],);
                }
            }
        } else if ($societyTariffParameterCheck['SocietyParameter']['tariff_id'] == Configure::read('TariffType.PerUnitArea')) {
            //Per Area
            $totalAreaTariffInfo = $this->SocietyTariffPerAreas->find('all', array('conditions' => array('SocietyTariffPerAreas.society_id' => $this->Session->read('Auth.User.id'))));
            if (!empty($totalAreaTariffInfo)) {
                foreach ($totalAreaTariffInfo as $tariffDetails) {
                    $memberTariffInfo['MemberTariff'][] = array('society_id' => $tariffDetails['SocietyTariffPerAreas']['society_id'],
                        'member_id' => $memberId,
                        'ledger_head_id' => $tariffDetails['SocietyTariffPerAreas']['ledger_head_id'],
                        'amount' => ($tariffDetails['SocietyTariffPerAreas']['amount'] * $memberDetails['area']),
                        'tariff_serial' => $tariffDetails['SocietyTariffPerAreas']['tariff_serial'],);
                }
            }
        }
        return $memberTariffInfo;
    }

    public function societyMemberBillExists($societyMemberId, $billGeneratedMonths, $billType = '') {
        if ($societyMemberId) {
            $societyMemberBillExists = $this->MemberBillGenerate->find('first', array('conditions' => array('MemberBillGenerate.member_id' => $societyMemberId, 'MemberBillGenerate.month' => $billGeneratedMonths, 'MemberBillGenerate.bill_type' => $billType)));
            if ($societyMemberBillExists) {
                return $societyMemberBillExists;
            }
            return array();
        }
        return false;
    }

    public function calculateMembersRegularBillByTariff($memberId, $memberTariffInfo, $paymentBillNo, $postData, $billLedgerHeadSettings, $memberDetails) {
        $tariffCounter = 0;
        $billGenerateArr = array();
        $responseError = array();
        $billType = Configure::read('BillType.Regular');
        $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('conditions' => array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id'))));
        //$societyTariffParemeterSet = isset($societyTariffParameterCheck['SocietyParameter']['tariff_id']) ? $societyTariffParameterCheck['SocietyParameter']['tariff_id'] : 0;
        $multiplyTariffAmountValue = $this->SocietyBill->multiplyTariffAmountByBillingFrequency();
        
        $billGeneratedMonths = (isset($postData['MemberBillGenerate']['month']) && !empty($postData['MemberBillGenerate']['month'])) ? $postData['MemberBillGenerate']['month'] : date('m');
        $societyMemberBillExists = $this->societyMemberBillExists($memberId, $billGeneratedMonths, $billType); //check same bill allready exists in system 
        if (empty($societyMemberBillExists)) {
            //$this->MemberBillGenerate->deleteAll(array('MemberBillGenerate.member_id' => $memberId, 'MemberBillGenerate.month' => $billGeneratedMonths,'MemberBillGenerate.bill_type'=>$billType));
            if ($this->MemberBillSummary->hasAny(array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.month' => $billGeneratedMonths, 'MemberBillSummary.bill_type' => $billType))) {
                //$this->MemberBillSummary->deleteAll(array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.month' => $billGeneratedMonths,'MemberBillSummary.bill_type'=>$billType));
            }
            //Check if last months bill details available
            $lastMonthBillDetails = $this->MemberBillSummary->find('first', array('conditions' => array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType), 'order' => array('MemberBillSummary.id' => 'desc')));

            if (isset($lastMonthBillDetails) && !empty($lastMonthBillDetails)) {
                $lastMonthBillDetails = $lastMonthBillDetails['MemberBillSummary'];
                $memberBillSummaryData['op_principal_arrears'] = $lastMonthBillDetails['principal_balance'];
                $memberBillSummaryData['op_interest_arrears'] = $lastMonthBillDetails['interest_balance'];
                $memberBillSummaryData['op_due_amount'] = $lastMonthBillDetails['balance_amount'];
                $memberBillSummaryData['tax_total'] = $lastMonthBillDetails['tax_total'];
                $memberBillSummaryData['op_tax_arrears'] = $lastMonthBillDetails['op_tax_arrears'] + $lastMonthBillDetails['tax_total'];
                $memberBillSummaryData['tax_balance'] = $lastMonthBillDetails['tax_balance'];
                
                $memberBillSummaryData['interest_on_due_amount'] = 0;
                if($memberBillSummaryData['op_due_amount'] > 0){
                    $memberBillSummaryData['interest_on_due_amount'] = $this->getInterestOnDueAmount($societyTariffParameterCheck, $lastMonthBillDetails, $postData);
                }
               } else {
                if (isset($memberDetails) && !empty($memberDetails)) {
                    $lastMonthBillDetails['principal_balance'] = isset($memberDetails['op_principal']) ? $memberDetails['op_principal'] : 0.00;
                    $lastMonthBillDetails['balance_amount'] = isset($memberDetails['op_principal']) ? $memberDetails['op_principal'] : 0.00;
                    $lastMonthBillDetails['bill_due_date'] = isset($memberDetails['op_bill_due_date']) ? $memberDetails['op_bill_due_date'] : '';
                    $lastMonthBillDetails['bill_generated_date'] = isset($memberDetails['op_bill_date']) ? $memberDetails['op_bill_date'] : '';
                    $lastMonthBillDetails['interest_free_amount'] = 0;
                    $memberBillSummaryData['op_principal_arrears'] = isset($memberDetails['op_principal']) ? $memberDetails['op_principal'] : 0.00;
                    $memberBillSummaryData['op_interest_arrears'] = isset($memberDetails['op_interest']) ? $memberDetails['op_interest'] : 0.00;
                    $memberBillSummaryData['op_tax_arrears'] = isset($memberDetails['op_tax']) ? $memberBillSummaryData['op_tax'] : 0.00;
                    $memberBillSummaryData['op_due_amount'] = $memberDetails['op_principal'] + $memberDetails['op_interest'] + $memberDetails['op_tax_arrears'];
                    $memberBillSummaryData['interest_on_due_amount'] = 0;
                    if (isset($lastMonthBillDetails['bill_due_date']) && $lastMonthBillDetails['bill_due_date'] != '0000-00-00' && $memberBillSummaryData['op_due_amount'] > 0) {
                        $memberBillSummaryData['interest_on_due_amount'] = $this->getInterestOnDueAmount($societyTariffParameterCheck, $lastMonthBillDetails, $postData);
                    }
                    $memberBillSummaryData['tax_balance'] = 0;
                    $memberBillSummaryData['op_tax_arrears'] = 0;
                    $memberBillSummaryData['tax_total'] = 0;
                } else {
                    $memberBillSummaryData['op_principal_arrears'] = 0;
                    $memberBillSummaryData['op_interest_arrears'] = 0;
                    $memberBillSummaryData['op_due_amount'] = 0;
                    $memberBillSummaryData['interest_on_due_amount'] = 0;
                    $memberBillSummaryData['op_tax_arrears'] = 0;
                    $memberBillSummaryData['tax_balance'] = 0;
                    $memberBillSummaryData['tax_total'] = 0;
                }
            }

            $memberBillSummaryData['bill_no'] = $paymentBillNo;
            $memberBillSummaryData['month'] = $billGeneratedMonths;
            $memberBillSummaryData['member_id'] = $memberId;
            $memberBillSummaryData['bill_type'] = $billType;
            $memberBillSummaryData['society_id'] = $this->societyloginID;
            $memberBillSummaryData['flat_no'] = isset($memberTariffInfo['Member']['flat_no']) ? $memberTariffInfo['Member']['flat_no'] : '';
            $memberBillSummaryData['monthly_amount'] = 0; //monthly member maintenance value
            $memberBillSummaryData['monthly_principal_amount'] = 0; //monthly_amount - discount;
            $memberBillSummaryData['discount'] = 0;
            $memberBillSummaryData['monthly_bill_amount'] = 0; //monthly_principal_amount + interest_on_due_amount;
            $memberBillSummaryData['amount_payable'] = 0; //op_due_amount + monthly_bill_amount;
            $memberBillSummaryData['principal_paid'] = 0;
            $memberBillSummaryData['interest_paid'] = 0;
            $memberBillSummaryData['principal_adjusted'] = 0;
            $memberBillSummaryData['interest_adjusted'] = 0;
            $memberBillSummaryData['principal_balance'] = 0; //op_principal_arrears + monthly_principal_amount - principal_paid;
            $memberBillSummaryData['interest_balance'] = 0; //op_interest_arrears + interest_on_due_amount - interest_paid;
            $memberBillSummaryData['balance_amount'] = 0; //principal_balance + interest_balance;
            $memberBillSummaryData['interest_adjusted'] = 0;
            $memberBillSummaryData['igst_total'] = 0;
            $memberBillSummaryData['cgst_total'] = 0;
            $memberBillSummaryData['sgst_total'] = 0;
            $memberBillSummaryData['tax_total'] = 0;
            $memberBillSummaryData['tax_paid'] = 0;
            $memberBillSummaryData['tax_adjusted'] = 0;
            $memberBillSummaryData['interest_free_amount'] = 0;
            $memberBillSummaryData['bill_tariff_type'] = $societyTariffParameterCheck['SocietyParameter']['tariff_id'];
            $memberBillSummaryData['bill_generated_date'] = isset($postData['MemberBillGenerate']['bill_generated_date']) ? trim($postData['MemberBillGenerate']['bill_generated_date']) : '';
            $memberBillSummaryData['bill_due_date'] = (isset($postData['MemberBillGenerate']['bill_due_date']) && !empty($postData['MemberBillGenerate']['bill_due_date'])) ? trim($postData['MemberBillGenerate']['bill_due_date']) : NULL;
            $memberBillSummaryData['cdate'] = $this->Util->getDateTime();
            $memberBillSummaryData['udate'] = $this->Util->getDateTime();
            //print_r($billLedgerHeadSettings);die;
            foreach ($memberTariffInfo['MemberTariff'] as $memberTariffData) {
                $billGenerateArr[$tariffCounter]['member_id'] = isset($memberTariffData['member_id']) ? $memberTariffData['member_id'] : $memberId;
                $billGenerateArr[$tariffCounter]['bill_generated_date'] = isset($postData['MemberBillGenerate']['bill_generated_date']) ? trim($postData['MemberBillGenerate']['bill_generated_date']) : NULL;
                $billGeneratedMonths = $billGenerateArr[$tariffCounter]['month'] = (isset($postData['MemberBillGenerate']['month']) && !empty($postData['MemberBillGenerate']['month'])) ? $postData['MemberBillGenerate']['month'] : NULL;
                $billGenerateArr[$tariffCounter]['bill_number'] = isset($paymentBillNo) ? $paymentBillNo : 0;
                $billGenerateArr[$tariffCounter]['amount'] = isset($memberTariffData['amount']) ? $memberTariffData['amount']*$multiplyTariffAmountValue : 0;
                $billGenerateArr[$tariffCounter]['ledger_head_id'] = isset($memberTariffData['ledger_head_id']) ? $memberTariffData['ledger_head_id'] : 0;
                $billGenerateArr[$tariffCounter]['society_id'] = $this->societyloginID;
                $billGenerateArr[$tariffCounter]['bill_type'] = $billType;
                $billGenerateArr[$tariffCounter]['cdate'] = $this->Util->getDateTime();

                $memberBillSummaryData['monthly_amount'] += isset($memberTariffData['amount']) ? $memberTariffData['amount']*$multiplyTariffAmountValue : 0;
                //If taxes applicable
                if (!empty($billLedgerHeadSettings) && isset($billLedgerHeadSettings['tax']) && in_array($memberTariffData['ledger_head_id'], $billLedgerHeadSettings['tax'])) {
                    $taxAmount = 0;
                    if ($societyTariffParameterCheck['SocietyParameter']['igst_tax_per'] > 0) {
                        $igstTaxAmount = ($memberTariffData['amount'] * $multiplyTariffAmountValue) * ($societyTariffParameterCheck['SocietyParameter']['igst_tax_per'] / 100);
                        $memberBillSummaryData['igst_total'] = $memberBillSummaryData['igst_total'] + $igstTaxAmount;
                        $billGenerateArr[$tariffCounter]['igst_total'] = $igstTaxAmount;
                        $taxAmount += $igstTaxAmount;
                    }
                    if ($societyTariffParameterCheck['SocietyParameter']['cgst_tax_per'] > 0) {
                        $cgstTaxAmount = ($memberTariffData['amount'] * $multiplyTariffAmountValue) * ($societyTariffParameterCheck['SocietyParameter']['cgst_tax_per'] / 100);
                        $memberBillSummaryData['cgst_total'] = $memberBillSummaryData['cgst_total'] + $cgstTaxAmount;
                        $billGenerateArr[$tariffCounter]['cgst_total'] = $cgstTaxAmount;
                        $taxAmount += $cgstTaxAmount;
                    }
                    if ($societyTariffParameterCheck['SocietyParameter']['sgst_tax_per'] > 0) {
                        $sgstTaxAmount = ($memberTariffData['amount'] * $multiplyTariffAmountValue) * ($societyTariffParameterCheck['SocietyParameter']['sgst_tax_per'] / 100);
                        $memberBillSummaryData['sgst_total'] = $memberBillSummaryData['sgst_total'] + $sgstTaxAmount;
                        $billGenerateArr[$tariffCounter]['sgst_total'] = $sgstTaxAmount;
                        $taxAmount += $sgstTaxAmount;
                    }
                    $memberBillSummaryData['tax_total'] = $memberBillSummaryData['tax_total'] + $taxAmount;
                    $billGenerateArr[$tariffCounter]['tax_total'] = $taxAmount;
                    $memberBillSummaryData['monthly_amount'] += $taxAmount;
                    $memberBillSummaryData['tax_balance'] += $taxAmount;
                }
                if (!empty($billLedgerHeadSettings) && isset($billLedgerHeadSettings['interestFree']) && in_array($memberTariffData['ledger_head_id'], $billLedgerHeadSettings['interestFree'])) {
                    $memberBillSummaryData['interest_free_amount'] += $memberTariffData['amount']*$multiplyTariffAmountValue;
                }
                $tariffCounter++;
            }
            if (isset($billGenerateArr) && count($billGenerateArr) > 0) {
                $memberBillSummaryData['monthly_principal_amount'] = $memberBillSummaryData['monthly_amount'] - $memberBillSummaryData['discount'];
                $memberBillSummaryData['monthly_bill_amount'] = $memberBillSummaryData['monthly_principal_amount'] + $memberBillSummaryData['interest_on_due_amount'];
                $memberBillSummaryData['amount_payable'] = $memberBillSummaryData['op_due_amount'] + $memberBillSummaryData['monthly_bill_amount'];
                $memberBillSummaryData['principal_balance'] = ($memberBillSummaryData['op_principal_arrears'] + $memberBillSummaryData['monthly_principal_amount']) - $memberBillSummaryData['principal_paid'] - $memberBillSummaryData['tax_total'];
                $memberBillSummaryData['interest_balance'] = ($memberBillSummaryData['op_interest_arrears'] + $memberBillSummaryData['interest_on_due_amount']) - $memberBillSummaryData['interest_paid'];
                $memberBillSummaryData['balance_amount'] = $memberBillSummaryData['principal_balance'] + $memberBillSummaryData['interest_balance'] + $memberBillSummaryData['tax_balance'];
                //$memberBillSummaryData['op_tax_arrears'] = ($memberBillSummaryData['op_tax_arrears'] + $memberBillSummaryData['tax_total']);
                //print_r($memberBillSummaryData);die;
                $this->MemberBillGenerate->saveAll($billGenerateArr);
                $this->MemberBillSummary->create();
                if ($this->MemberBillSummary->save($memberBillSummaryData)) {
                    $responseError['error'] = 0;
                    $responseError['error_message'] = 'Member bill has been generated successfully.';
                } else {
                    $responseError['error'] = 1;
                    $responseError['error_message'] = 'Member society bill not generated.';
                }
            }
        } else {
            $responseError['error'] = 1;
            $responseError['error_message'] = 'Members Bill already exists in the system with the same month .';
        }
        return $responseError;
    }

    public function getInterestOnDueAmount($societyTariffParameterCheck, $lastMonthBillDetails, $postData) {
        // 1-Delay Days, 2-Delay Months, 3-Complete Cycle Days, Complete Cycle Monthly 
        $interestOnDue = 0;
        $lastMonthBillprincipalAmount = 0;
        switch ($societyTariffParameterCheck['SocietyParameter']['method_id']) {
            case Configure::read('InterestMethod.DelayDays') :
                // Check how many days delayed  Due date from last bill to current months bill date
                if (!empty($lastMonthBillDetails['bill_due_date']) && !empty($postData['MemberBillGenerate']['bill_generated_date'])) {
                    $date1 = date_create($postData['MemberBillGenerate']['bill_generated_date']);
                    $date2 = date_create($lastMonthBillDetails['bill_due_date']);
                    $diff = date_diff($date1, $date2);
                    $delayDays = $diff->format("%a%");

                    //Check which interest type is applied 1-No, 2-Simple, 3-Compound
                    if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.No')) {
                        // No interest
                        $interestOnDue = 0;
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Simple')) {
                        // Only on Pricipal amount
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['principal_balance'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestForYear = $lastMonthBillprincipalAmount * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 100);
                        $interestOnDue = round(($interestForYear / 365) * $delayDays);
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Compound')) {
                        // On balance amount princicpal + interest
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['balance_amount'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestForYear = $lastMonthBillprincipalAmount * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 100);
                        $interestOnDue = round(($interestForYear / 365) * $delayDays);
                    }
                }
                break;
            case Configure::read('InterestMethod.DelayMonths') :
                // Check how many months delayed  Due date from last bill to current months bill date
                if (!empty($lastMonthBillDetails['bill_due_date']) && !empty($postData['MemberBillGenerate']['bill_generated_date'])) {
                    $date1 = date_create($postData['MemberBillGenerate']['bill_generated_date']);
                    $date2 = date_create($lastMonthBillDetails['bill_due_date']);
                    $diff = date_diff($date1, $date2);
                    $delayMonth = $diff->format("%m%");
                    $delayMonth = isset($delayMonth) && $delayMonth != 0 ? $delayMonth : 0; 
                    //Check which interest type is applied 1-No, 2-Simple, 3-Compound
                    if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.No')) {
                        // No interest
                        $interestOnDue = 0;
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Simple')) {
                        // Only on Pricipal amount
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['principal_balance'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestPerMonth = $lastMonthBillprincipalAmount * (($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100);
                        $interestOnDue = round($interestPerMonth * $delayMonth);
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Compound')) {
                        // On balance amount princicpal + interest
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['balance_amount'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestPerMonth = $lastMonthBillprincipalAmount * (($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100);
                        $interestOnDue = round($interestPerMonth * $delayMonth);
                    }
                }
                break;
            case Configure::read('InterestMethod.CompleteCycleDays') :
                // Check how many days delayed  Due date from last bill to current months bill date
                if (!empty($lastMonthBillDetails['bill_generated_date']) && !empty($postData['MemberBillGenerate']['bill_generated_date'])) {
                    $date1 = date_create($postData['MemberBillGenerate']['bill_generated_date']);
                    $date2 = date_create($lastMonthBillDetails['bill_generated_date']);
                    $diff = date_diff($date1, $date2);
                    $delayDays = $diff->format("%a%");

                    //Check which interest type is applied 1-No, 2-Simple, 3-Compound
                    if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.No')) {
                        // No interest
                        $interestOnDue = 0;
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Simple')) {
                        // Only on Pricipal amount
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['principal_balance'] - $lastMonthBillDetails['interest_free_amount'];

                        $interestForYear = $lastMonthBillprincipalAmount * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 100);
                        $interestOnDue = round(($interestForYear / 365) * $delayDays);
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Compound')) {
                        // On balance amount princicpal + interest
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['balance_amount'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestForYear = $lastMonthBillprincipalAmount * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 100);
                        $interestOnDue = round(($interestForYear / 365) * $delayDays);
                    }
                }
                break;
            case Configure::read('InterestMethod.CompleteCycleMonthly') :
                // Check how many months delayed  Due date from last bill to current months bill date
                if (!empty($lastMonthBillDetails['bill_generated_date']) && !empty($postData['MemberBillGenerate']['bill_generated_date'])) {
                    $date1 = date_create($postData['MemberBillGenerate']['bill_generated_date']);
                    $date2 = date_create($lastMonthBillDetails['bill_generated_date']);
                    $diff = date_diff($date1, $date2);
                    $delayMonth = $diff->format("%m%");

                    //Check which interest type is applied 1-No, 2-Simple, 3-Compound
                    if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.No')) {
                        // No interest
                        $interestOnDue = 0;
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Simple')) {
                        // Only on Pricipal amount
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['principal_balance'] - $lastMonthBillDetails['interest_free_amount'];
                        //echo $lastMonthBillprincipalAmount.'/'.$lastMonthBillDetails['principal_balance'];die;
                        $interestPerMonth = $lastMonthBillprincipalAmount * (($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100);
                        $interestOnDue = round($interestPerMonth * $delayMonth);
                    } else if ($societyTariffParameterCheck['SocietyParameter']['interest_type_id'] == Configure::read('InterestType.Compound')) {
                        // On balance amount princicpal + interest
                        $lastMonthBillprincipalAmount = $lastMonthBillDetails['balance_amount'] - $lastMonthBillDetails['interest_free_amount'];
                        $interestPerMonth = $lastMonthBillprincipalAmount * (($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100);
                        $interestOnDue = round($interestPerMonth * $delayMonth);
                    }
                }
                break;
        }
        return $interestOnDue;
    }

    public function print_member_bills() {
        $societyId = $this->societyloginID;
        $printBillDetails = array();
        $ledgerHeadDetails = array();
        $billMonth = (isset($this->request->query['month']) && !empty($this->request->query['month'])) ? $this->request->query['month'] : 1;
        $billNumberFrom = (isset($this->request->query['bill_from']) && !empty($this->request->query['bill_from'])) ? $this->request->query['bill_from'] : '';
        $billNumberTo = (isset($this->request->query['bill_to']) && !empty($this->request->query['bill_to'])) ? $this->request->query['bill_to'] : '';
        
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        $optionsQuery[] = array('AND' => array('MemberBillSummary.society_id' => $societyId, 'MemberBillSummary.month' => $billMonth));


        if (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no'])) {
            $optionsQuery['OR']['MemberBillSummary.flat_no'] = (isset($this->request->query['flat_no']) && !empty($this->request->query['flat_no'])) ? $this->request->query['flat_no'] : '';
        } else if (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date'])) {
            $optionsQuery['OR']['MemberBillSummary.bill_generated_date'] = (isset($this->request->query['bill_generated_date']) && !empty($this->request->query['bill_generated_date'])) ? $this->request->query['bill_generated_date'] : '';
        }
        if (isset($billNumberFrom) && !empty($billNumberFrom) && !empty($billNumberTo)) {
            $optionsQuery['OR']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array($billNumberFrom,$billNumberTo);
        } else if (isset($billNumberTo) && !empty($billNumberTo)) {
            $optionsQuery['AND']['MemberBillSummary.bill_no'] = $billNumberTo;
        } else if (isset($billNumberFrom) && !empty($billNumberFrom)) {
            $optionsQuery['AND']['MemberBillSummary.bill_no'] = $billNumberFrom;
        }
        
        //Society ledger head list which are part of bill
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyId))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyId), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyId, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }

        foreach ($societyLedgerHeadList as $key => $ledgerDetails) {
            $ledgerHeadDetails[$ledgerDetails['SocietyLedgerHeads']['id']] = $ledgerDetails['SocietyLedgerHeads']['title'];
        }

        $monthlyBills = $this->MemberBillSummary->find('all', array('conditions' => $optionsQuery, 'order' => array('MemberBillSummary.bill_no' => 'asc')));

        if ($monthlyBills) {
            foreach ($monthlyBills as $key => $bill) {                
                $printBillDetails[$bill['MemberBillSummary']['member_id']] = $bill;

                $tariffDetails = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $societyId, 'MemberBillGenerate.month' => $billMonth, 'MemberBillGenerate.member_id' => $bill['MemberBillSummary']['member_id'])));
                $printBillDetails[$bill['MemberBillSummary']['member_id']]['tariff'] = $tariffDetails;

                $memberDetails = $this->Member->find('first', array('conditions' => array('Member.id' => $bill['MemberBillSummary']['member_id'])));
                $printBillDetails[$bill['MemberBillSummary']['member_id']]['memberDetails'] = $memberDetails;

                $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('conditions' => array('MemberPayment.member_id' => $bill['MemberBillSummary']['member_id'])));
                $printBillDetails[$bill['MemberBillSummary']['member_id']]['receipts'] = $memberPaymentDetails;
            }
        }
        $this->set(compact('ledgerHeadDetails', 'printBillDetails','societyParameters'));
    }

    public function society_ledger_heads() {
        $this->loadModel('SocietyLedgerHeads');
    }

    public function checkMemberBillGeneratedExistsByMonth(){
        $this->layout = false;
        $response = array();
        $societyLoginId = $this->societyloginID;
        $response['error'] = 0;
        $billType = Configure::read('BillType.Regular');
        if ($this->RequestHandler->isAjax()) {
            $responseArr = $this->checkBillDateAndMonth($this->request->data);
            $response = $responseArr;
            if (isset($responseArr['error']) && $responseArr['error'] == 1) {
                $this->autoRender = false;
                $billGeneratedMonths = (isset($this->request->data['MemberBillGenerate']['month']) && !empty($this->request->data['MemberBillGenerate']['month'])) ? $this->request->data['MemberBillGenerate']['month'] : date('m');
                $conditionArry = array('MemberBillGenerate.society_id' => $societyLoginId, 'MemberBillGenerate.month' => $billGeneratedMonths,'MemberBillGenerate.bill_type'=>trim($billType));
                if ($this->MemberBillGenerate->hasAny($conditionArry)) {
                    $response['error'] = 1;
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }
    
    public function checkMemberSupplementaryBillByMonth(){
        $this->layout = false;
        $response = array();
        $societyLoginId = $this->societyloginID;
        $response['error'] = 0;
        $billType = Configure::read('BillType.Supplementary');
        if ($this->RequestHandler->isAjax()) {
            if (isset($responseArr['error']) && $responseArr['error'] == 1) {
                $this->autoRender = false;
                $billGeneratedMonths = (isset($this->request->data['MemberBillGenerate']['month']) && !empty($this->request->data['MemberBillGenerate']['month'])) ? $this->request->data['MemberBillGenerate']['month'] : date('m');
                $conditionArry = array('MemberBillGenerate.society_id' => $societyLoginId, 'MemberBillGenerate.month' => $billGeneratedMonths,'MemberBillGenerate.bill_type'=>trim($billType));
                if ($this->MemberBillGenerate->hasAny($conditionArry)) {
                    $response['error'] = 1;
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function checkBillDateAndMonth($postData = array()) {
        $response = array();
        $postData['MemberBillGenerate']['month'];
        if (isset($postData) && !empty($postData)) {
            $billGenerateMonth = (int) date('m', strtotime($postData['MemberBillGenerate']['bill_generated_date']));
            if ($postData['MemberBillGenerate']['month'] != $billGenerateMonth) {
                $response['error'] = 1;
                $response['error_message'] = 'Please select correct bill date.';
            } else {
                $response['error'] = 0;
            }
            return $response;
        }
    }

    public function getAllMembersBillSummaryDetails() {
        $this->layout = false;
        $response = array();
        $societyLoginId = $this->societyloginID;
        $response['error'] = 1;
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $billGeneratedMonths = (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month'])) ? $this->request->data['MemberBillSummary']['month'] : '';
            $options = array();
            $options['AND'][] = array('MemberBillSummary.society_id' => $societyLoginId);


            if (isset($billGeneratedMonths) && $billGeneratedMonths != '') {
                $options['AND'][] = array('MemberBillSummary.month' => $billGeneratedMonths);
            }

            if (isset($this->request->data['MemberBillSummary']['id']) && !empty($this->request->data['MemberBillSummary']['id'])) {
                $options['AND'][] = array('MemberBillSummary.id' => $this->request->data['MemberBillSummary']['id']);
            }
            if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no'])) {
                $options['AND'][] = array('MemberBillSummary.bill_no' => $this->request->data['MemberBillSummary']['bill_no']);
            }
            if (isset($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date'])) {
                $options['AND'][] = array('MemberBillSummary.bill_generated_date' => $this->request->data['MemberBillSummary']['bill_generated_date']);
            }

            if (isset($this->request->data['MemberBillSummary']['member_id']) && !empty($this->request->data['MemberBillSummary']['member_id'])) {
                $options['AND'][] = array('MemberBillSummary.member_id' => $this->request->data['MemberBillSummary']['member_id']);
            }

            $memberBillsDetails = $this->MemberBillSummary->find('first', array('conditions' => $options, 'order' => array('MemberBillSummary.id' => 'asc')));

            if (isset($memberBillsDetails) && count($memberBillsDetails) > 0) {
                $paymentMadeBillNo = isset($memberBillsDetails['MemberBillSummary']['bill_no']) ? $memberBillsDetails['MemberBillSummary']['bill_no'] : '';
                $billMonth = isset($memberBillsDetails['MemberBillSummary']['month']) ? $memberBillsDetails['MemberBillSummary']['month'] : '';
                $societyMemberId = isset($memberBillsDetails['MemberBillSummary']['member_id']) ? $memberBillsDetails['MemberBillSummary']['member_id'] : '';
                $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId, 'MemberPayment.bill_generated_id' => $paymentMadeBillNo);

                //$this->loadModel('MemberTariff');
                //$societyMemberTariff = $this->MemberTariff->find('all', array('conditions' => array('MemberTariff.member_id' => $societyMemberId, 'MemberTariff.society_id' => $this->Session->read('Auth.User.id'))));

                $memberBillTariff = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $this->Session->read('Auth.User.id'), 'MemberBillGenerate.month' => $billMonth, 'MemberBillGenerate.member_id' => $societyMemberId)));

                if (isset($memberBillTariff) && count($memberBillTariff) > 0) {
                    $tariffCount = 0;
                    $tariffSr = 1;
                    foreach ($memberBillTariff as $tariffData) {
                        $memberTariffData[$tariffCount]['tariff_serial'] = isset($tariffData['MemberBillGenerate']['tariff_serial']) ? $tariffData['MemberBillGenerate']['tariff_serial'] : $tariffSr;
                        $memberTariffData[$tariffCount]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                        $memberTariffData[$tariffCount]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                        $memberTariffData[$tariffCount]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                        $tariffCount++;
                        $tariffSr++;
                    }
                    $memberBillsDetails['MemberTariff'] = $memberTariffData;
                }

                //Next previous records
                $societyMemberBillNextId = $this->MemberBillSummary->find('first', array('fields' => array('MemberBillSummary.id', 'MemberBillSummary.month'), 'conditions' => array('MemberBillSummary.id >' => $memberBillsDetails['MemberBillSummary']['id'], 'MemberBillSummary.society_id' => $this->Session->read('Auth.User.id')), 'order' => array('MemberBillSummary.id' => 'asc')));
                $societyMemberBillPreviousId = $this->MemberBillSummary->find('first', array('fields' => array('MemberBillSummary.id', 'MemberBillSummary.month'), 'conditions' => array('MemberBillSummary.id <' => $memberBillsDetails['MemberBillSummary']['id'], 'MemberBillSummary.society_id' => $this->Session->read('Auth.User.id')), 'order' => array('MemberBillSummary.id' => 'desc')));
                $memberBillsDetails['MemberBillSummary']['next'] = 0;
                $memberBillsDetails['MemberBillSummary']['previous'] = 0;
                $memberBillsDetails['MemberBillSummary']['next_month'] = 0;
                $memberBillsDetails['MemberBillSummary']['previous_month'] = 0;
                $memberBillsDetails['MemberBillSummary']['next_id'] = 0;
                $memberBillsDetails['MemberBillSummary']['previous_id'] = 0;
                if (!empty($societyMemberBillNextId)) {
                    $memberBillsDetails['MemberBillSummary']['next'] = 1;
                    $memberBillsDetails['MemberBillSummary']['next_id'] = (isset($societyMemberBillNextId['MemberBillSummary']['id']) && $societyMemberBillNextId['MemberBillSummary']['id'] != '') ? $societyMemberBillNextId['MemberBillSummary']['id'] : 0;
                    $memberBillsDetails['MemberBillSummary']['next_month'] = (isset($societyMemberBillNextId['MemberBillSummary']['month']) && $societyMemberBillNextId['MemberBillSummary']['month'] != '') ? $societyMemberBillNextId['MemberBillSummary']['month'] : 0;
                }
                if (!empty($societyMemberBillPreviousId)) {
                    $memberBillsDetails['MemberBillSummary']['previous'] = 1;
                    $memberBillsDetails['MemberBillSummary']['previous_id'] = (isset($societyMemberBillPreviousId['MemberBillSummary']['id']) && $societyMemberBillPreviousId['MemberBillSummary']['id'] != '') ? $societyMemberBillPreviousId['MemberBillSummary']['id'] : 0;
                    $memberBillsDetails['MemberBillSummary']['previous_month'] = (isset($societyMemberBillPreviousId['MemberBillSummary']['month']) && $societyMemberBillPreviousId['MemberBillSummary']['month'] != '') ? $societyMemberBillPreviousId['MemberBillSummary']['month'] : 0;
                }

                if ($this->Member->MemberPayment->hasAny($conditionPayment)) {
                    $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('conditions' => $conditionPayment));
                    $memberBillsDetails['MemberPayment']['amount_paid'] = 00;
                    if (isset($memberPaymentDetails) && count($memberPaymentDetails) > 0) {
                        foreach ($memberPaymentDetails as $memberData) {
                            $memberBillsDetails['MemberPayment']['amount_paid'] += $memberData['MemberPayment']['amount_paid'];
                        }
                    }
                }
            }


            if (isset($memberBillsDetails) && !empty($memberBillsDetails)) {
                $response['error'] = 0;
                $response = array_merge($memberBillsDetails, $response);
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function updateMemberBillSummaryById(){
        $this->layout = false;
        $responseArr = array();
        $societyLoginId = $this->societyloginID;
        $responseArr['error'] = 1;
        $responseArr['error_message'] = 'Bill summary could not update';
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $postData['id'] = isset($this->request->data['MemberBillSummary']['id']) ? $this->request->data['MemberBillSummary']['id'] : '';
            $postData['discount'] = isset($this->request->data['MemberBillSummary']['discount']) ? $this->request->data['MemberBillSummary']['discount'] : '';
            $postData['principal_adjusted'] = isset($this->request->data['MemberBillSummary']['principal_adjusted']) ? $this->request->data['MemberBillSummary']['principal_adjusted'] : 0.00;
            $postData['interest_adjusted'] = isset($this->request->data['MemberBillSummary']['interest_adjusted']) ? $this->request->data['MemberBillSummary']['interest_adjusted'] : 0.00;
            $postData['member_id'] = isset($this->request->data['MemberBillSummary']['member_id']) ? $this->request->data['MemberBillSummary']['member_id'] : '';
            $postData['month'] = isset($this->request->data['MemberBillSummary']['month']) ? $this->request->data['MemberBillSummary']['month'] : '';
            $postData['bill_no'] = isset($this->request->data['MemberBillSummary']['bill_no']) ? $this->request->data['MemberBillSummary']['bill_no'] : '';

            $this->updateMemberTarrifDetails($this->request->data);

            $conditionsArr = array('MemberBillSummary.member_id' => $postData['member_id'], 'MemberBillSummary.society_id' => $societyLoginId, 'MemberBillSummary.id >=' => $postData['id']);
            $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $conditionsArr, 'recursive' => -1, 'order' => array('MemberBillSummary.id' => 'asc')));
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $billSummaryCounter = 0;
            $lastMonthCount = 0;

            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_adjusted'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_adjusted'] + $postData['principal_adjusted'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_adjusted'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_adjusted'] + $postData['interest_adjusted'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'] + $postData['discount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_adjusted'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_adjusted'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                        } else {
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            if ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;
                            } else {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = 0.00;
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];

                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                    } else {
                        $responseArr['error'] = 1;
                        $responseArr['error_message'] = 'Bill summary could not updated';
                    }
                }

                if (isset($memberBillSummaryDetails) && $this->MemberBillSummary->saveAll($memberBillSummaryDetails)) {
                    $responseArr['error'] = 0;
                    $responseArr['id'] = $postData['id'];
                    $responseArr['month'] = $postData['month'];
                    $responseArr['error_message'] = 'Bill summary updated  successfully';
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($responseArr), 'status' => 200));
    }

    public function loadLedgerDetailsSupplementaryBills() {
        $societyLoginID = $this->societyloginID;
        //$this->SocietyLedgerHeads->recursive = -1;
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }
        $this->set(compact('societyLedgerHeadList'));
        //print_r($societyLedgerHeadList);die;
    }

    public function generateSupplementaryMemberBills(){
        $this->layout = false;
        $response = array();
        $response['error'] = 1;
        $response['error_message'] = 'You have not selected Unit/Area.';
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            $this->Member->Behaviors->attach('Containable');
            if (isset($this->request->data['MemberBillGenerate']['bill_generated_date']) && !empty($this->request->data['MemberBillGenerate']['bill_generated_date']) && !empty($this->request->data['MemberBillGenerate']['bill_type'])) {
                $societyMemberLists = $this->Member->find('all', array('fields' => array('Member.id', 'Member.building_id', 'Member.wing_id', 'Member.unit_type', 'Member.area', 'Member.flat_no', 'Member.op_principal', 'Member.op_interest'), 'conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'recursive' => 0));
                //Based on society Parameters get tariffs for member
                if (count($societyMemberLists) > 0) {
                    foreach ($societyMemberLists as $details) {
                        $billNo = $this->SocietyBill->memberBillsUniqueNumber();
                        $memberDetails = $details['Member'];
                        $memberId = $memberDetails['id'];
                        if (isset($this->request->data['SupplementaryBillGenerate']) && !empty($this->request->data['SupplementaryBillGenerate'])) {
                            $responseArray = $this->calculateSocietyMembersSupplementaryBills($memberDetails, $memberId, $billNo, $this->request->data);
                            if (!empty($responseArray)) {
                                $response = $responseArray;
                            }
                        }
                    }
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function calculateSocietyMembersSupplementaryBills($memberDetails, $memberId, $billNo, $postData = array()){
        $billGenerateArr = array();
        $billType = Configure::read('BillType.Supplementary');
        $responseError = array();
        $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
        //$societyTariffParemeterSet = isset($societyTariffParameterCheck['SocietyParameter']['tariff_id']) ? $societyTariffParameterCheck['SocietyParameter']['tariff_id'] : 0;

        $billGeneratedMonths = (isset($postData['MemberBillGenerate']['month']) && !empty($postData['MemberBillGenerate']['month'])) ? $postData['MemberBillGenerate']['month'] : date('m');
        $societyMemberBillExists = $this->societyMemberBillExists($memberId, $billGeneratedMonths, $billType); //check same bill allready exists in system 
        if (empty($societyMemberBillExists)) {
            //$this->MemberBillGenerate->deleteAll(array('MemberBillGenerate.member_id' => $memberId, 'MemberBillGenerate.month' => $billGeneratedMonths, 'MemberBillGenerate.bill_type' => $billType));
            if ($this->MemberBillSummary->hasAny(array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.month' => $billGeneratedMonths, 'MemberBillSummary.bill_type' => $billType))) {
                //$this->MemberBillSummary->deleteAll(array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.month' => $billGeneratedMonths, 'MemberBillSummary.bill_type' => $billType));
            }

            //Check if last months bill details available
            $lastMonthBillDetails = $this->MemberBillSummary->find('first', array('conditions' => array('MemberBillSummary.member_id' => $memberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType), 'order' => array('MemberBillSummary.id' => 'desc')));

            if (isset($lastMonthBillDetails) && !empty($lastMonthBillDetails)) {
                $lastMonthBillDetails = $lastMonthBillDetails['MemberBillSummary'];
                $memberBillSummaryData['op_principal_arrears'] = $lastMonthBillDetails['principal_balance'];
                $memberBillSummaryData['op_interest_arrears'] = $lastMonthBillDetails['interest_balance'];
                $memberBillSummaryData['op_due_amount'] = $lastMonthBillDetails['balance_amount'];
                $memberBillSummaryData['tax_total'] = $lastMonthBillDetails['tax_total'];
                $memberBillSummaryData['op_tax_arrears'] = $lastMonthBillDetails['op_tax_arrears'] + $lastMonthBillDetails['tax_total'];
                $memberBillSummaryData['tax_balance'] = $lastMonthBillDetails['tax_balance'];

                $memberBillSummaryData['interest_on_due_amount'] = 0;
                if ($memberBillSummaryData['op_due_amount'] > 0) {
                    $memberBillSummaryData['interest_on_due_amount'] = $this->getInterestOnDueAmount($societyTariffParameterCheck, $lastMonthBillDetails, $postData);
                }
            } else {
                $memberBillSummaryData['op_principal_arrears'] = 0;
                $memberBillSummaryData['op_interest_arrears'] = 0;
                $memberBillSummaryData['op_due_amount'] = 0;
                $memberBillSummaryData['interest_on_due_amount'] = 0;
                $memberBillSummaryData['op_tax_arrears'] = 0;
                $memberBillSummaryData['tax_balance'] = 0;
                $memberBillSummaryData['tax_total'] = 0;
            }

            $memberBillSummaryData['bill_no'] = $billNo;
            $memberBillSummaryData['month'] = $billGeneratedMonths;
            $memberBillSummaryData['member_id'] = $memberId;
            $memberBillSummaryData['bill_type'] = $billType;
            $memberBillSummaryData['society_id'] = $this->societyloginID;
            $memberBillSummaryData['flat_no'] = isset($memberDetails['flat_no']) ? $memberDetails['flat_no'] : '';
            $memberBillSummaryData['monthly_amount'] = 0; //monthly member maintenance value
            $memberBillSummaryData['monthly_principal_amount'] = 0; //monthly_amount - discount;
            $memberBillSummaryData['discount'] = 0;
            $memberBillSummaryData['monthly_bill_amount'] = 0; //monthly_principal_amount + interest_on_due_amount;
            $memberBillSummaryData['amount_payable'] = 0; //op_due_amount + monthly_bill_amount;
            $memberBillSummaryData['principal_paid'] = 0;
            $memberBillSummaryData['interest_paid'] = 0;
            $memberBillSummaryData['principal_adjusted'] = 0;
            $memberBillSummaryData['interest_adjusted'] = 0;
            $memberBillSummaryData['principal_balance'] = 0; //op_principal_arrears + monthly_principal_amount - principal_paid;
            $memberBillSummaryData['interest_balance'] = 0; //op_interest_arrears + interest_on_due_amount - interest_paid;
            $memberBillSummaryData['balance_amount'] = 0; //principal_balance + interest_balance;
            $memberBillSummaryData['interest_adjusted'] = 0;
            $memberBillSummaryData['igst_total'] = 0;
            $memberBillSummaryData['cgst_total'] = 0;
            $memberBillSummaryData['sgst_total'] = 0;
            $memberBillSummaryData['tax_total'] = 0;
            $memberBillSummaryData['tax_paid'] = 0;
            $memberBillSummaryData['tax_adjusted'] = 0;
            $memberBillSummaryData['interest_free_amount'] = 0;
            $memberBillSummaryData['bill_tariff_type'] = $societyTariffParameterCheck['SocietyParameter']['tariff_id'];
            $memberBillSummaryData['bill_generated_date'] = isset($postData['MemberBillGenerate']['bill_generated_date']) ? trim($postData['MemberBillGenerate']['bill_generated_date']) : '';
            $memberBillSummaryData['bill_due_date'] = (isset($postData['MemberBillGenerate']['bill_due_date']) && !empty($postData['MemberBillGenerate']['bill_due_date'])) ? trim($postData['MemberBillGenerate']['bill_due_date']) : NULL;
            $memberBillSummaryData['cdate'] = $this->Util->getDateTime();
            $memberBillSummaryData['udate'] = $this->Util->getDateTime();

            //member tariff for supplementary bill
            if (isset($postData['SupplementaryBillGenerate'])) {
                $tariffCounter = 0;
                foreach ($postData['SupplementaryBillGenerate'] as $ledgerHeadID => $tarrifData) {
                    if (isset($tarrifData['rateUnitType']) && $tarrifData['rateUnitType'] != '' && !empty($tarrifData['rate'])) {
                        $billGenerateArr[$tariffCounter]['member_id'] = isset($memberDetails['id']) ? $memberDetails['id'] : $memberId;
                        $billGenerateArr[$tariffCounter]['bill_generated_date'] = isset($postData['MemberBillGenerate']['bill_generated_date']) ? trim($postData['MemberBillGenerate']['bill_generated_date']) : NULL;
                        $billGeneratedMonths = $billGenerateArr[$tariffCounter]['month'] = (isset($postData['MemberBillGenerate']['month']) && !empty($postData['MemberBillGenerate']['month'])) ? $postData['MemberBillGenerate']['month'] : NULL;
                        $billGenerateArr[$tariffCounter]['bill_number'] = isset($billNo) ? $billNo : 0;
                        switch ($tarrifData['rateUnitType']) {
                            case "Actual":
                                $billGenerateArr[$tariffCounter]['amount'] = isset($tarrifData['rate']) ? $tarrifData['rate'] : 0;
                                break;
                            case "Area":
                                $billGenerateArr[$tariffCounter]['amount'] = isset($tarrifData['rate']) ? ($tarrifData['rate'] * $memberDetails['area']) : 0;
                                break;
                            default:
                                $billGenerateArr[$tariffCounter]['amount'] = 0.00;
                                break;
                        }
                        $billGenerateArr[$tariffCounter]['ledger_head_id'] = $ledgerHeadID;
                        $billGenerateArr[$tariffCounter]['society_id'] = $this->societyloginID;
                        $billGenerateArr[$tariffCounter]['bill_type'] = $billType;
                        $billGenerateArr[$tariffCounter]['cdate'] = $this->Util->getDateTime();
                        $memberBillSummaryData['monthly_amount'] += isset($billGenerateArr[$tariffCounter]['amount']) ? $billGenerateArr[$tariffCounter]['amount'] : 0;
                        $taxStatus = $this->checkTaxesApplicableForLedgerHead($ledgerHeadID);
                        //If taxes applicable
                        if ($taxStatus) {
                            $taxAmount = 0;
                            if ($societyTariffParameterCheck['SocietyParameter']['igst_tax_per'] > 0) {
                                $igstTaxAmount = $billGenerateArr[$tariffCounter]['amount'] * ($societyTariffParameterCheck['SocietyParameter']['igst_tax_per'] / 100);
                                $billGenerateArr[$tariffCounter]['igst_total'] = $igstTaxAmount;
                                $taxAmount += $igstTaxAmount;
                            }
                            if ($societyTariffParameterCheck['SocietyParameter']['cgst_tax_per'] > 0) {
                                $cgstTaxAmount = $billGenerateArr[$tariffCounter]['amount'] * ($societyTariffParameterCheck['SocietyParameter']['cgst_tax_per'] / 100);
                                $billGenerateArr[$tariffCounter]['cgst_total'] = $cgstTaxAmount;
                                $taxAmount += $cgstTaxAmount;
                            }
                            if ($societyTariffParameterCheck['SocietyParameter']['sgst_tax_per'] > 0) {
                                $sgstTaxAmount = $billGenerateArr[$tariffCounter]['amount'] * ($societyTariffParameterCheck['SocietyParameter']['sgst_tax_per'] / 100);
                                $billGenerateArr[$tariffCounter]['sgst_total'] = $sgstTaxAmount;
                                $taxAmount += $sgstTaxAmount;
                            }
                            $memberBillSummaryData['tax_total'] = $memberBillSummaryData['tax_total'] + $taxAmount;
                            $billGenerateArr[$tariffCounter]['tax_total'] = $taxAmount;
                            $memberBillSummaryData['monthly_amount'] += $taxAmount;
                            $memberBillSummaryData['tax_balance'] += $taxAmount;
                        }
                    }
                    $tariffCounter++;
                }
            }

            if (isset($billGenerateArr) && count($billGenerateArr) > 0) {
                $memberBillSummaryData['monthly_principal_amount'] = $memberBillSummaryData['monthly_amount'] - $memberBillSummaryData['discount'];
                $memberBillSummaryData['monthly_bill_amount'] = $memberBillSummaryData['monthly_principal_amount'] + $memberBillSummaryData['interest_on_due_amount'];
                $memberBillSummaryData['amount_payable'] = $memberBillSummaryData['op_due_amount'] + $memberBillSummaryData['monthly_bill_amount'];
                $memberBillSummaryData['principal_balance'] = ($memberBillSummaryData['op_principal_arrears'] + $memberBillSummaryData['monthly_principal_amount']) - $memberBillSummaryData['principal_paid'];
                $memberBillSummaryData['interest_balance'] = ($memberBillSummaryData['op_interest_arrears'] + $memberBillSummaryData['interest_on_due_amount']) - $memberBillSummaryData['interest_paid'];
                $memberBillSummaryData['balance_amount'] = $memberBillSummaryData['principal_balance'] + $memberBillSummaryData['interest_balance'];

                $this->MemberBillGenerate->saveAll($billGenerateArr);
                $this->MemberBillSummary->create();
                if ($this->MemberBillSummary->save($memberBillSummaryData)) {
                    $responseError['error'] = 0;
                    $responseError['error_message'] = 'Supplementary bill has been generated successfully.';
                } else {
                    $responseError['error'] = 1;
                    $responseError['error_message'] = 'Supplementary bill count not generated.';
                }
            }
        } else {
            $responseError['error'] = 1;
            $responseError['error_message'] = 'Members Supplementary Bill already exists in the system with the same month .';
        }
        return $responseError;
    }

    public function checkTaxesApplicableForLedgerHead($ledgerHeadID = null) {
        $societyLedgerHeadList = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.id' => (int) $ledgerHeadID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.is_tax_applicable', 'SocietyLedgerHeads.is_rebate_applicable', 'SocietyLedgerHeads.is_interest_free'), 'recursive' => -1));
        if (isset($societyLedgerHeadList['SocietyLedgerHeads'])) {
            if ($societyLedgerHeadList['SocietyLedgerHeads']['is_tax_applicable'] == 1) {
                return true;
            }
        }
        return false;
    }

    public function memberRegularBillLedgerHeadSettings() {
        $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.is_tax_applicable', 'SocietyLedgerHeads.is_rebate_applicable', 'SocietyLedgerHeads.is_interest_free'), 'recursive' => -1));
        $billLedgerHeadSettings = array();
        foreach ($societyLedgerHeadList as $headDetails) {
            if ($headDetails['SocietyLedgerHeads']['is_tax_applicable'] == 1) {
                $billLedgerHeadSettings['tax'][] = $headDetails['SocietyLedgerHeads']['id'];
            }
            if ($headDetails['SocietyLedgerHeads']['is_rebate_applicable'] == 1) {
                $billLedgerHeadSettings['rebate'][] = $headDetails['SocietyLedgerHeads']['id'];
            }
            if ($headDetails['SocietyLedgerHeads']['is_interest_free'] == 1) {
                $billLedgerHeadSettings['interestFree'][] = $headDetails['SocietyLedgerHeads']['id'];
            }
        }
        return $billLedgerHeadSettings;
    }

    public function updateMemberTarrifDetails($postData = array()) {
        if (!empty($postData)) {
            //print_r($postData);die;
            $societyLoginId = $this->societyloginID;
            $memberID = isset($postData['MemberBillSummary']['member_id']) ? $postData['MemberBillSummary']['member_id'] : '';
            $billMonth = isset($postData['MemberBillSummary']['month']) ? $postData['MemberBillSummary']['month'] : '';
            $billNo = isset($postData['MemberBillSummary']['bill_no']) ? $postData['MemberBillSummary']['bill_no'] : '';
            $billGeneratedDate = isset($postData['MemberBillSummary']['bill_generated_date']) ? $postData['MemberBillSummary']['bill_generated_date'] : '';
            $oldMonthlyAmount = 0;
            $totalMonthlyAmount = 0;
            $billGenerateData = array();
            if (isset($postData['MemberTariff']) && !empty($postData['MemberTariff'])) {
                $count = 0;
                foreach ($postData['MemberTariff'] as $societyLedgerHeadId => $ledgerData) {
                    $totalMonthlyAmount += $ledgerData['amount'];
                    $condtitionArr = array('MemberBillGenerate.society_id' => $societyLoginId, 'MemberBillGenerate.month' => $billMonth, 'MemberBillGenerate.ledger_head_id' => $societyLedgerHeadId, 'MemberBillGenerate.member_id' => $memberID);
                    if ($this->MemberBillGenerate->hasAny($condtitionArr)) {
                        $this->MemberBillGenerate->updateAll(array('MemberBillGenerate.amount' => "'$ledgerData[amount]'"), array('MemberBillGenerate.society_id' => $societyLoginId, 'MemberBillGenerate.month' => $billMonth, 'MemberBillGenerate.ledger_head_id' => $societyLedgerHeadId, 'MemberBillGenerate.member_id' => $memberID));
                    } else {
                        $count++;
                        $billGenerateData[$count]['id'] = null;
                        $billGenerateData[$count]['member_id'] = $memberID;
                        $billGenerateData[$count]['society_id'] = $societyLoginId;
                        $billGenerateData[$count]['month'] = $billMonth;
                        $billGenerateData[$count]['ledger_head_id'] = $societyLedgerHeadId;
                        $billGenerateData[$count]['amount'] = $ledgerData['amount'];
                        $billGenerateData[$count]['bill_number'] = $billNo;
                        $billGenerateData[$count]['bill_generated_date'] = $billGeneratedDate;
                        $billGenerateData[$count]['bill_generated_date'] = $this->Util->getDateTime();
                    }
                }
            }

            if (!empty($billGenerateData)) {
                $this->MemberBillGenerate->saveAll($billGenerateData);
            }

            $billSummaryData = $this->MemberBillSummary->find('first', array('conditions' => array('MemberBillSummary.society_id' => $societyLoginId, 'MemberBillSummary.bill_no' => $billNo, 'MemberBillSummary.month' => $billMonth, 'MemberBillSummary.member_id' => $memberID), 'recursive' => -1));
            if (isset($billSummaryData['MemberBillSummary']) && !empty($billSummaryData['MemberBillSummary'])) {
                (int) $oldMonthlyAmount = $billSummaryData['MemberBillSummary']['monthly_amount'];
            }
            if ($oldMonthlyAmount != $totalMonthlyAmount) {
                $this->MemberBillSummary->updateAll(array('MemberBillSummary.monthly_amount' => "'$totalMonthlyAmount'"), array('MemberBillSummary.society_id' => $societyLoginId, 'MemberBillSummary.bill_no' => $billNo, 'MemberBillSummary.month' => $billMonth, 'MemberBillSummary.member_id' => $memberID));
            }
            return true;
        }
        return false;
    }

    public function society_generated_bills() {
        $this->Member->Behaviors->attach('Containable');
        $conditionsArr = array('MemberBillSummary.society_id' => $this->societyloginID, 'OR' => array('MemberBillSummary.bill_no !=' => ''));
        $memberBillSummaryDataArr = array();
        if ($this->MemberBillSummary->hasAny($conditionsArr)) {
            $memberBillSummaryDataArr = $this->Member->find('all', array('contain' => array('MemberBillSummary' => array('conditions' => $conditionsArr, 'limit' => 1, 'order' => array('MemberBillSummary.bill_no' => 'desc', 'MemberBillSummary.bill_generated_date' => 'desc')))));
            //print_r($memberBillSummaryDataArr); die;
        }
        $this->set(compact('memberBillSummaryDataArr'));
    }

    public function delete_generated_bills($billGeneratedId = null) {
        if (!$this->MemberBillSummary->exists($billGeneratedId)) {
            $this->Session->setFlash(__('Requested bill is not found. Please, try again.'), 'error');
            $this->redirect(array('action' => 'society_generated_bills'));
        }
        $billSummaryDetails = $this->MemberBillSummary->find('first', array('conditions' => array('MemberBillSummary.id' => $billGeneratedId, 'MemberBillSummary.society_id' => $this->societyloginID), 'order' => array('MemberBillSummary.bill_no' => 'asc'), 'recursive' => -1));
        if (isset($billSummaryDetails) && !empty($billSummaryDetails)) {
            $paymentCondition = array('MemberPayment.bill_generated_id' => $billSummaryDetails['MemberBillSummary']['bill_no'], 'MemberPayment.society_id' => $this->societyloginID);
            if ($this->Member->MemberPayment->hasAny($paymentCondition)) {
                $memberPaymentData = $this->Member->MemberPayment->find('first', array('conditions' => $paymentCondition, 'order' => array('MemberPayment.bill_generated_id' => 'asc'), 'recursive' => -1));
                if (isset($memberPaymentData) && !empty($memberPaymentData)) {
                    $this->Session->setFlash(__('Payment .'), 'error');
                }
            }
            print_r($memberPaymentData);
            die;
            $this->MemberBillSummary->id = $billGeneratedId;
            if ($this->MemberBillSummary->delete()) {
                $this->Session->setFlash(__('The bill has been deleted.'), 'success');
            } else {
                $this->Session->setFlash(__('The bill could not be deleted. Please, try again.'), 'error');
            }
            return $this->redirect(array('action' => 'society_generated_bills'));
        }
    }

    public function updateMemberPaymentByBillSummaryDetails($memberBillSummaryDetails = array()) {
        $aData['society_id'] = isset($memberBillSummaryDetails['MemberBillSummary']['society_id']) ? $memberBillSummaryDetails['MemberBillSummary']['society_id'] : '';
        $aData['id'] = NULL;
        $aData['member_id'] = isset($memberBillSummaryDetails['MemberBillSummary']['member_id']) ? $memberBillSummaryDetails['MemberBillSummary']['member_id'] : '';
        $aData['society_bank_id'] = isset($memberBillSummaryDetails['MemberBillSummary']['society_bank_id']) ? $memberBillSummaryDetails['MemberBillSummary']['society_bank_id'] : '';
        $aData['amount_paid'] = isset($memberBillSummaryDetails['MemberBillSummary']['principal_paid']) ? $memberBillSummaryDetails['MemberBillSummary']['principal_paid'] : 0.00;
        $aData['payment_mode'] = isset($memberBillSummaryDetails['MemberBillSummary']['payment_mode']) ? $memberBillSummaryDetails['MemberBillSummary']['payment_mode'] : '';
        $aData['cheque_reference_number'] = isset($memberBillSummaryDetails['MemberBillSummary']['cheque_reference_number']) ? $memberBillSummaryDetails['MemberBillSummary']['cheque_reference_number'] : 0;
        $aData['payment_date'] = isset($memberBillSummaryDetails['MemberBillSummary']['bill_generated_date']) ? $memberBillSummaryDetails['MemberBillSummary']['bill_generated_date'] : '';
        $aData['member_bank_id'] = isset($memberBillSummaryDetails['MemberBillSummary']['member_bank_id']) ? $memberBillSummaryDetails['MemberBillSummary']['member_bank_id'] : '';
        $aData['member_bank_ifsc'] = isset($memberBillSummaryDetails['MemberBillSummary']['member_bank_ifsc']) ? $memberBillSummaryDetails['MemberBillSummary']['member_bank_ifsc'] : '';
        $aData['member_bank_branch'] = isset($memberBillSummaryDetails['MemberBillSummary']['member_bank_branch']) ? $memberBillSummaryDetails['MemberBillSummary']['member_bank_branch'] : '';
        $aData['bill_type'] = isset($memberBillSummaryDetails['MemberBillSummary']['bill_type']) ? trim($memberBillSummaryDetails['MemberBillSummary']['bill_type']) : '';
        $aData['bill_month'] = isset($memberBillSummaryDetails['MemberBillSummary']['month']) ? trim($memberBillSummaryDetails['MemberBillSummary']['month']) : '';
        $aData['bill_generated_id'] = isset($memberBillSummaryDetails['MemberBillSummary']['bill_no']) ? trim($memberBillSummaryDetails['MemberBillSummary']['bill_no']) : '';
        $aData['entry_date'] = $this->Util->getDateTime();
        $aData['cdate'] = $this->Util->getDateTime();
        $aData['udate'] = $this->Util->getDateTime();
        if ($this->Member->MemberPayment->save($aData)) {
            return true;
        }
    }
    
    public function delete_all_bills_payments(){
        $societyLoginId = $this->societyloginID;
        $this->layout = false;
        if (!empty($societyLoginId)) {
            if ($this->MemberBillSummary->hasAny(array('MemberBillSummary.society_id' => $societyLoginId))){
                $this->MemberBillGenerate->deleteAll(array('MemberBillGenerate.society_id' => $societyLoginId));
                if ($this->MemberBillSummary->hasAny(array('MemberBillSummary.society_id' => $societyLoginId))){
                    $this->MemberBillSummary->deleteAll(array('MemberBillSummary.society_id' => $societyLoginId));
                }
                if ($this->Member->MemberPayment->hasAny(array('MemberPayment.society_id' => $societyLoginId))){
                    $this->Member->MemberPayment->deleteAll(array('MemberPayment.society_id' => $societyLoginId));
                }
            }
            $this->Session->setFlash(__('All bills & payments has been deleted.'),'success');
        }
        return $this->redirect(array('action' => 'society_generated_bills'));
    }
    
    public function upadate_last_member_bill($lastBillGeneratedId = null){
        $societyLoginId = $this->societyloginID;
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        if (isset($societyParameters['SocietyParameter'])) {
            $societyBillingFrequencyId = $societyParameters['SocietyParameter']['billing_frequency_id'];
            $billingMonthArray = $this->Util->societyBillingFrequency($societyBillingFrequencyId);
        }
        if($lastBillGeneratedId != ''){
            $lastBillGeneratedData = $this->MemberBillSummary->find('first', array('conditions' => array('MemberBillSummary.id' => $lastBillGeneratedId, 'MemberBillSummary.society_id' => $societyLoginId), 'recursive' => -1));
        }
        print_r($this->request->data);die;
        $this->set(compact('billingMonthArray', 'lastBillGeneratedData'));
    }
}
