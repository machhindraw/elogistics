<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error">
                            </div>
                            <div id="society_bank_slip">
                                <div class="print-bank-slip"> 
                                    <div class="bill-outer-border">
                                        <div class="row">
                                            <h5 class="text-center">Jogani Industrial Estate Premises</h5>
                                            <div class="address-heading">Registration No. BOM / GEN / 832 1975 Dated:</div>
                                            <div class="address-heading">541. SENAPTI BAPAT MARG. DADAR (W) MUMBAI-400028</div>
                                        </div>
                                        <div class="row1">
                                            <div class="bill">Bank Slip</div>
                                            <div class="row">
                                                <div class="col-md-8 col-xs-8">
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="">Bank Name : The Bharat Co-op. Bank (Mumbai)</div>
                                                        </div>

                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="">Branch : </div>
                                                        </div>
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="">A/c No. : </div>
                                                        </div>
                                                    </div>                                       
                                                </div>   
                                                <div class="col-md-4 col-xs-4">
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="">Slip No : 309</div>
                                                        </div>
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="">Date : 01/01/2018</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row1">
                                                <div style="padding: 0px;">
                                                    <div class="table-wrap">
                                                        <div class="table-responsive1">
                                                            <table id="datable_1" class="table padding-td-none padding-th-none table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Sr.</th>
                                                                        <th>Unit No</th>
                                                                        <th>Voucher No</th>
                                                                        <th>Cheque No.</th>
                                                                        <th>Cheque Date</th>
                                                                        <th>Bank Slip</th>
                                                                        <th>Drawee Bank</th>
                                                                        <th>Branch</th>
                                                                        <th class="text-center">Amount</th>
                                                                    </tr>
                                                                </thead>
                                                                <tr id="">
                                                                    <td style="text-align:center;">1</td>
                                                                    <td>38</td>
                                                                    <td>2</td>
                                                                    <td>174201</td>
                                                                    <td>19/04/2017</td>
                                                                    <td>0</td>
                                                                    <td>Central bank of India</td>
                                                                    <td>Dadar</td>
                                                                    <td class="text-right">80,275.00</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td style="text-align:center;">2</td>
                                                                    <td>38</td>
                                                                    <td>2</td>
                                                                    <td>174201</td>
                                                                    <td>19/04/2017</td>
                                                                    <td>0</td>
                                                                    <td>Central bank of India</td>
                                                                    <td>Dadar</td>
                                                                    <td class="text-right">80,275.00</td>
                                                                </tr>
                                                                <tr id="">
                                                                    <td colspan="6">Rupees Five Lac Seventy Thousand Eight Hundred Eighty Three Only</td>
                                                                    <td  class="text-center" colspan="2">Total (Rs.)</td>
                                                                    <td class="text-right">570,883.00</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br><br><br>
                                            <div class="row">
                                                <div class="col-md-3 col-xs-1"></div>  
                                                <div class="col-md-3 col-xs-5">Cashier's Signature</div> 
                                                <div class="col-md-3 col-xs-1"></div>  
                                                <div class="col-md-3 col-xs-5">Authorised Signatory</div> 
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <center>
                                <button type="button" class="btn btn-success btn-icon left-icon" onclick="printAllReports('society_bank_slip');"><i class="fa fa-print"></i><span> Print</span></button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>