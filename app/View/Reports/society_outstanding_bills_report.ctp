<div class="container-fluid">       
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark"></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <form action="" name="" id="" method="">
                                <div class="row">
                                    <h5 class="text-center">AMBIKA SHRUSTI C.H.S. LTD</h5>
                                    <div class="report-address-heading">Registration No. N.B.O/CIDCO/HHG(OH)/2858/JTR/YEAR 2008-2009 Dated: 19/01/2009</div>
                                    <div class="report-address-heading">PLOT NO.92 SECTOR-44 KARVE SEAWOODS NERUL NAVI MUMBAI 4000706</div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="report-bill">Billwise Outstanding - Regular Bill</div>
                                    <div class="text-center">As on 01/02/2018</div>
                                    <div class="report-bill-outer-section">
                                        <div class="table-wrap1">
                                                <table id="" class="table table-bordered padding-td-none padding-th-none" style="margin-top: -22px;">
                                                    <thead>
                                                        <tr>
                                                            <th>Bill No.</th>
                                                            <th>Bill Date</th>
                                                            <th>Financial Year</th>
                                                            <th>Principal</th>
                                                            <th>Interest</th>
                                                            <th>Tax</th>
                                                            <th>Amount Due</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>  
                                                        <tr id="">
                                                            <td style="border:none !important;"> 01</td>
                                                            <td colspan="2" style="border:none !important;">Member Name : Rashmi Ganesh Desai</td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border-left:none !important;"></td>
                                                        </tr>
                                                        <tr id="">
                                                            <td>23</td>
                                                            <td>01/07/2017</td>
                                                            <td>17-18</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="">
                                                            <td>12</td>
                                                            <td>01/07/2017</td>
                                                            <td>17-18</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="">
                                                            <td colspan="3" class="text-center">Regular Total</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="" style="border-bottom:2px solid #000;">
                                                            <td colspan="3" class="text-center">Total Regular + Supplemetary</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                    </tbody>  
                                                    <br><br>
                                                    <tbody>  
                                                        <tr id="">
                                                            <td style="border:none !important;"> 02</td>
                                                            <td colspan="2" style="border:none !important;">Member Name : Rashmi Ganesh Desai</td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border:none !important;"></td>
                                                            <td style="border-left:none !important;"></td>
                                                        </tr>
                                                        <tr id="">
                                                            <td>23</td>
                                                            <td>01/07/2017</td>
                                                            <td>17-18</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="">
                                                            <td>89</td>
                                                            <td>01/07/2017</td>
                                                            <td>17-18</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="">
                                                            <td colspan="3" class="text-center">Regular Total</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                        <tr id="" style="border-bottom:2px solid #000;">
                                                            <td colspan="3" class="text-center">Total Regular + Supplemetary</td>
                                                            <td>55462</td>
                                                            <td>0.00</td>
                                                            <td>0.00</td>
                                                            <td>55462</td>
                                                        </tr>
                                                    </tbody>  
                                                </table> 
                                        </div>
                                    </div>  
                                </div>                 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
