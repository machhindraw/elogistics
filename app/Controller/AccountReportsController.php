<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AccountReportsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler', 'Util', 'SocietyBill');
    public $helpers = array('Js');
    public $societyloginID = '';
    public $societyBillingFrequencyId = 0;

    //public $uses = array('Society');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
        $this->societyloginID = $this->Session->read('Auth.User.id');
        $this->set("utilObj", $this->Util);
        $this->set("societyObj", $this->SocietyBill);
        $this->loadModel('User');
        $this->loadModel('Member');
        $this->loadModel('Wing');
        $this->loadModel('Tenant');
        $this->loadModel('Society');
        $this->loadModel('MemberPayment');
        $this->loadModel('CashWithdraw');
        $this->loadModel('SocietyPayment');
        $this->loadModel('MemberBillSummary');
        $this->loadModel('SocietyParameter');
        $this->loadModel('MemberBillGenerate');
        $this->loadModel('SocietyOtherIncome');
        $this->loadAllNavigationModals();
    }

    function loadAllNavigationModals() {
        $this->SocietyBill->getSocietyBuildingLists();
        $this->SocietyBill->getSocietyLedgerHeadsDetails();
        $this->SocietyBill->navigateSocietyMemberDetails();
        $this->SocietyBill->loadAllNavigationModalsDropdowns();
        if ($this->societyloginID) {
            $billingFrequencyData = $this->Society->SocietyParameter->find('first', array('conditions' => array('SocietyParameter.society_id' => $this->societyloginID)));
            if (isset($billingFrequencyData) && count($billingFrequencyData) > 0) {
                $this->societyBillingFrequencyId = $billingFrequencyData['SocietyParameter']['billing_frequency_id'];
            }
        }
    }

    function account() {
        
    }

    function account_bank_book() {
        $this->loadModel('SocietyLedgerHeads');

        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyBankLists = $this->SocietyBill->getSocietyBankNameList();
        $accountBankBbook = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';

        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);
        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $optionsCashWithdraw['AND'] = array('CashWithdraw.society_id' => $societyLoginID);
        $optionsContraReceived['AND'] = array('CashWithdraw.society_id' => $societyLoginID);

        if (isset($this->request->data['BankBook']['society_bank_id']) && !empty($this->request->data['BankBook']['society_bank_id'])) {
            $options['AND']['MemberPayment.society_bank_id'] = $this->request->data['BankBook']['society_bank_id'];
            $optionsCashWithdraw['AND']['CashWithdraw.bank_ledger_head_id'] = $this->request->data['BankBook']['society_bank_id'];

            $optionsContraReceived['AND']['CashWithdraw.bank_to_ledger_head_id'] = $this->request->data['BankBook']['society_bank_id'];
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = $this->request->data['BankBook']['society_bank_id'];

            //For Ledger head opening balance
            $ledgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $this->request->data['BankBook']['society_bank_id'], 'SocietyLedgerHeads.society_id' => $societyLoginID)));
        }

        if (isset($this->request->data['BankBook']['operator']) && !empty($this->request->data['BankBook']['operator'])) {
            $operator = $this->request->data['BankBook']['operator'];
        }

        if (isset($this->request->data['BankBook']['amount']) && !empty($this->request->data['BankBook']['amount'])) {
            $options['AND']['MemberPayment.amount ' . $operator . ' '] = $this->request->data['BankBook']['amount'];
            $optionsCashWithdraw['AND']['CashWithdraw.amount ' . $operator . ' '] = $this->request->data['BankBook']['amount'];
            $optionsContraReceived['AND']['CashWithdraw.amount ' . $operator . ' '] = $this->request->data['BankBook']['amount'];
            $optionsSociety['AND']['SocietyPayment.amount ' . $operator . ' '] = $this->request->data['BankBook']['amount'];
        }

        if (isset($this->request->data['BankBook']['payment_date']) && !empty($this->request->data['BankBook']['payment_date']) && !empty($this->request->data['BankBook']['payment_date_to'])) {
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['BankBook']['payment_date'], $this->request->data['BankBook']['payment_date_to']);
            $optionsCashWithdraw['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->data['BankBook']['payment_date'], $this->request->data['BankBook']['payment_date_to']);
            $optionsContraReceived['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->data['BankBook']['payment_date'], $this->request->data['BankBook']['payment_date_to']);
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['BankBook']['payment_date'], $this->request->data['BankBook']['payment_date_to']);
        } else if (isset($this->request->data['BankBook']['payment_date']) && !empty($this->request->data['BankBook']['payment_date'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['BankBook']['payment_date'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data['BankBook']['payment_date'];
            $optionsContraReceived['AND']['CashWithdraw.payment_date'] = $this->request->data['BankBook']['payment_date'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['BankBook']['payment_date'];
        } else if (isset($this->request->data['BankBook']['payment_date_to']) && !empty($this->request->data['BankBook']['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['BankBook']['payment_date_to'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data['BankBook']['payment_date_to'];
            $optionsContraReceived['AND']['CashWithdraw.payment_date'] = $this->request->data['BankBook']['payment_date_to'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['BankBook']['payment_date_to'];
        }

        $bankBookData = array();
        $societyBankBbookDetails = array();
        $memberBankBbookDetails = array();
        $cashWithdrawDetails = array();
        $SocietyOtherIncomeDetails = array();

        if ($this->request->is(array('post'))) {
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            if (isset($this->request->data['BankBook']['report_type']) && !empty($this->request->data['BankBook']['report_type']) && $this->request->data['BankBook']['report_type'] == "Deposit") {
                $memberBankBbookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'deposit';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $SocietyOtherIncomeDetails = $this->SocietyOtherIncome->find('all', array('conditions' => array('SocietyOtherIncome.payment_mode'=>'Bank','SocietyOtherIncome.society_bank_id'=>$this->request->data['BankBook']['society_bank_id']),'order' => array('SocietyOtherIncome.payment_date' => 'asc')));
            } else if (isset($this->request->data['BankBook']['report_type']) && !empty($this->request->data['BankBook']['report_type']) && $this->request->data['BankBook']['report_type'] == "Withdrawal") {
                $societyBankBbookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                //print_r($SocietyOtherIncomeDetails);
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'withdraw';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else if (isset($this->request->data['BankBook']['report_type']) && !empty($this->request->data['BankBook']['report_type']) && $this->request->data['BankBook']['report_type'] == "Contra") {
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'contra';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $cashContraReceivedDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsContraReceived, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else {
                $memberBankBbookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $societyBankBbookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
                $cashContraReceivedDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsContraReceived, 'order' => array('CashWithdraw.payment_date' => 'asc')));
           }

            if (isset($societyBankBbookDetails) && !empty($societyBankBbookDetails)) {
                $sCounter = 0;
                foreach ($societyBankBbookDetails as $societyBankBbookData) {
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . ' &' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                    $bankBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                    unset($societyBankBbookData);
                    $sCounter++;
                }
            }
            
            if (isset($memberBankBbookDetails) && !empty($memberBankBbookDetails)) {
                $mCounter = 0;
                foreach ($memberBankBbookDetails as $memberBankBbookData) {
                    // echo '<pre>'; print_r($memberBankBbookData);
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['flat_no'] . ' - ' . $memberBankBbookData['Member']['member_name'] . '<br>' . 'Being Maintenance received';
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                    $bankBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                    unset($memberBankBbookData);
                    $mCounter++;
                }
            }
            $cashCounter = 0;
            if (isset($cashWithdrawDetails) && !empty($cashWithdrawDetails)) {
                foreach ($cashWithdrawDetails as $cashWithdrawData) {
                    $note = '';
                    switch ($cashWithdrawData['CashWithdraw']['txn_type']) {
                        case 'contra' :
                            $flag = 'Contra';
                            $note = 'Contra Trasferred';
                            break;
                        case 'deposit' :
                            $flag = 'Deposit';
                            $note = 'Deposited in Bank';
                            break;
                        case 'withdraw' :
                            $flag = 'Withdrawal';
                            $note = 'Withdrawn from Bank';
                            break;
                    }
                    //echo '<pre>'; print_r($cashWithdrawData);
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $cashWithdrawData['CashWithdraw']['payment_date'];
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $cashWithdrawData['CashWithdraw']['particulars'] . '<br>' . $note;
                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $cashWithdrawData['CashWithdraw']['cheque_no'];

                    if ($flag == 'Contra' || $flag == 'Withdrawal') {
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = $cashWithdrawData['CashWithdraw']['amount'];
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = '0.00';
                    } else if ($flag == 'Deposit') {
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                        $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $cashWithdrawData['CashWithdraw']['amount'];
                    }

                    $bankBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = $flag;
                    unset($cashWithdrawData);
                    $cashCounter++;
                }
                
            }
            
            //society other income bank general receipts only for withdraw data
            
            if (isset($SocietyOtherIncomeDetails) && !empty($SocietyOtherIncomeDetails)) {
                $otherIncomeCounter = $cashCounter;
                $flag = "Deposit";
                foreach ($SocietyOtherIncomeDetails as $otherIncomeData) {
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_date'] = $otherIncomeData['SocietyOtherIncome']['payment_date'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['particulars'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['withdrawal'] = "0.00";
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['deposit'] = $otherIncomeData['SocietyOtherIncome']['amount_paid'];
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['cheque_number'] = $otherIncomeData['SocietyOtherIncome']['cheque_no'];                    
                    $bankBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_flag'] = 'Deposit';
                    unset($otherIncomeData);
                    $otherIncomeCounter++;
                }
            }


            if (isset($cashContraReceivedDetails) && !empty($cashContraReceivedDetails)) {
                $contraCounter = 0;
                foreach ($cashContraReceivedDetails as $contraData) {

                    if ($contraData['CashWithdraw']['txn_type'] != 'contra') {
                        continue;
                    }

                    $note = 'Contra Received';
                    //echo '<pre>'; print_r($cashWithdrawData);
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $contraData['CashWithdraw']['payment_date'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $contraData['CashWithdraw']['particulars'] . '<br>' . $note;
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $contraData['CashWithdraw']['cheque_no'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $contraData['CashWithdraw']['amount'];
                    $bankBookData[$contraData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'Contra';
                    unset($contraData);
                    $contraCounter++;
                }
            }
            
            ksort($bankBookData); //sorting society and member payment by payment 
           //echo '<pre>';print_r($otherIncomeData);die;
        }
        $this->set(compact('postData', 'societyBankLists', 'accountBankBbook', 'societyDetails', 'bankBookData', 'ledgerHeadDetails'));
    }

    function account_bank_reconciliation() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $accountBankReconciliation = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';

        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);

        if (isset($this->request->data['MemberPayment']['amount']) && !empty($this->request->data['MemberPayment']['amount'])) {
            $options['AND']['MemberPayment.amount' . $operator . ' '] = $this->request->data['MemberPayment']['amount'];
        }
        if ($this->request->is(array('post'))) {
            $accountBankReconciliation = $this->MemberPayment->find('all', array('conditions' => $options));
        }
        $this->set(compact('postData', 'accountBankReconciliation', 'societyDetails'));
        // print_r($this->request->data);die;
    }

    function account_cash_book() {
        $this->loadModel('SocietyLedgerHeads');

        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $accountCashBook = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';

        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);
        $optionsCashWithdraw['AND'] = array('CashWithdraw.society_id' => $societyLoginID);

        if (isset($this->request->data['CashBook']['cash_ledger_head_id']) && !empty($this->request->data['CashBook']['cash_ledger_head_id'])) {
            $options['AND']['MemberPayment.society_bank_id'] = $this->request->data['CashBook']['cash_ledger_head_id'];
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = $this->request->data['CashBook']['cash_ledger_head_id'];

            //For Ledger head opening balance
            $ledgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $this->request->data['CashBook']['cash_ledger_head_id'], 'SocietyLedgerHeads.society_id' => $societyLoginID)));
        }

        if (isset($this->request->data['CashBook']['operator']) && !empty($this->request->data['CashBook']['operator'])) {
            $operator = $this->request->data['CashBook']['operator'];
        }

        if (isset($this->request->data['CashBook']['amount']) && !empty($this->request->data['CashBook']['amount'])) {
            $options['AND']['MemberPayment.amount ' . $operator . ' '] = $this->request->data['CashBook']['amount'];
            $optionsSociety['AND']['SocietyPayment.amount ' . $operator . ' '] = $this->request->data['CashBook']['amount'];
            $optionsCashWithdraw['AND']['CashWithdraw.amount ' . $operator . ' '] = $this->request->data['CashBook']['amount'];
        }

        if (isset($this->request->data['CashBook']['payment_date']) && !empty($this->request->data['CashBook']['payment_date']) && !empty($this->request->data['CashBook']['payment_date_to'])) {
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['CashBook']['payment_date'], $this->request->data['CashBook']['payment_date_to']);
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['CashBook']['payment_date'], $this->request->data['CashBook']['payment_date_to']);
            $optionsCashWithdraw['OR']['CashWithdraw.payment_date BETWEEN ? and ?'] = array($this->request->data['CashBook']['payment_date'], $this->request->data['CashBook']['payment_date_to']);
        } else if (isset($this->request->data['CashBook']['payment_date']) && !empty($this->request->data['CashBook']['payment_date'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['CashBook']['payment_date'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['CashBook']['payment_date'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data['CashBook']['payment_date'];
        } else if (isset($this->request->data['CashBook']['payment_date_to']) && !empty($this->request->data['CashBook']['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['CashBook']['payment_date_to'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['CashBook']['payment_date_to'];
            $optionsCashWithdraw['AND']['CashWithdraw.payment_date'] = $this->request->data['CashBook']['payment_date_to'];
        }
        $societyCashBookDetails = array();
        $memberCashBookDetails = array();
        $cashWithdrawDetails = array();
        $cashBookData = array();
            $SocietyOtherIncomeDetails = array();
		
        if ($this->request->is(array('post'))) {
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            if (isset($this->request->data['CashBook']['report_type']) && !empty($this->request->data['CashBook']['report_type']) && $this->request->data['CashBook']['report_type'] == "Receipt") {
                $memberCashBookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'withdraw';
                $SocietyOtherIncomeDetails = $this->SocietyOtherIncome->find('all', array('conditions' => array('SocietyOtherIncome.payment_mode'=>'Cash','SocietyOtherIncome.society_bank_id'=>$this->request->data['CashBook']['cash_ledger_head_id']),'order' => array('SocietyOtherIncome.payment_date' => 'asc')));
                //print_r($SocietyOtherIncomeDetails);die;
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else if (isset($this->request->data['CashBook']['report_type']) && !empty($this->request->data['CashBook']['report_type']) && $this->request->data['CashBook']['report_type'] == "Payment") {
                $societyCashBookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $optionsCashWithdraw['AND']['CashWithdraw.txn_type'] = 'deposit';
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            } else {
                $memberCashBookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                $societyCashBookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
                $cashWithdrawDetails = $this->CashWithdraw->find('all', array('conditions' => $optionsCashWithdraw, 'order' => array('CashWithdraw.payment_date' => 'asc')));
            }
        }

        if (isset($societyCashBookDetails) && !empty($societyCashBookDetails)) {
            $sCounter = 0;
            foreach ($societyCashBookDetails as $societyBankBbookData) {
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . '&' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                $cashBookData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                unset($societyBankBbookData);
                $sCounter++;
            }
        }

        if (isset($memberCashBookDetails) && !empty($memberCashBookDetails)) {
            $mCounter = 0;
            foreach ($memberCashBookDetails as $memberBankBbookData) {
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['member_name'] . ' Maint received';
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                $cashBookData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                unset($memberBankBbookData);
                $mCounter++;
            }
        }
        //echo '<pre>'; print_r($cashWithdrawDetails); exit;

            $cashCounter = 0;
        if (isset($cashWithdrawDetails) && !empty($cashWithdrawDetails)) {
            foreach ($cashWithdrawDetails as $cashWithdrawData) {
                $note = '';

                switch ($cashWithdrawData['CashWithdraw']['txn_type']) {
                    case 'deposit' :
                        $flag = 'Payment';
                        $note = 'Deposited in Bank';
                        break;
                    case 'withdraw' :
                        $flag = 'Receipt';
                        $note = 'Withdrawn from Bank';
                        break;
                }
                //echo '<pre>'; print_r($cashWithdrawData);die;
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_date'] = $cashWithdrawData['CashWithdraw']['payment_date'];
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['particulars'] = $cashWithdrawData['CashWithdraw']['particulars'] . '<br>' . $note;
                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['cheque_number'] = $cashWithdrawData['CashWithdraw']['cheque_no'];

                if ($flag == 'Payment') {
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = $cashWithdrawData['CashWithdraw']['amount'];
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = '0.00';
                } else if ($flag == 'Receipt') {
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['withdrawal'] = '0.00';
                    $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['deposit'] = $cashWithdrawData['CashWithdraw']['amount'];
                }

                $cashBookData[$cashWithdrawData['CashWithdraw']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'Contra';
                unset($cashWithdrawData);
                $cashCounter++;
            }
            
            if (isset($SocietyOtherIncomeDetails) && !empty($SocietyOtherIncomeDetails)) {
                $otherIncomeCounter = $cashCounter;
                $flag = 'Receipt';
                foreach ($SocietyOtherIncomeDetails as $otherIncomeData) {
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['payment_date'] = $otherIncomeData['SocietyOtherIncome']['payment_date'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['particulars'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['withdrawal'] = "0.00";
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['deposit'] = $otherIncomeData['SocietyOtherIncome']['amount_paid'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$otherIncomeCounter]['cheque_number'] = $otherIncomeData['SocietyOtherIncome']['title'];
                    $cashBookData[$otherIncomeData['SocietyOtherIncome']['payment_date']][$flag][$cashCounter]['payment_flag'] = 'General Receipt';
                    unset($otherIncomeData);
                    $otherIncomeCounter++;
                }
            }
        }

        ksort($cashBookData);
        
        $this->set(compact('postData', 'accountCashBook', 'societyDetails', 'cashBookData', 'ledgerHeadDetails'));
        //print_r($societyDetails);die;
    }

    function account_dues_from_member() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';
        $this->Member->Behaviors->attach('Containable');

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        $optionsMember['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));

        if (isset($this->request->data['MemberPayment']['operator']) && !empty($this->request->data['MemberPayment']['operator'])) {
            $operator = $this->request->data['MemberPayment']['operator'];
        }
        if (isset($this->request->data['MemberPayment']['amount']) && !empty($this->request->data['MemberPayment']['amount'])) {
            //$options['AND']['MemberBillSummary.balance_amount'.$operator.' '] = $this->request->data['MemberPayment']['amount'];
        }
        if (isset($this->request->data['MemberPayment']['report_type']) && !empty($this->request->data['MemberPayment']['report_type'])) {
            //$options['AND']['MemberPayment.report_type'] = $this->request->data['MemberPayment']['report_type'];
        }
        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $optionsMember['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $optionsMember['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        $societyMemberListsForDuesReport = $this->Member->find('all', array('contain' => array('Society', 'Building', 'Wing'), 'conditions' => $optionsMember, 'order' => array('Member.id' => 'asc')));

        $accountDuesMemberSummary = array();
        if ($this->request->is(array('post'))) {
            $mDuesCounter = 0;
            foreach ($societyMemberListsForDuesReport as $memberDuesListData) {
                $options['AND'] = array('MemberBillSummary.member_id' => $memberDuesListData['Member']['id'], 'MemberBillSummary.balance_amount >=' => 0);
                $accountDuesMemberSummaryData = $this->MemberBillSummary->find('first', array('conditions' => $options, 'fields' => array('MemberBillSummary.balance_amount'), 'order' => array('MemberBillSummary.bill_no' => 'desc')));
                $accountDuesMemberSummary[$mDuesCounter]['total_dues_amount'] = isset($accountDuesMemberSummaryData['MemberBillSummary']['balance_amount']) ? $accountDuesMemberSummaryData['MemberBillSummary']['balance_amount'] : 0.00;
                $accountDuesMemberSummary[$mDuesCounter]['flat_no'] = $memberDuesListData['Member']['flat_no'];
                $accountDuesMemberSummary[$mDuesCounter]['member_prefix'] = $memberDuesListData['Member']['member_prefix'];
                $accountDuesMemberSummary[$mDuesCounter]['member_name'] = $memberDuesListData['Member']['member_name'];
                $mDuesCounter ++;
            }
        }
        $this->set(compact('postData', 'accountDuesFromMember', 'societyDetails', 'accountDuesMemberSummary'));
    }

    function account_dues_advance_from_member() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $operator = '=';
        $this->Member->Behaviors->attach('Containable');

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        $optionsMember['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));

        if (isset($this->request->data['MemberPayment']['operator']) && !empty($this->request->data['MemberPayment']['operator'])) {
            $operator = $this->request->data['MemberPayment']['operator'];
        }
        if (isset($this->request->data['MemberPayment']['amount']) && !empty($this->request->data['MemberPayment']['amount'])) {
            //$options['AND']['MemberBillSummary.balance_amount'.$operator.' '] = $this->request->data['MemberPayment']['amount'];
        }
        if (isset($this->request->data['MemberPayment']['report_type']) && !empty($this->request->data['MemberPayment']['report_type'])) {
            //$options['AND']['MemberPayment.report_type'] = $this->request->data['MemberPayment']['report_type'];
        }
        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $optionsMember['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $optionsMember['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        $societyMemberListsForDuesAdvanceReport = $this->Member->find('all', array('contain' => array('Society', 'Building', 'Wing'), 'conditions' => $optionsMember, 'order' => array('Member.id' => 'asc')));

        $accountDuesAdvanceMemberSummary = array();
        if ($this->request->is(array('post'))) {
            $mDuesCounter = 0;
            foreach ($societyMemberListsForDuesAdvanceReport as $memberDuesListData) {
                $options['AND'] = array('MemberBillSummary.member_id' => $memberDuesListData['Member']['id'], 'MemberBillSummary.balance_amount <=' => 0);
                $accountDuesMemberSummaryData = $this->MemberBillSummary->find('first', array('conditions' => $options, 'fields' => array('MemberBillSummary.balance_amount'), 'order' => array('MemberBillSummary.bill_no' => 'desc')));
                $accountDuesAdvanceMemberSummary[$mDuesCounter]['total_dues_amount'] = isset($accountDuesMemberSummaryData['MemberBillSummary']['balance_amount']) ? $accountDuesMemberSummaryData['MemberBillSummary']['balance_amount'] : 0.00;
                $accountDuesAdvanceMemberSummary[$mDuesCounter]['flat_no'] = $memberDuesListData['Member']['flat_no'];
                $accountDuesAdvanceMemberSummary[$mDuesCounter]['member_prefix'] = $memberDuesListData['Member']['member_prefix'];
                $accountDuesAdvanceMemberSummary[$mDuesCounter]['member_name'] = $memberDuesListData['Member']['member_name'];
                $mDuesCounter ++;
            }
        }
        $this->set(compact('postData', 'accountDuesFromMember', 'societyDetails', 'accountDuesAdvanceMemberSummary'));
    }

    function account_member_ledger() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberBillSummary');
        $postData = $this->request->data;
        $accountMemberLedgerDetails = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberList = $this->SocietyBill->getSocietyMemberList();
        $societyBuildingLists = $this->SocietyBill->getSocietyBuildingListsById();
        $societyWingLists = $this->SocietyBill->getSocietyWingListsById();

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['building_id']) && !empty($this->request->data['MemberBillSummary']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['MemberBillSummary']['building_id'];
        }
        if (isset($this->request->data['MemberBillSummary']['wing_id']) && !empty($this->request->data['MemberBillSummary']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['MemberBillSummary']['wing_id'];
        }
        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $options['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }
        if ($this->request->is(array('post'))) {
            if (isset($societyMemberList) && count($societyMemberList) > 0) {
                foreach ($societyMemberList as $memberId => $memberName) {
                    $options['AND'] = array('MemberBillSummary.member_id' => $memberId);
                    $accountMemberBillSummary = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('month' => 'asc')));
                    if (isset($accountMemberBillSummary) && !empty($accountMemberBillSummary)) {
                        $summaryCounter = 0;

                        foreach ($accountMemberBillSummary as $accountMemberBillData) {
                            $accountMemberLedgerDetails[$memberId]['Member']['member_name'] = $accountMemberBillData['Member']['member_name'];
                            $accountMemberLedgerDetails[$memberId]['Member']['flat_no'] = $accountMemberBillData['Member']['flat_no'];
                            $accountMemberLedgerDetails[$memberId]['Member']['building_id'] = $accountMemberBillData['Member']['building_id'];
                            $accountMemberLedgerDetails[$memberId]['Member']['building_name'] = isset($societyBuildingLists[$accountMemberBillData['Member']['building_id']]) ? $societyBuildingLists[$accountMemberBillData['Member']['building_id']] : '';
                            $accountMemberLedgerDetails[$memberId]['Member']['wing_name'] = isset($societyWingLists[$accountMemberBillData['Member']['wing_id']]) ? $societyWingLists[$accountMemberBillData['Member']['wing_id']] : '';

                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['bill_generated_date'] = $accountMemberBillData['MemberBillSummary']['bill_generated_date'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['month'] = $accountMemberBillData['MemberBillSummary']['month'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['bill_no'] = $accountMemberBillData['MemberBillSummary']['bill_no'];

                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['monthly_amount'] = $accountMemberBillData['MemberBillSummary']['monthly_amount'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['op_principal_arrears'] = $accountMemberBillData['MemberBillSummary']['op_principal_arrears'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['op_interest_arrears'] = $accountMemberBillData['MemberBillSummary']['op_interest_arrears'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['op_due_amount'] = $accountMemberBillData['MemberBillSummary']['op_due_amount'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['debit_total'] = ($accountMemberBillData['MemberBillSummary']['monthly_amount'] + $accountMemberBillData['MemberBillSummary']['op_interest_arrears']);

                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['balance_amount'] = $accountMemberBillData['MemberBillSummary']['balance_amount'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['principal_balance'] = $accountMemberBillData['MemberBillSummary']['principal_balance'];
                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['interest_balance'] = $accountMemberBillData['MemberBillSummary']['interest_balance'];

                            $condition = array('MemberPayment.bill_generated_id' => $accountMemberBillData['MemberBillSummary']['bill_no'], 'MemberPayment.member_id' => $accountMemberBillData['MemberBillSummary']['member_id']);
                            if ($this->MemberPayment->hasAny($condition)) {
                                $memberPaymentInfo = $this->MemberPayment->find('all', array('conditions' => $condition, 'recursive' => -1));
                                if (isset($memberPaymentInfo) && count($memberPaymentInfo) > 0) {
                                    $summaryCounter++;
                                    foreach ($memberPaymentInfo as $memberPaymentData) {
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['bill_generated_date'] = $memberPaymentData['MemberPayment']['payment_date'];
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['cheque_reference_number'] = $memberPaymentData['MemberPayment']['cheque_reference_number'];
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['payment_date'] = $memberPaymentData['MemberPayment']['payment_date'];
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['amount_paid'] = $memberPaymentData['MemberPayment']['amount_paid'];
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['creadit_total'] = $memberPaymentData['MemberPayment']['amount_paid'];
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['principal_balance'] = $accountMemberBillData['MemberBillSummary']['amount_payable'] = ($accountMemberBillData['MemberBillSummary']['amount_payable'] - $memberPaymentData['MemberPayment']['amount_paid']);
                                        $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['balance_amount'] = $accountMemberLedgerDetails[$memberId]['MemberBillSummary']['principal_balance'];
                                        $summaryCounter++;
                                    }
                                }
                            }
                            //print_r($accountMemberLedgerDetails);die;
                            $summaryCounter++;
                        }
                    }
                }
            }
        }
        $this->set(compact('postData', 'accountMemberLedgerDetails', 'societyDetails'));
    }

    function account_income_exp_statement() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberPayment');
        $postData = $this->request->data;
        $incomeExpensesDetails = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        //$societyMemberDetails = $this->SocietyBill->navigateSocietyMemberDetails();

        if ($this->request->is(array('post'))) {
            $incomeExpensesDetails = $this->MemberPayment->find('all', array('conditions' => $societyLoginID));
        }
        $this->set(compact('postData', 'incomeExpensesDetails', 'societyDetails'));
        //print_r($this->request->data);die;
    }

    function account_balance_sheet() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberPayment');
        $postData = $this->request->data;
        $balanceSheetDetails = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        //$societyMemberDetails = $this->SocietyBill->navigateSocietyMemberDetails();

        if ($this->request->is(array('post'))) {
            $balanceSheetDetails = $this->MemberPayment->find('all', array('conditions' => $societyLoginID));
        }
        $this->set(compact('postData', 'balanceSheetDetails', 'societyDetails'));
        //print_r($this->request->data);die;
    }

    function account_payment_register() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyPayment');
        $postData = $this->request->data;
        $paymentRegisterData = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyBankLists = $this->SocietyBill->getSocietyBankNameList();
        //$societyMemberDetails = $this->SocietyBill->navigateSocietyMemberDetails();
        $options['AND'] = array('SocietyPayment.society_id' => $societyLoginID);

        if (isset($this->request->data['SocietyPayment']['payment_type']) && !empty($this->request->data['SocietyPayment']['payment_type']) && $this->request->data['SocietyPayment']['payment_type'] != "Both") {
            $options['AND']['SocietyPayment.payment_type'] = $this->request->data['SocietyPayment']['payment_type'];
        }

        if (isset($this->request->data['SocietyPayment']['payment_type']) && !empty($this->request->data['SocietyPayment']['payment_type']) && $this->request->data['SocietyPayment']['payment_type'] == "Both") {
            $options['OR'][]['SocietyPayment.payment_type'] = "Cash";
            $options['OR'][]['SocietyPayment.payment_type'] = "Bank";
        }

        if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $options['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
        } else if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date'])) {
            $options['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        } else if (isset($this->request->data['SocietyPayment']['payment_date_to']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $options['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        }

        if ($this->request->is(array('post'))) {
            $paymentRegisterData = $this->SocietyPayment->find('all', array('conditions' => $options));
        }
        $this->set(compact('postData', 'societyBankLists', 'paymentRegisterData', 'societyDetails'));
        //print_r($paymentRegisterData);die; 
    }

    function account_member_monthly_contribution() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberPayment');
        $postData = $this->request->data;
        $memberMonthlyContributionData = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberName = $this->SocietyBill->getSocietyMemberList();
        $societyMemberFlatlList = $this->SocietyBill->getSocietyMemberFlatList();
        //$societyMemberDetails = $this->SocietyBill->navigateSocietyMemberDetails();

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);

        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $options['AND']['Member.id'] = $this->request->data['MemberPayment']['flat_no'];
        }

        if (isset($this->request->data['MemberPayment']['member_id']) && !empty($this->request->data['MemberPayment']['member_id'])) {
            $options['AND']['Member.id'] = $this->request->data['MemberPayment']['member_id'];
        }

        if ($this->request->is(array('post'))) {
            $this->MemberBillSummary->Behaviors->attach('Containable');
            $billingMonthsLists = $this->Util->societyBillingFrequency(1);
            foreach ($billingMonthsLists as $monthNo => $monthName) {
                $options['AND']['MemberBillSummary.month'] = $monthNo;
                $memberBillDetailsbyMonth = $this->MemberBillSummary->find('all', array('contain' => array('Member'), 'conditions' => $options));
                if (isset($memberBillDetailsbyMonth) && !empty($memberBillDetailsbyMonth)) {
                    $memberMonthlyContributionData['Maintenanace'][$monthNo] = isset($memberBillDetailsbyMonth[0]['MemberBillSummary']['monthly_principal_amount']) ? $memberBillDetailsbyMonth[0]['MemberBillSummary']['monthly_principal_amount'] : 0.00;
                }
                unset($monthName);
            }
        }
        $this->set(compact('postData', 'memberMonthlyContributionData', 'societyDetails', 'societyMemberName', 'societyMemberFlatlList'));
    }

    function account_general_ledger() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberName = $this->SocietyBill->getSocietyMemberList();
        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);

        if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $optionsSociety['OR']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
            $options['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['SocietyPayment']['payment_date'], $this->request->data['SocietyPayment']['payment_date_to']);
        } else if (isset($this->request->data['SocietyPayment']['payment_date']) && !empty($this->request->data['SocietyPayment']['payment_date'])) {
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        } else if (isset($this->request->data['SocietyPayment']['payment_date_to']) && !empty($this->request->data['SocietyPayment']['payment_date_to'])) {
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['SocietyPayment']['payment_date_to'];
        }

        $societyLedgerData = array();
        $memberLedgerDetails = array();
        $societyLedgerDetails = array();

        //Cash in hand ledger id
        $cashInHandLedgerDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.title LIKE' => "Cash in hand"), 'order' => array('SocietyLedgerHeads.status' => 'asc')));
        if (isset($cashInHandLedgerDetails['SocietyLedgerHeads']) && !empty($cashInHandLedgerDetails['SocietyLedgerHeads'])) {
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = isset($cashInHandLedgerDetails['SocietyLedgerHeads']['id']) ? $cashInHandLedgerDetails['SocietyLedgerHeads']['id'] : 0;
        }

        if ($this->request->is(array('post'))) {
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            $memberLedgerDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
            $societyLedgerDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
        }

        if (isset($societyLedgerDetails) && !empty($societyLedgerDetails)) {
            $sCounter = 0;
            foreach ($societyLedgerDetails as $societyBankBbookData) {
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . '&' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['amount'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_type'] = $societyBankBbookData['SocietyPayment']['payment_type'];
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['payment_flag'] = "Payment";
                $societyLedgerData[$societyBankBbookData['SocietyPayment']['payment_date']]['Payment'][$sCounter]['deposit'] = "0.00";
                unset($societyBankBbookData);
                $sCounter++;
            }
        }

        if (isset($memberLedgerDetails) && !empty($memberLedgerDetails)) {
            $mCounter = 0;
            foreach ($memberLedgerDetails as $memberBankBbookData) {
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['particulars'] = $memberBankBbookData['Member']['member_name'] . ' Maint received';
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['withdrawal'] = "0.00";
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                $societyLedgerData[$memberBankBbookData['MemberPayment']['payment_date']]['Receipt'][$mCounter]['payment_flag'] = "Receipt";
                unset($memberBankBbookData);
                $mCounter++;
            }
        }
        ksort($societyLedgerData);
        $this->set(compact('postData', 'societyLedgerData', 'societyDetails', 'societyMemberName'));
    }

    function account_petty_cash() {
        
    }

    function account_member_ledger_default() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $accountMemberLedgerDetails = array();
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberList = $this->SocietyBill->getSocietyMemberList();
        $societyBuildingLists = $this->SocietyBill->getSocietyBuildingListsById();
        $societyWingLists = $this->SocietyBill->getSocietyWingListsById();

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['building_id']) && !empty($this->request->data['MemberBillSummary']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['MemberBillSummary']['building_id'];
        }
        if (isset($this->request->data['MemberBillSummary']['wing_id']) && !empty($this->request->data['MemberBillSummary']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['MemberBillSummary']['wing_id'];
        }
        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $options['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }

        if ($this->request->is(array('post'))) {
            if (isset($societyMemberList) && count($societyMemberList) > 0) {
                foreach ($societyMemberList as $memberId => $memberName) {
                    $options['AND'] = array('MemberBillSummary.member_id' => $memberId);
                    $accountMemberBillSummary = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('month' => 'asc')));
                    
                    if (isset($accountMemberBillSummary) && !empty($accountMemberBillSummary)) {
                        $summaryCounter = 0;
                        foreach ($accountMemberBillSummary as $billData) {
                            if (isset($billData['MemberBillSummary'])) {
                                $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter] = $billData['MemberBillSummary'];
                                $accountMemberLedgerDetails[$memberId]['Member'] = $billData['Member'];
                                //Change date format
                                $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['bill_generated_date'] = date('d/m/Y', strtotime($accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['bill_generated_date']));
                                if (isset($accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['balance_amount']) && $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['balance_amount'] > 0) {
                                    $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['debit_credit_value'] = "DR";
                                } else {
                                    $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['debit_credit_value'] = "CR";
                                }
                                $monthName = date("F", mktime(0, 0, 0, $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['month'], 10));
                                $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['monthName'] = $monthName;

                                $paymentMadeBillNo = isset($billData['MemberBillSummary']['bill_no']) ? $billData['MemberBillSummary']['bill_no'] : '';
                                $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $memberId, 'MemberPayment.bill_generated_id' => $paymentMadeBillNo);
                                if ($this->Member->MemberPayment->hasAny($conditionPayment)) {
                                    $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('conditions' => $conditionPayment));
                                    if (isset($memberPaymentDetails) && count($memberPaymentDetails) > 0) {
                                        $paymentCounter = 0;
                                        foreach ($memberPaymentDetails as $paymentData) {
                                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['MemberPayment'][$paymentCounter] = $paymentData['MemberPayment'];
                                            $accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['MemberPayment'][$paymentCounter]['payment_date'] = date('d/m/Y', strtotime($accountMemberLedgerDetails[$memberId]['MemberBillSummary'][$summaryCounter]['MemberPayment'][$paymentCounter]['payment_date']));
                                            $paymentCounter++;
                                        }
                                    }
                                }
                            }
                            $summaryCounter++;
                        }
                    }
                    
                    /*Journal voucher data*/
                    $journalVoucherMemberPaymentData = $this->SocietyBill->getJournalVoucherCreaditDebitDataByMemberId($memberId);
                    if (!empty($journalVoucherMemberPaymentData)){
                        $paymentCounter = 0;
                        foreach ($journalVoucherMemberPaymentData as $jvData) {
                            if ($jvData['JournalVoucher']['jv_credit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_credit_member_head_id'] == $memberId){
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['amount_paid'] = isset($jvData['JournalVoucher']['jv_amount_credited']) ? $jvData['JournalVoucher']['jv_amount_credited'] : 0.00;
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['payment_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $jvData['JournalVoucher']['voucher_date'] : "";
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['cheque_reference_number'] = '-';
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['particulars'] = "JV Credited";
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['voucher_no'] = $jvData['JournalVoucher']['voucher_no'];
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['type'] = 'Credit';
                                $paymentCounter++;
                            }

                            if ($jvData['JournalVoucher']['jv_debit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_debit_member_head_id'] == $memberId){
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['amount_paid'] = isset($jvData['JournalVoucher']['jv_amount_credited']) ? $jvData['JournalVoucher']['jv_amount_credited'] : 0.00;
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['payment_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $jvData['JournalVoucher']['voucher_date'] : "";
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['cheque_reference_number'] = '-';
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['particulars'] = "JV Debited";
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['voucher_no'] = $jvData['JournalVoucher']['voucher_no'];
                                $accountMemberLedgerDetails[$memberId]['JVData'][$paymentCounter]['type'] = 'Debit';
                                $paymentCounter++;
                            }
                        }
                    }
                }
            }
        }
        
        $this->set(compact('postData', 'accountMemberLedgerDetails', 'societyDetails'));
    }

    function bill_half_page() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $billingFrequenciesArray = $this->Util->societyBillingFrequency($this->societyBillingFrequencyId);
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');

        /*Society bill taiffs*/
        $societyLedgerHeadTitleList = array();
        $societyLedgerHeads = $this->SocietyTariffOrder->find('all', array('fields'=>array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'),'conditions' => array('SocietyLedgerHeads.society_id'=>$societyLoginID),'order'=>array('SocietyTariffOrder.tariff_serial' => 'ASC')));
        foreach($societyLedgerHeads as $head) {
            $societyLedgerHeadTitleList[$head['SocietyLedgerHeads']['id']] = $head['SocietyLedgerHeads']['title'];
        }
        
        $conditionQuery['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['month'], $this->request->data['MemberBillSummary']['month_to']);
        } else if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month'];
        } else if (isset($this->request->data['MemberBillSummary']['month_to']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_date'], $this->request->data['MemberBillSummary']['bill_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['OR']['MemberBillSummary.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_no'], $this->request->data['MemberBillSummary']['bill_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_no'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        $monthlyBillsSummaryDetails = array();
        $monthlyBillsSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $conditionQuery, 'order' => array('MemberBillSummary.bill_no' => 'asc')));

        if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
            $summaryCounter = 0;
            foreach ($monthlyBillsSummaryDetails as $monthlyData) {
                $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['month']);
                $tariffDetails = $this->MemberBillGenerate->find('all', array('fields' => array('MemberBillGenerate.ledger_head_id', 'MemberBillGenerate.amount', 'SocietyLedgerHeads.title'), 'conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.month' => $monthlyData['MemberBillSummary']['month'], 'MemberBillGenerate.member_id' => $monthlyData['MemberBillSummary']['member_id'], 'MemberBillGenerate.amount !=' => 0.00)));
                if (isset($tariffDetails) && !empty($tariffDetails)) {
                    foreach ($tariffDetails as $key => $tariffData) {
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    }
                }
                $summaryCounter++;
            }
        }

        $this->set(compact('postData', 'billingFrequenciesArray', 'societyDetails', 'societyParameters', 'monthlyBillsSummaryDetails','societyLedgerHeadTitleList'));
        if ($this->request->is('post')) {
            if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
                $this->render('account_bill_half_page');
            }
        }
    }

    function bill_tax_invoice_gst() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $billingFrequenciesArray = $this->Util->societyBillingFrequency($this->societyBillingFrequencyId);
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $memberDetailsByMemberIDAndFlatNo = $this->SocietyBill->getMemberDetailsByMemberIDAndFlatNo();
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');
        
        /*Society bill taiffs*/
        $societyLedgerHeadTitleList = array();
        $societyLedgerHeads = $this->SocietyTariffOrder->find('all', array('fields'=>array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'),'conditions' => array('SocietyLedgerHeads.society_id'=>$societyLoginID),'order'=>array('SocietyTariffOrder.tariff_serial' => 'ASC')));
        foreach($societyLedgerHeads as $head) {
            $societyLedgerHeadTitleList[$head['SocietyLedgerHeads']['id']] = $head['SocietyLedgerHeads']['title'];
        }
        
        $conditionQuery['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['month'], $this->request->data['MemberBillSummary']['month_to']);
        } else if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month'];
        } else if (isset($this->request->data['MemberBillSummary']['month_to']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_date'], $this->request->data['MemberBillSummary']['bill_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['OR']['MemberBillSummary.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_no'], $this->request->data['MemberBillSummary']['bill_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_no'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        $taxBillsSummaryDetails = array();
        $taxBillsSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $conditionQuery, 'order' => array('MemberBillSummary.bill_no' => 'asc')));

        if (isset($taxBillsSummaryDetails) && !empty($taxBillsSummaryDetails)) {
            $summaryCounter = 0;
            foreach ($taxBillsSummaryDetails as $monthlyData) {
                $taxBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($taxBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['month']);
                $tariffDetails = $this->MemberBillGenerate->find('all', array('fields' => array('MemberBillGenerate.ledger_head_id', 'MemberBillGenerate.amount', 'SocietyLedgerHeads.title', 'SocietyLedgerHeads.is_tax_applicable'), 'conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.month' => $monthlyData['MemberBillSummary']['month'], 'MemberBillGenerate.member_id' => $monthlyData['MemberBillSummary']['member_id'], 'MemberBillGenerate.amount !=' => 0.00)));
                if (isset($tariffDetails) && !empty($tariffDetails)) {
                    foreach ($tariffDetails as $key => $tariffData) {
                        if (isset($tariffData['SocietyLedgerHeads']['is_tax_applicable']) && $tariffData['SocietyLedgerHeads']['is_tax_applicable'] == 1) {
                            $taxBillsSummaryDetails[$summaryCounter]['taxGst']['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                            $taxBillsSummaryDetails[$summaryCounter]['taxGst']['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                            $taxBillsSummaryDetails[$summaryCounter]['taxGst']['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                        } else {
                            $taxBillsSummaryDetails[$summaryCounter]['noTaxGst']['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                            $taxBillsSummaryDetails[$summaryCounter]['noTaxGst']['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                            $taxBillsSummaryDetails[$summaryCounter]['noTaxGst']['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                        }
                    }
                }
                $summaryCounter++;
            }
        }
        $this->set(compact('postData', 'billingFrequenciesArray', 'societyDetails', 'memberDetailsByMemberIDAndFlatNo', 'societyParameters', 'taxBillsSummaryDetails','societyLedgerHeadTitleList'));

        if ($this->request->is('post')) {
            if (isset($taxBillsSummaryDetails) && !empty($taxBillsSummaryDetails)) {
                $this->render('account_tax_invoice_bill_gst');
            }
        }
    }

    function account_bank_slip() {
        
    }

    function account_journal_voucher_register() {
        
    }

    function account_member_list() {
        $societyLoginID = $this->societyloginID;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyMemberName = $this->Member->find('all', array('conditions' => array('Member.society_id' => $societyLoginID), 'fields' => array('Member.member_name', 'Member.flat_no', 'Member.member_phone', 'Member.area')));
        //print_r($societyLoginID);die;
        $this->set(compact('societyDetails', 'societyMemberName'));
    }

    function account_collection_sheet() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberPayment');
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $postData = $this->request->data;
       
        
        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['CollectionSheet']['building_id']) && !empty($this->request->data['CollectionSheet']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['CollectionSheet']['building_id'];
        }
        if (isset($this->request->data['CollectionSheet']['wing_id']) && !empty($this->request->data['CollectionSheet']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['CollectionSheet']['wing_id'];
        }
        if (isset($this->request->data['CollectionSheet']['flat_no']) && !empty($this->request->data['CollectionSheet']['flat_no']) && !empty($this->request->data['CollectionSheet']['flat_no_to'])) {
            $options['AND']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['CollectionSheet']['flat_no'], $this->request->data['CollectionSheet']['flat_no_to']);
        } else if (isset($this->request->data['CollectionSheet']['flat_no_to']) && !empty($this->request->data['CollectionSheet']['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['CollectionSheet']['flat_no_to'];
        } else if (isset($this->request->data['CollectionSheet']['flat_no']) && !empty($this->request->data['CollectionSheet']['flat_no'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['CollectionSheet']['flat_no'];
        }
        
        if (isset($this->request->data['CollectionSheet']['payment_date']) && !empty($this->request->data['CollectionSheet']['payment_date']) && !empty($this->request->data['CollectionSheet']['payment_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['CollectionSheet']['payment_date'], $this->request->data['CollectionSheet']['payment_date_to']);
        } else if (isset($this->request->data['CollectionSheet']['payment_date']) && !empty($this->request->data['CollectionSheet']['payment_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['CollectionSheet']['payment_date'];
        } else if (isset($this->request->data['CollectionSheet']['payment_date_to']) && !empty($this->request->data['CollectionSheet']['payment_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['CollectionSheet']['payment_date_to'];
        }
        
        $collectionSheetDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_no' => 'asc')));

        if (isset($collectionSheetDetails) && !empty($collectionSheetDetails)) {
            $summaryCounter = 0;
            foreach ($collectionSheetDetails as $collectionData) {                
                $collectionSheetDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($collectionSheetDetails[$summaryCounter]['MemberBillSummary']['month']);                
                $summaryCounter++;
            }
        }
        //print_r($collectionSheetDetails);die;
        $this->set(compact('societyDetails','postData','collectionSheetDetails'));
    }

    function account_trail_balalnce() {
        
    }

    function account_bill_register() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $this->loadModel('Member');
        $this->loadModel('MemberBillGenerate');
        $this->loadModel('MemberBillSummary');
        $societyDetails = $this->SocietyBill->getSocietyDetails();


        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);


        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $options['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date_to'])) {
            $options['OR']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_generated_date'], $this->request->data['MemberBillSummary']['bill_generated_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_generated_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_generated_date_to'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_generated_date'];
        }

        $billSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_generated_date' => 'asc', 'MemberBillSummary.bill_no' => 'asc')));


        //print_r($this->request->data);die;
        $memberDetails = $this->Member->find('all', array('conditions' => array('Member.society_id' => $societyLoginID)));
        $membersArray = array();
        if (!empty($memberDetails)) {
            foreach ($memberDetails as $member) {
                $membersArray[$member['Member']['id']] = $member['Member'];
            }
        }

        $billRegisterData = array();

        $uniqueTariffsInBills = array();

        if (!empty($billSummaryDetails)) {
            foreach ($billSummaryDetails as $billDetails) {
                $memberBillDetails = $billDetails['MemberBillSummary'];

                $billRegisterData[$memberBillDetails['id']]['MemberBillSummary'] = $billDetails['MemberBillSummary'];

                $billTariffDetails = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.member_id' => $memberBillDetails['member_id'], 'MemberBillGenerate.bill_number' => $memberBillDetails['bill_no'])));
                if (!empty($billTariffDetails)) {
                    foreach ($billTariffDetails as $tariff) {
                        $memberBillTariff = $tariff['MemberBillGenerate'];
                        $billRegisterData[$memberBillDetails['id']]['MemberBillGenerate'][$memberBillTariff['ledger_head_id']] = $memberBillTariff['amount'];

                        if (!array_key_exists($tariff['MemberBillGenerate']['ledger_head_id'], $uniqueTariffsInBills)) {
                            $uniqueTariffsInBills[$tariff['MemberBillGenerate']['ledger_head_id']] = $tariff['SocietyLedgerHeads']['title'];
                        }
                    }
                }
            }
        }

        //print_r($uniqueTariffsInBills);die;

        $this->set(compact('billRegisterData', 'societyDetails', 'postData', 'membersArray', 'uniqueTariffsInBills'));
    }

    function account_gst_register() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('Member');
        $this->loadModel('MemberBillGenerate');
        $this->loadModel('MemberBillSummary');
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyParameters = $this->SocietyBill->getSocietyParameters();

        $options['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $options['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $options['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $options['OR']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $options['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date_to'])) {
            $options['OR']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_generated_date'], $this->request->data['MemberBillSummary']['bill_generated_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_generated_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date_to'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_generated_date_to'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_generated_date']) && !empty($this->request->data['MemberBillSummary']['bill_generated_date'])) {
            $options['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_generated_date'];
        }

        $billSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $options, 'order' => array('MemberBillSummary.bill_generated_date' => 'asc', 'MemberBillSummary.bill_no' => 'asc')));

        $memberDetails = $this->Member->find('all', array('conditions' => array('Member.society_id' => $societyLoginID)));
        $membersArray = array();
        if (!empty($memberDetails)) {
            foreach ($memberDetails as $member) {
                $membersArray[$member['Member']['id']] = $member['Member'];
            }
        }

        $gstRegisterData = array();
        $uniqueTariffsInBills = array();
        if (!empty($billSummaryDetails)) {
            foreach ($billSummaryDetails as $billDetails) {
                $memberBillDetails = $billDetails['MemberBillSummary'];

                $gstRegisterData[$memberBillDetails['id']]['MemberBillSummary'] = array('bill_no' => $billDetails['MemberBillSummary']['bill_no'], 'bill_date' => $billDetails['MemberBillSummary']['bill_generated_date'], 'member_id' => $billDetails['MemberBillSummary']['member_id'], 'gstin_no' => $billDetails['Society']['gstin_no']);

                $billTariffDetails = $this->MemberBillGenerate->find('all', array('conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.member_id' => $memberBillDetails['member_id'], 'MemberBillGenerate.bill_number' => $memberBillDetails['bill_no'], 'SocietyLedgerHeads.is_tax_applicable' => 1)));
                if (!empty($billTariffDetails)) {
                    foreach ($billTariffDetails as $tariff) {
                        $memberBillTariff = $tariff['MemberBillGenerate'];
                        $gstRegisterData[$memberBillDetails['id']]['MemberBillGenerate'][$memberBillTariff['ledger_head_id']] = array('amount' => $memberBillTariff['amount'], 'cgst' => $memberBillTariff['cgst_total'], 'sgst' => $memberBillTariff['sgst_total']);

                        if (!array_key_exists($tariff['MemberBillGenerate']['ledger_head_id'], $uniqueTariffsInBills)) {
                            $uniqueTariffsInBills[$tariff['MemberBillGenerate']['ledger_head_id']] = $tariff['SocietyLedgerHeads']['title'];
                        }
                    }
                }
            }
        }

        //print_r($billSummaryDetails);die;
        $this->set(compact('gstRegisterData', 'societyDetails', 'postData', 'membersArray', 'societyParameters', 'uniqueTariffsInBills'));
    }

    function bill_full_page() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $billingFrequenciesArray = $this->Util->societyBillingFrequency($this->societyBillingFrequencyId);
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $wingDetails = $this->SocietyBill->getSocietyWingListsById();
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');

        /*Society bill taiffs*/
        $societyLedgerHeadTitleList = array();
        $societyLedgerHeads = $this->SocietyTariffOrder->find('all', array('fields'=>array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'),'conditions' => array('SocietyLedgerHeads.society_id'=>$societyLoginID),'order'=>array('SocietyTariffOrder.tariff_serial' => 'ASC')));
        foreach($societyLedgerHeads as $head) {
            $societyLedgerHeadTitleList[$head['SocietyLedgerHeads']['id']] = $head['SocietyLedgerHeads']['title'];
        }

        $conditionQuery['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['month'], $this->request->data['MemberBillSummary']['month_to']);
        } else if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month'];
        } else if (isset($this->request->data['MemberBillSummary']['month_to']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_date'], $this->request->data['MemberBillSummary']['bill_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['OR']['MemberBillSummary.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_no'], $this->request->data['MemberBillSummary']['bill_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_no'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        $monthlyBillsSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $conditionQuery, 'order' => array('MemberBillSummary.bill_no' => 'asc')));

        if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
            $summaryCounter = 0;
            foreach ($monthlyBillsSummaryDetails as $monthlyData) {
                $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['month']);
                $monthlyBillsSummaryDetails[$summaryCounter]['Member']['wing_name'] = isset($wingDetails[$monthlyBillsSummaryDetails[$summaryCounter]['Member']['wing_id']]) ? $wingDetails[$monthlyBillsSummaryDetails[$summaryCounter]['Member']['wing_id']] : '';
                $tariffDetails = $this->MemberBillGenerate->find('all', array('fields' => array('MemberBillGenerate.ledger_head_id', 'MemberBillGenerate.amount', 'SocietyLedgerHeads.title'), 'conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.month' => $monthlyData['MemberBillSummary']['month'], 'MemberBillGenerate.member_id' => $monthlyData['MemberBillSummary']['member_id'], 'MemberBillGenerate.amount !=' => 0.00)));
                
                if (isset($tariffDetails) && !empty($tariffDetails)) {
                    foreach ($tariffDetails as $key => $tariffData) {
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    }
                }
                $summaryCounter++;
            }
        }

        $this->set(compact('postData', 'billingFrequenciesArray', 'societyDetails', 'societyParameters', 'monthlyBillsSummaryDetails','societyLedgerHeadTitleList'));
        if ($this->request->is('post')) {
            if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
                $this->render('account_bill_full_page');
            }
        }
    }

    public function bill_with_interest_gst() {
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data; 
        $billingFrequenciesArray = $this->Util->societyBillingFrequency($this->societyBillingFrequencyId);
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyParameters = $this->SocietyBill->getSocietyParameters();
        $wingDetails = $this->SocietyBill->getSocietyWingListsById();
        //print_r($societyParameters);die;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');

        /*Society bill taiffs*/
        $societyLedgerHeadTitleList = array();
        $societyLedgerHeads = $this->SocietyTariffOrder->find('all', array('fields'=>array('SocietyLedgerHeads.id','SocietyLedgerHeads.title'),'conditions' => array('SocietyLedgerHeads.society_id'=>$societyLoginID),'order'=>array('SocietyTariffOrder.tariff_serial' => 'ASC')));
        foreach($societyLedgerHeads as $head) {
            $societyLedgerHeadTitleList[$head['SocietyLedgerHeads']['id']] = $head['SocietyLedgerHeads']['title'];
        }
        
        $conditionQuery['AND'] = array('MemberBillSummary.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['month'], $this->request->data['MemberBillSummary']['month_to']);
        } else if (isset($this->request->data['MemberBillSummary']['month']) && !empty($this->request->data['MemberBillSummary']['month'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month'];
        } else if (isset($this->request->data['MemberBillSummary']['month_to']) && !empty($this->request->data['MemberBillSummary']['month_to'])) {
            $conditionQuery['AND']['MemberBillSummary.month'] = $this->request->data['MemberBillSummary']['month_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_date'], $this->request->data['MemberBillSummary']['bill_date_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_date']) && !empty($this->request->data['MemberBillSummary']['bill_date'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_date_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_generated_date'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['OR']['MemberBillSummary.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['flat_no'], $this->request->data['MemberBillSummary']['flat_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['flat_no_to']) && !empty($this->request->data['MemberBillSummary']['flat_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no_to'];
        } else if (isset($this->request->data['MemberBillSummary']['flat_no']) && !empty($this->request->data['MemberBillSummary']['flat_no'])) {
            $conditionQuery['AND']['MemberBillSummary.flat_no'] = $this->request->data['MemberBillSummary']['flat_no'];
        }

        if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no BETWEEN ? and ?'] = array($this->request->data['MemberBillSummary']['bill_no'], $this->request->data['MemberBillSummary']['bill_no_to']);
        } else if (isset($this->request->data['MemberBillSummary']['bill_no']) && !empty($this->request->data['MemberBillSummary']['bill_no'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_no'];
        } else if (isset($this->request->data['MemberBillSummary']['bill_date_to']) && !empty($this->request->data['MemberBillSummary']['bill_no_to'])) {
            $conditionQuery['AND']['MemberBillSummary.bill_no'] = $this->request->data['MemberBillSummary']['bill_date_to'];
        }

        $monthlyBillsSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => $conditionQuery, 'order' => array('MemberBillSummary.bill_no' => 'asc')));
        
        if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
            $summaryCounter = 0;
            foreach ($monthlyBillsSummaryDetails as $monthlyData) {
                $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['monthName'] = $this->SocietyBill->monthWordFormatByBillingFrequency($monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['month']);
                $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('conditions' => array('MemberPayment.member_id' => $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['member_id'],'MemberPayment.member_id' => $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['member_id']),'recursive'=>-1));
                if(isset($memberPaymentDetails) && count($memberPaymentDetails) > 0){
                    $monthlyBillsSummaryDetails[$summaryCounter]['receipts'] = $memberPaymentDetails;
                }
                $tariffDetails = $this->MemberBillGenerate->find('all', array('fields' => array('MemberBillGenerate.ledger_head_id', 'MemberBillGenerate.amount','SocietyLedgerHeads.title','SocietyLedgerHeads.is_tax_applicable'), 'conditions' => array('MemberBillGenerate.society_id' => $societyLoginID, 'MemberBillGenerate.month' => $monthlyData['MemberBillSummary']['month'], 'MemberBillGenerate.member_id' => $monthlyData['MemberBillSummary']['member_id'], 'MemberBillGenerate.amount !=' => 0.00)));
                
                $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['total_taxable_amt'] = 0;
                if (isset($tariffDetails) && !empty($tariffDetails)) {
                    foreach ($tariffDetails as $key => $tariffData) {
                        if($tariffData['SocietyLedgerHeads']['is_tax_applicable'] == Configure::read('Active')){
                            $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['total_taxable_amt'] = $monthlyBillsSummaryDetails[$summaryCounter]['MemberBillSummary']['total_taxable_amt'] + $tariffData['MemberBillGenerate']['amount'];
                        }
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['ledger_head_id'] = isset($tariffData['MemberBillGenerate']['ledger_head_id']) ? $tariffData['MemberBillGenerate']['ledger_head_id'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['amount'] = isset($tariffData['MemberBillGenerate']['amount']) ? $tariffData['MemberBillGenerate']['amount'] : '';
                        $monthlyBillsSummaryDetails[$summaryCounter]['MemberTariff'][$key]['title'] = isset($tariffData['SocietyLedgerHeads']['title']) ? $tariffData['SocietyLedgerHeads']['title'] : '';
                    }
                }
                $summaryCounter++;
            }
        }
        
        $this->set(compact('postData', 'billingFrequenciesArray', 'societyDetails', 'societyParameters', 'wingDetails','monthlyBillsSummaryDetails','societyLedgerHeadTitleList'));
        if ($this->request->is('post')) {
            if (isset($monthlyBillsSummaryDetails) && !empty($monthlyBillsSummaryDetails)) {
                $this->render('account_bill_with_interest_gst');
            }
        }
    }
    public function member_collection_register(){
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $this->Member->Behaviors->attach('Containable');
        $optionsMember['AND'] = array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'));


        if (isset($this->request->data['MemberPayment']['building_id']) && !empty($this->request->data['MemberPayment']['building_id'])) {
            $optionsMember['AND']['Member.building_id'] = $this->request->data['MemberPayment']['building_id'];
        }
        if (isset($this->request->data['MemberPayment']['wing_id']) && !empty($this->request->data['MemberPayment']['wing_id'])) {
            $optionsMember['AND']['Member.wing_id'] = $this->request->data['MemberPayment']['wing_id'];
        }

        if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['flat_no'], $this->request->data['MemberPayment']['flat_no_to']);
        } else if (isset($this->request->data['MemberPayment']['flat_no_to']) && !empty($this->request->data['MemberPayment']['flat_no_to'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no_to'];
        } else if (isset($this->request->data['MemberPayment']['flat_no']) && !empty($this->request->data['MemberPayment']['flat_no'])) {
            $optionsMember['AND']['Member.flat_no'] = $this->request->data['MemberPayment']['flat_no'];
        }
        $conditionPayment = '';
        if (isset($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date_to'])) {
            $conditionPayment['OR']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['payment_date'], $this->request->data['MemberPayment']['payment_date_to']);
        } else if (isset($this->request->data['MemberPayment']['payment_date_to']) && !empty($this->request->data['MemberPayment']['payment_date_to'])) {
            $conditionPayment['AND']['MemberPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date_to'];
        } else if (isset($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date'])) {
            $conditionPayment['AND']['MemberPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date'];
        }

        $societyMemberPaymentCollectionData = $this->Member->find('all', array('contain' => array('Society', 'Building', 'Wing', 'MemberPayment' => array('conditions' => $conditionPayment)), 'conditions' => $optionsMember, 'order' => array('Member.id' => 'asc')));


        $this->set(compact('postData', 'societyDetails', 'societyMemberPaymentCollectionData'));
    }
    
    public function account_receipt_payment(){
        $this->loadModel('SocietyLedgerHeads');
        $societyLoginID = $this->societyloginID;
        $postData = $this->request->data;
        $societyDetails = $this->SocietyBill->getSocietyDetails();
        $societyBankBalanceDetails = $this->SocietyBill->societyBankBalanceHeadsLists();
        $societyCashInHandetails = $this->SocietyBill->societyCashBalanceHeadsLists();
        $cashBankLedgersArray = $societyBankBalanceDetails + $societyCashInHandetails;
        $recieptPaymentHeader = array('Sr.', 'Particular_Receipt');

        if (isset($cashBankLedgersArray) && count($cashBankLedgersArray) > 0) {
            foreach ($cashBankLedgersArray as $ledgerName) {
                array_push($recieptPaymentHeader, $ledgerName);
            }
            array_push($recieptPaymentHeader, 'Total');
            array_push($recieptPaymentHeader, 'Particular_Payment');
            foreach ($cashBankLedgersArray as $ledgerName) {
                array_push($recieptPaymentHeader, $ledgerName);
            }
            array_push($recieptPaymentHeader, 'Total');
        }

        $recieptPaymentDetails = array();
        $counter = 0;
        $recieptPaymentDetails[$counter]['sr'] = $counter + 1;
        $recieptPaymentDetails[$counter]['member_reciept'] = "To Opening Balance";
        $cashLedgerOpeningAmtTotal = 0;

        foreach ($cashBankLedgersArray as $ledgerId => $ledgerName) {
            $ledgerHeadDetails = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $ledgerId, 'SocietyLedgerHeads.society_id' => $societyLoginID), 'recursive' => -1));
            $cashLedgerOpeningAmtTotal += $recieptPaymentDetails[$counter]['rec_' . $ledgerId] = isset($ledgerHeadDetails['SocietyLedgerHeads']['opening_amount']) ? $ledgerHeadDetails['SocietyLedgerHeads']['opening_amount'] : "0.00";
        }
        $recieptPaymentDetails[$counter]['r_total'] = $cashLedgerOpeningAmtTotal;
        $recieptPaymentDetails[$counter]['payment'] = "By Opening Balance";
        foreach ($cashBankLedgersArray as $ledgerId => $ledgerName) {
            $recieptPaymentDetails[$counter]['pay_' . $ledgerId] = "0.00";
        }
        $recieptPaymentDetails[$counter]['p_total'] = "0.00";
        $counter++;

        $recieptPaymentDetails[$counter]['sr'] = $counter + 1;
        $recieptPaymentDetails[$counter]['member_reciept'] = "Member Reciept";
        $recieptAmtTotal = 0;
        $this->MemberPayment->virtualFields = array('amount_sum' => 'sum(amount_paid)');
        foreach ($cashBankLedgersArray as $ledgerId => $ledgerName) {
            $MemberRecieptDetails = $this->MemberPayment->find('first', array('fields' => array('MemberPayment.id', 'amount_sum'), 'conditions' => array('MemberPayment.society_bank_id' => $ledgerId, 'MemberPayment.society_id' => $societyLoginID), 'group' => ('MemberPayment.society_bank_id'), 'recursive' => -1));
            $recieptAmtTotal += $recieptPaymentDetails[$counter]['rec_' . $ledgerId] = isset($MemberRecieptDetails['MemberPayment']['amount_sum']) ? $MemberRecieptDetails['MemberPayment']['amount_sum'] : "0.00";
        }
        $recieptPaymentDetails[$counter]['r_total'] = $recieptAmtTotal;
        $recieptPaymentDetails[$counter]['payment'] = "payment";
        $societyPaymentAmtTotal = 0;
        $this->SocietyPayment->virtualFields = array('amount_sum' => 'sum(total_amount)');
        foreach ($cashBankLedgersArray as $ledgerId => $ledgerName) {
            $MemberRecieptDetails = $this->SocietyPayment->find('first', array('fields' => array('SocietyPayment.id', 'amount_sum'), 'conditions' => array('SocietyPayment.payment_by_ledger_id' => $ledgerId, 'SocietyPayment.society_id' => $societyLoginID), 'group' => ('SocietyPayment.payment_by_ledger_id'), 'recursive' => -1));
            $societyPaymentAmtTotal += $recieptPaymentDetails[$counter]['pay_' . $ledgerId] = isset($MemberRecieptDetails['SocietyPayment']['amount_sum']) ? $MemberRecieptDetails['SocietyPayment']['amount_sum'] : "0.00";
        }
        $recieptPaymentDetails[$counter]['p_total'] = $societyPaymentAmtTotal;

        $counter++;
        $recieptPaymentDetails[$counter]['sr'] = $counter + 1;
        $recieptPaymentDetails[$counter + 1]['sr'] = $counter + 1;
        foreach ($recieptPaymentDetails as $totalData) {
            foreach ($totalData as $ledgerKey => $value) {
                if (is_numeric($value)) {
                    $recieptPaymentDetails[$counter + 1][$ledgerKey] = isset($recieptPaymentDetails[$counter + 1][$ledgerKey]) ? $recieptPaymentDetails[$counter + 1][$ledgerKey] + $value : $value;
                    $recieptPaymentDetails[$counter][$ledgerKey] = isset($recieptPaymentDetails[$counter][$ledgerKey]) ? $recieptPaymentDetails[$counter][$ledgerKey] + $value : $value;
                } else {
                    $recieptPaymentDetails[$counter + 1][$ledgerKey] = "Closing Balance";
                    $recieptPaymentDetails[$counter][$ledgerKey] = "Total";
                }
            }
        }

        $counter++;

        $this->set(compact('postData', 'societyDetails', 'recieptPaymentHeader', 'recieptPaymentDetails'));
    }

}