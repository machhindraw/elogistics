<body>
    <?php
    //print_r($menus['menu']);die;
    ?>
    <div class="preloader-it">
        <div class="la-anim-1"></div>
    </div>
    <div class="wrapper theme-1-active pimary-color-red">
        <!-- Top Menu Items -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="mobile-only-brand pull-left">
                <div class="nav-header pull-left">
                    <div class="logo-wrap">
                        <a href="index.html">
                            <img class="brand-img" src="<?php echo $this->webroot; ?>img/fvcon.png" alt="brand"/>
                            <span class="brand-text">Easy Logics</span>
                            
                        </a>
                    </div>
                </div>	
                <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
              <!--  <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a> -->
                <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
               <!-- <form id="search_form" role="search" class="top-nav-search collapse pull-left">
                    <div class="input-group">
                        <input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
                        </span>
                    </div>
                </form> -->
               
            </div>
            <?php if ($this->Session->read('Auth.User.role') == 'Society'){ ?>
            <div class="dash-top-header pull-left">
                   <div class="dash-top-header-menu">
                    <?php //echo $this->Html->link('Ledger Heads',array('controller' => 'SocietyBillsController','action' => 'ledger_head_bills')); ?>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#society_ledger_heads_modal');"><i class="fa fa-check-square-o"></i> Ledger Heads</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#flat_shop_details');"><i class="fa fa-list"></i> Flat / Shop Detail</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#bill_modal');"><i class="fa fa-file-text-o"></i> Bill</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#member_receipt_modal');"><i class="fa fa-outdent"></i> Member Receipts</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#payment_entry_modal');"><i class="fa fa-rupee"></i> Payment Entry</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#Member_Generate_Bill');"><i class="fa fa-th-list"></i> Generate Bill</a>
                     <a href="javascript:void(0);" onclick="loadSocietyBillsModel('#print_bill_modal');"><i class="fa fa-print"></i> Print Bill</a>
                     <!--<a href="javascript:void(0);" onclick="loadSocietyBillsModel('#print_bill');"><i class="fa fa-print"></i> Print Bill</a>-->
                   </div>
            </div>
           <?php } ?>
            <div id="mobile_only_nav" class="mobile-only-nav pull-right">
                <ul class="nav navbar-right top-nav pull-right">
                    <li class="dropdown auth-drp">
                        <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="<?php echo $this->webroot; ?>img/user2.png" alt="user_auth" class="user-auth-img img-circle"/> &nbsp; <?php if($this->Session->read('Auth.User')){ ?><?php echo $this->Session->read('Auth.User.username'); ?><?php } ?><span class="user-online-status"></span></a>
                        <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                            <li>
                                <?php echo $this->Html->link('<i class="fa fa-compass"></i><span>Change Password</span>',array('controller' => 'societys','action' => 'change_password'),array('escape' => false)); ?>
                            </li>
                            <li class="divider"></li>
                            <?php if ($this->Session->read('Auth.User')){ ?>
                            <li> <?php echo $this->Html->link('<i class="zmdi zmdi-power"></i><span>Log Out</span>',array('controller' => 'Users', 'action' => 'logout'),array('escape' => false)); ?></li>
                            <?php } 
                            if($this->Session->read('Auth.reseller_Flag') && $this->Session->read('Auth.reseller_Flag') == 1){
                            ?>
                            <li> <a href="javascript:void(0);" onclick="societyAutoLogin('<?php echo $this->Session->read('Auth.reseller_username'); ?>', '<?php echo $this->Session->read('Auth.reseller_password'); ?>', 1)"><i class="zmdi zmdi-city-alt mr-20"></i><span>Go To Another Society</span></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>
            
        </nav>
        <div class="fixed-sidebar-left">
            <ul class="nav navbar-nav side-nav nicescroll-bar">
                <li class="navigation-header">
                    <span>Main</span> 
                    <i class="zmdi zmdi-more"></i>
                </li>
                <?php if(isset($menus['menu'])) {
                    $menuId = '';
                    foreach ($menus['menu'] as $mkey => $menu) { 
                        $menuId++;
                        $menuLink = '#.';
                        $menuICon = isset($menu['icon']) ? $menu['icon'] : 'zmdi zmdi-google-pages mr-20';

                    ?>
                    <?php if(isset($menu['link'])) { 
                        $menuLink = $menu['link'];
                        ?>
                <li>
                    <a class="collapsed" onClick="selectMenu(<?php echo $menuId; ?>)" id="menu-<?php echo $menuId; ?>" href="<?php echo $menuLink; ?>"><div class="pull-left"><?php echo $menuICon; ?><span class="right-nav-text"><?php echo $mkey; ?></span></div><div class="clearfix"></div></a>
                </li>
                    <?php } ?>
                    <?php if(isset($menu['submenu'])) { ?>
                <li>
                    <a class="" href="javascript:void(0);" onClick="selectMenu(<?php echo $menuId; ?>)" id="menu-<?php echo $menuId; ?>" data-toggle="collapse" data-target="#dashboard_dr<?php echo $menuId; ?>"><div class="pull-left"><?php echo $menuICon; ?><span class="right-nav-text"><?php echo $mkey; ?></span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                    <ul id="dashboard_dr<?php echo $menuId; ?>" class="collapse collapse-level-1">
                        <?php 
                        if(count($menu['submenu']) > 0):
                            foreach ($menu['submenu'] as $smkey => $smvalue) {
                            $submenuLink = isset($smkey) ? $smkey : '#.';
                        ?>
                        <li>
                            <a  href="<?php echo $submenuLink; ?>"><?php echo $smvalue; ?></a>
                        </li>
                        <?php  } endif; ?>
                    </ul>
                </li>
                    <?php } ?>    
                    <?php }}?>
            </ul>
        </div>
        <div class="right-sidebar-backdrop"></div>
        <div class="page-wrapper">