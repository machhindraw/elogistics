<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * society members 
 */

class SocietysMembersController extends AppController {

    public $components = array('Session', 'RequestHandler', 'Util', 'SocietyBill','PhpExcel');
    public $helpers = array('Js','PhpExcel');
    public $societyloginID = '';
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
        $this->societyloginID = $this->Session->read('Auth.User.id');
        $this->set("utilObj", $this->Util);
        $this->set("societyBillObj",$this->SocietyBill);
        $this->loadModel('User');
        $this->loadModel('Member');
        $this->loadModel('Society');
        $this->loadAllNavigationModals();
    }

    function loadAllNavigationModals(){
        $this->SocietyBill->getSocietyBuildingLists();
        $this->SocietyBill->getSocietyLedgerHeadsDetails();
        $this->SocietyBill->navigateSocietyMemberDetails();
        $this->SocietyBill->loadAllNavigationModalsDropdowns();
    }

    public function member_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $societyDetails = $this->SocietyBill->getSocietyDetails(); 
        $this->loadModel('MemberTariffDetails');
        //$this->SocietyLedgerHeads->recursive = -1;
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }
        
        $societyMemberFlatLists = $this->SocietyBill->getSocietyMemberFlatList();

        $postMemberTariff = array();
        if ($this->request->is('post', 'put')) {
            $postMemberTariff['society_id'] = $societyLoginID;
            $postMemberTariff['member_id'] = isset($this->request->data['MemberTariff']['member_id']) ? $this->request->data['MemberTariff']['member_id'] : '';
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            if (isset($this->request->data['MemberTariff']['ledger_head_id']) && count($this->request->data['MemberTariff']['ledger_head_id']) > 0) {
                foreach ($this->request->data['MemberTariff']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->MemberTariff->create();
                        $societyMemberExists = $this->MemberTariff->find('first', array('conditions' => array('MemberTariff.member_id' => $postMemberTariff['member_id'], 'MemberTariff.ledger_head_id' => $societyLedgerId, 'MemberTariff.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyMemberExists)) {
                            $this->MemberTariff->id = $societyMemberExists['MemberTariff']['id'];
                            $message = 'updated';
                        }
                        $postMemberTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postMemberTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postMemberTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postMemberTariff['updated_date'] = $this->Util->getDateTime();
                        if ($this->MemberTariff->save($postMemberTariff)) {
                            $this->Session->setFlash(__('The member tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }

            $postMemberTariffDetails['member_id'] = isset($this->request->data['MemberTariff']['member_id']) ? $this->request->data['MemberTariff']['member_id'] : '';
            $effectiveDate = $postMemberTariffDetails['tariff_effective_since'] = (isset($this->request->data['MemberTariffDetails']['member_id']) && !empty($this->request->data['MemberTariffDetails']['member_id'])) ? $this->request->data['MemberTariffDetails']['tariff_effective_since'] : $this->Util->getDateTime('Y-m-d');
            $postMemberTariffDetails['remark'] = isset($this->request->data['MemberTariffDetails']['remark']) ? $this->request->data['MemberTariffDetails']['remark'] : '';
            $postMemberTariffDetails['updated_date'] = $this->Util->getDateTime();

            $societyMemberTariffDetailsExists = $this->MemberTariffDetails->find('first', array('conditions' => array('MemberTariffDetails.member_id' => $postMemberTariffDetails['member_id'], 'MemberTariffDetails.tariff_effective_since' => $effectiveDate)));
            if (isset($societyMemberTariffDetailsExists) && count($societyMemberTariffDetailsExists) > 0) {
                $this->MemberTariffDetails->id = $societyMemberTariffDetailsExists['MemberTariffDetails']['id'];
            }

            $this->MemberTariffDetails->save($postMemberTariffDetails);
        }
        $this->set(compact('societyMemberFlatLists','societyLedgerHeadList','societyDetails'));
    }

    public function society_tariffs() {
        $societyLoginID = $this->societyloginID;
                
        $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
        
        switch($societyTariffParameterCheck['SocietyParameter']['tariff_id']) {
            case Configure::read('TariffType.Building'):
                $this->redirect(array('controller' => 'societys_members','action' =>'society_building_tariffs'));
                exit;
                break;
            case Configure::read('TariffType.Wing') :
                $this->redirect(array('controller' => 'societys_members','action' =>'society_wing_tariffs'));
                exit;
                break;
            case Configure::read('TariffType.UnitNo') :
                $this->redirect(array('controller' => 'societys_members','action' => 'member_tariffs'));
                exit;
                break;
            case Configure::read('TariffType.UnitType') :
                $this->redirect(array('controller' => 'societys_members','action' => 'society_unit_type_tariffs'));
               exit;
                break;
            case Configure::read('TariffType.TotalArea') :
                $this->redirect(array('controller' => 'societys_members','action' => 'society_total_area_tariffs'));
                exit;
                break;
            case Configure::read('TariffType.PerUnitArea') :
                $this->redirect(array('controller' => 'societys_members','action' => 'society_per_area_tariffs'));
                exit;
                break;
            case Configure::read('TariffType.PerPerson') :
                $this->redirect(array('controller' => 'societys_members','action' => 'member_tariffs'));
                exit;
                break;
        }   
    }
    
    public function society_building_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('Building');
        $this->loadModel('SocietyTariffBuildings');
        
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }

        $societyBuildingLists = $this->Society->Building->find('list', array('conditions' => array('Building.society_id' => $societyLoginID,'Building.status' => 1), 'fields' => array('id', 'building_name'), 'order' => array('Building.building_name' => 'asc')));

        $postSocietyTariff = array();
        if ($this->request->is('post', 'put')) {
            $postSocietyTariff['society_id'] = $societyLoginID;
            $postSocietyTariff['building_id'] = isset($this->request->data['SocietyTariffBuildings']['building_id']) ? $this->request->data['SocietyTariffBuildings']['building_id'] : '';
            $postSocietyTariff['effective_date'] = isset($this->request->data['SocietyTariffBuildings']['effective_date']) ? $this->request->data['SocietyTariffBuildings']['effective_date'] : '';
            
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            
           // echo '<pre>';print_r($this->request->data['SocietyTariffBuildings']['ledger_head_id']);exit;
                    
            if (isset($this->request->data['SocietyTariffBuildings']['ledger_head_id']) && count($this->request->data['SocietyTariffBuildings']['ledger_head_id']) > 0) {
                foreach ($this->request->data['SocietyTariffBuildings']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->SocietyTariffBuildings->create();
                        $societyBuildingExists = $this->SocietyTariffBuildings->find('first', array('conditions' => array('SocietyTariffBuildings.building_id' => $postSocietyTariff['building_id'], 'SocietyTariffBuildings.ledger_head_id' => $societyLedgerId, 'SocietyTariffBuildings.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyBuildingExists)) {
                            $this->SocietyTariffBuildings->id = $societyBuildingExists['SocietyTariffBuildings']['id'];
                            $message = 'updated';
                        }
                        $postSocietyTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postSocietyTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postSocietyTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postSocietyTariff['updated_date'] = $this->Util->getDateTime();
                        if ($this->SocietyTariffBuildings->save($postSocietyTariff)) {
                            $this->Session->setFlash(__('The society tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }
        }
        $this->set(compact('societyBuildingLists', 'societyLedgerHeadList'));
    }

    public function society_wing_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('Building');
        $this->loadModel('SocietyTariffWings');
        
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }

        $societyBuildingLists = $this->Society->Building->find('list', array('conditions' => array('Building.society_id' => $societyLoginID), 'fields' => array('id', 'building_name'), 'order' => array('Building.building_name' => 'asc')));

        $postSocietyTariff = array();
        if ($this->request->is('post', 'put')) {
            $postSocietyTariff['society_id'] = $societyLoginID;
            $postSocietyTariff['building_id'] = isset($this->request->data['SocietyTariffWings']['building_id']) ? $this->request->data['SocietyTariffWings']['building_id'] : '';
            $postSocietyTariff['wing_id'] = isset($this->request->data['SocietyTariffWings']['wing_id']) ? $this->request->data['SocietyTariffWings']['wing_id'] : '';
            $postSocietyTariff['effective_date'] = isset($this->request->data['SocietyTariffWings']['effective_date']) ? $this->request->data['SocietyTariffWings']['effective_date'] : '';
            
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            
           // echo '<pre>';print_r($this->request->data['SocietyTariffBuildings']['ledger_head_id']);exit;
                    
            if (isset($this->request->data['SocietyTariffWings']['ledger_head_id']) && count($this->request->data['SocietyTariffWings']['ledger_head_id']) > 0) {
                foreach ($this->request->data['SocietyTariffWings']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->SocietyTariffWings->create();
                        $societyWingExists = $this->SocietyTariffWings->find('first', array('conditions' => array('SocietyTariffWings.building_id' => $postSocietyTariff['building_id'],'SocietyTariffWings.wing_id' => $postSocietyTariff['wing_id'], 'SocietyTariffWings.ledger_head_id' => $societyLedgerId, 'SocietyTariffWings.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyWingExists)) {
                            $this->SocietyTariffWings->id = $societyWingExists['SocietyTariffWings']['id'];
                            $message = 'updated';
                        }
                        $postSocietyTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postSocietyTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postSocietyTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postSocietyTariff['updated_date'] = $this->Util->getDateTime();
                        if ($this->SocietyTariffWings->save($postSocietyTariff)) {
                            $this->Session->setFlash(__('The society tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }
        }
        $this->set(compact('societyBuildingLists', 'societyLedgerHeadList'));
    }
    
    public function society_unit_type_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('SocietyTariffUnitTypes');
        
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }

        $unitTypeList = array('R'=>'Residencial','C'=>'Commercial','B'=>'Both');

        $postSocietyTariff = array();
        if ($this->request->is('post', 'put')) {
            $postSocietyTariff['society_id'] = $societyLoginID;
            $postSocietyTariff['unit_type'] = isset($this->request->data['SocietyTariffUnitTypes']['unit_type']) ? $this->request->data['SocietyTariffUnitTypes']['unit_type'] : '';
            $postSocietyTariff['effective_date'] = isset($this->request->data['SocietyTariffUnitTypes']['effective_date']) ? $this->request->data['SocietyTariffUnitTypes']['effective_date'] : '';
            
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            
           // echo '<pre>';print_r($this->request->data['SocietyTariffBuildings']['ledger_head_id']);exit;
                    
            if (isset($this->request->data['SocietyTariffUnitTypes']['ledger_head_id']) && count($this->request->data['SocietyTariffUnitTypes']['ledger_head_id']) > 0) {
                foreach ($this->request->data['SocietyTariffUnitTypes']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->SocietyTariffUnitTypes->create();
                        $societyUnitTypeExists = $this->SocietyTariffUnitTypes->find('first', array('conditions' => array('SocietyTariffUnitTypes.unit_type' => $postSocietyTariff['unit_type'], 'SocietyTariffUnitTypes.ledger_head_id' => $societyLedgerId, 'SocietyTariffUnitTypes.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyUnitTypeExists)) {
                            $this->SocietyTariffUnitTypes->id = $societyUnitTypeExists['SocietyTariffUnitTypes']['id'];
                            $message = 'updated';
                        }
                        $postSocietyTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postSocietyTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postSocietyTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postSocietyTariff['updated_date'] = $this->Util->getDateTime();
                        if ($this->SocietyTariffUnitTypes->save($postSocietyTariff)) {
                            $this->Session->setFlash(__('The society tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }
        }
        $this->set(compact('unitTypeList', 'societyLedgerHeadList'));
    }
    
    public function society_per_area_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('SocietyTariffPerAreas');
        
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }
        
        $currentTariffDetails = array();
        $societyPerAreaTariff = $this->SocietyTariffPerAreas->find('all', array('conditions' => array('SocietyTariffPerAreas.society_id' => $this->Session->read('Auth.User.id'))));
        if(!empty($societyPerAreaTariff)) {
            foreach($societyPerAreaTariff as $details) {
                $currentTariffDetails[$details['SocietyTariffPerAreas']['ledger_head_id']] = $details['SocietyTariffPerAreas']['amount'];
            }
        }
        
        
        $postSocietyTariff = array();
        if ($this->request->is('post', 'put')) {
            $postSocietyTariff['effective_date'] = isset($this->request->data['SocietyTariffPerAreas']['effective_date']) ? $this->request->data['SocietyTariffPerAreas']['effective_date'] : '';
            $postSocietyTariff['society_id'] = $societyLoginID;
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            
           // echo '<pre>';print_r($this->request->data['SocietyTariffBuildings']['ledger_head_id']);exit;
                    
            if (isset($this->request->data['SocietyTariffPerAreas']['ledger_head_id']) && count($this->request->data['SocietyTariffPerAreas']['ledger_head_id']) > 0) {
                foreach ($this->request->data['SocietyTariffPerAreas']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->SocietyTariffPerAreas->create();
                        $societyPerAreaExists = $this->SocietyTariffPerAreas->find('first', array('conditions' => array('SocietyTariffPerAreas.ledger_head_id' => $societyLedgerId, 'SocietyTariffPerAreas.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyPerAreaExists)) {
                            $this->SocietyTariffPerAreas->id = $societyPerAreaExists['SocietyTariffPerAreas']['id'];
                            $message = 'updated';
                        }
                        $postSocietyTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postSocietyTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postSocietyTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postSocietyTariff['updated_date'] = $this->Util->getDateTime();
                        
                        if ($this->SocietyTariffPerAreas->save($postSocietyTariff)) {
                            $this->Session->setFlash(__('The society tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }
        }
        $this->set(compact('societyPerAreaTariff', 'societyLedgerHeadList','currentTariffDetails'));
    }   
    
    public function society_total_area_tariffs() {
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberTariff');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('SocietyTariffTotalAreas');
        
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }
        
        $postSocietyTariff = array();
        if ($this->request->is('post', 'put')) {
            $postSocietyTariff['effective_date'] = isset($this->request->data['SocietyTariffTotalAreas']['effective_date']) ? $this->request->data['SocietyTariffTotalAreas']['effective_date'] : '';
            $postSocietyTariff['society_id'] = $societyLoginID;
            //$postMemberTariff['id'] = isset($this->request->data['MemberTariff']['id']) ? $this->request->data['MemberTariff']['id'] : null;
            $serialOrderCount = 1;
            
           // echo '<pre>';print_r($this->request->data['SocietyTariffBuildings']['ledger_head_id']);exit;
                    
            if (isset($this->request->data['SocietyTariffTotalAreas']['ledger_head_id']) && count($this->request->data['SocietyTariffTotalAreas']['ledger_head_id']) > 0) {
                foreach ($this->request->data['SocietyTariffTotalAreas']['ledger_head_id'] as $societyLedgerId => $societyLedgerAmount) {
                    if (isset($societyLedgerAmount) && $societyLedgerAmount >= 0) {
                        $this->SocietyTariffTotalAreas->create();
                        $societyTotalAreaExists = $this->SocietyTariffTotalAreas->find('first', array('conditions' => array('SocietyTariffTotalAreas.ledger_head_id' => $societyLedgerId, 'SocietyTariffTotalAreas.society_id' => $societyLoginID)));
                        $message = 'saved';
                        if (!empty($societyTotalAreaExists)) {
                            $this->SocietyTariffTotalAreas->id = $societyTotalAreaExists['SocietyTariffTotalAreas']['id'];
                            $message = 'updated';
                        }
                        $postSocietyTariff['ledger_head_id'] = isset($societyLedgerId) ? $societyLedgerId : null;
                        $serialOrderDetails = $this->SocietyTariffOrder->find('first', array('conditions' => array('SocietyTariffOrder.society_id' => $this->Session->read('Auth.User.id'), 'SocietyTariffOrder.ledger_head_id' => $societyLedgerId)));
                        $postSocietyTariff['tariff_serial'] = isset($serialOrderDetails['SocietyTariffOrder']['tariff_serial']) ? $serialOrderDetails['SocietyTariffOrder']['tariff_serial'] : null;
                        $postSocietyTariff['amount'] = isset($societyLedgerAmount) ? $societyLedgerAmount : 0;
                        $postSocietyTariff['updated_date'] = $this->Util->getDateTime();
                        
                        if ($this->SocietyTariffTotalAreas->save($postSocietyTariff)) {
                            $this->Session->setFlash(__('The society tarrif has been ' . $message . '.'), 'success');
                        } else {
                            $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                        }
                        $serialOrderCount++;
                    }
                }
            }
        }
        
        $currentTariffDetails = array();
        $societyTotalAreaTariff = $this->SocietyTariffTotalAreas->find('all', array('conditions' => array('SocietyTariffTotalAreas.society_id' => $this->Session->read('Auth.User.id'))));
        if(!empty($societyTotalAreaTariff)) {
            foreach($societyTotalAreaTariff as $details) {
                $currentTariffDetails[$details['SocietyTariffTotalAreas']['ledger_head_id']] = $details['SocietyTariffTotalAreas']['amount'];
            }
        }
        
        $this->set(compact('societyTotalAreaTariff', 'societyLedgerHeadList','currentTariffDetails'));
    }
    
    public function member_payments() {  
        $this->loadModel('MemberPayment');
        $memberPaymentData = $this->MemberPayment->find('all',array('conditions' => array('MemberPayment.society_id' => $this->Session->read('Auth.User.id')), 'fields' => array('Member.member_name','MemberPayment.receipt_id','MemberPayment.id','MemberPayment.payment_date', 'MemberPayment.amount_paid','MemberPayment.payment_mode','MemberPayment.society_bank_id','MemberPayment.cheque_reference_number','MemberPayment.bill_type','MemberPayment.bill_generated_id','MemberPayment.amount_payable','MemberPayment.balance_amount','MemberPayment.bill_month'), 'order' => array('MemberPayment.receipt_id' => 'asc')));
        $this->set(compact('memberPaymentData'));
    }
    
    public function add_member_payment($memberPaymentId = null){
        $societyUserId = $this->Session->read('Auth.User.id');
        $societyLoginID = $this->societyloginID;
        $this->loadModel('MemberPayment');
        $postData = array();
        $societyBankLists = $this->SocietyBill->getSocietyBankNameList();
        $paymentModeLists = $this->SocietyBill->getSocietyPaymentModeLists();
        $societyMemberFlatLists = $this->SocietyBill->getSocietyMemberFlatList();
        $bankLists = $this->SocietyBill->getBankList();
        $paymentReceiptNumber = $this->SocietyBill->memberPaymentReceiptUniqueNumber();
        $returnPaymentArr = array();
        if ($this->request->is(array('get'))) {
            $postData = $this->MemberPayment->find('first', array('conditions' => array('MemberPayment.society_id' => $societyLoginID, 'MemberPayment.id' => $memberPaymentId)));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['MemberPayment']) {
                $aData['society_id'] = isset($societyUserId) ? $societyUserId : 0;
                if ($memberPaymentId == null) {
                    $aData['receipt_id'] = isset($paymentReceiptNumber) ? $paymentReceiptNumber : 0;
                }
                $societyMemberPaymentId = $aData['id'] = isset($this->request->data['MemberPayment']['id']) && !empty($this->request->data['MemberPayment']['id']) ? $this->request->data['MemberPayment']['id'] : NULL;
                $societyMemberId = $aData['member_id'] = isset($this->request->data['MemberPayment']['member_id']) ? $this->request->data['MemberPayment']['member_id'] : '';
                $aData['society_bank_id'] = isset($this->request->data['MemberPayment']['society_bank_id']) ? $this->request->data['MemberPayment']['society_bank_id'] : '';
                $principalPaid = $aData['amount_paid'] = isset($this->request->data['MemberPayment']['amount_paid']) ? $this->request->data['MemberPayment']['amount_paid'] : 0.00;
                $aData['payment_mode'] = isset($this->request->data['MemberPayment']['payment_mode']) ? $this->request->data['MemberPayment']['payment_mode'] : '';
                $aData['cheque_reference_number'] = isset($this->request->data['MemberPayment']['cheque_reference_number']) ? $this->request->data['MemberPayment']['cheque_reference_number'] : 0;
                $aData['payment_date'] = isset($this->request->data['MemberPayment']['payment_date']) ? $this->request->data['MemberPayment']['payment_date'] : '';
                $aData['credited_date'] = isset($this->request->data['MemberPayment']['credited_date']) ? $this->request->data['MemberPayment']['credited_date'] : '';
                $aData['member_bank_id'] = isset($this->request->data['MemberPayment']['member_bank_id']) ? $this->request->data['MemberPayment']['member_bank_id'] : '';
                $aData['member_bank_ifsc'] = isset($this->request->data['MemberPayment']['member_bank_ifsc']) ? $this->request->data['MemberPayment']['member_bank_ifsc'] : '';
                $aData['member_bank_branch'] = isset($this->request->data['MemberPayment']['member_bank_branch']) ? $this->request->data['MemberPayment']['member_bank_branch'] : '';
                $billType = $aData['bill_type'] = isset($this->request->data['MemberPayment']['bill_type']) ? trim($this->request->data['MemberPayment']['bill_type']) : '';
                $aData['entry_date'] = $this->Util->getDateTime();
                $aData['cdate'] = $this->Util->getDateTime();
                $aData['udate'] = $this->Util->getDateTime();
                //print_r($aData);die;
                if (isset($principalPaid) && $principalPaid != '') {
                    $updateBillSummaryByBillId = isset($this->request->data['bill_generated_id']) ? $this->request->data['bill_generated_id'] : '';
                    if ($billType == "reg") {
                        $returnPaymentArr = $this->addMemberPaymentRegularBillSummaryDetails($principalPaid, $societyMemberId, $updateBillSummaryByBillId, $societyMemberPaymentId, $aData);
                        if (isset($returnPaymentArr[0]['MemberBillSummary']) && !empty($returnPaymentArr[0]['MemberBillSummary'])) {
                            $aData['bill_generated_id'] = isset($returnPaymentArr[0]['MemberBillSummary']['bill_no']) ? $returnPaymentArr[0]['MemberBillSummary']['bill_no'] : 0;
                            $aData['principal_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['principal_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['principal_balance'] : 0;
                            $aData['interest_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['interest_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['interest_balance'] : 0;
                            $aData['balance_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['balance_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['balance_amount'] : 0;
                            $aData['monthly_bill_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount'] : 0;
                            $aData['amount_payable'] = isset($returnPaymentArr[0]['MemberBillSummary']['amount_payable']) ? $returnPaymentArr[0]['MemberBillSummary']['amount_payable'] : 0;
                            $aData['bill_month'] = isset($returnPaymentArr[0]['MemberBillSummary']['month']) ? $returnPaymentArr[0]['MemberBillSummary']['month'] : 0;
                        }
                    } else if ($billType == "sup") {
                        $returnPaymentArr = $this->updateMemberSupplementaryBillSummaryDetails($principalPaid, $societyMemberId, $updateBillSummaryByBillId, $societyMemberPaymentId, $aData);
                        if (isset($returnPaymentArr[0]['MemberBillSummary']) && !empty($returnPaymentArr[0]['MemberBillSummary'])) {
                            $aData['bill_generated_id'] = isset($returnPaymentArr[0]['MemberBillSummary']['bill_no']) ? $returnPaymentArr[0]['MemberBillSummary']['bill_no'] : 0;
                            $aData['principal_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['principal_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['principal_balance'] : 0;
                            $aData['interest_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['interest_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['interest_balance'] : 0;
                            $aData['balance_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['balance_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['balance_amount'] : 0;
                            $aData['monthly_bill_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount'] : 0;
                            $aData['amount_payable'] = isset($returnPaymentArr[0]['MemberBillSummary']['amount_payable']) ? $returnPaymentArr[0]['MemberBillSummary']['amount_payable'] : 0;
                            $aData['bill_month'] = isset($returnPaymentArr[0]['MemberBillSummary']['month']) ? $returnPaymentArr[0]['MemberBillSummary']['month'] : 0;
                        }
                    }
                }
                if (!empty($returnPaymentArr)) {
                    if ($this->MemberPayment->save($aData)) {
                        $this->Session->setFlash(__('Member payment has been made successfully.'),'success');
                        $this->redirect(array('action' => 'member_payments'));
                    } else {
                        $this->Session->setFlash(__('The Member payment could not be saved. Please, try again.'),'error');
                    }
                } else {
                    $this->Session->setFlash(__('No data found for ' . $billType . ' bill.Please generate bill.'),'error');
                }
            }
        }
        $this->set(compact('bankLists', 'postData', 'societyMemberFlatLists', 'paymentModeLists', 'societyBankLists'));
    }

    public function delete_member_payments($memberPaymentId = null) {
        $this->loadModel('MemberPayment');
        if (!$this->MemberPayment->exists($memberPaymentId)) {
            throw new NotFoundException(__('Requested building is not found.'));
        }
        //$this->request->allowMethod('post', 'delete');
        $this->deleteMemberRegularBillSummaryDetails($memberPaymentId);
        $this->MemberPayment->id = $memberPaymentId;
        if ($this->MemberPayment->delete()){
            $this->Session->setFlash(__('The payment has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The payment could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'member_payments'));
    }
    
    public function addMemberPaymentRegularBillSummaryDetails($principalPaid, $societyMemberId, $updateBillSummaryBillNo = '', $societyMemberPaymentId = '') {
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberTariff');
        $this->loadModel('Member');
        $billType = Configure::read('BillType.Regular');
        $conditionsArr = array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType);
        $memberBillSummaryData = array();
        $checkBillCountForFirstBillUpdate = 1;
        if ($this->MemberBillSummary->hasAny($conditionsArr)){
            if ($updateBillSummaryBillNo != '') {
                $memberBillSummaryData = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_no >=' => $updateBillSummaryBillNo, 'MemberBillSummary.bill_type' => $billType), 'order' => array('MemberBillSummary.bill_no' => 'asc', 'MemberBillSummary.bill_generated_date' => 'asc'), 'recursive' => -1));
                $memberBillSummaryDetails = $this->updateMemberPaymentRegularBillSummaryDetails($principalPaid, $societyMemberId, $memberBillSummaryData);
            } else {
                $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType, 'MemberBillSummary.balance_amount >' => 0), 'order' => array('MemberBillSummary.bill_no' => 'asc'), 'recursive' => -1));
                if (isset($memberBillSummaryDetails) && empty($memberBillSummaryDetails)) {
                    $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType), 'limit' => 1, 'order' => array('MemberBillSummary.bill_no' => 'desc'), 'recursive' => -1));
                }
            }
            //print_r($memberBillSummaryDetails);die;
            $condition = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'),'MemberPayment.bill_type' => $billType,'MemberPayment.member_id' => $societyMemberId);
            $memberPaymentCount = $this->Member->MemberPayment->find('count', array('conditions' => $condition));
            $checkBillCountForFirstBillUpdate = $memberPaymentCount;

            $billSummaryCounter = 0;
            $paymentMadeBillNo = 0;
            $lastMonthCount = 0;
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $lastBillAmountPaid = 0;
            $paidAmountDeduction = 0;


            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {

                            $paymentMadeBillNo = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['bill_no'];
                            if ($societyMemberPaymentId) {
                                $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId, 'MemberPayment.bill_generated_id' => $paymentMadeBillNo,'MemberPayment.bill_type' => $billType,'MemberPayment.id !=' => $societyMemberPaymentId);
                                if ($this->Member->MemberPayment->hasAny($conditionPayment)) {
                                    $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('fields' => array('SUM(amount_paid) as amount_paid'), 'conditions' => $conditionPayment));
                                    if (isset($memberPaymentDetails[0][0]['amount_paid'])) {
                                        $lastBillAmountPaid = $memberPaymentDetails[0][0]['amount_paid'];
                                    }
                                }
                            }

                            if ($checkBillCountForFirstBillUpdate > 1) {
                                $paidAmountDeduction = $principalPaid + $lastBillAmountPaid;
                            } else {
                                $paidAmountDeduction = $principalPaid;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] > 0 && $paidAmountDeduction > 0) {
                                $tempPaidAmount = $paidAmountDeduction;
                                $paidAmountDeduction = $paidAmountDeduction - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                                if ($paidAmountDeduction >= 0) {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                                } else {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = $tempPaidAmount;
                                }
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] > 0 && $paidAmountDeduction > 0) {
                                $tempPaidAmount = $paidAmountDeduction;
                                $paidAmountDeduction = $paidAmountDeduction - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                                if ($paidAmountDeduction >= 0) {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                                } else {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = $tempPaidAmount;
                                }
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance']) && $paidAmountDeduction > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = $paidAmountDeduction;
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'];
                            }

                            /* $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = (($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - ($lastBillAmountPaid + $principalPaid));
                              if ($updateBillSummaryByBillId != '') {
                              if($memberPaymentCount <= 1){
                              $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - ($principalPaid);
                              }
                              } */
                            //$memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'];
                            //$memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                            //$memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = $principalPaid;
                        } else {/* Update All next bills */
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            if ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;
                            } else {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = 0.00;
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] - ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total']);

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                        unset($lastBillAmountPaid);
                    }
                }
            }

            //print_r($memberBillSummaryDetails);die;
            if ($this->MemberBillSummary->saveAll($memberBillSummaryDetails)) {
                return $memberBillSummaryDetails;
            }
            return array();
        }
        return array();
    }

    public function updateMemberPaymentRegularBillSummaryDetails($principalPaid, $societyMemberId, $memberBillSummaryDetails = array()) {
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberTariff');
        $this->loadModel('Member');
        $billType = Configure::read('BillType.Regular');
        $conditionsArr = array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType);
        if ($this->MemberBillSummary->hasAny($conditionsArr)) {
            $billSummaryCounter = 0;
            $lastMonthCount = 0;
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $lastBillAmountPaid = 0;

            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = 0.00;
                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = 0.00;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = 0.00;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'])) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = 0.00;
                            }

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                        } else {//Update All next bills
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            if ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;
                            } else {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = 0.00;
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                        unset($lastBillAmountPaid);
                    }
                }
            }
            return $memberBillSummaryDetails;
        }
        return array();
    }
    
    public function downloadSocietyMemberTariffData() {
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('Member');
        $societyLoginID = $this->societyloginID;
        if ($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id' => $societyLoginID))) {
            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyTariffOrder.id' => 'asc')));
        } else {
            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'), 'recursive' => -1));
        }

        $this->PhpExcel->createWorksheet()->setDefaultFont('Arial', 9);
        $this->PhpExcel->createSheet(0);
        $this->PhpExcel->getDefaultStyle()
                ->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $this->PhpExcel->setActiveSheetIndex(0);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->PhpExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->PhpExcel->getActiveSheet()->freezePane('A2');
        $this->PhpExcel->getActiveSheet()->setTitle('Member Tariff');
        $this->PhpExcel->getActiveSheet()->getSheetView()->setZoomScale(100);

        $tariffCounter = 0;
        $headerRow = array();
        $columnInnercount = 2;
        $rownInnercount = 'A';
        array_push($headerRow, 'Member Name');
        array_push($headerRow, 'Member ID');
        array_push($headerRow, 'Flat No');
        foreach ($societyLedgerHeadList as $societyData) {
            array_push($headerRow, isset($societyData['SocietyLedgerHeads']['title']) ? $societyData['SocietyLedgerHeads']['title'] . '|*|' . $societyData['SocietyLedgerHeads']['id'] : '');
        }
        array_push($headerRow, 'Remark');
        $this->PhpExcel->setRow(1);
        $this->PhpExcel->addTableRow($headerRow, array('name' => 'Arial', 'bold' => true, 'size' => 12));
        $this->Member->Behaviors->attach('Containable');
        $societyMemberLists = $this->Member->find('all', array('conditions' => array('Member.status' => Configure::read('Active'), 'Member.society_id' => $societyLoginID), 'order' => array('Member.id' => 'asc'), 'contain' => array('MemberTariff', 'MemberTariffDetails')));
        foreach ($societyMemberLists as $memberTariffData) {
            $this->PhpExcel->getActiveSheet()->getStyle($rownInnercount . $columnInnercount . ':' . $rownInnercount . $columnInnercount)->getFont()->setBold(true);
            $this->PhpExcel->setActiveSheetIndex(0)->setCellValue($rownInnercount . '' . $columnInnercount, $memberTariffData['Member']['member_name']);
            $rownInnercount++;
            $this->PhpExcel->getActiveSheet()->getStyle($rownInnercount . $columnInnercount . ':' . $rownInnercount . $columnInnercount)->getFont()->setBold(true);
            $this->PhpExcel->setActiveSheetIndex(0)->setCellValue($rownInnercount . '' . $columnInnercount, $memberTariffData['Member']['id']);
            $rownInnercount++;
            $this->PhpExcel->getActiveSheet()->getStyle($rownInnercount . $columnInnercount . ':' . $rownInnercount . $columnInnercount)->getFont()->setBold(true);
            $this->PhpExcel->setActiveSheetIndex(0)->setCellValue($rownInnercount . '' . $columnInnercount, $memberTariffData['Member']['flat_no']);
            $rownInnercount++;
            if (isset($memberTariffData['MemberTariff']) && !empty($memberTariffData['MemberTariff'])) {
                foreach ($memberTariffData['MemberTariff'] as $tariffData) {
                    $this->PhpExcel->getActiveSheet()->getStyle($rownInnercount . $columnInnercount . ':' . $rownInnercount . $columnInnercount)->getFont()->setBold(true);
                    $this->PhpExcel->setActiveSheetIndex(0)->setCellValue($rownInnercount . '' . $columnInnercount, $tariffData['amount']);
                    $rownInnercount++;
                }
            }
            if (isset($memberTariffData['MemberTariffDetails'][0]['id']) && !empty($memberTariffData['MemberTariffDetails'][0]['id'])) {
                $remark = (isset($memberTariffData['MemberTariffDetails'][0]['remark']) && !empty($memberTariffData['MemberTariffDetails'][0]['remark'])) ? $memberTariffData['MemberTariffDetails'][0]['remark'] : '';
                $this->PhpExcel->getActiveSheet()->getStyle($rownInnercount . $columnInnercount . ':' . $rownInnercount . $columnInnercount)->getFont()->setBold(true);
                $this->PhpExcel->setActiveSheetIndex(0)->setCellValue($rownInnercount . '' . $columnInnercount, $remark);
                $rownInnercount++;
            }
            $rownInnercount = 'A';
            $columnInnercount++;
        }
        date_default_timezone_set("Asia/Kolkata");
        $filename = "Member_Tariff";
        $this->PhpExcel->output($filename . ".csv");
        $this->PhpExcel->exit();
    }

    public function uploadSocietyMemberTariffData() {
        $this->layout = false;
        $this->autoRender = false;
        $returnArr['error'] = 1;
        $returnArr['error_message'] = 'Please upload correct data file.';
        $this->loadModel('MemberTariff');
        $this->loadModel('MemberTariffDetails');
        $societyLoginID = $this->societyloginID;
        if ($this->RequestHandler->isAjax()) {
            if (isset($this->request->data['MemberTariff']['member_tariff_csv']) && !empty($this->request->data['MemberTariff']['member_tariff_csv']['name'])) {
                $allowed_ext = array("csv");
                $fileData = isset($this->request->data['MemberTariff']['member_tariff_csv']) ? $this->request->data['MemberTariff']['member_tariff_csv'] : '';
                $extensionArr = explode('.', $fileData['name']);
                $extension = array_pop($extensionArr);
                $fileMemberData = fopen($fileData['tmp_name'], 'r');
                if (in_array($extension, $allowed_ext)) {
                    $tariffCount = 0;
                    $memberTariffPost = array();
                    $memberHeaderRow = array();
                    while ($memberTariffData = fgetcsv($fileMemberData)) {
                        if ($tariffCount == 0) {
                            $memberHeaderRow = $memberTariffData;
                        }
                        if ($tariffCount > 0) {
                            $count = count($memberTariffData); //Member Tariff count
                            for ($i = 0; $i < $count; $i++) {
                                if ($i > 2) {
                                    $tariffNameArray = isset($memberHeaderRow[$i]) ? explode('|*|', $memberHeaderRow[$i]) : '';
                                    $tariffID = isset($tariffNameArray[1]) ? $tariffNameArray[1] : '';
                                    $tariffName = isset($tariffNameArray[0]) ? $tariffNameArray[0] : '';
                                    if (isset($tariffID) && $tariffID != '') {
                                        $memberTariffPost[$memberTariffData[1]][$tariffID] = isset($memberTariffData[$i]) ? $memberTariffData[$i] : 0;
                                    }
                                    if (isset($tariffName) && $tariffName != '') {
                                        $memberTariffPost[$memberTariffData[1]]['Remark'] = isset($memberTariffData[$i]) ? $memberTariffData[$i] : '';
                                    }
                                }
                            }
                        }
                        $tariffCount++;
                    }
                    //print_r($memberTariffPost);die;
                    foreach ($memberTariffPost as $memberID => $membTariffDetails) {
                        $tariffserialNo = 1;
                        foreach ($membTariffDetails as $ledgerHeadId => $tariffAmount) {
                            if ($tariffAmount != '' && $ledgerHeadId != 'Remark') {
                                $this->MemberTariff->create();
                                $condition = array('MemberTariff.society_id' => $societyLoginID, 'MemberTariff.member_id' => $memberID, 'MemberTariff.ledger_head_id' => $ledgerHeadId);
                                if ($this->MemberTariff->hasAny($condition)) {
                                    $societyMemberTariffInfo = $this->MemberTariff->find('first', array('conditions' => $condition, 'recursive' => -1, 'order' => array('MemberTariff.id' => 'asc')));
                                    if (isset($societyMemberTariffInfo['MemberTariff']['id'])) {
                                        $this->MemberTariff->id = $societyMemberTariffInfo['MemberTariff']['id'];
                                    }
                                }
                                $dataArr['society_id'] = isset($societyLoginID) ? $societyLoginID : '';
                                $dataArr['member_id'] = $memberID;
                                $dataArr['ledger_head_id'] = $ledgerHeadId;
                                $dataArr['amount'] = isset($tariffAmount) ? $tariffAmount : 0;
                                $dataArr['tariff_serial'] = isset($tariffserialNo) ? $tariffserialNo : '';
                                $dataArr['updated_date'] = $this->Util->getDateTime();
                                if ($this->MemberTariff->save($dataArr)) {
                                    $returnArr['error'] = 0;
                                    $returnArr['error_message'] = 'Society member has been added successfully.';
                                }
                                $tariffserialNo++;
                            }

                            if ($ledgerHeadId == "Remark") {
                                $this->MemberTariffDetails->create();
                                $conditionTariffDetails = array('MemberTariffDetails.member_id' => $memberID);
                                if ($this->MemberTariffDetails->hasAny($conditionTariffDetails)) {
                                    $societyMemberTariffDetails = $this->MemberTariffDetails->find('first', array('conditions' => $conditionTariffDetails, 'order' => array('MemberTariffDetails.id' => 'asc')));
                                    if (isset($societyMemberTariffDetails['MemberTariffDetails']['id'])) {
                                        $this->MemberTariffDetails->id = $societyMemberTariffDetails['MemberTariffDetails']['id'];
                                    }
                                }
                                $mData['member_id'] = $memberID;
                                $mData['remark'] = $tariffAmount;
                                $mData['tariff_effective_since'] = $this->Util->getDateTime('Y-m-d');
                                $mData['updated_date'] = $this->Util->getDateTime();
                                if ($this->MemberTariffDetails->save($mData)) {
                                    $returnArr['error'] = 0;
                                    $returnArr['error_message'] = 'Society member has been added successfully.';
                                }
                            }
                        }
                    }
                } else {
                    $returnArr['error'] = 1;
                    $returnArr['error_message'] = 'Please upload csv file.';
                }
            } else {
                $returnArr['error'] = 1;
                $returnArr['error_message'] = 'Please upload file.';
            }
        }
        return new CakeResponse(array('body' => json_encode($returnArr), 'status' => 200));
    }

    public function uploadSocietyMemberListCsv() {
        $this->layout = false; //Update add Society/Login 
        $returnArr['error'] = 1;
        $returnArr['error_message'] = 'Please upload file.';
        $societyLoginID = $this->Auth->user('id');
        $this->loadModel('Member');
        if ($this->request->is('ajax')) {
            if (isset($this->request->data['Member']['member_csv']) && !empty($this->request->data['Member']['member_csv']['name'])) {
                $allowed_ext = array("csv");
                $fileData = isset($this->request->data['Member']['member_csv']) ? $this->request->data['Member']['member_csv'] : '';
                $extensionArr = explode('.', $fileData['name']);
                $extension = array_pop($extensionArr);
                $fileMemberData = fopen($fileData['tmp_name'], 'r');
                if (in_array($extension, $allowed_ext)) {
                    $membCount = 0;
                    while ($memberIdentitiesData = fgetcsv($fileMemberData)) {
                        if ($membCount > 0) {
                            $dataArr['building_id'] = isset($this->request->data['Member']['building_id']) ? $this->request->data['Member']['building_id'] : '';
                            $dataArr['wing_id'] = isset($this->request->data['Member']['wing_id']) ? $this->request->data['Member']['wing_id'] : '';
                            $dataArr['society_id'] = isset($societyLoginID) ? $societyLoginID : '';
                            $dataArr['member_prefix'] = isset($memberIdentitiesData[0]) ? $memberIdentitiesData[0] : '';
                            $dataArr['member_name'] = isset($memberIdentitiesData[1]) ? $memberIdentitiesData[1] : '';
                            $dataArr['member_email'] = isset($memberIdentitiesData[2]) ? $memberIdentitiesData[2] : '';
                            $dataArr['member_phone'] = isset($memberIdentitiesData[3]) ? $memberIdentitiesData[3] : '';
                            $dataArr['floor_no'] = !empty($memberIdentitiesData[4]) ? $memberIdentitiesData[4] : 0;
                            $dataArr['unit_type'] = isset($memberIdentitiesData[5]) ? $memberIdentitiesData[5] : '';
                            $dataArr['flat_no'] = isset($memberIdentitiesData[6]) ? $memberIdentitiesData[6] : '';
                            $dataArr['area'] = isset($memberIdentitiesData[7]) ? $memberIdentitiesData[7] : 0;
                            $dataArr['carpet'] = !empty($memberIdentitiesData[8]) ? $memberIdentitiesData[8] : 0;
                            $dataArr['commercial'] = !empty($memberIdentitiesData[9]) ? $memberIdentitiesData[9] : 0;
                            $dataArr['residential'] = !empty($memberIdentitiesData[10]) ? $memberIdentitiesData[10] : 0;
                            $dataArr['terrace'] = !empty($memberIdentitiesData[11]) ? $memberIdentitiesData[11] : 0;
                            $dataArr['op_principal'] = !empty($memberIdentitiesData[12]) ? $memberIdentitiesData[12] : 0;
                            $dataArr['op_interest'] = !empty($memberIdentitiesData[13]) ? $memberIdentitiesData[13] : 0;
                            $dataArr['op_tax'] = !empty($memberIdentitiesData[14]) ? $memberIdentitiesData[14] : 0;
                            $dataArr['op_bill_date'] = !empty($memberIdentitiesData[15]) ? $this->Util->getFormatDate(trim($memberIdentitiesData[15])) : 0;
                            $dataArr['op_bill_due_date'] = !empty($memberIdentitiesData[16]) ? $this->Util->getFormatDate(trim($memberIdentitiesData[16]),'Y-m-d') : 0;
                            $dataArr['gstin_no'] = isset($memberIdentitiesData[17]) ? $memberIdentitiesData[17] : '';
                            $dataArr['member_parking_no	'] = isset($memberIdentitiesData[18]) ? $memberIdentitiesData[18] : '';
                            
                            $condition = array('Member.society_id' => $societyLoginID, 'Member.building_id' => $dataArr['building_id'], 'Member.flat_no' => $dataArr['flat_no']);
                            $this->Member->create();
                            if ($this->Member->hasAny($condition)) {
                                $societyMemberInfo = $this->Member->find('first', array('conditions' => $condition, 'recursive' => -1, 'order' => array('Member.id' => 'asc')));
                                if (isset($societyMemberInfo['Member']['id'])) {
                                    $this->Member->id = $societyMemberInfo['Member']['id'];
                                }
                            }
                            if ($this->Member->save($dataArr)) {
                                $returnArr['error'] = 0;
                                $returnArr['error_message'] = 'Society member has been added successfully.';
                            }
                        }
                        $membCount++;
                    }
                } else {
                    $returnArr['error'] = 1;
                    $returnArr['error_message'] = 'Please upload csv file.';
                }
            } else {
                $returnArr['error'] = 1;
                $returnArr['error_message'] = 'Please upload file.';
            }
        }
        return new CakeResponse(array('body' => json_encode($returnArr), 'status' => 200));
    }

    public function loadSocietyMemberPaymentReciepts() {
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel("Member");
            $this->loadModel("Bank");
            $this->loadModel('SocietyLedgerHeads');
            $societyLoginID = $this->societyloginID;
            if ($this->request->is(array('post', 'put'))) {
                if (isset($this->request->data['Member']['building_id']) && !empty($this->request->data['Member']['building_id'])) {
                    $conditionQuery[] = array('AND' => array('Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active'), 'Member.building_id' => $this->request->data['Member']['building_id']));
                    if (isset($this->request->data['Member']['wing_id']) && !empty($this->request->data['Member']['wing_id'])) {
                        $conditionQuery['OR']['Member.wing_id'] = $this->request->data['Member']['wing_id'];
                    }
                } else {
                    $conditionQuery[] = array('AND' => array('Member.society_id' => $societyLoginID));
                }
                $societyMemberLists = $this->Member->find('all', array('conditions' => $conditionQuery, 'order' => array('Member.id' => 'asc'), 'recursive' => -1));
                $societyBankLists = $this->Bank->find('all', array('order' => array('Bank.bank_name' => 'asc')));
                $payment_mode = $this->SocietyBill->checkPaymentModeBySocietyLedgerId($this->request->data['MemberPayment']['society_bank_id']);
                $this->set(compact('societyMemberLists', 'societyBankLists', 'payment_mode'));
                //die;
            }
        }
    }

    public function saveSocietyMemberPaymentReciepts(){
        $this->layout = false;
        $responseArr['error'] = 1;
        $responseArr['error_message'] = 'Member payment data could not saved.';
        if ($this->RequestHandler->isAjax()) {
            $societyLoginID = $this->societyloginID;
            $this->loadModel('MemberPayment');
            $paymentMode = $this->SocietyBill->checkPaymentModeBySocietyLedgerId($this->request->data['MemberPayment']['society_bank_id'], true);
            if ($this->request->is(array('post', 'put'))) {
                if (isset($this->request->data['MemberPayments']) && !empty($this->request->data['MemberPayments'])) {
                    $paymentCounter = 1;
                    $insertFlag = false;
                    foreach ($this->request->data['MemberPayments'] as $memberPaymentPostData) {
                        $paymentReceiptNumber = $this->SocietyBill->memberPaymentReceiptUniqueNumber();
                        $this->MemberPayment->create();
                        $aData['society_id'] = isset($societyLoginID) ? $societyLoginID : 0;
                        $aData['id'] = isset($memberPaymentPostData['id']) && !empty($memberPaymentPostData['id']) ? $memberPaymentPostData['id'] : NULL;
                        $societyMemberId = $aData['member_id'] = isset($memberPaymentPostData['member_id']) ? $memberPaymentPostData['member_id'] : '';
                        $aData['society_bank_id'] = isset($this->request->data['MemberPayment']['society_bank_id']) ? $this->request->data['MemberPayment']['society_bank_id'] : '';
                        $aData['bank_slip_no'] = isset($this->request->data['MemberPayment']['bank_slip_no']) ? $this->request->data['MemberPayment']['bank_slip_no'] : '';
                        $principalPaid = $aData['amount_paid'] = isset($memberPaymentPostData['amount_paid']) ? $memberPaymentPostData['amount_paid'] : '0.00';
                        $aData['receipt_id'] = isset($paymentReceiptNumber) ? $paymentReceiptNumber : 0;
                        $aData['payment_mode'] = $paymentMode;
                        $aData['cheque_reference_number'] = isset($memberPaymentPostData['cheque_reference_number']) ? $memberPaymentPostData['cheque_reference_number'] : 0;
                        $aData['payment_date'] = isset($memberPaymentPostData['payment_date']) ? $this->Util->getFormatDate($memberPaymentPostData['payment_date']) : '';
                        $aData['credited_date'] = isset($memberPaymentPostData['credited_date']) ? $memberPaymentPostData['credited_date'] : '';
                        $aData['member_bank_id'] = isset($memberPaymentPostData['member_bank_id']) ? $memberPaymentPostData['member_bank_id'] : '';
                        $aData['member_bank_ifsc'] = isset($memberPaymentPostData['member_bank_ifsc']) ? $memberPaymentPostData['member_bank_ifsc'] : '';
                        $aData['member_bank_branch'] = isset($memberPaymentPostData['member_bank_branch']) ? $memberPaymentPostData['member_bank_branch'] : '';
                        $aData['entry_date'] = $this->Util->getDateTime();
                        $aData['cdate'] = $this->Util->getDateTime();
                        $aData['udate'] = $this->Util->getDateTime();

                        if (isset($principalPaid) && $principalPaid != '') {
                            $updateBillSummaryByBillId = isset($this->request->data['bill_generated_id']) ? $this->request->data['bill_generated_id'] : '';
                            $returnPaymentArr = $this->addMemberPaymentRegularBillSummaryDetails($principalPaid, $societyMemberId, $updateBillSummaryByBillId);
                            if (isset($returnPaymentArr[0]['MemberBillSummary']) && !empty($returnPaymentArr[0]['MemberBillSummary'])) {
                                $aData['bill_generated_id'] = isset($returnPaymentArr[0]['MemberBillSummary']['bill_no']) ? $returnPaymentArr[0]['MemberBillSummary']['bill_no'] : 0;
                                $aData['principal_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['principal_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['principal_balance'] : 0;
                                $aData['interest_balance'] = isset($returnPaymentArr[0]['MemberBillSummary']['interest_balance']) ? $returnPaymentArr[0]['MemberBillSummary']['interest_balance'] : 0;
                                $aData['balance_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['balance_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['balance_amount'] : 0;
                                $aData['monthly_bill_amount'] = isset($returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount']) ? $returnPaymentArr[0]['MemberBillSummary']['monthly_bill_amount'] : 0;
                                $aData['amount_payable'] = isset($returnPaymentArr[0]['MemberBillSummary']['amount_payable']) ? $returnPaymentArr[0]['MemberBillSummary']['amount_payable'] : 0;
                                $aData['bill_month'] = isset($returnPaymentArr[0]['MemberBillSummary']['month']) ? $returnPaymentArr[0]['MemberBillSummary']['month'] : 0;
                            }

                            if ($this->MemberPayment->save($aData)) {
                                $insertFlag = true;
                            }
                        }
                        $paymentCounter++;
                    }
                    if ($insertFlag) {
                        $responseArr['error'] = 0;
                        $responseArr['error_message'] = 'Member payment has been made successfully.';
                    }
                }
            }
            return new CakeResponse(array('body' => json_encode($responseArr), 'status' => 200));
        }
    }

    public function updateMemberSupplementaryBillSummaryDetails($principalPaid, $societyMemberId, $updateBillSummaryByBillId = '', $societyMemberPaymentId = '', $postData = array()) {
        //Update Member generated bills
        $this->loadModel('MemberBillSummary');
        $this->loadModel('Member');
        $billType = Configure::read('BillType.Supplementary');

        $conditionsArr = array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType);
        if ($this->MemberBillSummary->hasAny($conditionsArr)) {
            if ($updateBillSummaryByBillId != '') {
                $memberBillSummaryData = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_no >=' => $updateBillSummaryByBillId, 'MemberBillSummary.bill_type' => $billType), 'order' => array('MemberBillSummary.bill_no' => 'asc', 'MemberBillSummary.bill_generated_date' => 'asc'), 'recursive' => -1));
                $memberBillSummaryDetails = $this->updateMemberPaymentSupplementaryBillSummaryDetails($principalPaid, $societyMemberId, $memberBillSummaryData);
            } else {
                $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType, 'MemberBillSummary.balance_amount >' => 0), 'order' => array('MemberBillSummary.bill_no' => 'asc'), 'recursive' => -1));
                if (isset($memberBillSummaryDetails) && empty($memberBillSummaryDetails)) {
                    $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType), 'limit' => 1, 'order' => array('MemberBillSummary.bill_no' => 'desc'), 'recursive' => -1));
                }
            }
            //print_r($memberBillSummaryDetails);die;
            $condition = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId, 'MemberPayment.bill_type' => $billType);
            $memberPaymentCount = $this->Member->MemberPayment->find('count', array('conditions' => $condition));
            $checkBillCountForFirstBillUpdate = $memberPaymentCount;

            $billSummaryCounter = 0;
            $paymentMadeBillNo = 0;
            $lastMonthCount = 0;
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $lastBillAmountPaid = 0;
            $paidAmountDeduction = 0;

            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {
                            $paymentMadeBillNo = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['bill_no'];
                            if ($societyMemberPaymentId) {
                                $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId, 'MemberPayment.bill_generated_id' => $paymentMadeBillNo, 'MemberPayment.bill_type' => $billType, 'MemberPayment.id !=' => $societyMemberPaymentId);
                                if ($this->Member->MemberPayment->hasAny($conditionPayment)) {
                                    $memberPaymentDetails = $this->Member->MemberPayment->find('all', array('fields' => array('SUM(amount_paid) as amount_paid'), 'conditions' => $conditionPayment));
                                    if (isset($memberPaymentDetails[0][0]['amount_paid'])) {
                                        $lastBillAmountPaid = $memberPaymentDetails[0][0]['amount_paid'];
                                    }
                                }
                            }

                            if ($checkBillCountForFirstBillUpdate > 1) {
                                $paidAmountDeduction = $principalPaid + $lastBillAmountPaid;
                            } else {
                                $paidAmountDeduction = $principalPaid;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] > 0 && $paidAmountDeduction > 0) {
                                $tempPaidAmount = $paidAmountDeduction;
                                $paidAmountDeduction = $paidAmountDeduction - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                                if ($paidAmountDeduction >= 0) {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                                } else {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = $tempPaidAmount;
                                }
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] > 0 && $paidAmountDeduction > 0) {
                                $tempPaidAmount = $paidAmountDeduction;
                                $paidAmountDeduction = $paidAmountDeduction - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                                if ($paidAmountDeduction >= 0) {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                                } else {
                                    $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = $tempPaidAmount;
                                }
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance']) && $paidAmountDeduction > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = $paidAmountDeduction;
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'];
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                        } else {/* Update All next bills */
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            if ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;
                            } else {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = 0.00;
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] - ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total']);

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                        unset($lastBillAmountPaid);
                    }
                }
            }
            //print_r($memberBillSummaryDetails);die;
            if ($this->MemberBillSummary->saveAll($memberBillSummaryDetails)) {
                return $memberBillSummaryDetails;
            }
            return array();
        }
        return array();
    }

    public function updateMemberPaymentSupplementaryBillSummaryDetails($principalPaid, $societyMemberId, $memberBillSummaryDetails = array()) {
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberTariff');
        $this->loadModel('Member');
        $billType = Configure::read('BillType.Supplementary');
        $conditionsArr = array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType);
        if ($this->MemberBillSummary->hasAny($conditionsArr)) {
            $billSummaryCounter = 0;
            $lastMonthCount = 0;
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $lastBillAmountPaid = 0;

            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = 0.00;
                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'] = 0.00;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid']) && $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'] = 0.00;
                            }

                            if (isset($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'])) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = 0.00;
                            }

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                        } else {//Update All next bills
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['tax_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            if ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] > 0) {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;
                            } else {
                                $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = 0.00;
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_total'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_tax_arrears'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_paid'];

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['tax_balance'];
                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                        unset($lastBillAmountPaid);
                    }
                }
            }
            return $memberBillSummaryDetails;
        }
        return array();
    }

    public function deleteMemberRegularBillSummaryDetails($memberPaymentID = null) {
        //Delete Member generated bills
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberPayment');
        $billType = Configure::read('BillType.Regular');

        $conditionsArr = array('MemberPayment.id' => $memberPaymentID, 'MemberPayment.society_id' => $this->societyloginID, 'MemberPayment.bill_type' => $billType);
        if ($this->MemberPayment->hasAny($conditionsArr)) {
            $societyMemberPayment = $this->MemberPayment->find('first', array('conditions' => $conditionsArr, 'recursive' => -1));

            $societyMemberId = isset($societyMemberPayment['MemberPayment']['member_id']) ? $societyMemberPayment['MemberPayment']['member_id'] : '';
            $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType, 'OR' => array('MemberBillSummary.principal_paid' => 0.00, 'MemberBillSummary.balance_amount >' => 0)), 'order' => array('MemberBillSummary.bill_no' => 'asc'), 'recursive' => -1));
            if (isset($memberBillSummaryDetails) && empty($memberBillSummaryDetails)) {
                $memberBillSummaryDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.member_id' => $societyMemberId, 'MemberBillSummary.society_id' => $this->societyloginID, 'MemberBillSummary.bill_type' => $billType), 'order' => array('MemberBillSummary.bill_no' => 'asc', 'MemberBillSummary.bill_generated_date' => 'asc'), 'recursive' => -1));
            }

            $billSummaryCounter = 0;
            $lastMonthCount = 0;
            $societyTariffParameterCheck = $this->Society->SocietyParameter->find('first', array('SocietyParameter.society_id' => $this->Session->read('Auth.User.id')));
            $lastBillAmountPaid = 0;

            if (isset($memberBillSummaryDetails) && count($memberBillSummaryDetails) > 0) {
                foreach ($memberBillSummaryDetails as $billData) {
                    if (!empty($billData)) {
                        if ($billSummaryCounter == 0) {
                            if (!empty($memberPaymentID)) {
                                $condition = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberId);
                            }
                            $lastBillAmountPaid = 0;
                            if ($this->MemberPayment->hasAny($condition)) {
                                $memberPaymentDetails = $this->MemberPayment->find('first', array('fields' => array('SUM(amount_paid) as amount_paid'), 'conditions' => $condition));
                                if (isset($memberPaymentDetails[0]['amount_paid'])) {
                                    $lastBillAmountPaid = $memberPaymentDetails[0]['amount_paid'];
                                }
                            }
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = (($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - ($lastBillAmountPaid - $societyMemberPayment['MemberPayment']['amount_paid']));
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'] = $lastBillAmountPaid - $societyMemberPayment['MemberPayment']['amount_paid'];
                        } else {//Update All next bills
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['principal_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['interest_balance'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? $memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] : 0;
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'] = isset($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount']) ? round(($memberBillSummaryDetails[$lastMonthCount]['MemberBillSummary']['balance_amount'] * ($societyTariffParameterCheck['SocietyParameter']['interest_rate'] / 12) / 100)) : 0;

                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_amount'] - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['discount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['amount_payable'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_due_amount'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_bill_amount'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_principal_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['monthly_principal_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'] = ($memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['op_interest_arrears'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_on_due_amount']) - $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_paid'];
                            $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['balance_amount'] = $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['principal_balance'] + $memberBillSummaryDetails[$billSummaryCounter]['MemberBillSummary']['interest_balance'];
                            $lastMonthCount++;
                        }
                        $billSummaryCounter++;
                        unset($lastBillAmountPaid);
                    }
                }
            }
            //print_r($memberBillSummaryDetails);die;
            if ($this->MemberBillSummary->saveAll($memberBillSummaryDetails)) {
                return true;
            }
        }
        return true;
    }
}
