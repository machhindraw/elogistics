<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */




class ResellersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler');
    public $helpers = array('Js');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
    }

    public function dashboard() {        
        //echo $this->Session->read('Auth.User.username');
        //die;
    }
    
    public function index(){        
    }
    
    public function update_profiles() {
        $this->loadModel('User');
        $this->loadModel('Country');
        $this->loadModel('State');
        $resellerId = $this->Session->read('Auth.User.id');
        if (!$this->User->exists($resellerId)) {
            throw new NotFoundException(__('Invalid reseller'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Reseller']['user_id'] = $resellerId;
            $options = array('conditions' => array('Reseller.user_id' => $resellerId));
            $sellerInfo = $this->Reseller->find('first', $options);
            if (isset($sellerInfo) && !empty($sellerInfo)) {
                $editID = isset($sellerInfo['Reseller']['id']) ? $sellerInfo['Reseller']['id'] : '';
                $this->Reseller->id = $editID;
            }
            if ($this->Reseller->save($this->request->data)) {
                $this->Session->setFlash(__('The reseller profile info has been saved.'), 'success');
            } else {
                $this->Session->setFlash(__('The reseller could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Reseller.user_id' => $resellerId));
            $this->request->data = $this->Reseller->find('first', $options);
        }
        $countryList = $this->Country->find('list', array('fields' => array('country_id', 'country_name'), 'order' => array('Country.country_name' => 'asc')));
        $stateList = $this->State->find('list', array('fields' => array('state_id', 'state_name'), 'order' => array('State.state_name' => 'asc')));
        //print_r($stateList);die;
        
        $this->set(compact('countryList', 'stateList'));
    }

    public function assigned_societys($resellerID = null){
        $resellerID = AuthComponent::user('id');
        if($resellerID == ''){
            throw new NotFoundException(__('Invalid request'));
        }
        $this->loadModel('ResellerSociety');
        $conditions = array('ResellerSociety.reseller_id' => $resellerID);
        $fields = array('User.id','User.username','User.password','Society.user_id','Society.society_name','Society.society_code','Society.udate');
        $assignedSocietysList = $this->ResellerSociety->find('all',array('fields'=>$fields,'conditions' =>$conditions,'order' => array('society_name' => 'asc')));
        $this->set(compact('assignedSocietysList'));
        
        //get all assigned societies list by admin
    }
    
}
