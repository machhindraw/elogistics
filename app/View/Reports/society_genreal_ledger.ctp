<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">General Ledger</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form method="post" id="societyGeneralLedger" name="societyGeneralLedger" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="member-tariff-left-box"> 
                                    <div class="row">                                       
                                        <div class="col-md-3">    
                                            <div class="society-report-form-box-label">Report Type &nbsp;</div>                                                                     
                                            <select class="society-report-form-box-field-dropdown-50" id="operator" name="data[][]">
                                                <option value="">Select Report Type</option>
                                                <option value="">Detail</option>
                                                <option value="">Summary</option>
                                            </select> 
                                        </div>
                                        <div class="col-md-4">    
                                            <div class="society-report-form-box-label">Ledger for &nbsp;</div>  
                                            <select class="society-report-form-box-field-dropdown" id="operator" name="data[][]">
                                                <option value="">Select Ledger</option>
                                            </select> 
                                        </div>      
                                        <div class="col-md-5">    
                                            <div class="society-report-form-box-label">Particular A/c Name &nbsp;</div>  
                                            <select class="society-report-form-box-field-dropdown" id="operator" name="data[][]">
                                                <option value="">Select a/c Name</option>
                                            </select> 
                                        </div>                                                              
                                    </div>  
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3 nopadding">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">&nbsp;&nbsp;&nbsp;&nbsp;From &nbsp;&nbsp;&nbsp;</div>                                   
                                                <input type="date" class="society-report-form-box-field-date-70" id="" name="data[][]" value="">
                                            </div>
                                        </div>  
                                        <div class="col-md-3 nopadding">
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">To &nbsp;&nbsp;</div>                                    
                                                <input type="date" class="society-report-form-box-field-70" id="" name="data[][]" value="">
                                            </div>                             
                                        </div> 
                                        <div class="col-md-6 nopadding">                            
                                        <div class="form-group">
                                            <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btn-sm"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>     
                                            <a type="button" data-toggle="tooltip" data-original-title="Print Friendly" class="btn btn-success btn-anim btn-sm" href="javascript:void(0);" onclick="printAllReports('print_society_genreal_ledger');"><i class="fa fa-print"></i><span class="btn-text">Print Friendly</span></a>                                                                            
                                            <a data-toggle="tooltip" data-original-title="Export to PDF" class="btn btn-success btn-anim btn-sm" href="javascript:void(0);" onclick=""><i class="fa fa-file-excel-o"></i><span class="btn-text">Export to PDF</span></a>                                    
                                            <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btn-sm" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a>                                                                            
                                        </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="clearfix"></div>
                                <br><br><br>
                            </form> 

                            <div id="print_society_genreal_ledger">
                                <div class="print-society-genreal-ledger">
                                <div class="row1">
                                    <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>

                                    <div class="report-bill">General Ledger</div>
                                    <div class="text-center">For the Year 2018-2019</div> 
                                    <div class="report-bill-outer-section">
                                        <div class="table-wrap1">
                                            <table class="table table-bordered padding-td-none padding-th-none" >
                                            <thead>
                                                <tr>
                                                    <th width="5%" rowspan="2" class="text-center">Date</th>
                                                    <th colspan="2" class="text-center">Reference</th>
                                                    <th colspan="2" class="text-center">Cheque</th>
                                                    <th width="8%" rowspan="2" class="text-center">Dr</th>
                                                    <th width="8%" rowspan="2" class="text-center">Cr</th>
                                                    <th width="8%" rowspan="2" class="text-center">Balance</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" width="15%">Type</th>
                                                    <th class="text-center" width="8%">No</th>
                                                    <th class="text-center" width="8%">No</th>
                                                    <th class="text-center" width="8%">Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="8" class="font-weight-bold">Advance for Deemed conveyance</td>
                                                </tr>
                                                <tr>
                                                    <td>01/04/2018</td>
                                                    <td class="">Opening Balance</td>
                                                    <td class="">11</td>
                                                    <td class="">012549</td>
                                                    <td class="">04/05/2018</td>
                                                    <td class="text-right">2010000</td>
                                                    <td class="text-right">2010000</td>
                                                    <td class="text-right">2010000</td>
                                                </tr>  
                                                <tr>
                                                   <td class="text-center font-weight-bold" colspan="2">Total</td>
                                                    <td class="font-weight-bold"></td>
                                                    <td class="font-weight-bold"></td>
                                                    <td class=""></td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                </tr>
                                                 <tr>
                                                    <td colspan="8" class="font-weight-bold">Advance for Deemed conveyance</td>
                                                </tr>
                                                <tr>
                                                    <td>01/04/2018</td>
                                                    <td>Opening Balance</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-right">2010000</td>
                                                    <td class="text-right">2010000</td>
                                                    <td class="text-right">2010000</td>
                                                </tr>  
                                                <tr>
                                                   <td class="text-center font-weight-bold" colspan="2">Total</td>
                                                    <td class="font-weight-bold"></td>
                                                    <td class="font-weight-bold"></td>
                                                    <td class=""></td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                    <td class="text-right font-weight-bold">2010000</td>
                                                </tr>
                                            </tbody>   
                                        </table> 
                                        </div>
                                    </div>  
                                </div>    
                                </div>    
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>