<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SocietysController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler', 'Util','SocietyBill');
    public $helpers = array('Js');
    public $societyloginID = '';

    //public $uses = array('Society');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'society_dashboard';
        $this->societyloginID = $this->Session->read('Auth.User.id');
        $this->set("utilObj", $this->Util);
        $this->loadModel('User');
        $this->loadModel('Member');
        $this->loadModel('Society');
        $this->loadModel('Wing');
        $this->loadModel('Tenant');        
        $this->loadModel('SocietyPayment');        
        $this->loadModel('CashWithdraw');
        $this->loadModel('SocietyLedgerHeads');
        $this->loadModel('MemberBillSummary');
        $this->loadModel('MemberPayment');
        $this->loadAllNavigationModals();
    }
    
    function loadAllNavigationModals(){
        $this->SocietyBill->getSocietyBuildingLists();
        $this->SocietyBill->getSocietyLedgerHeadsDetails();
        $this->SocietyBill->navigateSocietyMemberDetails();
        $this->SocietyBill->loadAllNavigationModalsDropdowns();
    }

    public function dashboard() {
        $this->loadModel('MemberBillGenerate');
        $societyCollectionSummary = array();
        $societyFlatsSummary = array();
        $dashboardBillSummaryDetails = array();

        $getSocietyCashPaymentDetails = $this->SocietyPayment->find('first', array('conditions' => array('SocietyPayment.society_id' => $this->Session->read('Auth.User.id'), 'SocietyPayment.payment_type' => Configure::read('PaymentType.Cash')), 'fields' => array('SUM(SocietyPayment.amount) as cashPayment'), 'order' => array('SocietyPayment.payment_date' => 'desc')));
        $getSocietyBankPaymentDetails = $this->SocietyPayment->find('first', array('conditions' => array('SocietyPayment.society_id' => $this->Session->read('Auth.User.id'), 'SocietyPayment.payment_type' => Configure::read('PaymentType.Bank')), 'fields' => array('SUM(SocietyPayment.amount) as bankPayment'), 'order' => array('SocietyPayment.payment_date' => 'desc')));
        if (isset($getSocietyCashPaymentDetails) && !empty($getSocietyCashPaymentDetails)) {
            $societyCollectionSummary['cashPayment'] = isset($getSocietyCashPaymentDetails[0]['cashPayment']) ? $getSocietyCashPaymentDetails[0]['cashPayment'] : 0;
        }
        if (isset($getSocietyBankPaymentDetails) && !empty($getSocietyBankPaymentDetails)) {
            $societyCollectionSummary['bankPayment'] = isset($getSocietyBankPaymentDetails[0]['bankPayment']) ? $getSocietyBankPaymentDetails[0]['bankPayment'] : 0;
        }

        $countTotalFlats = $this->Member->find('count', array('conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'recursive' => -1));
        $countCommercialFlats = $this->Member->find('count', array('conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'), 'unit_type' => Configure::read('UnitType.Commercial')), 'recursive' => -1));
        $countResidentialFlats = $this->Member->find('count', array('conditions' => array('Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active'), 'unit_type' => Configure::read('UnitType.Residency')), 'recursive' => -1));

        $societyFlatsSummary['flatCount'] = isset($countTotalFlats) ? $countTotalFlats : 0;
        $societyFlatsSummary['commercialFlats'] = isset($countCommercialFlats) ? $countCommercialFlats : 0;
        $societyFlatsSummary['residentialCount'] = isset($countResidentialFlats) ? $countResidentialFlats : 0;

        $getSocietyExpensesDetails = $this->SocietyPayment->find('all', array('conditions' => array('SocietyPayment.society_id' => $this->Session->read('Auth.User.id'),'SocietyPayment.status' => Configure::read('Active')), 'fields' => array('SocietyPayment.id', 'SocietyPayment.particulars', 'SocietyPayment.amount'), 'limit' => 5, 'order' => array('SocietyPayment.payment_date' => 'desc')));
        $getDuesFromMemberDetails = $this->MemberBillSummary->find('all', array('conditions' => array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id')), 'fields' => array('MemberBillSummary.id', 'MemberBillSummary.flat_no', 'Member.member_name', 'MemberBillSummary.amount_payable'), 'limit' => 5, 'order' => array('MemberBillSummary.amount_payable' => 'desc')));
        $getReceiptDetails = $this->MemberPayment->find('all', array('conditions' => array('MemberPayment.society_id' => $this->Session->read('Auth.User.id')), 'fields' => array('MemberPayment.id', 'Member.flat_no', 'Member.member_name', 'MemberPayment.bill_generated_id', 'MemberPayment.amount_paid'), 'limit' => 5, 'order' => array('MemberPayment.id' => 'desc')));
        $getSocietyLatestPaymentDetails = $this->SocietyPayment->find('all', array('conditions' => array('SocietyPayment.society_id' => $this->Session->read('Auth.User.id'),'SocietyPayment.status' => Configure::read('Active')), 'fields' => array('SocietyPayment.id', 'SocietyPayment.payment_date', 'SocietyLedgerHeads.title', 'SocietyPayment.amount'), 'limit' => 5, 'order' => array('SocietyPayment.payment_date' => 'desc')));
        //Dashboard Bill Summary
        $monthList = $this->MemberBillSummary->find('list', array('fields' => array('MemberBillSummary.month', 'MemberBillSummary.month'), 'conditions' => array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id')), 'limit' => 5, 'order' => array('MemberBillSummary.month' => 'asc'), 'recursive' => -1));
        if (isset($monthList) && !empty($monthList)) {
            foreach ($monthList as $month) {
                $memberBillSummaryDetails = $this->MemberBillSummary->find('first', array('fields' => array('SUM(MemberBillSummary.amount_payable) as amount_payable', 'SUM(MemberBillSummary.principal_paid) as principal_paid', 'SUM(MemberBillSummary.op_due_amount) as op_due_amount'), 'conditions' => array('MemberBillSummary.society_id' => $this->Session->read('Auth.User.id'), 'MemberBillSummary.month' => $month), 'order' => array('MemberBillSummary.month' => 'asc'), 'recursive' => -1));
                $dashboardBillSummaryDetails[$month]['amount'] = isset($memberBillSummaryDetails[0]['amount_payable']) ? $memberBillSummaryDetails[0]['amount_payable'] : 0;
                $dashboardBillSummaryDetails[$month]['collectionAmount'] = isset($memberBillSummaryDetails[0]['principal_paid']) ? $memberBillSummaryDetails[0]['principal_paid'] : 0;
                $dashboardBillSummaryDetails[$month]['dueAmount'] = isset($memberBillSummaryDetails[0]['op_due_amount']) ? $memberBillSummaryDetails[0]['op_due_amount'] : 0;
                $dashboardBillSummaryDetails[$month]['monthName'] = $this->Util->getMonthWordFormat($month);
            }
        }
        $this->set(compact('societyFlatsSummary', 'societyCollectionSummary', 'getSocietyExpensesDetails', 'getDuesFromMemberDetails', 'getReceiptDetails', 'getSocietyLatestPaymentDetails', 'dashboardBillSummaryDetails'));
    }

    public function index() {

    }

    public function update_societys($societyUserID = null) {
        if ($this->request->is('ajax')) {
            $response = array();
            $response['error'] = 1;
            $response['error_message'] = 'Opps! Somthing went wrong.';
            if ($this->request->data) {
                if ($this->Society->save($this->request->data['Society'])) {
                    $response['error'] = 0;
                    $response['error_message'] = 'Society Data has been updated successfully';
                }
            }
            return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
        } else {
            $conditions = array('Society.user_id' => $societyUserID);
            if ($this->Society->hasany($conditions)) {
                $singleSocietyRecord = $this->Society->find("first", array('conditions' => array('Society.user_id' => $societyUserID)));
                $this->set('singleSocietyRecord', $singleSocietyRecord);
            } else {
                throw new NotFoundException(__('Invalid society id'));
            }
        }
    }

    public function building_identitys($buildingId = null) {
        $societyUserId = $this->Session->read('Auth.User.id');
        if (!$this->User->exists($societyUserId)) {
            throw new NotFoundException(__('Invalid reseller'));
        }
        $message = "added";
        if ($this->request->is(array('get'))) {
            $this->request->data = $this->Society->Building->find('first', array('conditions' => array('Building.id' => $buildingId)));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($buildingId != '') {
                $message = "updated";
                $this->Society->Building->id = $buildingId;
            }
            //$buildingModelObj = ClassRegistry::init('Building');
            $this->Society->Building->set($this->request->data);

            if (!$this->Society->Building->validates()) {
                $errors = $this->Society->Building->validationErrors;
                if (isset($errors['num_flats'])) {
                    $this->Session->setFlash(__('The building could not be saved .' . $errors['num_flats'][0]), 'error');
                }
                $this->redirect(array('controller' => 'societys', 'action' => 'building_identitys', $buildingId));
            } else {
                $this->request->data['Building']['cdate'] = $this->Util->getDateTime();
                $this->request->data['Building']['udate'] = $this->Util->getDateTime();
                if ($this->Society->Building->save($this->request->data)) {
                    $this->Session->setFlash(__('The building has been ' . $message . '.'), 'success');
                } else {
                    $this->Session->setFlash(__('The building could not be saved. Please, try again.'), 'error');
                }
            }
            $this->redirect(array('controller' => 'societys', 'action' => 'building_identitys'));
        }
        $societyBuildingLists = $this->Society->Building->find('all', array('conditions' => array('Building.status' => Configure::read('Active'), 'Building.society_id' => $societyUserId), 'fields' => array('Building.building_name', 'Society.society_name', 'Building.society_id', 'Building.num_flats', 'Building.udate', 'Society.user_id', 'Building.id'), 'order' => array('Building.building_name' => 'asc')));
        $this->set(compact('societyBuildingLists'));
    }

    public function wing_identitys($buildingWingId = null) {
        $societyUserId = $this->Session->read('Auth.User.id');
        $message = "added";
        if ($this->request->is(array('get'))) {
            $this->request->data = $this->Wing->find('first', array('fields' => array('Wing.wing_name', 'Wing.id', 'Building.id'), 'conditions' => array('Wing.id' => $buildingWingId)));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($buildingWingId != '') {
                $message = "updated";
                $this->Wing->id = $buildingWingId;
            }
            $this->request->data['Wing']['society_id'] = $societyUserId;
            if ($this->Wing->save($this->request->data)) {
                $this->Session->setFlash(__('The Wing has been ' . $message . '.'), 'success');
            } else {
                $this->Session->setFlash(__('The Wing could not be saved. Please, try again.'), 'error');
            }
            $this->redirect(array('controller' => 'societys', 'action' => 'wing_identitys'));
        }
        $societyBuildingLists = $this->Society->Building->find('list', array('conditions' => array('Building.society_id' => $societyUserId), 'fields' => array('id', 'building_name'), 'order' => array('Building.building_name' => 'asc')));
        $buildingWingData = $this->Wing->find('all', array('conditions' => array('Wing.status' => Configure::read('Active'), 'Wing.society_id' => $societyUserId), 'fields' => array('Wing.wing_name', 'Society.society_name', 'Building.building_name', 'Wing.society_id', 'Wing.udate', 'Society.user_id', 'Wing.id'), 'order' => array('Wing.wing_name' => 'asc')));
        $this->set(compact('societyBuildingLists', 'buildingWingData'));
    }

    public function member_identitys() {
        $this->Member->Behaviors->attach('Containable');
        $wingMemberData = $this->Member->find('all',array('contain'=>array('Building','Society','Wing'),'conditions' => array('Member.status' => Configure::read('Active'), 'Member.society_id' => $this->Session->read('Auth.User.id')), 'fields' => array('Member.member_prefix','Member.member_name', 'Member.id', 'Member.member_photo', 'Member.floor_no','Member.op_principal','Member.op_interest','Member.op_tax','Building.building_name', 'Society.society_name', 'Wing.wing_name','Member.flat_no'), 'order' => array('Member.id' => 'asc')));
        $this->set(compact('wingMemberData'));
    }

    public function add_member_identitys($buildingMemberId = null) {
        $societyUserId = $this->Session->read('Auth.User.id');
        $societyBuildingLists = $this->Society->Building->find('list', array('conditions' => array('Building.status' => Configure::read('Active'), 'Building.society_id' => $societyUserId), 'fields' => array('Building.id', 'Building.building_name'), 'order' => array('Building.building_name' => 'asc'), 'recursive' => -1));
        $error = 'Member has been added successfully.';
        if ($this->request->is(array('get'))) {
            $this->request->data = $this->Member->find('first', array('conditions' => array('Member.id' => $buildingMemberId)));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['Member']) {
                $postData['society_id'] = isset($societyUserId) ? $societyUserId : 0;
                $postData['member_name'] = isset($this->request->data['Member']['member_name']) ? $this->request->data['Member']['member_name'] : '';
                $postData['member_prefix'] = isset($this->request->data['Member']['member_prefix']) ? $this->request->data['Member']['member_prefix'] : '';
                $postData['member_id'] = isset($this->request->data['Member']['member_id']) ? $this->request->data['Member']['member_id'] : '';
		$postData['flat_no'] = isset($this->request->data['Member']['flat_no']) ? $this->request->data['Member']['flat_no'] : '';
                $postData['building_id'] = isset($this->request->data['Member']['building_id']) ? $this->request->data['Member']['building_id'] : '';
                $postData['member_email'] = isset($this->request->data['Member']['member_email']) ? $this->request->data['Member']['member_email'] : '';
                $postData['member_phone'] = isset($this->request->data['Member']['member_phone']) ? $this->request->data['Member']['member_phone'] : '';
                $postData['wing_id'] = isset($this->request->data['Member']['wing_id']) ? $this->request->data['Member']['wing_id'] : 0;
                $postData['floor_no'] = !empty($this->request->data['Member']['floor_no']) ? $this->request->data['Member']['floor_no'] : 0;
                $postData['unit_type'] = isset($this->request->data['Member']['unit_type']) ? $this->request->data['Member']['unit_type'] : '';
                $postData['area'] = !empty($this->request->data['Member']['area']) ? $this->request->data['Member']['area'] : 0;
                $postData['carpet'] = !empty($this->request->data['Member']['carpet']) ? $this->request->data['Member']['carpet'] : 0;
                $postData['commercial'] = !empty($this->request->data['Member']['commercial']) ? $this->request->data['Member']['commercial'] : 0;
                $postData['residential'] = !empty($this->request->data['Member']['residential']) ? $this->request->data['Member']['residential'] : 0;
                $postData['terrace'] = !empty($this->request->data['Member']['terrace']) ? $this->request->data['Member']['terrace'] : 0;
                $postData['op_principal'] = !empty($this->request->data['Member']['op_principal']) ? $this->request->data['Member']['op_principal'] : 0;
                $postData['op_interest'] = !empty($this->request->data['Member']['op_interest']) ? $this->request->data['Member']['op_interest'] : 0;
                $postData['op_tax'] = !empty($this->request->data['Member']['op_tax']) ? $this->request->data['Member']['op_tax'] : 0;
                $postData['op_bill_due_date'] = !empty($this->request->data['Member']['op_bill_due_date']) ? $this->Util->getFormatDate($this->request->data['Member']['op_bill_due_date']) : 0;
                $postData['op_bill_date'] = !empty($this->request->data['Member']['op_bill_date']) ? $this->Util->getFormatDate($this->request->data['Member']['op_bill_date']) : 0;
                $postData['gstin_no'] = !empty($this->request->data['Member']['gstin_no']) ? $this->request->data['Member']['gstin_no'] : 0;
                $postData['member_parking_no'] = !empty($this->request->data['Member']['member_parking_no']) ? $this->request->data['Member']['member_parking_no'] : 0;
                $postData['udate'] = $this->Util->getDateTime();

                if (isset($postData['member_id']) && $postData['member_id'] != '') {
                    $memberId = isset($postData['member_id']) ? $postData['member_id'] : 0;
                    $this->Member->id = $memberId;
                    $this->Member->save($postData);
                    $error = "Member has been updated successfully.";
                } else {
                    $postData['cdate'] = $this->Util->getDateTime();
                    $memberSaveData = $this->Member->save($postData);
                    if ($memberSaveData) {
                        $memberId = isset($memberSaveData['Member']['id']) ? $memberSaveData['Member']['id'] : 0;
                    }
                }

                if (isset($this->request->data['Member']['member_photo']['name']) && !empty($this->request->data['Member']['member_photo']['name'])) {
                    $errorResponce = $this->saveUpdateMemberProfilePhoto($this->request->data['Member']['member_photo'], $memberId);
                    if ($errorResponce) {
                        $error = $errorResponce;
                        $this->Session->setFlash(__($error), 'error');
                    } else {
                        $this->Session->setFlash(__($error), 'success');
                        $this->redirect(array('action' => 'member_identitys'));
                    }
                } else {
                    $this->Session->setFlash(__($error), 'success');
                    $this->redirect(array('action' => 'member_identitys'));
                }
            }
        }

        $this->set(compact('societyBuildingLists'));
    }

    public function tenant_member_identitys() {
        $societyUserId = $this->Session->read('Auth.User.id');
        $wingTenantData = $this->Tenant->find('all', array('conditions' => array('Tenant.status' => Configure::read('Active'), 'Tenant.society_id' => $societyUserId), 'fields' => array('Tenant.tenant_name', 'Tenant.id', 'Tenant.lease_type', 'Tenant.agreement_on', 'Tenant.rent_per_month', 'Tenant.phone'), 'order' => array('Tenant.tenant_name' => 'asc')));
        $this->set(compact('wingTenantData'));
    }

    public function add_tenant_member_identitys($tenantMemberId = null) {
        if ($this->request->is(array('get'))) {
            $this->request->data = $this->Tenant->find('first', array('conditions' => array('Tenant.id' => $tenantMemberId)));
        }
        if ($this->request->is('post', 'put')) {
            //pr($this->request->data);exit;
            $this->Tenant->set($this->request->data);
            if (!$this->Tenant->validates()) {
                $errors = $this->Tenant->validationErrors;
                if (isset($errors) && count($errors) > 0) {
                    $error = $this->Util->getModelValidationError($errors);
                    $this->Session->setFlash(__('The tenant could not be saved .' . $error), 'error');
                }
            } else {
                $this->request->data['Tenant']['society_id'] = $this->Session->read('Auth.User.id');
                $this->request->data['Tenant']['cdate'] = $this->Util->getDateTime();
                $this->request->data['Tenant']['udate'] = $this->Util->getDateTime();
                if ($this->Tenant->save($this->request->data)) {
                    $this->Session->setFlash(__('Tenant data saved successfully.'), 'success');
                    $this->redirect(array('controller' => 'societys', 'action' => 'tenant_member_identitys'));
                } else {
                    $this->Session->setFlash(__('The tenant could not be saved .'), 'error');
                }
            }
        }
        $stateList = $this->Util->getStateList();
        $countryList = $this->Util->getCountryList();
        $this->set(compact('stateList', 'countryList'));
    }

    public function saveUpdateMemberProfilePhoto($filesData, $memberIdentityId) {
        $target_dir = "societyMembers/";
        $error = '';
        if (isset($filesData['tmp_name']) && !empty($filesData['tmp_name'])) {
            $target_file = $target_dir . basename($filesData["name"]);
            $imgExt = pathinfo($target_file, PATHINFO_EXTENSION);
            $uniqueSavename = uniqid(rand()) . '.' . $imgExt;
            $fileName = isset($uniqueSavename) ? $memberIdentityId . '_' . $uniqueSavename : $memberIdentityId . '_' . trim($filesData["name"]);
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
            $imageSize = $filesData["size"];
            $tmpDir = $filesData["tmp_name"];

            if (!is_dir($target_dir)) {
                mkdir($target_dir, 0777);
            }

            if (in_array($imgExt, $valid_extensions)) {
                if ($imageSize < 5000000) {
                    move_uploaded_file($tmpDir, $target_dir . $fileName);
                    $this->Member->updateAll(array("Member.member_photo" => "'" . $target_dir . $fileName . "'"), array("Member.id" => $memberIdentityId));
                } else {
                    $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                }
            } else {
                $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        }
        return $error;
    }

    public function delete_wing_identitys($buildingWingId = null) {
        if (!$this->Wing->exists($buildingWingId)) {
            throw new NotFoundException(__('Requested wing is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->Wing->id = $buildingWingId;
        if ($this->Wing->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The wing has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The wing could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'wing_identitys'));
    }

    public function delete_building_identitys($societyBuildingId = null) {
        if (!$this->Society->Building->exists($societyBuildingId)) {
            throw new NotFoundException(__('Requested building is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->Society->Building->id = $societyBuildingId;
        if ($this->Society->Building->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The building has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The building could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'building_identitys'));
    }

    public function delete_member_identitys($buildingMemberId = null) {
        if (!$this->Member->exists($buildingMemberId)) {
            throw new NotFoundException(__('Requested Member is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->Member->id = $buildingMemberId;
        if ($this->Member->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The Member has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The Member could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'member_identitys'));
    }

    public function delete_tenant_identitys($tanantId = null) {
        if (!$this->Tenant->exists($tanantId)) {
            throw new NotFoundException(__('Requested Tenant is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->Tenant->id = $tanantId;
        if ($this->Tenant->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The Tenant has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The Tenant could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'tenant_member_identitys'));
    }

    public function society_parameter() {
        $this->loadModel('BillingFrequency');
        $this->loadModel('InterestType');
        $this->loadModel('TariffType');
        $this->loadModel('InterestMethod');
        $this->loadModel('SocietyParameter');
        $societyLoginID = $this->societyloginID;
        $billingFrequencyLists = $this->BillingFrequency->find('list', array('conditions' => array('BillingFrequency.status' => Configure::read('Active')), 'fields' => array('BillingFrequency.id', 'BillingFrequency.frequency_type'), 'order' => array('BillingFrequency.frequency_type' => 'asc')));
        $InterestTypeLists = $this->InterestType->find('list', array('conditions' => array('InterestType.status' => Configure::read('Active')), 'fields' => array('InterestType.id', 'InterestType.interest_type'), 'order' => array('InterestType.interest_type' => 'asc')));
        $TariffTypeLists = $this->TariffType->find('list', array('conditions' => array('TariffType.status' => Configure::read('Active')), 'fields' => array('TariffType.id', 'TariffType.tariff_type'), 'order' => array('TariffType.tariff_type' => 'asc')));
        $InterestMethodLists = $this->InterestMethod->find('list', array('conditions' => array('InterestMethod.status' => Configure::read('Active')), 'fields' => array('InterestMethod.id', 'InterestMethod.method_title'), 'order' => array('InterestMethod.method_title' => 'asc')));

        if ($this->request->is('post')) {
            $this->request->data['SocietyParameter']['society_id'] = $societyLoginID;
             //print_r($this->request->data);die;
            if ($this->SocietyParameter->save($this->request->data)) {
                $this->Session->setFlash(__('Society parameter has been set successfully.'), 'success');
                $this->redirect(array('controller' => 'societys', 'action' => 'society_parameter'));
            } else {
                $this->Session->setFlash(__('The Society parameter could not be saved .'), 'error');
            }
        }else{
            if ($this->SocietyParameter->hasAny(array('SocietyParameter.society_id' => $societyLoginID))) {
                $this->request->data = $this->SocietyParameter->find('first', array('conditions' => array('SocietyParameter.society_id' => $societyLoginID)));
            }
        }
        
        $this->set(compact('billingFrequencyLists', 'InterestTypeLists','TariffTypeLists','InterestMethodLists'));
    }

    public function society_head_sub_categories($headSubCategoryId = null) {
        $this->loadModel('AccountCategory');
        $this->loadModel('SocietyHeadSubCategory');
        $societyLoginID = $this->Auth->user('id');
        $societyAccountCategoryLists = $this->AccountCategory->find('list', array('conditions' => array('AccountCategory.status' => Configure::read('Active')), 'fields' => array('AccountCategory.id', 'AccountCategory.title'), 'order' => array('AccountCategory.title' => 'asc')));
        $societyHeadSubCategoryData = $this->SocietyHeadSubCategory->find('all', array('fields' => array('SocietyHeadSubCategory.id', 'SocietyHeadSubCategory.account_status', 'SocietyHeadSubCategory.title','AccountCategory.title','AccountHead.title'), 'order' => array('SocietyHeadSubCategory.title' => 'asc')));
        if ($this->request->is(array('get')) && $headSubCategoryId) {
            $this->request->data = $this->SocietyHeadSubCategory->find('first', array('conditions' => array('SocietyHeadSubCategory.id' => $headSubCategoryId)));
        }
        if ($this->request->is('post')) {
            $this->request->data['SocietyHeadSubCategory']['society_id'] = $societyLoginID;
            $subgroups = explode("\n", $this->request->data['SocietyHeadSubCategory']['title']);
            $errorFlag = 0;
            if (!empty($subgroups)) {
                foreach ($subgroups as $title) {
                    if (empty($title)) {
                        continue;
                    }

                    $this->request->data['SocietyHeadSubCategory']['title'] = $title;
                    if (!$this->SocietyHeadSubCategory->save($this->request->data)) {
                        $this->Session->setFlash(__('The Society head category could not be saved .'), 'error');
                        $errorFlag = 1;
                        break;
                    }
                }
            }
            if ($errorFlag != 1) {
                $this->Session->setFlash(__('Society head category data saved/Updated successfully.'), 'success');
            }
            $this->redirect(array('controller' => 'societys', 'action' => 'society_head_sub_categories'));
        } else if ($this->request->is('put')) {
            $this->request->data['SocietyHeadSubCategory']['society_id'] = $societyLoginID;
            if ($this->SocietyHeadSubCategory->save($this->request->data)) {
                $this->Session->setFlash(__('Society head category data saved/Updated successfully.'), 'success');
                $this->redirect(array('controller' => 'societys', 'action' => 'society_head_sub_categories'));
            } else {
                $this->Session->setFlash(__('The Society head category could not be saved .'), 'error');
            }
        }

        $this->set(compact('societyAccountCategoryLists', 'societyHeadSubCategoryData'));
    }

    public function society_ledger_heads() {
        $this->loadModel('SocietyLedgerHeads');
        $societyLoginID = $this->societyloginID;
        $societyLedgerHeadsLists = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.status' => Configure::read('Active')), 'fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.title', 'AccountHead.title', 'AccountCategory.title', 'SocietyHeadSubCategory.title'), 'order' => array('SocietyLedgerHeads.title' => 'asc')));
        $this->set(compact('societyLedgerHeadsLists'));
    }

    public function add_society_ledger_heads($societyLedgerHeadsId = null) {
        $this->loadModel('SocietyHeadSubCategory');
        $this->loadModel('SocietyLedgerHeads');
        $societyLoginID = $this->societyloginID;
        $societyHeadSubCategoryLists = $this->SocietyHeadSubCategory->find('list', array('conditions' => array('SocietyHeadSubCategory.status' => Configure::read('Active')), 'fields' => array('SocietyHeadSubCategory.id', 'SocietyHeadSubCategory.title'), 'order' => array('SocietyHeadSubCategory.title' => 'asc')));
        if ($this->request->is(array('get')) && $societyLedgerHeadsId) {
            $this->request->data = $this->SocietyLedgerHeads->find('first', array('conditions' => array('SocietyLedgerHeads.id' => $societyLedgerHeadsId)));
        }
        if ($this->request->is('post', 'put')) {
            $this->request->data['SocietyLedgerHeads']['society_id'] = $societyLoginID;
            $ledgerPostArr = array();
            if (isset($this->request->data['SocietyLedgerHead']) && !empty($this->request->data['SocietyLedgerHead'])) {
                $ledgerCount = 0;
                foreach ($this->request->data['SocietyLedgerHead'] as $postData) {
                    if(isset($postData['title']) && isset($postData['short_code']) && $postData['title'] != '' || $postData['short_code'] != ''){
                    $ledgerPostArr[$ledgerCount]['society_head_sub_category_id'] = isset($this->request->data['SocietyLedgerHeads']['society_head_sub_category_id']) ? $this->request->data['SocietyLedgerHeads']['society_head_sub_category_id'] : '';
                    $ledgerPostArr[$ledgerCount]['account_category_id'] = isset($this->request->data['SocietyLedgerHeads']['account_category_id']) ? $this->request->data['SocietyLedgerHeads']['account_category_id'] : '';
                    $ledgerPostArr[$ledgerCount]['account_head_id'] = isset($this->request->data['SocietyLedgerHeads']['account_head_id']) ? $this->request->data['SocietyLedgerHeads']['account_head_id'] : '';
                    $ledgerPostArr[$ledgerCount]['society_id'] = isset($this->request->data['SocietyLedgerHeads']['society_id']) ? $this->request->data['SocietyLedgerHeads']['society_id'] : '';
                    $ledgerPostArr[$ledgerCount]['title'] = isset($postData['title']) ? $postData['title'] : '';
                    $ledgerPostArr[$ledgerCount]['short_code'] = isset($postData['short_code']) ? $postData['short_code'] : '';
                    $ledgerPostArr[$ledgerCount]['op_amount'] = isset($postData['op_amount']) ? $postData['op_amount'] : '';
                    $ledgerPostArr[$ledgerCount]['is_in_bill_charges'] = isset($postData['is_in_bill_charges']) ? $postData['is_in_bill_charges'] : 0;
                    $ledgerPostArr[$ledgerCount]['is_tax_applicable'] = isset($postData['is_tax_applicable']) ? $postData['is_tax_applicable'] : 0;
                    $ledgerPostArr[$ledgerCount]['is_rebate_applicable'] = isset($postData['is_rebate_applicable']) ? $postData['is_rebate_applicable'] : 0;
                    $ledgerPostArr[$ledgerCount]['is_interest_free'] = isset($postData['is_interest_free']) ? $postData['is_interest_free'] : 0;
                    $ledgerPostArr[$ledgerCount]['society_id'] = $societyLoginID;
                    $ledgerPostArr[$ledgerCount]['udate'] = $this->Util->getDateTime();
                    $ledgerPostArr[$ledgerCount]['cdate'] = $this->Util->getDateTime();
                    $ledgerCount = $ledgerCount + 1;
                    }
                }
                
                if (count($ledgerPostArr) > 0) {
                    if ($this->SocietyLedgerHeads->saveAll($ledgerPostArr)) {
                        $this->Session->setFlash(__('Society ledger head data saved/Updated successfully.'), 'success');
                        $this->redirect(array('controller' => 'societys', 'action' => 'society_ledger_heads'));
                    }
                }
            }
            
            $this->request->data['SocietyLedgerHeads']['op_amount'] = isset($this->request->data['SocietyLedgerHeads']['op_amount']) ? $this->request->data['SocietyLedgerHeads']['op_amount'] : 0;
            $this->request->data['SocietyLedgerHeads']['is_in_bill_charges'] = isset($this->request->data['SocietyLedgerHeads']['is_in_bill_charges']) ? $this->request->data['SocietyLedgerHeads']['is_in_bill_charges'] : 0;
            $this->request->data['SocietyLedgerHeads']['is_tax_applicable'] = isset($this->request->data['SocietyLedgerHeads']['is_tax_applicable']) ? $this->request->data['SocietyLedgerHeads']['is_tax_applicable'] : 0;
            $this->request->data['SocietyLedgerHeads']['is_rebate_applicable'] = isset($this->request->data['SocietyLedgerHeads']['is_rebate_applicable']) ? $this->request->data['SocietyLedgerHeads']['is_rebate_applicable'] : 0;
            $this->request->data['SocietyLedgerHeads']['is_interest_free'] = isset($this->request->data['SocietyLedgerHeads']['is_interest_free']) ? $this->request->data['SocietyLedgerHeads']['is_interest_free'] : 0;
            $this->request->data['SocietyLedgerHeads']['udate'] = $this->Util->getDateTime();
            $this->request->data['SocietyLedgerHeads']['cdate'] = $this->Util->getDateTime();
            if ($this->SocietyLedgerHeads->save($this->request->data)) {
                $this->Session->setFlash(__('Society ledger head data saved/Updated successfully.'), 'success');
                $this->redirect(array('controller' => 'societys', 'action' => 'society_ledger_heads'));
            } else {
                $this->Session->setFlash(__('The Society ledger head could not be saved .'), 'error');
            }
        }
        $this->set(compact('societyHeadSubCategoryLists'));
    }

    public function society_payments($societyPaymentId=null){
        $societyLoginID = $this->societyloginID;
        $societyPaymentData = $this->SocietyPayment->find('all',array('fields'=>array('SocietyPayment.id','SocietyPayment.particulars','SocietyPayment.bill_voucher_number','SocietyPayment.payment_date','SocietyPayment.cheque_date','SocietyPayment.payment_type','SocietyLedgerHeads.title','SocietyPayment.amount','SocietyPayment.total_amount','SocietyPayment.tax_amount'),'conditions' => array('SocietyPayment.society_id' => $societyLoginID,'SocietyPayment.status'=>Configure::read('Active')),'order'=>array('SocietyPayment.payment_date' => 'desc')));
        $this->set(compact('societyPaymentData'));
    }

    public function add_society_payments($societyPaymentId = null) {
        $this->loadModel('SocietyParameter');
        $societyLoginID = $this->societyloginID;
        if ($this->request->is(array('get')) && $societyPaymentId) {
            $this->request->data = $this->SocietyPayment->find('first', array('conditions' => array('SocietyPayment.id' => $societyPaymentId, 'SocietyPayment.society_id' => $societyLoginID)));
        }
        $billVoucherNumber = $this->SocietyBill->billVoucherNumber();
        if ($this->request->is('post','put')) {
            $this->request->data['SocietyPayment']['id'] = $societyPaymentId;
            $this->request->data['SocietyPayment']['society_id'] = $societyLoginID;
            if($societyPaymentId == null){
                $this->request->data['SocietyPayment']['bill_voucher_number'] = $billVoucherNumber;
            }
            $checkSocietyParameters = $this->SocietyParameter->find('first', array('conditions' => array('SocietyParameter.society_id' => $societyLoginID)));
            if (isset($checkSocietyParameters['SocietyParameter']) && !empty($checkSocietyParameters['SocietyParameter'])) {
                $paymentAmount = isset($this->request->data['SocietyPayment']['amount']) ? $this->request->data['SocietyPayment']['amount'] * 1 : 0.00;
                $cgstTaxAmount = round($paymentAmount * $checkSocietyParameters['SocietyParameter']['cgst_tax_per'] / 100, 2);
                $sgstTaxAmount = round($paymentAmount * $checkSocietyParameters['SocietyParameter']['sgst_tax_per'] / 100, 2);
                $this->request->data['SocietyPayment']['tax_amount'] = $cgstTaxAmount + $sgstTaxAmount;
                $this->request->data['SocietyPayment']['total_amount'] = $paymentAmount + $this->request->data['SocietyPayment']['tax_amount'];
            }
            if ($this->SocietyPayment->save($this->request->data)) {
                $this->Session->setFlash(__('Society Payment saved successfully.'), 'success');
                $this->redirect(array('controller' => 'societys', 'action' => 'society_payments'));
            } else {
                $this->Session->setFlash(__('The SocietyPayment could not be saved .'), 'error');
            }
        }
        $this->set(compact('societyExpenseLedgerHeadsLists', 'societyBankBalanceHeadsLists'));
    }
    
    public function cash_withdraws($cashWithdrawId=null){
        $societyLoginID = $this->societyloginID;
        $cashWithdrawData = $this->CashWithdraw->find('all',array('conditions' => array('CashWithdraw.society_id' => $societyLoginID),'order'=>array('CashWithdraw.payment_date' => 'desc')));
        $this->set(compact('cashWithdrawData'));
    }

    public function add_cash_withdraws($cashWithdrawId=null) {
        $societyLoginID = $this->societyloginID;
        if ($this->request->is(array('get')) && $cashWithdrawId) {
            $this->request->data = $this->CashWithdraw->find('first', array('conditions' => array('CashWithdraw.id' => $cashWithdrawId, 'CashWithdraw.society_id' => $societyLoginID)));
        }
        
        if ($this->request->is('post','put')) {
            $this->request->data['CashWithdraw']['id'] = $cashWithdrawId;
            $this->request->data['CashWithdraw']['society_id'] = $societyLoginID;
            
            if ($this->CashWithdraw->save($this->request->data)) {
                $this->Session->setFlash(__(ucfirst($this->request->data['CashWithdraw']['txn_type']). ' added successfully.'), 'success');
                $this->redirect(array('controller' => 'societys', 'action' => 'cash_withdraws'));
            } else {
                $this->Session->setFlash(__(ucfirst($this->request->data['CashWithdraw']['txn_type']). ' could not be saved .'), 'error');
            }
        }
        
        $this->set(compact('societyBankBalanceHeadsLists'));
    }
    
    public function delete_cash_withdraws($id = null) {
        if (!$this->CashWithdraw->exists($id)) {
            throw new NotFoundException(__('Requested record not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->CashWithdraw->id = $id;
        
        if($this->CashWithdraw->delete($id)) {
            $this->Session->setFlash(__('Record has been deleted.'), 'success');
        } else {
           $this->Session->setFlash(__('Record could not be deleted .'), 'error'); 
        }
        return $this->redirect(array('action' => 'cash_withdraws'));
    }

    public function delete_society_payments($paymentId = null) {
        if (!$this->SocietyPayment->exists($paymentId)) {
            throw new NotFoundException(__('Requested payment is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->SocietyPayment->id = $paymentId;
        if ($this->SocietyPayment->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The payment has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The payment could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'society_payments'));
    }

	public function society_tariff_orders(){
        $societyLoginID = $this->societyloginID;
        $this->loadModel('SocietyTariffOrder');
        $this->loadModel('SocietyLedgerHeads');
        $this->SocietyLedgerHeads->Behaviors->attach('Containable');
//        if($this->SocietyTariffOrder->hasAny(array('SocietyTariffOrder.society_id'=>$societyLoginID))){
//            $societyLedgerHeadList = $this->SocietyTariffOrder->find('all',array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'),'SocietyLedgerHeads.society_id'=>$societyLoginID,'SocietyLedgerHeads.is_in_bill_charges' => 1),'fields' => array('SocietyLedgerHeads.title','SocietyLedgerHeads.id'),'order' => array('SocietyTariffOrder.id' => 'asc')));
//        }else{
//            $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID,'SocietyLedgerHeads.is_in_bill_charges' => 1), 'fields' => array('SocietyLedgerHeads.title', 'SocietyLedgerHeads.id'), 'order' => array('SocietyLedgerHeads.title' => 'asc'),'recursive'=>-1));
//        }
        $societyLedgerHeadList = $this->SocietyLedgerHeads->find('all', array('contain' => array('SocietyTariffOrder'),'conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID,'SocietyLedgerHeads.is_in_bill_charges' => 1),'order' => array('SocietyTariffOrder.tariff_serial' => 'asc')));
        $this->set(compact('societyLedgerHeadList'));
    }

    public function delete_society_ledger_heads($societyHeadId = null) {
        $this->loadModel('SocietyLedgerHeads');
        if (!$this->SocietyLedgerHeads->exists($societyHeadId)) {
            throw new NotFoundException(__('Requested building is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->SocietyLedgerHeads->id = $societyHeadId;
        if ($this->SocietyLedgerHeads->saveField('status',Configure::read('InActive'))) {
            $this->Session->setFlash(__('The ledger head has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The ledger head could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'society_ledger_heads'));
    }

    public function saveSocietyPaymentEntryReciepts(){
        $this->layout = false;
        $responseArr['error'] = 1;
        $responseArr['error_message'] = 'Society payment data could not saved.';
        if ($this->RequestHandler->isAjax()) {
            $societyLoginID = $this->societyloginID;
            $this->loadModel('SocietyPayment');
            $this->loadModel('SocietyParameter');
            if ($this->request->is(array('post', 'put'))) {
                $checkSocietyParameters = $this->SocietyParameter->find('first', array('conditions' => array('SocietyParameter.society_id' => $societyLoginID)));
                if ($this->request->data['SocietyPayments']) {
                    foreach ($this->request->data['SocietyPayments'] as $societyPaymentPostData) {
                        if ($societyPaymentPostData['ledger_head_id'] != '' && $societyPaymentPostData['amount'] > 0) {
                            $billVoucherNumber = $this->SocietyBill->billVoucherNumber();
                            $postData['society_id'] = isset($societyLoginID) ? $societyLoginID : 0;
                            $postData['amount'] = isset($societyPaymentPostData['amount']) ? $societyPaymentPostData['amount'] : '0.00';
                            $postData['bill_voucher_number'] = isset($billVoucherNumber) ? $billVoucherNumber : 0;
                            $postData['ledger_head_id'] = isset($societyPaymentPostData['ledger_head_id']) ? $societyPaymentPostData['ledger_head_id'] : '';
                            $postData['cheque_reference_number'] = isset($societyPaymentPostData['cheque_reference_number']) ? $societyPaymentPostData['cheque_reference_number'] : '';
                            $postData['cheque_date'] = isset($societyPaymentPostData['cheque_date']) ? $societyPaymentPostData['cheque_date'] : '';
                            $postData['notes'] = isset($societyPaymentPostData['notes']) ? $societyPaymentPostData['notes'] : '';
                            $postData['particulars'] = isset($societyPaymentPostData['particulars']) ? $societyPaymentPostData['particulars'] : '';
                            $postData['payment_type'] = isset($this->request->data['SocietyPayment']['payment_type']) ? $this->request->data['SocietyPayment']['payment_type'] : '';
                            $postData['payment_by_ledger_id'] = isset($this->request->data['SocietyPayment']['payment_by_ledger_id']) ? $this->request->data['SocietyPayment']['payment_by_ledger_id'] : 0;
                            $postData['payment_date'] = isset($societyPaymentPostData['payment_date']) ? $societyPaymentPostData['payment_date'] : '';
                            if (isset($checkSocietyParameters['SocietyParameter']) && !empty($checkSocietyParameters['SocietyParameter'])) {
                                $paymentAmount = isset($societyPaymentPostData['amount']) ? $societyPaymentPostData['amount'] * 1 : 0.00;
                                $cgstTaxAmount = round($paymentAmount * $checkSocietyParameters['SocietyParameter']['cgst_tax_per'] / 100, 2);
                                $sgstTaxAmount = round($paymentAmount * $checkSocietyParameters['SocietyParameter']['sgst_tax_per'] / 100, 2);
                                $postData['tax_amount'] = $cgstTaxAmount + $sgstTaxAmount;
                                $postData['total_amount'] = $paymentAmount + $postData['tax_amount'];
                            }
                            $postData['cdate'] = $this->Util->getDateTime();
                            $postData['udate'] = $this->Util->getDateTime();
                            $this->SocietyPayment->create();
                            if ($this->SocietyPayment->save($postData)) {
                                $responseArr['error'] = 0;
                                $responseArr['error_message'] = 'Society payment has been made successfully.';
                            }
                        }
                    }
                }
            }
        }
        return new CakeResponse(array('body' => json_encode($responseArr), 'status' => 200));
    }

    public function cheque_clear_date(){
        $societyLoginID = $this->societyloginID;   
        $postData = $this->request->data;
        $societyBankLists = $this->SocietyBill->getSocietyBankNameList();
        $societyCashBookDetails = array();
        $memberCashBookDetails = array();
        $cashBookData = array();
        
        $options['AND'] = array('MemberPayment.society_id' => $societyLoginID);
        $optionsSociety['AND'] = array('SocietyPayment.society_id' => $societyLoginID);
        if (isset($this->request->data['MemberPayment']['society_bank_id']) && !empty($this->request->data['MemberPayment']['society_bank_id'])) {
            $options['AND']['MemberPayment.society_bank_id'] = $this->request->data['MemberPayment']['society_bank_id'];
            $optionsSociety['AND']['SocietyPayment.payment_by_ledger_id'] = $this->request->data['MemberPayment']['society_bank_id'];
        }
        if (isset($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['payment_date'], $this->request->data['MemberPayment']['payment_date_to']);
            $optionsSociety['AND']['SocietyPayment.payment_date BETWEEN ? and ?'] = array($this->request->data['MemberPayment']['payment_date'], $this->request->data['MemberPayment']['payment_date_to']);
        } else if (isset($this->request->data['MemberPayment']['payment_date']) && !empty($this->request->data['MemberPayment']['payment_date'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date'];
        } else if (isset($this->request->data['MemberPayment']['payment_date_to']) && !empty($this->request->data['MemberPayment']['payment_date_to'])) {
            $options['AND']['MemberPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date_to'];
            $optionsSociety['AND']['SocietyPayment.payment_date'] = $this->request->data['MemberPayment']['payment_date_to'];
        }
        
        if ($this->request->is(array('post'))) {
            $this->MemberPayment->Behaviors->attach('Containable');
            $this->SocietyPayment->Behaviors->attach('Containable'); //Make sure report type is deposit or withdrawal
            if (isset($this->request->data['MemberPayment']['reciept_type']) && !empty($this->request->data['MemberPayment']['reciept_type']) && $this->request->data['MemberPayment']['reciept_type'] == "Receipt") {
                $memberCashBookDetails = $this->MemberPayment->find('all', array('contain' => array('Member'), 'conditions' => $options, 'order' => array('MemberPayment.payment_date' => 'asc')));
                if(isset($this->request->data['ClearChequePayment']) && !empty($this->request->data['ClearChequePayment'])){
                    $this->updateChequeClearedDate($this->request->data['MemberPayment']['reciept_type'],$this->request->data['ClearChequePayment']);
                }
            } else if (isset($this->request->data['MemberPayment']['reciept_type']) && !empty($this->request->data['MemberPayment']['reciept_type']) && $this->request->data['MemberPayment']['reciept_type'] == "Payment") {
                $societyCashBookDetails = $this->SocietyPayment->find('all', array('contain' => array('SocietyLedgerHeads'), 'conditions' => $optionsSociety, 'order' => array('SocietyPayment.payment_date' => 'asc')));
            }
        }
        
        if (isset($societyCashBookDetails) && !empty($societyCashBookDetails)) {
            $sCounter = 0;
            foreach ($societyCashBookDetails as $societyBankBbookData) {
                $cashBookData[$sCounter]['id'] = $societyBankBbookData['SocietyPayment']['id'];
                $cashBookData[$sCounter]['voucher_no'] = $societyBankBbookData['SocietyPayment']['bill_voucher_number'];
                $cashBookData[$sCounter]['payment_date'] = $societyBankBbookData['SocietyPayment']['payment_date'];
                $cashBookData[$mCounter]['cleared_date'] = $societyBankBbookData['SocietyPayment']['debited_date'];
                $cashBookData[$sCounter]['cheque_date'] = $societyBankBbookData['SocietyPayment']['cheque_date'];
                $cashBookData[$sCounter]['particulars'] = $societyBankBbookData['SocietyPayment']['particulars'] . '&' . $societyBankBbookData['SocietyLedgerHeads']['title'];
                $cashBookData[$sCounter]['cheque_number'] = $societyBankBbookData['SocietyPayment']['cheque_reference_number'];
                $cashBookData[$sCounter]['withdrawal'] = $societyBankBbookData['SocietyPayment']['total_amount'];
                $cashBookData[$sCounter]['payment_flag'] = "Payment";
                $cashBookData[$sCounter]['deposit'] = "0.00";
                unset($societyBankBbookData);
                $sCounter++;
            }
        }
        
        if (isset($memberCashBookDetails) && !empty($memberCashBookDetails)) {
            $mCounter = 0;
            foreach ($memberCashBookDetails as $memberBankBbookData) {
                $cashBookData[$mCounter]['id'] = $memberBankBbookData['MemberPayment']['id'];
                $cashBookData[$mCounter]['voucher_no'] = $memberBankBbookData['MemberPayment']['bill_generated_id'];
                $cashBookData[$mCounter]['payment_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $cashBookData[$mCounter]['cheque_date'] = $memberBankBbookData['MemberPayment']['payment_date'];
                $cashBookData[$mCounter]['cleared_date'] = $memberBankBbookData['MemberPayment']['credited_date'];
                $cashBookData[$mCounter]['particulars'] = $memberBankBbookData['Member']['member_name'] . ' Maint received';
                $cashBookData[$mCounter]['cheque_number'] = $memberBankBbookData['MemberPayment']['cheque_reference_number'];
                $cashBookData[$mCounter]['withdrawal'] = "0.00";
                $cashBookData[$mCounter]['deposit'] = $memberBankBbookData['MemberPayment']['amount_paid'];
                $cashBookData[$mCounter]['payment_flag'] = "Receipt";
                unset($memberBankBbookData);
                $mCounter++;
            }
        }
        ksort($cashBookData);
        $this->set(compact('postData','cashBookData','societyBankLists'));
    }
    public function updateChequeClearedDate($recieptType ='',$dataArr = array()){
        if(isset($dataArr) && !empty($dataArr)){
            foreach($dataArr as $paymentID =>$chequeData){
                if($recieptType == "Receipt"){
                    $this->MemberPayment->id = $paymentID;
                    $this->MemberPayment->saveField('credited_date',$chequeData['cleared_date']);
                } else if($recieptType == "Payment"){
                    $this->SocietyPayment->id = $paymentID;
                    $this->SocietyPayment->saveField('debited_date',$chequeData['cleared_date']);
                }
            }
        }
    }
    
    public function member_identifications(){
        $this->loadModel('MemberIdentification');
        $MemberIdentificationDetails = $this->MemberIdentification->find('all',array('conditions' => array('MemberIdentification.status' => Configure::read('Active'),'MemberIdentification.society_id' => $this->Session->read('Auth.User.id')),'order' => array('MemberIdentification.flat_no' => 'asc')));
        //print_r($MemberIdentificationDetails);die;
        $this->set(compact('MemberIdentificationDetails'));
    }
    
    public function add_member_identifications($societyIdentityId = null) {
        $this->loadModel('MemberIdentification');
        $societyMemberFlatLists = $this->SocietyBill->getSocietyMemberFlatList();
        $societyLoginID = $this->societyloginID;
        $postData = array();
        $memberIdentificationDetails = array();
        if ($this->request->is(array('post'))) {
            $postData['id'] = isset($this->request->data['MemberIdentification']['id']) ? $this->request->data['MemberIdentification']['id'] : null;
            $postData['society_id'] = isset($societyLoginID) ? $societyLoginID : 0;
            $postData['member_id'] = isset($this->request->data['MemberIdentification']['member_id']) ? $this->request->data['MemberIdentification']['member_id'] : null;
            $postData['flat_no'] = isset($this->request->data['MemberIdentification']['flat_no']) ? $this->request->data['MemberIdentification']['flat_no'] : '';
            $postData['second_member'] = isset($this->request->data['MemberIdentification']['second_member']) ? $this->request->data['MemberIdentification']['second_member'] : '';
            $postData['third_member'] = isset($this->request->data['MemberIdentification']['third_member']) ? $this->request->data['MemberIdentification']['third_member'] : '';
            $postData['associate_member'] = isset($this->request->data['MemberIdentification']['associate_member']) ? $this->request->data['MemberIdentification']['associate_member'] : '';
            $postData['joint_member'] = isset($this->request->data['MemberIdentification']['joint_member']) ? $this->request->data['MemberIdentification']['joint_member'] : '';
            $postData['date_of_birth'] = isset($this->request->data['MemberIdentification']['date_of_birth']) ? $this->request->data['MemberIdentification']['date_of_birth'] : '';
            $postData['class'] = isset($this->request->data['MemberIdentification']['class']) ? $this->request->data['MemberIdentification']['class'] : '';
            $postData['GSTIN'] = isset($this->request->data['MemberIdentification']['GSTIN']) ? $this->request->data['MemberIdentification']['GSTIN'] : '';
            $postData['date_of_admission_1'] = isset($this->request->data['MemberIdentification']['date_of_admission_1']) ? $this->request->data['MemberIdentification']['date_of_admission_1'] : '';
            $postData['date_of_admission_2'] = isset($this->request->data['MemberIdentification']['date_of_admission_2']) ? $this->request->data['MemberIdentification']['date_of_admission_2'] : '';
            $postData['purchased_on'] = isset($this->request->data['MemberIdentification']['purchased_on']) ? $this->request->data['MemberIdentification']['purchased_on'] : '';
            $postData['disposed_on'] = isset($this->request->data['MemberIdentification']['disposed_on']) ? $this->request->data['MemberIdentification']['disposed_on'] : '';
            $postData['agreement_date'] = isset($this->request->data['MemberIdentification']['agreement_date']) ? $this->request->data['MemberIdentification']['agreement_date'] : '';
            $postData['agreement_value'] = isset($this->request->data['MemberIdentification']['agreement_value']) ? $this->request->data['MemberIdentification']['agreement_value'] : '';
            $postData['stamp_duty_no'] = isset($this->request->data['MemberIdentification']['stamp_duty_no']) ? $this->request->data['MemberIdentification']['stamp_duty_no'] : '';
            $postData['gender'] = isset($this->request->data['MemberIdentification']['gender']) ? $this->request->data['MemberIdentification']['gender'] : '';
            $postData['age'] = isset($this->request->data['MemberIdentification']['age']) ? $this->request->data['MemberIdentification']['age'] : '';
            $postData['religion'] = isset($this->request->data['MemberIdentification']['religion']) ? $this->request->data['MemberIdentification']['religion'] : '';
            $postData['blood_group'] = isset($this->request->data['MemberIdentification']['blood_group']) ? $this->request->data['MemberIdentification']['blood_group'] : '';
            $postData['profession'] = isset($this->request->data['MemberIdentification']['profession']) ? $this->request->data['MemberIdentification']['profession'] : '';
            $postData['nationality'] = isset($this->request->data['MemberIdentification']['nationality']) ? $this->request->data['MemberIdentification']['nationality'] : '';
            $postData['members_bank'] = isset($this->request->data['MemberIdentification']['members_bank']) ? $this->request->data['MemberIdentification']['members_bank'] : '';
            $postData['pan_no'] = isset($this->request->data['MemberIdentification']['pan_no']) ? $this->request->data['MemberIdentification']['pan_no'] : '';
            $postData['date_of_allotment'] = isset($this->request->data['MemberIdentification']['date_of_allotment']) ? $this->request->data['MemberIdentification']['date_of_allotment'] : '';
            $postData['certification_no'] = isset($this->request->data['MemberIdentification']['certification_no']) ? $this->request->data['MemberIdentification']['certification_no'] : '';
            $postData['folio_no'] = isset($this->request->data['MemberIdentification']['folio_no']) ? $this->request->data['MemberIdentification']['folio_no'] : '';
            $postData['no_of_shares'] = isset($this->request->data['MemberIdentification']['no_of_shares']) ? $this->request->data['MemberIdentification']['no_of_shares'] : '';
            $postData['shares_value'] = isset($this->request->data['MemberIdentification']['shares_value']) ? $this->request->data['MemberIdentification']['shares_value'] : '';
            $postData['from_share_no'] = isset($this->request->data['MemberIdentification']['from_share_no']) ? $this->request->data['MemberIdentification']['from_share_no'] : '';
            $postData['to_share_no'] = isset($this->request->data['MemberIdentification']['to_share_no']) ? $this->request->data['MemberIdentification']['to_share_no'] : '';
            $postData['shares_status'] = isset($this->request->data['MemberIdentification']['shares_status']) ? $this->request->data['MemberIdentification']['shares_status'] : '';
            $postData['admission_fees'] = isset($this->request->data['MemberIdentification']['admission_fees']) ? $this->request->data['MemberIdentification']['admission_fees'] : '';
            $postData['full_name'] = isset($this->request->data['MemberIdentification']['full_name']) ? $this->request->data['MemberIdentification']['full_name'] : '';
            $postData['unit_no'] = isset($this->request->data['MemberIdentification']['unit_no']) ? $this->request->data['MemberIdentification']['unit_no'] : '';
            $postData['address'] = isset($this->request->data['MemberIdentification']['address']) ? $this->request->data['MemberIdentification']['address'] : '';
            $postData['occupation'] = isset($this->request->data['MemberIdentification']['occupation']) ? $this->request->data['MemberIdentification']['occupation'] : '';
            $postData['age_on_date_of_admission'] = isset($this->request->data['MemberIdentification']['age_on_date_of_admission']) ? $this->request->data['MemberIdentification']['age_on_date_of_admission'] : '';
            $postData['nominee_name'] = isset($this->request->data['MemberIdentification']['nominee_name']) ? $this->request->data['MemberIdentification']['nominee_name'] : '';
            $postData['nomination_date'] = isset($this->request->data['MemberIdentification']['nomination_date']) ? $this->request->data['MemberIdentification']['nomination_date'] : '';
            $postData['nominee_address'] = isset($this->request->data['MemberIdentification']['nominee_address']) ? $this->request->data['MemberIdentification']['nominee_address'] : '';
            $postData['date_of_cessation_membership'] = isset($this->request->data['MemberIdentification']['date_of_cessation_membership']) ? $this->request->data['MemberIdentification']['date_of_cessation_membership'] : '';
            $postData['reason_for_cessation'] = isset($this->request->data['MemberIdentification']['reason_for_cessation']) ? $this->request->data['MemberIdentification']['reason_for_cessation'] : '';
            $postData['remarks'] = isset($this->request->data['MemberIdentification']['remarks']) ? $this->request->data['MemberIdentification']['remarks'] : '';
            $postData['cdate'] = $this->Util->getDateTime();
            $postData['udate'] = $this->Util->getDateTime();
            //print_r($postData);die;
            $memberIdentificationDetails = $this->MemberIdentification->save($postData);
            $this->Session->setFlash(__('Member Information Save Successfully'), 'success');
            $this->redirect(array('controller' => 'societys', 'action' => 'member_identifications'));
        } else {
            $postData = $this->MemberIdentification->find('first', array('conditions' => array('MemberIdentification.id' => $societyIdentityId, 'MemberIdentification.society_id' => $societyLoginID)));
        }
        $this->set(compact('postData', 'memberIdentificationDetails', 'societyMemberFlatLists'));
    }
    
    public function delete_member_identification($meberIdentityId = null){
        $this->loadModel('MemberIdentification');
        if (!$this->MemberIdentification->exists($meberIdentityId)) {
            throw new NotFoundException(__('Requested Member is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->MemberIdentification->id = $meberIdentityId;
        if ($this->MemberIdentification->saveField('status',Configure::read('InActive'))) {
            $this->Session->setFlash(__('The Member Identification has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The Member Identification could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'member_identifications'));
    }
    public function general_receipt(){
        $this->loadModel('SocietyOtherIncome');
        $societyLoginID = $this->societyloginID;
        $societyOtherIncomeData = $this->SocietyOtherIncome->find('all',array('conditions' => array('SocietyOtherIncome.society_id' => $societyLoginID,'SocietyOtherIncome.status'=>Configure::read('Active')),'order'=>array('SocietyOtherIncome.payment_date' => 'desc')));
        
        $this->set(compact('societyOtherIncomeData'));
    }
    
    public function add_general_receipt($societyPaymentId = null) {
        $responseArr['error'] = 1;
        $this->loadModel('SocietyOtherIncome');
        $this->loadModel('SocietyLedgerHeads');
        $societyLoginID = $this->societyloginID;
        $responseArr['error_message'] = 'General Receipt data could not saved.';
        
        
        if ($this->request->is(array('post', 'put'))) {
                if ($this->request->data['SocietyOtherIncome']) {
                    if (isset($this->request->data['SocietyOtherIncome']) && count($this->request->data['SocietyOtherIncome']) > 0) {
                        foreach ($this->request->data['SocietyOtherIncome'] as $societyOhterIncomeData) {
                            if (isset($societyOhterIncomeData['ledger_head_id']) && $societyOhterIncomeData['ledger_head_id'] != '') {
                                $billVoucherNumber = $this->SocietyBill->billVoucherNumber();
                                $postData['general_receipt_number'] = isset($billVoucherNumber) ? $billVoucherNumber : 0;
                                $postData['society_id'] = isset($societyLoginID) ? $societyLoginID : 0;
                                $postData['id'] = isset($societyOhterIncomeData['id']) ? $societyOhterIncomeData['id'] : null;
                                $postData['amount_paid'] = isset($societyOhterIncomeData['amount_paid']) && !empty($societyOhterIncomeData['amount_paid']) ? $societyOhterIncomeData['amount_paid'] : '0.00';
                                $postData['cheque_no'] = isset($societyOhterIncomeData['cheque_no']) && !empty($societyOhterIncomeData['cheque_no']) ? $societyOhterIncomeData['cheque_no'] : '0.00';
                                $postData['society_bank_id'] = isset($this->request->data['SocietyOtherIncomeHeader']['society_bank_id']) ? $this->request->data['SocietyOtherIncomeHeader']['society_bank_id'] : 'null';
                                $postData['description'] = isset($societyOhterIncomeData['description']) ? $societyOhterIncomeData['description'] : '';
                                $postData['title'] = isset($societyOhterIncomeData['title']) ? $societyOhterIncomeData['title'] : '';
                                $postData['payment_mode'] = isset($this->request->data['SocietyOtherIncomeHeader']['payment_mode']) ? trim($this->request->data['SocietyOtherIncomeHeader']['payment_mode']) : 'null';
                                $postData['ledger_head_id'] = isset($societyOhterIncomeData['ledger_head_id']) ? $societyOhterIncomeData['ledger_head_id'] : 0;
                                $postData['payment_date'] = isset($societyOhterIncomeData['payment_date']) ? $societyOhterIncomeData['payment_date'] : '';
                                $postData['entry_date'] = $this->Util->getDateTime();
                                $postData['cdate'] = $this->Util->getDateTime();
                                $postData['udate'] = $this->Util->getDateTime();
                                $this->SocietyOtherIncome->create();
                                if ($this->SocietyOtherIncome->save($postData)) {
                                    $this->Session->setFlash(__('The general receipts has been added.'), 'success');
                                } else {
                                    $this->Session->setFlash(__('The general receipts could not be deleted. Please, try again.'), 'error');
                                }
                                return $this->redirect(array('action' => 'general_receipt'));
                            }
                        }
                    }
                
            }
        }
        $otherIncomePostData = array();
        if ($this->request->is(array('get')) && $societyPaymentId) {
            $otherIncomePostData = $this->SocietyOtherIncome->find('all', array('conditions' => array('SocietyOtherIncome.id' => $societyPaymentId, 'SocietyOtherIncome.society_id' => $societyLoginID), 'recursive' => -1));
        }
        //print_r($otherIncomePostData);die;
        $this->set(compact('otherIncomePostData'));
    }
    
    public function delete_general_receipts($paymentId = null){
        $this->loadModel('SocietyOtherIncome');
        if (!$this->SocietyOtherIncome->exists($paymentId)) {
            throw new NotFoundException(__('Requested payment is not found.'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->SocietyOtherIncome->id = $paymentId;
        if ($this->SocietyOtherIncome->saveField('status', Configure::read('InActive'))) {
            $this->Session->setFlash(__('The payment has been deleted.'),'error');
        } else {
            $this->Session->setFlash(__('The payment could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'general_receipt'));
    }
    
    public function change_password() {
        $this->layout = 'society_dashboard';
        
        $Email = new CakeEmail();
        //$Email->config('smtp');
        $Email->addTo('machhindra.mbtb@gmail.com');
        $Email->viewVars(array('username' => $this->Auth->user('username')));     
        /*$Email->template('change_password', 'fancy')
                ->emailFormat('html')
                ->from('ankush.mbtb@gmail.com')
                ->subject('New Project Added By Manager.')
                ->send();*/
        if ($this->request->is(array('post'))) {
            $conditions = array(
                'User.id' => $this->Auth->user('id'),
                'User.password' => $this->Auth->password($this->data['User']['old_password'])
            );
            if ($this->User->hasAny($conditions)) {
                $password = $this->Auth->password($this->data['User']['old_password']);
                $new_pwd = $this->Auth->password($this->data['User']['new_password']);
                $c_pwd = $this->Auth->password($this->data['User']['confirm_password']);
                //if (preg_match($regex, $this->data['User']['new_password'])) {
                if ($password == $new_pwd) {
                    $this->Session->setFlash(__('New Password cannot be same as Old Password.'), 'error');
                } else if ($new_pwd == $c_pwd) {
                    $this->User->id = $this->Auth->user('id');
                    if ($this->User->saveField('password', $this->data['User']['new_password'])) {
                        
}
                    $this->Session->setFlash(__('Password updated successfully. Please re-login with the new password.'));
                    return $this->redirect(array('action' => 'logout'));
                } else {
                    $this->Session->setFlash(__('Confirm Password should match New Password. '), 'error');
                }
            } else {
                $this->Session->setFlash(__('Old password is incorrect.'), 'error');
            }
        }
    }

}
