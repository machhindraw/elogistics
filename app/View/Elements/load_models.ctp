<!-- Modal for Print Bill -->
<div class="modal fade" id="print_bill" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-bg">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title modal-title-color">Print Bill</h6>
            </div>
            <div class="modal-body modal-body-bg">                
                <form action="" name="" id="" method="">
                    <div class="row">
                        <h5 class="text-center">AMBIKA SHRUSTI C.H.S. LTD</h5>
                        <div class="address-heading">Registration No. N.B.O/CIDCO/HHG(OH)/2858/JTR/YEAR 2008-2009 Dated: 19/01/2009</div>
                        <div class="address-heading">PLOT NO.92 SECTOR-44 KARVE SEAWOODS NERUL NAVI MUMBAI 4000706</div>
                    </div>
                    <div class="row">
                        <div class="bill">BILL</div>
                        <div class="bill-outer-border">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">Unit No : 101</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">Unit Area : 600 SqFt</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">Unit type :</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="">Name : Sandesh Y Ghorpade</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="">Bill For : January</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">Wing :</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">Floor No :</div>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="">Bill No : 136</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="">Bill Date : 11/01/2018</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="">Due Date : 15/01/2018</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="padding: 0px;">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="datable_1" class="table table-hover padding-td-none table-bordered display  pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Particular of Charges</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                    <tr id="">
                                                        <td>1</td>
                                                        <td>Maintenance charges</td>
                                                        <td>900.00</td>
                                                    </tr>
                                                    <tr id="">
                                                        <td>2</td>
                                                        <td>Sinking Fund</td>
                                                        <td>100.00</td>
                                                    </tr>
                                                    <tr id="">
                                                        <td>3</td>
                                                        <td>Parking charges</td>
                                                        <td>100.00</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>     
                            
                            <div class="row" style="padding:8px;">
                                <strong>Notes :</strong>
                                <ul>
                                    <li>1. As this Computer Generated Bill No. Signature is Required.</li>
                                    <li>2. Please pay by A/c Cheque Only.</li>
                                    <li>3. Kindly pay this bill by the due date mentioned on the bill. thereafter interest 21% p.a. will be charged for entire period.</li>
                                </ul>  
                            </div>
                            <br>
                            <div class="row simple-border" style="padding:0px;"></div>

                            <div class="row">
                                <div class="receipt-title">RECEIPT</div>
                                <div class="col-md-8">Received with thanks from <strong>Sandesh Y Ghorpade</strong></div>                                                
                                <div class="col-md-4">Unit No :<strong> 1010</strong></div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">Details of payments received are as under :-</div>
                                <div class="col-md-4" style="float:right;">period : 30/09/2017 To 31/03/2018</div>                                
                            </div> 
                            <div class="row">
                                <div style="padding: 0px;">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="datable_1" class="table table-bordered display padding-td-none pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th>Receipt</th>
                                                        <th>Date</th>
                                                        <th>Chq No.</th>
                                                        <th>Chq Date</th>
                                                        <th>Bank & Branch</th>
                                                        <th>Towards bill No.</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                    <tr id="">
                                                        <td >1</td>
                                                        <td>03/04/2017</td>
                                                        <td>Neft</td>
                                                        <td>03/04/2017</td>
                                                        <td></td>
                                                        <td>1, Bill Date: 01/04/2017</td>
                                                        <td>9400</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="">(Subject to Realisation of Cheque)</div>
                                </div>
                                <div class="col-md-7">
                                    <p class="pull-right">for AMBIKA SHRUSTI C.H.S. LTD</p>
                                    <br> <br> <br>
                                    <p class="pull-right">Authorized Signature</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <table class="table table-bordered">
		<tr>
			<td>Sr</td>
			<td colspan="3" >particular</td>
			<td>Ammount</td>
		</tr>
		<tr>
			<td>1</td>
			<td  colspan="3" >kojghvbn</td>
			<td>100</td>
		</tr>
		<tr>
			<td>2</td>
			<td colspan="3" >jhvjgfuy</td>
			<td>1203</td>
		</tr>
		<tr>
			<td>3</td>
			<td colspan="3" >ghjmlm</td>
			<td>3201</td>
		</tr>
		
		<tr>
			<td colspan="2" rowspan="4" > E.&.O.E </td>
			<td  colspan="2">dfbbdvbd</td>
			<td>3000</td>
		</tr>
		<tr>
			
			<td colspan="2">dfbbdvbd</td>
			<td>3000</td>
		</tr>
		<tr>
			
			<td >dfbbdvbd</td>
			<td >dfbbdvbd</td>
			<td>3000</td>
		</tr>
		<tr>
			
			<td>dfbbdvbd</td>
			<td>dfbbdvbd</td>
			<td>3000</td>
		</tr>
		<tr>
			<td colspan="2" >total</td>
			<td colspan="2" >sbsc </td>

			<td>66000</td>
		</tr>
		
	</table>
                </form>
            </div>
        </div>
    </div>
</div>