<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Adjustment Register</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>
                            <form method="post" id="societyAdjustmentRegister" name="societyAdjustmentRegister" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off"> 
                                <div class="member-tariff-left-box"> 
                                    <div class="row">                                       
                                        <div class="col-md-4">    
                                            <div class="society-report-form-box-label">Report Type &nbsp;</div>                                                                     
                                            <select class="society-report-form-box-field-dropdown-50" id="operator" name="data[][]">
                                                <option value="">Type</option>
                                                <option value="">Debit Note</option>
                                                <option value="">Credit Note</option>
                                                <option value="">Journal Voucher</option>
                                            </select> 
                                        </div>
                                        <div class="col-md-4">   
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">&nbsp;&nbsp;&nbsp;&nbsp;From &nbsp;&nbsp;&nbsp;</div>                                   
                                                <input type="date" class="society-report-form-box-field-date-70" id="" name="data[][]" value="">
                                            </div>
                                        </div>      
                                        <div class="col-md-4"> 
                                            <div class="society-report-form-field-box">
                                                <div class="society-report-form-box-label">&nbsp;&nbsp;&nbsp;&nbsp;To &nbsp;&nbsp;&nbsp;</div>                                   
                                                <input type="date" class="society-report-form-box-field-date-70" id="" name="data[][]" value="">
                                            </div>
                                        </div>                                                              
                                    </div>  
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="row">                           
                                        <div class="form-group">
                                            <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btn-sm"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>     
                                            <a type="button" data-toggle="tooltip" data-original-title="Print Friendly" class="btn btn-success btn-anim btn-sm" href="javascript:void(0);" onclick="printAllReports('print_society_outstanding_bill');"><i class="fa fa-print"></i><span class="btn-text">Print Friendly</span></a>                                                                            
                                            <a data-toggle="tooltip" data-original-title="Export to PDF" class="btn btn-success btn-anim btn-sm" href="javascript:void(0);" onclick=""><i class="fa fa-file-excel-o"></i><span class="btn-text">Export to PDF</span></a>                                    
                                            <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btn-sm" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a>                                                                            
                                        </div>
                                    </div>
                                </div> 
                                <div class="clearfix"></div>
                                <br><br><br>
                            </form> 

                            <div id="print_society_outstanding_bill">
                                <div class="print-society-outstanding-bill">
                                <div class="row1">
                                    <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                    <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>

                                    <div class="report-bill"></div>
                                    <div class="text-center"></div> 
                                    <br>
                                    <div class="col-md-9 col-xs-9">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Unit No: <strong><?php ?></strong></div>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Member Name : <strong><?php ?></strong></div>
                                                </div> 
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Particulars : <strong><?php ?></strong></div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-3">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Voucher No &nbsp;&nbsp;: &nbsp;&nbsp; 12</div>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="">Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; 01/04/2018</div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="report-bill-outer-section">
                                        <div class="table-wrap1">
                                            <table id="" class="table table-bordered1 padding-td-none padding-th-none" >
                                            <thead>
                                                <tr>
                                                    <th class="">Particular</th>
                                                    <th class="">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>   
                                        </table> 
                                        </div>
                                    </div>  
                                </div>    
                                </div>    
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>