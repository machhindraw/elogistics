<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Journal Voucher</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error">
                            </div>
                            <!--<div class="col-md-3 nopadding">
                                <div class="society-report-form-field-box">    
                                    <div class="society-report-form-box-label">VOUCHER NO.</div>                                                                     
                                    <input type="text" class="account-report-form-box-field-date" id="" name="data[JournalVoucher][voucher_no]" value="<?php echo isset($postData['JournalVoucher']['voucher_no']) ? $postData['JournalVoucher']['voucher_no'] :''; ?>">
                                </div>
                            </div>-->
                            
                            <div class="row">
                                <form class="" method="post" id="chequeClearDateFrm" name="chequeClearDateFrm" data-parsley-trigger="blur" class="form_parsley" data-validate="parsley" autocomplete="off">
                                    <div class="col-md-3 nopadding">
                                        <div class="society-report-form-field-box">    
                                            <div class="society-report-form-box-label">Voucher Date</div>                                                                     
                                            <input type="date" class="account-report-form-box-field-date" id="" name="data[JournalVoucher][voucher_date]" value="<?php echo isset($postData['JournalVoucher']['voucher_date']) ? $postData['JournalVoucher']['voucher_date'] :$utilObj->getFormatDate(); ?>" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3 nopadding">
                                        <div class="society-report-form-field-box">    
                                            <div class="society-report-form-box-label">Notes</div>                                                                     
                                            <textarea class="account-report-form-box-field-date" id="" name="data[JournalVoucher][note]" value=""><?php echo isset($postData['JournalVoucher']['note']) ? $postData['JournalVoucher']['note'] :''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="table-wrap">
                                        <div class="table-responsive1">
                                            <table class="table table-bordered padding-td-none padding-th-none">
                                                <thead>
                                                    <tr>
                                                        <th>Sr No.</th>
                                                        <th>Type</th>
                                                        <th>Account Name</th>
                                                        <th>DR. AMOUNT</th>
                                                        <th>CR. AMOUNT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-center" style="width:5%;">1</td>
                                                        <td class="text-center" style="width:5%;">
                                                            <select class="form-control1" id="" name="data[JournalVoucher][jv_creadit_type]">
                                                                <option value="Credit" <?php echo isset($postData['JournalVoucher']['jv_creadit_type']) && $postData['JournalVoucher']['jv_creadit_type'] == "Credit" ? "selected" :''; ?> >Credit</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control select2" id="id" name="data[JournalVoucher][jv_credit_member_head_id]">
                                                                <option value="">Select Members</option>
                                                                <optgroup label="Society Members List">
                                                                    <?php
                                                                    if (isset($memberListForJournalVouchers) && count($memberListForJournalVouchers) > 0) :
                                                                        $s = 1;
                                                                        foreach ($memberListForJournalVouchers as $societyMemberId => $societyMemberName){
                                                                            if (isset($postData['JournalVoucher']['jv_credit_member_head_id']) && $postData['JournalVoucher']['jv_credit_member_head_id'] == $societyMemberId) {
                                                                              ?>
                                                                    <option value="<?php echo $societyMemberId; ?>" selected=""><?php echo $societyMemberName; ?></option>
                                                                                                <?php } else { ?>
                                                                    <option value="<?php echo $societyMemberId; ?>"><?php echo "$societyMemberName"; ?></option>
                                                                             <?php
                                                                                $s++;
                                                                            }
                                                                        } endif;
                                                                    ?>  
                                                                </optgroup>
                                                            </select>
                                                            <select class="form-control select2" id="id" name="data[JournalVoucher][jv_credit_ledger_head_id]">
                                                                <option value="">Select Ledger</option>
                                                                <optgroup label="Society Ledger List">
                                                                    <?php
                                                                        if (isset($ledgerHeadForJournalVouchers) && count($ledgerHeadForJournalVouchers) > 0) :
                                                                            $s = 1;
                                                                            foreach ($ledgerHeadForJournalVouchers as $ledgerId => $ledgerName){
                                                                                if (isset($postData['JournalVoucher']['jv_credit_ledger_head_id']) && $postData['JournalVoucher']['jv_credit_ledger_head_id'] == $ledgerId) {
                                                                                  ?>
                                                                        <option value="<?php echo $ledgerId; ?>" selected=""><?php echo $ledgerName; ?></option>
                                                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $ledgerId; ?>"><?php echo "$ledgerName"; ?></option>
                                                                                 <?php
                                                                                $s++;
                                                                            }
                                                                        } endif;
                                                                    ?>
                                                                </optgroup>
                                                            </select>
                                                        </td>
                                                        <td><input type="hidden" name="data[JournalVoucher][id]" value="<?php echo isset($postData['JournalVoucher']['id']) ? $postData['JournalVoucher']['id'] :''; ?>"><input type="text" class="account-report-form-box-field-date" id=""  value="<?php echo "0.00"; ?>" readonly=""></td>
                                                        <td><input type="text" class="account-report-form-box-field-date" id="" name="data[JournalVoucher][jv_amount_credited]" value="<?php echo isset($postData['JournalVoucher']['jv_amount_credited']) ? $postData['JournalVoucher']['jv_amount_credited'] :'0.00'; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center" style="width:5%;">1</td>
                                                        <td class="text-center" style="width:5%;">
                                                            <select class="form-control1" id="" name="data[JournalVoucher][jv_debit_type]">
                                                                <option value="Debit" <?php echo isset($postData['JournalVoucher']['jv_debit_type']) && $postData['JournalVoucher']['jv_debit_type'] == "Debit" ? "selected" :''; ?>>Debit</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control select2" id="id" name="data[JournalVoucher][jv_debit_member_head_id]">
                                                                <option value="">Select Members</option>
                                                                <optgroup label="Society Members List">
                                                                    <?php
                                                                    if (isset($memberListForJournalVouchers) && count($memberListForJournalVouchers) > 0) :
                                                                        $s = 1;
                                                                        foreach ($memberListForJournalVouchers as $societyMemberId => $societyMemberName){
                                                                            if (isset($postData['JournalVoucher']['jv_debit_member_head_id']) && $postData['JournalVoucher']['jv_debit_member_head_id'] == $societyMemberId) {
                                                                              ?>
                                                                    <option value="<?php echo $societyMemberId; ?>" selected=""><?php echo $societyMemberName; ?></option>
                                                                                                <?php } else { ?>
                                                                    <option value="<?php echo $societyMemberId; ?>"><?php echo "$societyMemberName"; ?></option>
                                                                             <?php
                                                                                $s++;
                                                                            }
                                                                        } endif;
                                                                    ?>
                                                                </optgroup>
                                                            </select>
                                                            <select class="form-control select2" id="id" name="data[JournalVoucher][jv_debit_ledger_head_id]">
                                                                <option value="">Select Ledger</option>
                                                                <optgroup label="Society Ledger List">
                                                                <?php
                                                                if (isset($ledgerHeadForJournalVouchers) && count($ledgerHeadForJournalVouchers) > 0) :
                                                                    $s = 1;
                                                                    foreach ($ledgerHeadForJournalVouchers as $ledgerId => $ledgerName){
                                                                        if (isset($postData['JournalVoucher']['jv_debit_ledger_head_id']) && $postData['JournalVoucher']['jv_debit_ledger_head_id'] == $ledgerId) {
                                                                          ?>
                                                                <option value="<?php echo $ledgerId; ?>" selected=""><?php echo $ledgerName; ?></option>
                                                                                            <?php } else { ?>
                                                                <option value="<?php echo $ledgerId; ?>"><?php echo "$ledgerName"; ?></option>
                                                                         <?php
                                                                            $s++;
                                                                        }
                                                                    } endif;
                                                                ?>
                                                                </optgroup>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" class="account-report-form-box-field-date" id="" name="data[JournalVoucher][jv_amount_debited]" value="<?php echo isset($postData['JournalVoucher']['jv_amount_debited']) ? $postData['JournalVoucher']['jv_amount_debited'] :'0.00'; ?>"></td>
                                                        <td><input type="text" class="account-report-form-box-field-date" id="" value="<?php echo '0.00'; ?>" readonly=""></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-1 nopadding"></div>  
                                    <div class="clearfix"></div><br>
                                    <div class="col-md-12 account-report-form"> 
                                        <button type="submit" data-toggle="tooltip" data-original-title="" class="btn btn-success btn-anim btns"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button> 
                                        <a type="button" data-toggle="tooltip" data-original-title="Go to member payment list" class="btn btn-success btn-anim btns" href=""><i class="icon-rocket"></i><span class="btn-text">Cancel</span></a> 
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="table-responsive">
                                    <table id="datable_1" class="dataTable table table-hover table-bordered padding-th-none padding-td-none">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>VOUCHER NO.</th>
                                                <th>VOUCHER DATE</th>
                                                <th> TYPE</th>
                                                <th>DR. ACCOUNT NAME</th>
                                                <th>DR. AMOUNT</th>
                                                <th> TYPE</th>
                                                <th>CR. ACCOUNT NAME</th>
                                                <th>CR. AMOUNT</th>
                                                <th>Notes</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                        
                                    <?php if(isset($journalVoucherData) && count($journalVoucherData) > 0){
                                      $idCount = 1;
                                      foreach($journalVoucherData as $jVoucherData){
                                   ?>
                                            <tr id="<?php echo $idCount; ?>">
                                                <td><?php echo $idCount; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['voucher_no']) ? $jVoucherData['JournalVoucher']['voucher_no'] :'' ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['voucher_date']) && !empty($jVoucherData['JournalVoucher']['voucher_date']) ? $utilObj->getFormatDate($jVoucherData['JournalVoucher']['voucher_date'],'d/m/Y'):'' ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_debit_type']) && !empty($jVoucherData['JournalVoucher']['jv_debit_type']) ? $jVoucherData['JournalVoucher']['jv_debit_type'] :$jVoucherData['JournalVoucher']['jv_creadit_type'] ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_debit_ledger_head_id']) && !empty($jVoucherData['JournalVoucher']['jv_debit_ledger_head_id']) ? $ledgerHeadForJournalVouchers[$jVoucherData['JournalVoucher']['jv_debit_ledger_head_id']] : $memberListForJournalVouchers[$jVoucherData['JournalVoucher']['jv_debit_member_head_id']]; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_amount_debited']) ? $jVoucherData['JournalVoucher']['jv_amount_debited'] :'' ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_creadit_type']) && !empty($jVoucherData['JournalVoucher']['jv_creadit_type']) ? $jVoucherData['JournalVoucher']['jv_creadit_type'] :'' ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_credit_ledger_head_id']) && !empty($jVoucherData['JournalVoucher']['jv_credit_ledger_head_id']) ? $ledgerHeadForJournalVouchers[$jVoucherData['JournalVoucher']['jv_credit_ledger_head_id']] : $memberListForJournalVouchers[$jVoucherData['JournalVoucher']['jv_credit_member_head_id']]; ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['jv_amount_credited']) ? $jVoucherData['JournalVoucher']['jv_amount_credited'] :'' ; ?></td>
                                                <td><?php echo isset($jVoucherData['JournalVoucher']['note']) ? $jVoucherData['JournalVoucher']['note'] :'' ; ?></td>
                                                <td class='text-nowrap'>
                                                    <?php echo $this->Html->link('<i class="fa fa-pencil text-inverse m-r-10"></i>', array('controller' => 'reports', 'action' => 'journal_vouchers', $jVoucherData['JournalVoucher']['id']), array('class' => 'mr-25', 'data-toggle' => 'tooltip', 'data-original-title' => 'Edit', 'escape' => false)); ?>    
                                                    <?php echo $this->Form->postLink('<i class="fa fa-close text-danger"></i>',array('action' => 'journal_vouchers', $jVoucherData['JournalVoucher']['id']),array('class' => 'mr-25','data-toggle' => 'tooltip','data-original-title' => 'Delete', 'escape' => false), __('Are you sure you want to delete # %s?',$jVoucherData['JournalVoucher']['id'])); ?>
                                               </td>
                                            </tr>
                                  <?php $idCount++; } }?>
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>