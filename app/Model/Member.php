<?php
App::uses('AppModel', 'Model');
/**
 * Role Model
 *
 * @property User $User
 */
class Member extends AppModel {
    public $hasMany = array(
        'MemberTariff' => array(
            'className' => 'MemberTariff',
            'foreignKey' => 'member_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),'MemberTariffDetails' => array(
            'className' => 'MemberTariffDetails',
            'foreignKey' => 'member_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),'MemberPayment' => array(
            'className' => 'MemberPayment',
            'foreignKey' => 'member_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'MemberBillSummary' => array(
            'className' => 'MemberBillSummary',
            'foreignKey' => 'member_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $belongsTo = array(
        'Society' => array(
            'className' => 'Society',
            'foreignKey' => 'society_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Building' => array(
            'className' => 'Building',
            'foreignKey' => 'building_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ), 'Wing' => array(
            'className' => 'Wing',
            'foreignKey' => 'wing_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

