<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <button type="button" class="btn btn-success btn-icon left-icon" onclick="printAllReports('print_member_opening_balance');"><i class="fa fa-print"></i><span> Print</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div id="show_notify_error"></div>

                            <div class="clearfix"></div><br>                          
                            <div id="print_member_opening_balance">
                                <div class="print-member-opening-balance">
                                    <div class="row1">
                                        <h5 class="text-center"><?php echo isset($societyDetails['Society']['society_name']) ? $societyDetails['Society']['society_name'] : ''; ?></h5>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['registration_no']) ? $societyDetails['Society']['registration_no'] : ''; ?></div>
                                        <div class="report-address-heading"><?php echo isset($societyDetails['Society']['address']) ? $societyDetails['Society']['address'] : ''; ?></div>

                                        <div class="report-bill">Member Opening Balance</div>
                                        <div class="clearfix"></div>
                                        <div class="report-bill-outer-section">
                                            <div class="table-wrap1">
                                                <table class="table table-bordered padding-td-none padding-th-none">
                                                    <thead>
                                                        <tr>
                                                            <th>Unit No</th>
                                                            <th class="">Member Name</th>
                                                            <th class="">Principal</th>
                                                            <th class="">Interest</th>
                                                            <th class="">S Tax</th>
                                                            <th class="">Dr. Amount</th>
                                                            <th class="">Cr. Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="7" class="border-none"><strong>Building : </strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" class="border-none"><strong>Wing : </strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Shop-10</td>
                                                            <td class="border-bottom-none">Deshmukh</td>
                                                            <td class="text-right border-bottom-none">12000</td>
                                                            <td class="text-right border-bottom-none">550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                            <td class="text-right border-bottom-none">12550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Shop-10</td>
                                                            <td class=" border-bottom-none">Deshmukh</td>
                                                            <td class="text-right border-bottom-none">12000</td>
                                                            <td class="text-right border-bottom-none">550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                            <td class="text-right border-bottom-none">12550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Shop-10</td>
                                                            <td class=" border-bottom-none">Deshmukh</td>
                                                            <td class="text-right border-bottom-none">12000</td>
                                                            <td class="text-right border-bottom-none">550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                            <td class="text-right border-bottom-none">12550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="text-center border-top-bottom-bold"><strong>Total : </strong></td>    
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">21464.50</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">44735.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">264196</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" class="border-bottom-none"><strong>Wing : </strong></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom-none">Shop-10</td>
                                                            <td class="border-bottom-none">Deshmukh</td>
                                                            <td class="text-right border-bottom-none">12000</td>
                                                            <td class="text-right border-bottom-none">550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                            <td class="text-right border-bottom-none">12550</td>
                                                            <td class="text-right border-bottom-none">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="text-center border-top-bottom-bold"><strong>Total : </strong></td>    
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">21464.50</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">44735.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">264196</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="text-center border-top-bottom-bold"><strong>Total of Building : </strong></td>    
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">21464.50</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">44735.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">264196</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="text-center border-top-bottom-bold"><strong>Grand Total : </strong></td>    
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">21464.50</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">44735.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">264196</td>
                                                            <td class="border-top-bottom-bold text-right font-weight-bold">0.00</td>
                                                        </tr>
                                                    </tbody>  
                                                    <br><br>
                                                </table> 
                                            </div>
                                        </div>  
                                    </div>    
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

