<?php
App::uses('Component', 'Controller');
class UtilComponent extends Component {

    public  $arr_ones = array("", "One", "Two", "Three", "Four", "Five", "Six","Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen","Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen","Nineteen");
    public  $arr_tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }
    
    public function getDateTime($format='Y-m-d H:i:s') { // date to mysql date
        if($format){
            return date($format);
        }else{
            return date('Y-m-d H:i:s');
        }
    }
    
    public function getMysqlDateWordFormat($date=''){
        if($date){
            return date('F d Y', strtotime($date));
        }
    }
    
     public function getMonthWordFormat($monthNum=''){
        if($monthNum){
           return  date("F", mktime(0, 0, 0, $monthNum, 10));
        }
    }
    
    
    
    public  function mysqlToDate($date, $seperator = '') { // mysql date to date
        if ($timeStamp = strtotime($date)) {
            if ($seperator != '') {
                return date('d' . $seperator . 'm' . $seperator . 'Y', $timeStamp);
            } else {
                return date('d-m-Y', $timeStamp);
            }
        } else {
            return '';
        }
    }
    
    public  function getFormatDate($originalDate = '',$format='Y-m-d'){ // mysql date to date
            if ($originalDate != '') {
                return date($format,strtotime($originalDate));
            } else {
                return date('Y-m-d');
            }
    }
    
    public function getUserRole($accesslevel){
        $roleArr = array('1'=>'Admin','2'=>'Society','3'=>'Reseller');
        if(isset($roleArr[$accesslevel])){
            return $roleArr[$accesslevel];
        }
        return '';
    }
    
    public function compressImageSize($source, $destination, $quality = 90) {
        if ($source) {
            $imageSizeInfo = getimagesize($source);
            if ($imageSizeInfo['mime'] == 'image/jpeg')
                $image = imagecreatefromjpeg($source);
            elseif ($imageSizeInfo['mime'] == 'image/gif')
                $image = imagecreatefromgif($source);
            elseif ($imageSizeInfo['mime'] == 'image/png')
                $image = imagecreatefrompng($source);
            imagejpeg($image, $destination, $quality);
            return $destination;
        }
        return false;
    }

    public function getStateList(){
        App::import('model', 'State');
        $stateObj = new State();
        $stateList = $stateObj->find('list',array('fields'=>array('state_id','state_name')));
        if($stateList){
            return $stateList;
        }
        return array();
    }

    public function getCountryList(){
        App::import('model', 'Country');
        $countryObj = new Country();
        $countryList = $countryObj->find('list',array('fields'=>array('country_id','country_name')));
        if($countryList){
            return $countryList;
        }
        return array();
    }

    public function getModelValidationError($errors = array()){
            $error_message = '';
            foreach($errors as $errorData){
                $error_message = isset($errorData[0]) ? $errorData[0] : '';
            }
            return $error_message;
    }
    
    public function monthDropdownArray(){
        $formattedMonthArray = array(
                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December");
        return $formattedMonthArray;
    }
    
    public function societyBillingFrequency($billFrequencyType = null){
        //1-monthly 6-yearly 3-quarterly 5- half yearly
        $billingFrequenciesArray = array(
            1=>array("4" => "April","5" => "May", "6" => "June", "7" => "July", "8" => "August","9" => "September", "10" => "October", "11" => "November", "12" => "December","1" => "January", "2" => "February", "3" => "March"),
            2=>array("4" => "Apr-May","6" => "Jun-Jul", "8" => "Aug-Sep", "10" => "Oct-Nov", "12" => "Dec-Jan","2-3" => "Feb-Mar"),
            3=>array("4" => "Apr-May-Jun","7" => "Jul-Aug-Sep", "10" => "Oct-Nov-Dec", "1" => "Jan-Feb-Mar"),
            4=>array(),
            5=>array("4" => "Apr-Sep","10" => "Oct-Mar"),
            6=>array("4" => "April-March"));
        if (isset($billingFrequenciesArray[$billFrequencyType])) {
            return $billingFrequenciesArray[$billFrequencyType];
        }
        return array();
    }
    
    public function convertToAmount(&$aInputArr, $sAmountKey) {
        $aInputArr[$sAmountKey . '_display'] = number_format($aInputArr[$sAmountKey], 2);
        $aInputArr[$sAmountKey . '_words'] = self::getAmountInRupees($aInputArr[$sAmountKey]);
    }

    public function getAmountInRupees($sInputStr) {
        $value = str_replace(',', '', $sInputStr);
        $split = explode(".", $value);
        $rspart = "Rupees " . self::convertToWords($split[0]);
        $pspart = "";
        if (count($split) == 2) {
            $pspart = ($split[1] > 0) ? " and " . self::convertToWords($split[1], $amountType = 'paise') . " Paise" : "";
        }
        return $rspart . $pspart . ' Only';
    }
    
    public static function getAssesmentYearFromDate($checkDate) {
        $checkDate = new DateTime($checkDate);
        $month = $checkDate->format('m');
        $year = $checkDate->format('Y');
        $year2 = $checkDate->format('y');
        $assYear = '';
        if ($month > 3) {
            $newDate = $checkDate->add(new DateInterval('P2Y'));
            $assYear = ($year+1) . '-' . ($newDate->format('y'));
        } else {
            $newDate = $checkDate->add(new DateInterval('P1Y'));
            $assYear = $year . '-' . ($year2+1);
        }
        return $assYear;
    }
    
    public static function getFinancialYearFromDate($checkDate){
        $checkDate = new DateTime($checkDate);
        $month = $checkDate->format('m');
        $year = $checkDate->format('Y');
        $year2 = $checkDate->format('y');
        $finYear = '';
        if ($month > 3) {
            $newDate = $checkDate->add(new DateInterval('P1Y'));
            $finYear = $year . '-' . ($newDate->format('y'));
        } else {
            $newDate = $checkDate->sub(new DateInterval('P1Y'));
            $finYear = ($newDate->format('Y')) . '-' . $year2;
        }
        return $finYear;
    }
    
    public static function getBillDateBilldueDateFromFinancialYearDate($checkYear,$checkMonth){
        $returnFinanceArray = array();
        $checkDate = $checkYear.'-'.$checkMonth.'-01';
        $checkDate = new DateTime($checkDate);
        $month = $checkDate->format('m');
        $year = $checkDate->format('Y');
        $finYear = '';
        if ($month <= 3) {
            $newDate = $checkDate->add(new DateInterval('P1Y'));
            $finYear = $newDate->format('Y');
        } else {
            $newDate = $checkDate->sub(new DateInterval('P1Y'));
            $finYear = $year;
        }
        $finalDate = $finYear.'-'.$checkMonth.'-01';
        $first_date_find = strtotime(date("Y-m-d", strtotime($finalDate)) . ", first day of this month");
        $first_date = date("Y-m-d", $first_date_find);
        
        $last_date_find = strtotime(date("Y-m-d", strtotime($finalDate)) . ", last day of this month");
        $last_date = date("Y-m-d", $last_date_find);
        $returnFinanceArray['firstDate'] = $first_date;
        $returnFinanceArray['lastDate'] = $last_date;
        return $returnFinanceArray;
    }
    
    public function convertToWords($number, $amountType = 'rupees') {
//		if (($number < 0) || ($number > 99999999999)) {
//			return "$number out of script range";
//		}

        $arab = floor($number / 1000000000);  /* arab (giga) */
        $number -= $arab * 1000000000;
        $crores = floor($number / 10000000);  /* crore (giga) */
        $number -= $crores * 10000000;
        $lakhs = floor($number / 100000);  /* lakhs (giga) */
        $number -= $lakhs * 100000;
        $thousands = floor($number / 1000);  /* Thousands (kilo) */
        $number -= $thousands * 1000;
        $hundreds = floor($number / 100);   /* Hundreds (hecto) */
        $number -= $hundreds * 100;
        $tens = floor($number / 10);    /* Tens (deca) */
        $ones = $number % 10;      /* Ones */
        $res = "";
        if ($arab) {
            $res .= self::convertToWords($arab);
            $res .= ($arab > 10) ? " Arabs " : " Arab ";
        }
        if ($crores) {
            $res .= self::convertToWords($crores);
            $res .= ($crores > 10) ? " Crores " : " Crore ";
        }
        if ($lakhs) {
            $res .= self::convertToWords($lakhs);
            $res .= ($lakhs > 10) ? " Lakhs" : " Lakh";
        }
        if ($thousands) {
            $res .= (empty($res) ? "" : " ") . self::convertToWords($thousands) . " Thousand";
        }

        if ($hundreds) {
            $res .= (empty($res) ? "" : " ") . self::convertToWords($hundreds) . " Hundred";
        }


        if ($tens || $ones) {
            if (!empty($res)) {
                $res .= " and ";
            }
            if ($tens < 2) {
                $res .= $this->arr_ones[$tens * 10 + $ones];
            } else {
                $res .= $this->arr_tens[$tens];
                if ($ones) {
                    //$res .= "-" . self::$arr_ones[$ones];
                    $res .= " " .$this->arr_ones[$ones];
                }
            }
        }

        if (empty($res)) {
            $res = "zero";
        }
        return $res;
    }

    public  function getWordsArray($number, $amountType = 'rupees') {
        $crores = floor($number / 10000000);  /* crore (giga) */
        $number -= $crores * 10000000;
        $lakhs = floor($number / 100000);  /* lakhs (giga) */
        $number -= $lakhs * 100000;
        $thousands = floor($number / 1000);  /* Thousands (kilo) */
        $number -= $thousands * 1000;
        $hundreds = floor($number / 100);   /* Hundreds (hecto) */
        $number -= $hundreds * 100;
        $tens = floor($number / 10);    /* Tens (deca) */
        $ones = $number % 10;      /* Ones */
        $res = "";
//        $res['arab'] = ucfirst(self::convertToWords($arab));
        $res['crore'] = ucfirst(self::convertToWords($crores));
        $res['lakh'] = ucfirst(self::convertToWords($lakhs));
        $res['thousand'] = ucfirst(self::convertToWords($thousands));
        $res['hundred'] = ucfirst(self::convertToWords($hundreds));
        $res['tens'] = ucfirst(self::convertToWords($tens));
        $res['unit'] = ucfirst(self::convertToWords($ones));
        return $res;
    }
    
    public function NumberFormat2Decimal($amt = ''){
        if($amt != ''){
            $flotAmt = number_format((float)$amt, 2, '.', '');
            return $flotAmt;
        }
        return false;
    }
    
    public function CreditDebitAmountCheck($amtValue = ''){
        if($amtValue != ''){
            if($amtValue > 0){
                return number_format((float)abs($amtValue), 2, '.', '').' Dr';
            }else if($amtValue < 0){
                return number_format((float)abs($amtValue), 2, '.', '').' Cr';
            }else{
                return $amtValue;
            }
        }
        return false;
    }
}
