<div class="col-sm-12">
    <div class="table-wrap">
        <div class="table-responsive">
            <table id="datable_4" class="dataTable table table-bordered padding-td-none padding-th-none">
                <thead>
                    <tr>
                        <!--<th>Building</th>
                        <th>Wing</th>-->
                        <th>UnitNo</th>
                        <th>Member</th>
                        <th>Amount</th>
                        <th>Receipt Date</th>
                        <?php if(strtolower($payment_mode) != 'cash') { ?>
                        <th>ChequeNo</th>
                        <th>ChequeDate</th>
                        <th>Bank</th>
                        <th>Branch</th>
                        <th>IFSC Code</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($societyMemberLists) && count($societyMemberLists) > 0) :
                        $s = 1;
                        foreach ($societyMemberLists as $societyMemberId => $societyMemberName) {?>
                            <tr>
                                <!--<td><?php echo isset($societyMemberName['Building']['building_name'])? $societyMemberName['Building']['building_name'] :'';?> </td>
                                <td><?php echo isset($societyMemberName['Wing']['wing_name'])? $societyMemberName['Wing']['wing_name'] :'';?> </td>-->
                                <td><?php echo isset($societyMemberName['Member']['flat_no'])? $societyMemberName['Member']['flat_no'] :'';?> </td>
                                <td><input type="hidden"  name="data[MemberPayments][<?php echo $s; ?>][member_id]" value="<?php echo isset($societyMemberName['Member']['id'])? $societyMemberName['Member']['id'] :'';?>"><?php echo isset($societyMemberName['Member']['member_name'])? $societyMemberName['Member']['member_name'] :'';?></td>
                                <td><input type="text" class="numeric td-editable form-control numeric-field" id="amount_paid" name="data[MemberPayments][<?php echo $s; ?>][amount_paid]" data-parsley-type="digits"></td>
                                <td><input type="date" class="td-editable form-control" id="credited_date" name="data[MemberPayments][<?php echo $s; ?>][payment_date]" value="<?php echo isset($this->request->data['MemberPayment']['payment_date'])? $this->request->data['MemberPayment']['payment_date'] :date('Y-m-d');?>"></td>
                                <?php if(strtolower($payment_mode) != 'cash') { ?>
                                <td><input type="text" class="numeric td-editable form-control numeric-field" id="cheque_reference_number" name="data[MemberPayments][<?php echo $s; ?>][cheque_reference_number]" data-parsley-type="number"></td>
                                <td><input type="date" class="td-editable form-control" id="credited_date" name="data[MemberPayments][<?php echo $s; ?>][cheque_date]" value="<?php echo isset($this->request->data['MemberPayment']['cheque_date'])? $this->request->data['MemberPayment']['cheque_date'] :'';?>"></td>
                                <td>
                                    <select id="society_bank_id form-control" name="data[MemberPayments][<?php echo $s; ?>][member_bank_id]" style="width:150px;">
                                        <option value="" selected>Select Bank</option>
                                            <?php
                                            foreach ($societyBankLists as $bankDetails) { ?>
                                                <option value="<?php echo $bankDetails['Bank']['id']; ?>" selected=""><?php echo $bankDetails['Bank']['bank_name']; ?></option>
                                            <?php }    
                                            ?>  
                                    </select>
                                </td>
                                <td><input type="text" class="td-editable form-control" id="email" name="data[MemberPayments][<?php echo $s; ?>][member_bank_branch]" data-parsley-type="onlyLetterNumber"></td>
                                <td><input type="text" class="td-editable form-control" id="email" name="data[MemberPayments][<?php echo $s; ?>][member_bank_ifsc]" data-parsley-type="ifsc"></td>
                                <?php } ?>
                            </tr>
                        <?php $s++;} endif;?>
                </tbody>
            </table>
        </div>             
    </div>	
</div>                  