<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
            <?php echo $this->Session->flash(); ?>
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Society Parameter</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in ">
                    <form  method="post" id="societyParameters" name="societyParameters" autocomplete="off" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="col-md-4" style="border:1px solid #cfcfcf;padding:5px;">
                                <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="frequency_type" class="control-label">Billing Frequency</label>                                            
                                                <select class="form-control" id="frequency_type" name="data[SocietyParameter][billing_frequency_id]">
                                                    <option value="">Select Billing Frequency </option>
                                            <?php
                                                if (isset($billingFrequencyLists) && count($billingFrequencyLists) > 0) :
                                                    $s = 1;
                                                    foreach ($billingFrequencyLists as $billingFrequencyId => $billingFrequencyName) {
                                                        if (isset($this->request->data['SocietyParameter']['billing_frequency_id']) && $this->request->data['SocietyParameter']['billing_frequency_id'] == $billingFrequencyId) {
                                                            ?>
                                                    <option value="<?php echo $billingFrequencyId; ?>" selected=""><?php echo $billingFrequencyName; ?></option>
                                                        <?php } else { ?>
                                                    <option value="<?php echo $billingFrequencyId; ?>"><?php echo $billingFrequencyName; ?></option>
                                                            <?php
                                                            $s++;
                                                        }
                                                    } endif;
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_type" class="control-label">Interest Type</label>                                            
                                                <select class="form-control" id="interest_type" name="data[SocietyParameter][interest_type_id]">                                               
                                                    <option value="">Select Interest Type </option>
                                            <?php
                                                if (isset($InterestTypeLists) && count($InterestTypeLists) > 0) :
                                                    $s = 1;
                                                    foreach ($InterestTypeLists as $InterestTypeId => $InterestTypeName) {
                                                        if (isset($this->request->data['SocietyParameter']['interest_type_id']) && $this->request->data['SocietyParameter']['interest_type_id'] == $InterestTypeId) {
                                                            ?>
                                                    <option value="<?php echo $InterestTypeId; ?>" selected=""><?php echo $InterestTypeName; ?></option>
                                                        <?php } else { ?>
                                                    <option value="<?php echo $InterestTypeId; ?>"><?php echo $InterestTypeName; ?></option>
                                                            <?php
                                                            $s++;
                                                        }
                                                    } endif;
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_method" class="control-label">Interest Method</label>                                            
                                                <select class="form-control" id="interest_method" name="data[SocietyParameter][method_id]">                                               
                                                    <option value="">Select Interest Method </option>
                                            <?php
                                                if (isset($InterestMethodLists) && count($InterestMethodLists) > 0) :
                                                    $s = 1;
                                                    foreach ($InterestMethodLists as $InterestMethodId => $InterestMethodName) {
                                                        if (isset($this->request->data['SocietyParameter']['method_id']) && $this->request->data['SocietyParameter']['method_id'] == $InterestMethodId) {
                                                            ?>
                                                    <option value="<?php echo $InterestMethodId; ?>" selected=""><?php echo $InterestMethodName; ?></option>
                                                        <?php } else { ?>
                                                    <option value="<?php echo $InterestMethodId; ?>"><?php echo $InterestMethodName; ?></option>
                                                            <?php
                                                            $s++;
                                                        }
                                                    } endif;
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_rate" class="control-label">Tariff Type</label>                                            
                                                <select class="form-control" id="interest_rate" name="data[SocietyParameter][tariff_id]">                                               
                                                    <option value="">Select Tariff Type </option>
                                            <?php
                                                if (isset($TariffTypeLists) && count($TariffTypeLists) > 0) :
                                                    $s = 1;
                                                    foreach ($TariffTypeLists as $TariffTypeId => $TariffTypeName) {
                                                        //Temporarily hidden
                                                        if($TariffTypeName == 'Composition' || $TariffTypeName == 'Floor') {
                                                            continue;
                                                        }
                                                        if (isset($this->request->data['SocietyParameter']['tariff_id']) && $this->request->data['SocietyParameter']['tariff_id'] == $TariffTypeId) {
                                                            ?>
                                                    <option value="<?php echo $TariffTypeId; ?>" selected=""><?php  echo $TariffTypeName; ?></option>
                                                        <?php } else { ?>
                                                    <option value="<?php echo $TariffTypeId; ?>"><?php echo $TariffTypeName; ?></option>
                                                            <?php
                                                            $s++;
                                                        }
                                                    } endif;
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_rate" class="control-label">Interest Rate (% yearly)</label>                                            
                                                <input type="hidden" name="data[SocietyParameter][id]" value="<?php echo isset($this->request->data['SocietyParameter']['id']) ? $this->request->data['SocietyParameter']['id'] :''; ?>">
                                                <input type="text" class="form-control" id="title" name="data[SocietyParameter][interest_rate]" value="<?php echo isset($this->request->data['SocietyParameter']['interest_rate']) ? $this->request->data['SocietyParameter']['interest_rate'] :''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_rate" class="control-label">IGST(%)</label>                                            
                                                <input type="text" class="form-control" id="title" name="data[SocietyParameter][igst_tax_per]" value="<?php echo isset($this->request->data['SocietyParameter']['igst_tax_per']) ? $this->request->data['SocietyParameter']['igst_tax_per'] :''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_rate" class="control-label">SGST(%)</label>                                            
                                                <input type="text" class="form-control" id="title" name="data[SocietyParameter][sgst_tax_per]" value="<?php echo isset($this->request->data['SocietyParameter']['interest_rate']) ? $this->request->data['SocietyParameter']['sgst_tax_per'] :''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="interest_rate" class="control-label">CGST (%)</label>                                            
                                                <input type="hidden" name="data[SocietyParameter][id]" value="<?php echo isset($this->request->data['SocietyParameter']['id']) ? $this->request->data['SocietyParameter']['id'] :''; ?>">
                                                <input type="text" class="form-control" id="title" name="data[SocietyParameter][cgst_tax_per]" value="<?php echo isset($this->request->data['SocietyParameter']['cgst_tax_per']) ? $this->request->data['SocietyParameter']['cgst_tax_per'] :''; ?>">
                                            </div>
                                        </div>                                        
                                        <div class="col-md-12">
                                            <div class="form-group">                                                
                                                <label for="bill_note" class="control-label">Bill Notes</label> 
                                                <textarea rows="4" cols="50" style="resize: none;" type="text" class="form-control" id="bill_note" name="data[SocietyParameter][bill_note]" value="<?php echo isset($this->request->data['SocietyParameter']['bill_note']) ? $this->request->data['SocietyParameter']['bill_note'] :''; ?>"><?php echo isset($this->request->data['SocietyParameter']['bill_note']) ? $this->request->data['SocietyParameter']['bill_note'] :''; ?></textarea>
                                            </div>
                                        </div> 
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="is_tariff_mothly" class="control-label">Is Tariff Monthly</label>                                            
                                                <select class="form-control" id="is_tariff_mothly" name="data[SocietyParameter][is_tariff_mothly]">                                                                                                   
                                                    <option value="0"<?php if(isset($this->request->data['SocietyParameter']['is_tariff_mothly']) && $this->request->data['SocietyParameter']['is_tariff_mothly'] == 0) { echo 'selected';} ?>>No</option>                                            
                                                    <option value="1"<?php if(isset($this->request->data['SocietyParameter']['is_tariff_mothly']) && $this->request->data['SocietyParameter']['is_tariff_mothly'] == 1) { echo 'selected';} ?>>Yes</option>                                            
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="show_all_tariff_name" class="control-label">Show all tariff in bill</label>                                            
                                                <select class="form-control" id="show_all_tariff_name" name="data[SocietyParameter][show_all_tariff_name]">                                                                                                   
                                                    <option value="0"<?php if(isset($this->request->data['SocietyParameter']['show_all_tariff_name']) && $this->request->data['SocietyParameter']['show_all_tariff_name'] == 0) { echo 'selected';} ?>>No</option>                                            
                                                    <option value="1"<?php if(isset($this->request->data['SocietyParameter']['show_all_tariff_name']) && $this->request->data['SocietyParameter']['show_all_tariff_name'] == 1) { echo 'selected';} ?>>Yes</option>                                            
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-12">
                                            <div class="form-group">                                                
                                                <label for="special_field" class="control-label">Special Field</label> 
                                                <textarea rows="4" cols="50" style="resize: none;" type="text" class="form-control" id="special_field" name="data[SocietyParameter][special_field]" value="<?php echo isset($this->request->data['SocietyParameter']['special_field']) ? $this->request->data['SocietyParameter']['special_field'] :''; ?>"><?php echo isset($this->request->data['SocietyParameter']['special_field']) ? $this->request->data['SocietyParameter']['special_field'] :''; ?></textarea>
                                            </div>
                                        </div> 
                                        <div class="form-group mb-0">
                                            <div class="col-sm-offset-2 col-sm-10"> 
                                                <button type="submit" data-toggle="tooltip" data-original-title="Update Parameters" class="btn btn-success btn-anim" onclick=""><i class="icon-rocket"></i><span class="btn-text">Add/Update Parameters</span></button>                                                                       
                                            </div>
                                        </div>
                                    </div>
                                </div>						
                            </div>
                        </div>                    
                    </form>
                </div>	
                 <!--<div class="panel-wrapper collapse in ">
                    <form  method="post" id="societyParameters" name="societyParameters" autocomplete="off" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="col-md-3" style="border:1px solid #c8c8c8;padding:10px;">
                                <div class="form-group">
                                    <label for="frequency_type" class="control-label">Billing Frequency</label>                                            
                                    <select class="form-control" id="frequency_type" name="data[SocietyParameter][billing_frequency_id]">
                                        <option value="">Select Billing Frequency </option>
                                        <?php
                                        if (isset($billingFrequencyLists) && count($billingFrequencyLists) > 0) :
                                            $s = 1;
                                            foreach ($billingFrequencyLists as $billingFrequencyId => $billingFrequencyName) {
                                                if (isset($this->request->data['SocietyParameter']['billing_frequency_id']) && $this->request->data['SocietyParameter']['billing_frequency_id'] == $billingFrequencyId) {
                                                    ?>
                                        <option value="<?php echo $billingFrequencyId; ?>" selected=""><?php echo $billingFrequencyName; ?></option>
                                                <?php } else { ?>
                                        <option value="<?php echo $billingFrequencyId; ?>"><?php echo $billingFrequencyName; ?></option>
                                                <?php
                                                $s++;
                                            }
                                        } endif;
                                        ?> 
                                    </select>
                                </div>	
                                <div class="form-group">
                                    <input type="checkbox">Hide Zero value Tariffs
                                </div>
                                <div class="form-group">
                                    <input type="checkbox">Parking By No.
                                    <input type="checkbox">Merge Parking 
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Tariff To Be Ruonded <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Bill <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                            </div>
                            <div class="col-md-3" style="border:1px solid #c8c8c8;padding:10px;">
                                <div class="form-group">
                                    <label for="interest_type" class="control-label">Interest Type</label>                                            
                                    <select class="form-control" id="interest_type" name="data[SocietyParameter][interest_type_id]">                                               
                                        <option value="">Select InterestType </option>
                                    <?php
                                    if (isset($InterestTypeLists) && count($InterestTypeLists) > 0) :
                                        $s = 1;
                                        foreach ($InterestTypeLists as $InterestTypeId => $InterestTypeName) {
                                            if (isset($this->request->data['SocietyParameter']['interest_type_id']) && $this->request->data['SocietyParameter']['interest_type_id'] == $InterestTypeId) {
                                                ?>
                                        <option value="<?php echo $InterestTypeId; ?>" selected=""><?php echo $InterestTypeName; ?></option>
                                            <?php } else { ?>
                                        <option value="<?php echo $InterestTypeId; ?>"><?php echo $InterestTypeName; ?></option>
                                                <?php
                                                $s++;
                                            }
                                        } endif;
                                        ?> 
                                    </select>
                                </div>	
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Method <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <input type="checkbox">Allow Manual interest Calculation
                                </div>
                                <div class="form-group">
                                   <input type="checkbox"> Interest to be Round Off
                                   <span style="display:block;margin-top:5px;"><input type="textbox" style="width:80px;"> Penalty Rs.</span>
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Tariff Type</label>                                            
                                    <select class="form-control" id="interest_rate" name="data[SocietyParameter][tariff_id]">                                               
                                        <option value="">Select Tariff Type </option>
                                            <?php
                                                if (isset($TariffTypeLists) && count($TariffTypeLists) > 0) :
                                                    $s = 1;
                                                    foreach ($TariffTypeLists as $TariffTypeId => $TariffTypeName) {
                                                        if (isset($this->request->data['SocietyParameter']['tariff_id']) && $this->request->data['SocietyParameter']['tariff_id'] == $TariffTypeId) {
                                                            ?>
                                        <option value="<?php echo $TariffTypeId; ?>" selected=""><?php echo $TariffTypeName; ?></option>
                                                        <?php } else { ?>
                                        <option value="<?php echo $TariffTypeId; ?>"><?php echo $TariffTypeName; ?></option>
                                                            <?php
                                                            $s++;
                                                        }
                                                    } endif;
                                                    ?> 
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" style="border:1px solid #c8c8c8;padding:10px;">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">Interest</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="Interest on Arrears" value=""  required="">
                                    </div>
                                </div>
                                <br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">Rebate</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                    </div>
                                </div>
                                <br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">None Occupancy</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="None Occupancy Cahrges" value=""  required="">
                                    </div>
                                </div>
                                <br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">Sinking Fund</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="Sinking Fund" value=""  required="">
                                    </div>
                                </div>
                                <br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">TDS Payable A/c</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                    </div>
                                </div>
                                <br><br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">Chq. Return A/c</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                    </div>
                                </div>
                                <br><br><br>
                                 <div class="form-group">
                                    <label class="control-label col-md-3" for="society_id">IE_ Account</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="" name="" placeholder="income & Expediture a/c" value=""  required="">
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label for="interest_rate" class="control-label">Interest <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div> 
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Rebate  <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">None Occupancy <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Sinking Fund <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">TDS Payable A/c <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">Chq. Return A/c <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                                <div class="form-group">
                                    <label for="interest_rate" class="control-label">IE_ Account <span class="required">*</span></label>                                            
                                    <input type="text" class="form-control" id="" name="" placeholder="" value=""  required="">
                                </div>
                            </div>                            
                            <div class="form-group mb-0">
                                <div class="col-sm-offset-2 col-sm-10"> 
                                    <button type="submit" data-toggle="tooltip" data-original-title="Update Parameters" class="btn btn-success btn-anim" onclick=""><i class="icon-rocket"></i><span class="btn-text">Add/Update Parameters</span></button>                                                                       
                                </div>
                            </div>
                        </div>                    
                    </form>
                </div>-->
            </div>
        </div>
    </div>
</div>