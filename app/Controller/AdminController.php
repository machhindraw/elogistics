<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */




class AdminController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','Session','RequestHandler','Util');
    public $helpers = array('Html','Form');
    public $useModel = true;
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('User');
        $this->loadModel('ResellerSociety');
        $this->layout = 'admin_dashboard';
    }

    public function dashboard(){
        $this->loadModel('Member');
        $this->loadModel('Society');
        if ($this->Session->read('Auth.User.role') == 'Society') {
            $this->redirect(array('controller' => 'societys', 'action' => 'dashboard'));
        } else if ($this->Session->read('Auth.User.role') == 'Reseller') {
            $this->redirect(array('controller' => 'resellers', 'action' => 'dashboard'));
        }
        $countReseller = $this->User->find('count', array('conditions' => array('User.access_level' => Configure::read('AccessLevel.reseller')), 'recursive' => -1));
        $resellerSocietiesCount = $this->ResellerSociety->find('count', array('conditions' => array('ResellerSociety.status' => Configure::read('Active')), 'recursive' => -1));
        $countSocieties = $this->User->find('count', array('conditions' => array('User.access_level' => Configure::read('AccessLevel.society')), 'recursive' => -1));
        $allSocietysMemberCount = $this->Member->find('count', array('conditions' => array('Member.status' => Configure::read('Active')), 'recursive' => -1));
        $currentYearSocietiesLists = $this->Society->find('count', array('conditions' => array('Society.status' => Configure::read('Active'), 'YEAR(Society.cdate)' => date('Y')), 'recursive' => -1));
        $droppedSocietiesLists = $this->Society->find('count', array('conditions' => array('Society.status' => Configure::read('InActive')), 'recursive' => -1));
        $resellerAssignSocietys = array();
        $this->User->Behaviors->attach('Containable');
        $resellerSocietiesLists = $this->User->find('all', array('conditions' => array('User.access_level' => Configure::read('AccessLevel.reseller')), 'recursive' => -1));
        $allSocietiesLists = $this->User->find('all', array('contain' => array('Society'), 'conditions' => array('User.status' => Configure::read('Active'), 'User.access_level' => Configure::read('AccessLevel.society')), 'limit' => 5, 'order' => array('User.username' => 'asc')));

        if (isset($resellerSocietiesLists) && count($resellerSocietiesLists) > 0) {
            $rCounter = 0;
            foreach ($resellerSocietiesLists as $resellerData) {
                $assignSocieties = $this->ResellerSociety->find('all', array('conditions' => array('ResellerSociety.status' => Configure::read('Active'), 'ResellerSociety.reseller_id' => $resellerData['User']['id'])));
                $resellerAssignSocietys[$rCounter]['resellerData'] = $resellerData['User'];
                if (isset($assignSocieties) && !empty($assignSocieties)) {
                    $aCounter = 0;
                    foreach ($assignSocieties as $assignSocietiesDatails) {
                        $resellerAssignSocietys[$rCounter]['assignSocietiesData'][$aCounter]['Society']['id'] = isset($assignSocietiesDatails['Society']['id']) ? $assignSocietiesDatails['Society']['id'] : '';
                        $resellerAssignSocietys[$rCounter]['assignSocietiesData'][$aCounter]['Society']['society_name'] = isset($assignSocietiesDatails['Society']['society_name']) ? $assignSocietiesDatails['Society']['society_name'] : '';
                        $aCounter++;
                    }
                }
                $rCounter++;
            }
        }
        $this->set(compact('countSocieties', 'resellerSocietiesCount', 'countReseller', 'allSocietysMemberCount', 'currentYearSocietiesLists', 'droppedSocietiesLists', 'resellerAssignSocietys','allSocietiesLists'));
    }

    public function add_societys($societyID = null){
        if($societyID != ''){
            $this->User->recursive = 1;
            $singleSocietyRecord = $this->User->findById((int)$societyID);
            $this->set('singleSocietyRecord', $singleSocietyRecord);
        }
    }    
    public function view_societys(){
        $this->User->recursive = 1;
        $societyData = $this->User->find('all',array('conditions'=>array('User.added_by' => $this->Auth->user('id')),'contain'=>array('Society')));
        $this->set('societyData',$societyData);
        //print_r($societyData);die;
    }
    public function society_parameter(){
        $this->User->recursive = 1;
        $this->loadModel('BillingFrequency');
        $this->loadModel('InterestType');
        $this->loadModel('TariffType');
        $this->loadModel('AccountCategory');
        $this->loadModel('AccountHead');
        
        $billingFrequencyData = $this->BillingFrequency->find('all',array('fields'=>array('frequency_type')));        
        $interestTypeData = $this->InterestType->find('all',array('fields'=>array('interest_type')));
        $tariffTypeData = $this->TariffType->find('all',array('fields'=>array('tariff_type')));
        $accountCategoryData = $this->AccountCategory->find('all',array('fields'=>array('title')));
        $accountHeadData = $this->AccountHead->find('all',array('fields'=>array('title','transaction_type')));
        $this->set(compact('tariffTypeData','interestTypeData','billingFrequencyData','accountCategoryData','accountHeadData'));
    }    
  
    public function add_update_login_credentials(){
            $this->layout = false;//Update add Society/Login 
            $response = array();
            $response['error'] = 1;
            $response['error_message'] = 'Opps! Somthing went wrong.';
            $insertFlag = true;
            $checkUserNameFlag = true;
            $userModelObj = ClassRegistry::init('User');
            $SocietyModelObj = ClassRegistry::init('Society');
            if ($this->RequestHandler->isAjax()) {
                $this->autoRender = false;
                $userModelObj->set($this->request->data);
                $societyUserID = isset($this->request->data['Admin']['society_user_id']) ? $this->request->data['Admin']['society_user_id'] : '';
                $aData['username'] = isset($this->request->data['Admin']['society_username']) ? $this->request->data['Admin']['society_username'] : '';
                if ($societyUserID == '') {
                    $aData['password'] = isset($this->request->data['Admin']['society_Password']) ? $this->request->data['Admin']['society_Password'] : '';
                    $aData['cdate'] = $this->Util->getDateTime();
                }
                $aData['added_by'] = $this->Session->read('Auth.User.id');
                $aData['access_level'] = isset($this->request->data['Admin']['access_level']) ? trim($this->request->data['Admin']['access_level']) : '';
                $aData['role'] = isset($this->request->data['Admin']['access_level']) ? $this->Util->getUserRole($this->request->data['Admin']['access_level']) : '';
                $aData['status'] = Configure::read('Active');
                $aData['udate'] = $this->Util->getDateTime();
                //print_r($aData);die;
                if ($userModelObj->isUniqueUsername($aData)) {
                    $checkUserNameFlag = false;
                }
                if (!empty($societyUserID)) {
                    $checkUserNameFlag = true;
                }
                if ($checkUserNameFlag) {
                        if (!empty($aData)) {
                            if (!empty($societyUserID)) {
                                $userModelObj->id = $societyUserID;
                            if ($SocietyModelObj->hasAny(array('Society.user_id' => $societyUserID))) {
                                $insertFlag = false;
                            } else {
                                $insertFlag = true;
                            }
                        }
                        $userSaveData = $userModelObj->save($aData);
                        if ($userSaveData) {
                            $societyUserID = isset($userSaveData['User']['id']) ? $userSaveData['User']['id'] : $userSaveData['User']['Admin']['society_user_id'];
                        }
                        if($societyUserID != '' && (int) $aData['access_level'] == 2){
                            if ($insertFlag) {
                                $response = $this->insertIntoSocietysByAdmin($this->request->data, $societyUserID);
                            }else{
                                $response = $this->updateIntoSocietysByAdmin($this->request->data, $societyUserID);
                            }
                        }else{
                            $response['error'] = 0;
                            $response['error_message'] = 'Reseller login has been create successfully';
                        }//Make sure access level is society
                    }
                } else {
                    $response['error'] = 1;
                    $response['error_message'] = 'This username name is already in use . please use different';
                }
            }
        return new CakeResponse(array('body' => json_encode($response), 'status' => 200));
    }

    public function insertIntoSocietysByAdmin($sData,$societyUserID){
        if(count($sData) > 0) {
            $SocietyModelObj = ClassRegistry::init('Society');
            $societyData['id'] = isset($societyUserID) ? $societyUserID : '';
            $societyData['user_id'] = isset($societyUserID) ? $societyUserID : '';
            $societyData['society_name'] = isset($sData['Admin']['society_name']) ? $sData['Admin']['society_name'] : '';
            $societyData['society_code'] = isset($sData['Admin']['society_code']) ? $sData['Admin']['society_code'] : '';
            $societyData['status'] = Configure::read('Active');
            $societyData['cdate'] = $this->Util->getDateTime();
            $societyData['udate'] = $this->Util->getDateTime();
            
            if ($SocietyModelObj->save($societyData)) {
                $response['error'] = 0;
                $response['error_message'] = 'Society login has been create successfully';
            }

            return $response;
        }
    }
    
    public function updateIntoSocietysByAdmin($sData = array(), $societyUserID = null){
        //echo print_r($sData);die;
        if (count($sData) > 0 && $societyUserID != '') {
            $SocietyModelObj = ClassRegistry::init('Society');
            $societyData['id'] = isset($societyUserID) ? $societyUserID : null;
            $societyData['user_id'] = isset($societyUserID) ? $societyUserID : '';
            $societyData['society_name'] = isset($sData['Admin']['society_name']) ? $sData['Admin']['society_name'] : '';
            $societyData['society_code'] = isset($sData['Admin']['society_code']) ? $sData['Admin']['society_code'] : '';
            $societyData['status'] = Configure::read('Active');
            $societyData['udate'] = $this->Util->getDateTime();
            $conditions = array('Society.user_id' => $societyUserID);
            if ($SocietyModelObj->hasAny($conditions)) {
                $presentSocietyData = $SocietyModelObj->find("first", array('conditions' => array('Society.user_id' => $societyUserID)));
                if (isset($presentSocietyData['Society'])) {
                    $societyData['id'] = isset($presentSocietyData['Society']['id']) ? $presentSocietyData['Society']['id'] : '';
                    if ($SocietyModelObj->save($societyData)) {
                        $response['error'] = 0;
                        $response['error_message'] = 'Society login has been updated successfully';
                    }
                    return $response;
                }
            }
        }
    }
    public function assign_societies() {
        $this->loadModel('Society');
        $this->loadModel('ResellerSociety');
        $resellerList = $this->User->find('list', array('conditions' => array('User.added_by' => $this->Auth->user('id'), 'User.access_level' => 3), 'fields' => array('User.id', 'User.username'), 'order' => 'User.username asc'));
        $societyList = $this->Society->find('all', array('conditions' => array('Society.status' => 0, 'Society.society_name !=' => ''), 'fields' => array('Society.id', 'Society.society_name', 'Society.user_id'), 'order' => 'Society.society_name asc'));
        $postArr = array();
        $insertFlag = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            if (isset($this->request->data['ResellerSociety']) && !empty($this->request->data['ResellerSociety'])) {
                $resellerId = isset($this->request->data['ResellerSociety']['reseller_id']) ? $this->request->data['ResellerSociety']['reseller_id'] : '';
                $options = array('ResellerSociety.reseller_id' => $resellerId, 'ResellerSociety.added_by' => $this->Auth->user('id'));
                if ($this->ResellerSociety->hasAny($options)) {
                    $this->ResellerSociety->deleteAll($options);
                }
                if (count($this->request->data['ResellerSociety']['societie_id']) > 0) {
                    foreach ($this->request->data['ResellerSociety']['societie_id'] as $societyId) {
                        $this->ResellerSociety->create();
                        $postArr['reseller_id'] = isset($this->request->data['ResellerSociety']['reseller_id']) ? $this->request->data['ResellerSociety']['reseller_id'] : '';
                        $postArr['added_by'] = $this->Auth->user('id');
                        $postArr['cdate'] = $this->Util->getDateTime();
                        $postArr['udate'] = $this->Util->getDateTime();
                        $postArr['status'] = Configure::read('Active');
                        $postArr['societie_id'] = $societyId;
                        if($societyId){
                            $this->Society->id = $societyId;
                            $societyUserID = $this->Society->field('user_id');
                            $postArr['user_id'] = $societyUserID;
                        }
                        $conditions = array('ResellerSociety.societie_id' => $societyId, 'ResellerSociety.reseller_id' => $resellerId, 'ResellerSociety.added_by' => $this->Auth->user('id'));
                        if (!$this->ResellerSociety->hasAny($conditions)) {
                            if ($this->ResellerSociety->save($postArr)) {
                                $insertFlag = true;
                            }
                            unset($postArr);
                        }
                    }
                }
                if ($insertFlag) {
                    $this->Session->setFlash(__('Society assigned sucessfully.'));
                    $this->redirect(array('action' => 'assign_societies'));
                }
            }
        }
        $this->set(compact('resellerList', 'societyList'));
    }
    
    public function getAssignedSocieties(){
        $this->layout = false; //Return All Socities ids which are assigned to the reseller.
        $response = array();
        $response['error_flag'] = 1;
        $response['error_message'] = 'Opps! Somthing went wrong.';
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = false;
            if (isset($this->request->data['resellerId']) && $this->request->data['resellerId'] != '') {
                $resellerID = isset($this->request->data['resellerId']) ? $this->request->data['resellerId'] : '';
                $resellerModelObj = ClassRegistry::init('ResellerSociety');
                $societyModelObj = ClassRegistry::init('Society');
                $assignedSocietyList = $resellerModelObj->find('list', array('conditions' => array('ResellerSociety.reseller_id' => $resellerID, 'ResellerSociety.added_by' => $this->Auth->user('id')), 'fields' => array('ResellerSociety.societie_id')));
                $societyList['societyList'] = $societyModelObj->find('all', array('conditions' => array('Society.status' => 0, 'Society.society_name !=' => ''), 'fields' => array('Society.id', 'Society.society_name', 'Society.user_id'), 'order' => 'Society.society_name asc','recursive'=>-1));
                if (!empty($assignedSocietyList)) {
                    $response['error_flag'] = 0;
                    $countID = 0;
                    foreach ($assignedSocietyList as $societyId) {
                        $response['assignedSocietyList'][$countID] = $societyId;
                        $countID++;
                    }
					
                }else{
                    $response['assignedSocietyList'] = array();
                }
                $response = array_merge($response,$societyList);
            }
        }
        return new CakeResponse(array('body' => json_encode($response, 1), 'status' => 200));
    }

    public function delete_societys($societyLoginID = null) {
        $userModelObj = ClassRegistry::init('User');
        if (!$userModelObj->exists($societyLoginID)) {
            throw new NotFoundException(__('Requested society is not registered.'));
        }
        $this->request->allowMethod('post', 'delete');
        $userModelObj->id = $societyLoginID;
        if ($userModelObj->saveField('status', true)) {
            $this->Session->setFlash(__('The society has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The society could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'view_societys'));
    }  
    }
