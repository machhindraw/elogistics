<?php

App::uses('Component', 'Controller');

class SocietyBillComponent extends Component {
    public $components = array('Session','Util');
    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    public function getSocietyBuildingLists(){
        App::import('model', 'Society');
        $societyObj = new Society();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyBuildingLists = $societyObj->Building->find('list', array('fields' => array('Building.id', 'Building.building_name'), 'conditions' => array('Building.society_id' => $societyLoginID)));
        $this->controller->set('societyBuildingLists', $societyBuildingLists);
    }

    
    public function getSocietyBuildingListsById(){
        App::import('model', 'Society');
        $societyObj = new Society();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyBuildingLists = $societyObj->Building->find('list', array('fields' => array('Building.id', 'Building.building_name'), 'conditions' => array('Building.society_id' => $societyLoginID)));
        if($societyBuildingLists){
            return $societyBuildingLists;
        }
        return array();
    }
    
    public function getSocietyWingListsById(){
        App::import('model', 'Society');
        $societyObj = new Society();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyBuildingLists = $societyObj->Wing->find('list', array('fields' => array('Wing.id', 'Wing.wing_name'), 'conditions' => array('Wing.society_id' => $societyLoginID)));
        if($societyBuildingLists){
            return $societyBuildingLists;
        }
        return array();
    }
    
    public function getSocietyParameters() {
        App::import('model', 'SocietyParameter');
        $societyParameterObj = new SocietyParameter();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyParameters = $societyParameterObj->find('first', array('conditions' => array('SocietyParameter.society_id' => $societyLoginID)));
        return $societyParameters;
    }
    
    public function loadAllNavigationModalsDropdowns(){
        $societyAccountCategoryLists = $this->getAccountCategoryData();
        if (count($societyAccountCategoryLists) > 0) {
            $this->controller->set('societyAccountCategoryLists', $societyAccountCategoryLists);
        }

        $societyHeadSubCategoryData = $this->getSocietyHeadSubCategories();
        if (count($societyHeadSubCategoryData) > 0) {
            $this->controller->set('societyHeadSubCategoryData', $societyHeadSubCategoryData);
        }

        $societyBankBalanceHeadsLists = $this->societyBankBalanceHeadsLists();
        if (count($societyBankBalanceHeadsLists) > 0) {
            $this->controller->set('societyBankBalanceHeadsLists', $societyBankBalanceHeadsLists);
        }

        $societyCashBalanceHeadsLists = $this->societyCashBalanceHeadsLists();
        if (count($societyCashBalanceHeadsLists) > 0) {
            $this->controller->set('societyCashBalanceHeadsLists', $societyCashBalanceHeadsLists);
        }

        $societyExpenseLedgerHeadsLists = $this->societyExpenseLedgerHeadsLists();
        if (count($societyExpenseLedgerHeadsLists) > 0) {
            $this->controller->set('societyExpenseLedgerHeadsLists', $societyExpenseLedgerHeadsLists);
        }

        $societyOtherIncomeHeadsLists = $this->societyOtherIncomeHeadsLists();
        if (count($societyOtherIncomeHeadsLists) > 0) {
            $this->controller->set('societyOtherIncomeHeadsLists', $societyOtherIncomeHeadsLists);
        }

        $societyMemberListData = $this->getSocietyMemberList();

        if (count($societyMemberListData) > 0) {
            $this->controller->set('societyMemberListData', $societyMemberListData);
        }

        $societyBankLists = $this->getSocietyBankNameList();
        if (count($societyBankLists) > 0) {
            $this->controller->set('societyBankLists', $societyBankLists);
        }

        $societyParameters = $this->getSocietyParameters();
        $this->controller->set('societyParameters',$societyParameters);

        $financialYear = $this->getFinancialYearFromDate();
        if (isset($financialYear) && $financialYear != '') {
            $checkYear = isset($financialYear) ? substr($financialYear, 0, 4) : date('Y');
            $checkMonth = date('m');
            $financialStartEndDate = $this->Util->getBillDateBilldueDateFromFinancialYearDate($checkYear, $checkMonth);
            if (isset($financialStartEndDate) && $financialStartEndDate != '') {
                $this->controller->set('financialStartEndDate', $financialStartEndDate);
            }
            $this->controller->set('financialYear', $financialYear);
        }
    }
    public function sortMultiDimentionalArray($a,$b){
        return (strtotime($a["formatted_date"]) <= strtotime($b["formatted_date"])) ? -1 : 1;
    }
    public function getSocietyLedgerHeadsDetails(){
        App::import('model', 'SocietyLedgerHeads');
        App::import('model','MemberBillGenerate');
        App::import('model','SocietyPayment');
        
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $memberBillGenerateObj = new MemberBillGenerate();
        $societyPaymentObj = new SocietyPayment();
        
        $societyLoginID = $this->Session->read('Auth.User.id');
        //$societyLedgerHeadsObj->recursive = -1;
        $societyLedgerHeadsDetails = $societyLedgerHeadsObj->find('first', array('conditions' => array('SocietyLedgerHeads.society_id' => $societyLoginID), 'order' => array('SocietyLedgerHeads.id' => 'asc')));
        if (isset($societyLedgerHeadsDetails['SocietyLedgerHeads']['id'])) {
            $societyLedgerHeadsNextRecord = $societyLedgerHeadsObj->find('first', array('fields' => array('SocietyLedgerHeads.id', 'SocietyLedgerHeads.title'), 'conditions' => array('SocietyLedgerHeads.id >' => $societyLedgerHeadsDetails['SocietyLedgerHeads']['id'], 'SocietyLedgerHeads.society_id' => $this->Session->read('Auth.User.id'), 'SocietyLedgerHeads.status' => Configure::read('Active')), 'order' => array('SocietyLedgerHeads.id' => 'asc')));
            $this->controller->set('societyLedgerHeadsNextRecord', $societyLedgerHeadsNextRecord);
        }
        
        //print_r($societyCashBalanceHeadsLists);die;
        
        if (count($societyLedgerHeadsDetails) > 0) {
            //Get society ledger head expenses and payments
            $txnType = $societyLedgerHeadsDetails['AccountHead']['transaction_type'];
            if (stripos($txnType, 'credit') !== false) {
                $txnType = 'credit';
            } else if (stripos($txnType, 'debit') !== false) {
                $txnType = 'debit';
            }

            $isInBill = $societyLedgerHeadsDetails['SocietyLedgerHeads']['is_in_bill_charges'];

            $societyPaymentData = array();

            $societyLedgerHeadId = $societyLedgerHeadsDetails['SocietyLedgerHeads']['id'];

            if ($isInBill == 0) {
                $societyPayments = $societyPaymentObj->find('all', array('conditions' => array('SocietyPayment.ledger_head_id' => $societyLedgerHeadId, 'SocietyPayment.society_id' => $this->Session->read('Auth.User.id'))));
                if (!empty($societyPayments)) {
                    foreach ($societyPayments as $paymentDetails) {
                        $paymentData = $paymentDetails['SocietyPayment'];
                        $paymentDate = date('d/m/Y', strtotime($paymentData['payment_date']));
                        $formattedDate = date('Y-m-d',strtotime($paymentData['bill_generated_date']));
                        $societyPaymentData[] = array('payment_date' => $paymentDate,'formatted_date' => $formattedDate,'amount' => $paymentData['total_amount'], 'txn_type' => $txnType);
                    }
                }
            } else {
                $memberBills = $memberBillGenerateObj->find('all', array('conditions' => array('MemberBillGenerate.ledger_head_id' => $societyLedgerHeadId, 'MemberBillGenerate.society_id' => $this->Session->read('Auth.User.id'))));
                if (!empty($memberBills)) {
                    foreach ($memberBills as $paymentDetails) {
                        $paymentData = $paymentDetails['MemberBillGenerate'];
                        $paymentDate = date('d/m/Y', strtotime($paymentData['bill_generated_date']));
                        $formattedDate = date('Y-m-d',strtotime($paymentData['bill_generated_date']));
                        $societyPaymentData[] = array('payment_date' => $paymentDate,'formatted_date' => $formattedDate,'amount' => $paymentData['amount'], 'txn_type' => $txnType);
                    }
                }
            }
            
            $journalVoucherLedgerPaymentData = $this->getJournalVoucherCreaditDebitDataByLedgerHeadId($societyLedgerHeadId);
            
            if (!empty($journalVoucherLedgerPaymentData)) {
                //$societyPaymentData[] = $JournalVoucherPaymentReceiptDetails;
                foreach ($journalVoucherLedgerPaymentData as $jvData) {
                    $txnJvType = '';
                    $jvAmt = 0;
                    $JVPARTICULAR = "";
                    if ($jvData['JournalVoucher']['jv_debit_ledger_head_id'] != '') {
                        $txnJvType = "debit";
                        $jvAmt = $jvData['JournalVoucher']['jv_amount_debited'];
                        $JVPARTICULAR = "JV Debited. V.No ".$jvData['JournalVoucher']['voucher_no'];
                    }

                    if ($jvData['JournalVoucher']['jv_credit_ledger_head_id'] != '') {
                        $txnJvType = "credit";
                        $jvAmt = $jvData['JournalVoucher']['jv_amount_credited'];
                        $JVPARTICULAR = "JV Credited. V.No ".$jvData['JournalVoucher']['voucher_no'];
                    }

                    $paymentDate = isset($jvData['JournalVoucher']['voucher_date']) ? date('d/m/Y',strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                    $formattedDate = isset($jvData['JournalVoucher']['voucher_date']) ? date('Y-m-d',strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                    $societyPaymentData[] = array('particularNote' => $JVPARTICULAR,'payment_date' => $paymentDate,'formatted_date' => $formattedDate,'amount' => $jvAmt, 'txn_type' => $txnJvType);
                }
            }
            
            usort($societyPaymentData,array($this,"sortMultiDimentionalArray"));
           // print_r($societyPaymentData);die;
            $societyLedgerHeadsDetails['ledgerPaymentData'] = $societyPaymentData;
            $societyLedgerHeadsDetails['txn_type'] = $txnType;
            
            $this->controller->set('societyLedgerHeadsDetails', $societyLedgerHeadsDetails);
        }
    }
    
    

    public function navigateSocietyMemberDetails(){
        //flat shop details
        App::import('model', 'Member');
        $societyMemberObj = new Member();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $this->getSocietyBuildingLists();
        $societyMemberObj->Behaviors->attach('Containable');
        $societyMemberLists = $societyMemberObj->find('first', array('contain' => array('Society', 'Building', 'Wing'), 'conditions' => array('Member.status' => Configure::read('Active'), 'Member.society_id' => $societyLoginID), 'order' => array('Member.id' => 'asc')));
        if (isset($societyMemberLists['Member']['id'])) {
            $societyMemberNextRecord = $societyMemberObj->find('first', array('fields' => array('Member.id', 'Member.member_name'), 'conditions' => array('Member.id >' => $societyMemberLists['Member']['id'], 'Member.society_id' => $societyLoginID, 'Member.status' => Configure::read('Active')), 'order' => array('Member.id' => 'asc'), 'recursive' => -1));
            $this->controller->set('societyMemberNextRecord', $societyMemberNextRecord);

            $condition = array('MemberBillSummary.society_id' => $societyLoginID, 'MemberBillSummary.member_id' => $societyMemberLists['Member']['id']);
            $societyMemberBillPaymentData = array();
            $memberBillSummary = $societyMemberObj->MemberBillSummary->find('all', array('conditions' => $condition));
            
            
            if (isset($memberBillSummary) && count($memberBillSummary) > 0){
                $summaryCounter = 0;
                foreach ($memberBillSummary as $billData){
                    if (isset($billData['MemberBillSummary'])){
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary'] = $billData['MemberBillSummary'];
                        //Change date format
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date'] = date('d/m/Y', strtotime($societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date']));
                        if (isset($societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['balance_amount']) && $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['balance_amount'] > 0) {
                            $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['debit_credit_value'] = "DR";
                        } else {
                            $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['debit_credit_value'] = "CR";
                        }
                        $monthName = date("F", mktime(0, 0, 0, $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month'], 10));
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthName'] = $this->monthWordFormatByBillingFrequency($societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month']);
                        $paymentMadeBillNo = isset($billData['MemberBillSummary']['bill_no']) ? $billData['MemberBillSummary']['bill_no'] : '';
                        $conditionPayment = array('MemberPayment.society_id' => $this->Session->read('Auth.User.id'), 'MemberPayment.member_id' => $societyMemberLists['Member']['id'], 'MemberPayment.bill_generated_id' => $paymentMadeBillNo);
                        if ($societyMemberObj->MemberPayment->hasAny($conditionPayment)){
                            $memberPaymentDetails = $societyMemberObj->MemberPayment->find('all', array('conditions' => $conditionPayment));
                            if (isset($memberPaymentDetails) && count($memberPaymentDetails) > 0) {
                                $paymentCounter = 0;
                                foreach ($memberPaymentDetails as $paymentData) {
                                    $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter] = $paymentData['MemberPayment'];
                                    $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter]['payment_date'] = date('d/m/Y', strtotime($societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter]['payment_date']));
                                    $paymentCounter++;
                                }
                            }
                        }
                    }
                    $summaryCounter++;
                }
            }
            
            $journalVoucherMemberPaymentData = $this->getJournalVoucherCreaditDebitDataByMemberId($societyMemberLists['Member']['id']);
            if (!empty($journalVoucherMemberPaymentData)){
                $paymentCounter = 0;
				foreach ($journalVoucherMemberPaymentData as $jvData) {
                    if ($jvData['JournalVoucher']['jv_credit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_credit_member_head_id'] == $societyMemberLists['Member']['id']){
                        $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter]['amount_paid'] = isset($jvData['JournalVoucher']['jv_amount_credited']) ? $jvData['JournalVoucher']['jv_amount_credited'] : 0.00;
                        $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter]['payment_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $this->Util->mysqlToDate($jvData['JournalVoucher']['voucher_date'], '/') : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberPayment'][$paymentCounter]['cheque_reference_number'] = "JV Credited. V.No ".$jvData['JournalVoucher']['voucher_no'];
						$paymentCounter++;
                    }
                    
                    if ($jvData['JournalVoucher']['jv_debit_member_head_id'] != '' && $jvData['JournalVoucher']['jv_debit_member_head_id'] == $societyMemberLists['Member']['id']){
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['id'] = "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_no'] = "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_principal_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_bill_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['bill_generated_date'] = isset($jvData['JournalVoucher']['voucher_date']) ? $this->Util->mysqlToDate($jvData['JournalVoucher']['voucher_date'], '/') : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthName'] = isset($jvData['JournalVoucher']['voucher_date']) ? "JV Debited. V. No ".$jvData['JournalVoucher']['voucher_no'] : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['month'] = isset($jvData['JournalVoucher']['voucher_date']) ? date('m', strtotime($jvData['JournalVoucher']['voucher_date'])) : "";
                        $societyMemberBillPaymentData[$summaryCounter]['MemberBillSummary']['monthly_principal_amount'] = isset($jvData['JournalVoucher']['jv_amount_debited']) ? $jvData['JournalVoucher']['jv_amount_debited'] : 0.00;
                        $summaryCounter++;
                    }
                }
            }
            
            $this->controller->set('societyMemberBillPaymentData', $societyMemberBillPaymentData);
        }
        $this->controller->set('societyMemberLists', $societyMemberLists);
    }

    public function getFlatShopDetails(){
        App::import('model', 'Member');
        $Member = new Member();
        $societyLoginID = $this->Session->read('Auth.User.id');
        //$societyLedgerHeadsObj->recursive = -1;
        $societyFlatShopDetails = $Member->find('first', array('conditions' => array('Member.society_id' => $societyLoginID), 'order' => array('Member.id' => 'asc')));
        if (isset($societyFlatShopDetails['societyFlatShopDetails']['id'])) {
            $societyFlatShopDetailsNextRecord = $Member->find('first', array('fields' => array('Member.id', 'Member.member_name'), 'conditions' => array('Member.id >' => $societyFlatShopDetails['Member']['id'], 'Member.society_id' => $this->Session->read('Auth.User.id'), 'Member.status' => Configure::read('Active')), 'order' => array('Member.id' => 'asc')));
            $this->controller->set('societyFlatShopDetailsNextRecord', $societyFlatShopDetailsNextRecord);
        }
    }
    
    public function getAccountCategoryData(){
        App::import('model','AccountCategory');
        $accountCategoryObj = new AccountCategory();
        $societyAccountCategoryLists = $accountCategoryObj->find('list', array('conditions' => array('AccountCategory.status' => Configure::read('Active')), 'fields' => array('AccountCategory.id', 'AccountCategory.title'), 'order' => array('AccountCategory.title' => 'asc')));
        if($societyAccountCategoryLists){
            return $societyAccountCategoryLists;
        }
        return array();
    }
    
    public function getSocietyHeadSubCategories(){
        App::import('model','SocietyHeadSubCategory');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyHeadSubCategoryObj = new SocietyHeadSubCategory();
        $societyHeadSubCategoryData = $societyHeadSubCategoryObj->find('all', array('conditions' => array('SocietyHeadSubCategory.society_id' => $societyLoginID), 'fields' => array('SocietyHeadSubCategory.id', 'SocietyHeadSubCategory.account_status', 'SocietyHeadSubCategory.title'), 'order' => array('SocietyHeadSubCategory.title' => 'asc')));
         if($societyHeadSubCategoryData){
            return $societyHeadSubCategoryData;
        }
        return array();
    }
    
    public function getSocietyMemberPaymentBillNo() {
        $billNo = 1;
        App::import('model', 'MemberBillGenerate');
        $memberBillGenerateObj = new MemberBillGenerate();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $lastGeneratedBillData = $memberBillGenerateObj->find('first', array('conditions' => array('MemberBillGenerate.society_id' => $societyLoginID), 'fields' => array('MAX(bill_number) AS bill_number')));
        if (isset($lastGeneratedBillData[0]['bill_number']) && !empty($lastGeneratedBillData[0]['bill_number'])) {
            $billNo = $billNo + $lastGeneratedBillData[0]['bill_number'];
        } else {
            $billNo = $billNo;
        }
        return $billNo;
    }
    
    public function setUpdateBillCounterNo(){
        $billNo = 1;
        App::import('model', 'MasterCounter');
        $masterCounterObj = new MasterCounter();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $cData['society_id'] = $societyLoginID;
        $cData['counter_name'] = Configure::read('CounterName');
        $cData['counter'] = $billNo;
        $cData['status'] = Configure::read('Active');
        $cData['udate'] = $this->Util->getDateTime();
        
        $MasterDataCounter = $masterCounterObj->find('first', array('conditions' => array('MasterCounter.society_id' => $societyLoginID)));
        if(!empty($MasterDataCounter)){
             $counterValue = isset($MasterDataCounter['MasterCounter']['counter']) ? $MasterDataCounter['MasterCounter']['counter'] : 0;
             $billNo = $counterValue + 1;
             $masterCounterID = isset($MasterDataCounter['MasterCounter']['id']) ? $MasterDataCounter['MasterCounter']['id'] : 0;
             $masterCounterObj->updateAll(array("MasterCounter.counter" => "'".$billNo."'","MasterCounter.udate" => "'".$this->Util->getDateTime()."'"),array("MasterCounter.id" => $masterCounterID));
        }
        if(empty($MasterDataCounter)){
            $cData['cdate'] = $this->Util->getDateTime();
            $masterCounterObj->save($cData);
        }
        return $billNo;
    }
    
    public function getSocietyBankNameList() {
        $societyBankBalanceHeadsLists = $this->societyBankBalanceHeadsLists();
        if($societyBankBalanceHeadsLists){
            return $societyBankBalanceHeadsLists;
        }
    }
    
    public function getSocietyPaymentModeLists() {
        App::import('model', 'PaymentMode');
        $societyPaymentModeObj = new PaymentMode();
        $societyPaymentModeLists = $societyPaymentModeObj->find('list', array('fields' => array('PaymentMode.id','PaymentMode.payment_mode'), 'order' => array('PaymentMode.payment_mode' => 'asc')));
        if (count($societyPaymentModeLists) > 0) {
            return $societyPaymentModeLists;
        }
        return array();
    }
    
    public function getSocietyMemberList(){ 
        App::import('model', 'Member');
        $societyMemberObj = new Member();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyMemberLists = $societyMemberObj->find('list', array('conditions' => array('Member.status' => Configure::read('Active'), 'Member.society_id' => $societyLoginID), 'fields' => array('Member.id', 'Member.member_name'), 'order' => array('Member.id' => 'asc')));
        if (count($societyMemberLists) > 0) {
            return $societyMemberLists;
        }
        return array();
    }
	
	public function getSocietyMemberFlatList(){
        App::import('model', 'Member');
        $societyMemberObj = new Member();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyMemberLists = $societyMemberObj->find('list', array('conditions' => array('Member.status' => Configure::read('Active'), 'Member.society_id' => $societyLoginID), 'fields' => array('Member.flat_no','Member.member_name','Member.id'), 'order' => array('Member.id' => 'asc')));
        if (count($societyMemberLists) > 0) {
            return $societyMemberLists;
        }
        return array();
    }
    
    public function getBankList(){
        App::import('model', 'Bank');
        $BankObj = new Bank();
        $bankLists = $BankObj->find('list', array('conditions' => array('Bank.status' => Configure::read('Active')), 'fields' => array('Bank.id', 'Bank.bank_name'), 'order' => array('Bank.bank_name' => 'asc')));
         if (count($bankLists) > 0) {
            return $bankLists;
        }
        return array();
    }
    
    public function societyExpenseLedgerHeadsLists() {
        App::import('model', 'SocietyLedgerHeads');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $societyExpenseLedgerHeadsLists = $societyLedgerHeadsObj->find('list', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID, 'SocietyLedgerHeads.account_category_id' => 4), 'fields' => array('SocietyLedgerHeads.title'), 'order' => array('SocietyLedgerHeads.title' => 'asc')));
        //$societyExpenseLedgerHeadsLists = $societyHeadSubCategoryObj->find('list',array('fields' => array('id','title'),'conditions' => array('SocietyHeadSubCategory.status' => Configure::read('Active'), 'SocietyHeadSubCategory.society_id' => $societyLoginID, 'SocietyHeadSubCategory.account_head_id' => 4),'recursive' => -1));
        if($societyExpenseLedgerHeadsLists){
            return $societyExpenseLedgerHeadsLists;
        }
    }
    
    public function societyBankBalanceHeadsLists(){
        App::import('model', 'SocietyLedgerHeads');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $BankBalanceDetails = $this->societyBankBalanceDetails();
        $bankBalanceHeadSubCategoryId = isset($BankBalanceDetails['SocietyHeadSubCategory']['id']) ? $BankBalanceDetails['SocietyHeadSubCategory']['id'] :'';
        $societyBankBalanceHeadsLists = $societyLedgerHeadsObj->find('list', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'),'SocietyLedgerHeads.society_id' => $societyLoginID,'SocietyLedgerHeads.society_head_sub_category_id'=>$bankBalanceHeadSubCategoryId),'fields' => array('SocietyLedgerHeads.title'), 'order' => array('SocietyLedgerHeads.title' => 'asc')));
        if($societyBankBalanceHeadsLists){
            return $societyBankBalanceHeadsLists;
        }
    }
    
    public function societyCashBalanceHeadsLists(){
        App::import('model', 'SocietyLedgerHeads');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $cashBalanceDetails = $this->societyCashBalanceDetails();
        $cashBalanceHeadSubCategoryId = isset($cashBalanceDetails['SocietyHeadSubCategory']['id']) ? $cashBalanceDetails['SocietyHeadSubCategory']['id'] :'';
        $societyCashBalanceHeadsLists = $societyLedgerHeadsObj->find('list', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'),'SocietyLedgerHeads.society_id' => $societyLoginID,'SocietyLedgerHeads.society_head_sub_category_id'=>$cashBalanceHeadSubCategoryId),'fields' => array('SocietyLedgerHeads.title'), 'order' => array('SocietyLedgerHeads.title' => 'asc')));
        if($societyCashBalanceHeadsLists){
            return $societyCashBalanceHeadsLists;
        }
        return array();
    }
    
    public function societyBankBalanceDetails(){
        App::import('model','SocietyHeadSubCategory');
        $societyHeadSubCategoryObj = new SocietyHeadSubCategory();
        $BankBalanceDetails = $societyHeadSubCategoryObj->find('first',array('conditions'=>array('SocietyHeadSubCategory.title LIKE'=>"%Bank Balances%",'SocietyHeadSubCategory.status'=>Configure::read('Active')),'order' =>array('SocietyHeadSubCategory.id' =>'asc'),'recursive'=>-1));
        if(isset($BankBalanceDetails['SocietyHeadSubCategory']) && !empty($BankBalanceDetails['SocietyHeadSubCategory'])){
            return $BankBalanceDetails;
        }
        return array();
    }
    
    public function societyCashBalanceDetails(){
        App::import('model','SocietyHeadSubCategory');
        $societyHeadSubCategoryObj = new SocietyHeadSubCategory();
        $CashBalanceDetails = $societyHeadSubCategoryObj->find('first',array('conditions'=>array('SocietyHeadSubCategory.title LIKE'=>"%Cash Balance%",'SocietyHeadSubCategory.status'=>Configure::read('Active')),'order' =>array('SocietyHeadSubCategory.id' =>'asc'),'recursive'=>-1));
        if(isset($CashBalanceDetails['SocietyHeadSubCategory']) && !empty($CashBalanceDetails['SocietyHeadSubCategory'])){
            return $CashBalanceDetails;
        }
        return array();
    }
    
    public function getSocietyDetails(){
        App::import('model', 'Society');
        $societyObj = new Society();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyDetails = $societyObj->find('first',array('conditions' => array('Society.id' => $societyLoginID,'Society.status' => Configure::read('Active')),'order' => array('Society.id' => 'asc')));
        if(isset($societyDetails) && count($societyDetails) > 0){
            return $societyDetails;
        }
        return array();
    }
    
    public function getMemberDetailsByMemberIDAndFlatNo($fieldValue=null){
        App::import('model', 'Member');
        $societyMemberObj = new Member();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyMemberLists = $societyMemberObj->find('all',array('conditions' => array('Member.status' =>Configure::read('Active'),'Member.society_id' => $societyLoginID,'OR'=>array('Member.id'=>$fieldValue,'Member.flat_no'=>$fieldValue)),'order' => array('Member.floor_no'=>'asc','Member.flat_no'=>'asc')));
        if (count($societyMemberLists) > 0) {
            return $societyMemberLists;
        }
        return array();
    }
    
    public function getTariffDetailsBySocietyParameterTariffType($tariffType = null,$memberDetails = array()) {
        switch ($tariffType) {
            case Configure::read('TariffType.Building'):
                App::import('model','SocietyTariffBuildings');
                $societyTariffBuildingsObj = new SocietyTariffBuildings();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $conditions = array('SocietyTariffBuildings.society_id' => $societyLoginID,'SocietyTariffBuildings.amount >' => 0);
                if(isset($memberDetails['building_id']) && !empty($memberDetails['building_id'])){
                    $conditions['SocietyTariffBuildings.building_id'] = $memberDetails['building_id'];
                }
                $societyBuildingTariffDetails = $societyTariffBuildingsObj->find('all', array('conditions' => $conditions));
                if (isset($societyBuildingTariffDetails) && !empty($societyBuildingTariffDetails)) {
                    return $societyBuildingTariffDetails;
                }
                return array();
                exit;
                break;
            case Configure::read('TariffType.Wing') :
                App::import('model', 'SocietyTariffWings');
                $societyTariffWingsObj = new SocietyTariffWings();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $conditions = array('SocietyTariffWings.society_id' => $societyLoginID, 'SocietyTariffWings.amount >' => 0);
                if (isset($memberDetails['wing_id']) && !empty($memberDetails['wing_id'])) {
                    $conditions['SocietyTariffWings.building_id'] = $memberDetails['building_id'];
                    $conditions['SocietyTariffWings.wing_id'] = $memberDetails['wing_id'];
                }
                $societyTariffWingsDetails = $societyTariffWingsObj->find('all', array('conditions' => $conditions));
                if (isset($societyTariffWingsDetails) && !empty($societyTariffWingsDetails)) {
                    return $societyTariffWingsDetails;
                }
                return array();
                exit;
                break;
            case Configure::read('TariffType.UnitNo'):
            case Configure::read('TariffType.PerPerson'):    
                App::import('model', 'MemberTariff');
                $societyMemberTariffObj = new MemberTariff();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $conditions = array('MemberTariff.society_id' => $societyLoginID, 'MemberTariff.amount >' => 0, 'SocietyLedgerHeads.is_in_bill_charges' => 1);
                if (isset($memberDetails['id']) && !empty($memberDetails['id'])) {
                    $conditions['MemberTariff.member_id'] = $memberDetails['id'];
                }
                $societyMemberTariffDetails = $societyMemberTariffObj->find('all', array('fields' => array('MemberTariff.society_id', 'MemberTariff.member_id', 'MemberTariff.ledger_head_id', 'MemberTariff.amount', 'MemberTariff.tariff_serial', 'SocietyLedgerHeads.title', 'SocietyLedgerHeads.id', 'SocietyLedgerHeads.is_in_bill_charges', 'Member.id', 'Member.member_name', 'Member.flat_no'), 'conditions' => $conditions));
                if (isset($societyMemberTariffDetails) && !empty($societyMemberTariffDetails)) {
                    return $societyMemberTariffDetails;
                }
                return array();
                exit;
                exit;
                break;
            case Configure::read('TariffType.UnitType') :
                App::import('model', 'SocietyTariffUnitTypes');
                $societyTariffUnitTypesObj = new SocietyTariffUnitTypes();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $unityTypeCondition = array('SocietyTariffUnitTypes.society_id' => $societyLoginID);
                if (isset($memberDetails['unit_type']) && !empty($memberDetails['unit_type'])) {
                    $unityTypeCondition['SocietyTariffUnitTypes.unit_type'] = $memberDetails['unit_type'];
                }
                $societyTariffUnitTypesDetails = $societyTariffUnitTypesObj->find('all', array('conditions' => $unityTypeCondition));
                if (isset($societyTariffUnitTypesDetails) && !empty($societyTariffUnitTypesDetails)) {
                    return $societyTariffUnitTypesDetails;
                }
                return array();
                exit;
                break;
            case Configure::read('TariffType.TotalArea'):
                App::import('model', 'SocietyTariffTotalAreas');
                $societyTariffTotalAreasObj = new SocietyTariffTotalAreas();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $societyTotalAreasTariffDetails = $societyTariffTotalAreasObj->find('all', array('conditions' => array('SocietyTariffTotalAreas.society_id' => $societyLoginID)));
                if (isset($societyTotalAreasTariffDetails) && !empty($societyTotalAreasTariffDetails)) {
                    return $societyTotalAreasTariffDetails;
                }
                return array();
                exit;
                break;
            case Configure::read('TariffType.PerUnitArea'):
                App::import('model', 'SocietyTariffPerAreas');
                $societyTariffPerAreasObj = new SocietyTariffPerAreas();
                $societyLoginID = $this->Session->read('Auth.User.id');
                $societyTariffPerAreasDetails = $societyTariffPerAreasObj->find('all', array('conditions' => array('SocietyTariffPerAreas.MemberTariff' => $societyLoginID)));
                if (isset($societyTariffPerAreasDetails) && !empty($societyTariffPerAreasDetails)) {
                    return $societyTariffPerAreasDetails;
                }
                return array();
                exit;
                break;
            default:
                return array();
                break;
        }
    }
    
    public function checkPaymentModeBySocietyLedgerId($societyLedgerID,$returnPaymentID=false){
        App::import('model', 'SocietyLedgerHeads');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $societyLedgerData = $societyLedgerHeadsObj->find('first', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'),'SocietyLedgerHeads.id'=>$societyLedgerID),'recursive' => -1));
        if (isset($societyLedgerData['SocietyLedgerHeads']['title']) && !empty($societyLedgerData['SocietyLedgerHeads']['title'])) {
            if(stripos($societyLedgerData['SocietyLedgerHeads']['title'],'Cash') !== FALSE) {
                   $paymentMode = 'Cash';
                } else {
                   $paymentMode = 'Cheque'; 
                }
                $PaymentModeLists = $this->getSocietyPaymentModeLists();
                if($returnPaymentID){
                    foreach($PaymentModeLists as $paymentID =>$paymentModeValue){
                        if($paymentModeValue == $paymentMode){
                            $paymentMode = $paymentID;
                            break;
                        }
                    }
                }
           return $paymentMode;     
        }
        return false;
    }
    
    public function getCashInHandSocietyLedgerHeadId(){
        App::import('model', 'SocietyLedgerHeads');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $cashInHandLedgerDetails = $societyLedgerHeadsObj->find('first',array('conditions'=>array('SocietyLedgerHeads.title LIKE'=>"Cash in hand",'SocietyLedgerHeads.status'=>Configure::read('Active'),'SocietyLedgerHeads.society_id'=>$this->Session->read('Auth.User.id')),'order' =>array('SocietyLedgerHeads.id' => 'asc'),'recursive'=>-1));
        if(isset($cashInHandLedgerDetails['SocietyLedgerHeads']) && !empty($cashInHandLedgerDetails['SocietyLedgerHeads'])){
            return $cashInHandLedgerDetails;
        }
        return array();
    }
    
    public function getFinancialYearFromDate(){
        App::import('model', 'Society');
        $societyObj = new Society();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyObj->id = $societyLoginID;
        $FinancialYear = $societyObj->field('financial_year');
        $checkDate = $FinancialYear . '-' . date('m-d');
        $FinancialYearDate = $this->Util->getFinancialYearFromDate($checkDate);
        return $FinancialYearDate;
    }
    
    public function billVoucherNumber(){
        App::import('model', 'SocietyPayment');
        $societyPaymentObj = new SocietyPayment();
        $billVoucherNumber = 1;
        $societyLoginID = $this->Session->read('Auth.User.id');
        $billVoucherArr = $societyPaymentObj->find('all',array('conditions' => array('SocietyPayment.society_id' =>$societyLoginID),'fields' => array('MAX(SocietyPayment.bill_voucher_number) as max_bill_voucher'),'recursive' => 1));
        if(isset($billVoucherArr[0][0]['max_bill_voucher']) && $billVoucherArr[0][0]['max_bill_voucher'] >= 0){
            $billVoucherNumber = $billVoucherArr[0][0]['max_bill_voucher']+1;
        }
        return $billVoucherNumber;
    }
    
    public function memberPaymentReceiptUniqueNumber(){
        App::import('model', 'MemberPayment');
        $memberPaymentObj = new MemberPayment();
        $paymentReceiptNumber = 1;
        $societyLoginID = $this->Session->read('Auth.User.id');
        $billReceiptArr = $memberPaymentObj->find('all',array('conditions' => array('MemberPayment.society_id' =>$societyLoginID),'fields' => array('MAX(MemberPayment.receipt_id) as max_receipt_number'),'recursive' => 1));
        if(isset($billReceiptArr[0][0]['max_receipt_number']) && $billReceiptArr[0][0]['max_receipt_number'] >= 0){
            $paymentReceiptNumber = $billReceiptArr[0][0]['max_receipt_number']+1;
        }
        return $paymentReceiptNumber;
    }
    
    public function memberBillsUniqueNumber(){
        App::import('model', 'MemberBillSummary');
        $memberMemberBillSummaryObj = new MemberBillSummary();
        $billNumber = 1;
        $societyLoginID = $this->Session->read('Auth.User.id');
        $billReceiptArr = $memberMemberBillSummaryObj->find('all',array('conditions' => array('MemberBillSummary.society_id' =>$societyLoginID),'fields' => array('MAX(MemberBillSummary.bill_no) as bill_number'),'recursive' => 1));
        if(isset($billReceiptArr[0][0]['bill_number']) && $billReceiptArr[0][0]['bill_number'] >= 0){
            $billNumber = $billReceiptArr[0][0]['bill_number']+1;
        }
        return $billNumber;
    }
    
    public function monthWordFormatByBillingFrequency($monthNo=null){
        $societyParameters = $this->getSocietyParameters();
        if (isset($societyParameters['SocietyParameter'])) {
            $societyBillingFrequencyId = $societyParameters['SocietyParameter']['billing_frequency_id'];
            $billingFrequenciesArray = $this->Util->societyBillingFrequency($societyBillingFrequencyId);
            if (isset($billingFrequenciesArray[$monthNo])){
                return $billingFrequenciesArray[$monthNo];
            }
        }
        return false;
    }
    
     public function getAllBankList(){
        App::import('model', 'Bank');
        $bankObj = new Bank();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $bankLists = $bankObj->find('all',array('order'=>array('Bank.id asc'))); 
        if(isset($bankLists) && count($bankLists) > 0){
            return $bankLists;
        }
        return array();
    }
    
    public function multiplyTariffAmountByBillingFrequency(){
        $societyParameters = $this->getSocietyParameters();
        $multiplyBillingValue = 1;
        if (isset($societyParameters['SocietyParameter']['is_tariff_mothly']) && $societyParameters['SocietyParameter']['is_tariff_mothly'] == 1){
            switch ($societyParameters['SocietyParameter']['billing_frequency_id']) {
                case Configure::read('BillingFrequency.Monthly'):
                    $multiplyBillingValue = 1;
                    break;
                case Configure::read('BillingFrequency.BiMonthly') :
                    $multiplyBillingValue = 2;
                    break;
                case Configure::read('BillingFrequency.Quarterly'):
                    $multiplyBillingValue = 3;
                    break;
                case Configure::read('BillingFrequency.Quadruple') :
                    $multiplyBillingValue = 4;
                    break;
                case Configure::read('BillingFrequency.HalfYearly'):
                    $multiplyBillingValue = 6;
                    break;
                case Configure::read('BillingFrequency.Yearly'):
                    $multiplyBillingValue = 12;
                    break;
                default:
                    $multiplyBillingValue = 1;
                    break;
            }
        }
        return $multiplyBillingValue;
    }
    public function societyOtherIncomeHeadsLists() {
        App::import('model', 'SocietyLedgerHeads');
        $societyLoginID = $this->Session->read('Auth.User.id');
        $societyLedgerHeadsObj = new SocietyLedgerHeads();
        $societyOtherIncomeHeadsLists = $societyLedgerHeadsObj->find('list', array('conditions' => array('SocietyLedgerHeads.status' => Configure::read('Active'), 'SocietyLedgerHeads.society_id' => $societyLoginID), 'fields' => array('SocietyLedgerHeads.title'), 'order' => array('SocietyLedgerHeads.title' => 'asc')));
        //$societyExpenseLedgerHeadsLists = $societyHeadSubCategoryObj->find('list',array('fields' => array('id','title'),'conditions' => array('SocietyHeadSubCategory.status' => Configure::read('Active'), 'SocietyHeadSubCategory.society_id' => $societyLoginID, 'SocietyHeadSubCategory.account_head_id' => 4),'recursive' => -1));
        if($societyOtherIncomeHeadsLists){
            return $societyOtherIncomeHeadsLists;
        }
    }
    
    public function memberJournalVoucherUniqueNumber(){
        App::import('model', 'JournalVoucher');
        $journalVoucherObj = new JournalVoucher();
        $journalVoucherNumber = 1;
        $societyLoginID = $this->Session->read('Auth.User.id');
        $billReceiptArr = $journalVoucherObj->find('all',array('conditions' => array('JournalVoucher.society_id' =>$societyLoginID),'fields' => array('MAX(JournalVoucher.id) as max_voucher_number'),'recursive' => 1));
        if(isset($billReceiptArr[0][0]['max_voucher_number']) && $billReceiptArr[0][0]['max_voucher_number'] >= 0){
            $journalVoucherNumber = $billReceiptArr[0][0]['max_voucher_number']+1;
        }
        return $journalVoucherNumber;
    }
    
    public function getJournalVoucherCreaditDebitDataByLedgerHeadId($ledgerHeadId = ""){
        App::import('model', 'JournalVoucher');
        $journalVoucherObj = new JournalVoucher();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $options['AND'] = array('JournalVoucher.society_id' => $societyLoginID);
        $options['AND']['OR'][] = array('JournalVoucher.jv_debit_ledger_head_id' => $ledgerHeadId);
        $options['AND']['OR'][] = array('JournalVoucher.jv_credit_ledger_head_id' => $ledgerHeadId);
        
        $journalVoucherLedgerPaymentData = $journalVoucherObj->find('all', array('conditions' => $options,'order' => array('JournalVoucher.voucher_date' => 'asc'),'recursive'=>-1));
        
        if(isset($journalVoucherLedgerPaymentData) && !empty($journalVoucherLedgerPaymentData)){
           return $journalVoucherLedgerPaymentData;
        }
        return array();
    }
    
    
    public function getJournalVoucherCreaditDebitDataByMemberId($memberId = ""){
        App::import('model', 'JournalVoucher');
        $journalVoucherObj = new JournalVoucher();
        $societyLoginID = $this->Session->read('Auth.User.id');
        $options['AND'] = array('JournalVoucher.society_id' => $societyLoginID);
        $options['AND']['OR'][] = array('JournalVoucher.jv_debit_member_head_id' => $memberId);
        $options['AND']['OR'][] = array('JournalVoucher.jv_credit_member_head_id' => $memberId);

        $journalVoucherMemberPaymentData = $journalVoucherObj->find('all', array('conditions' => $options, 'order' => array('JournalVoucher.voucher_date' => 'asc'), 'recursive' => -1));

        if (isset($journalVoucherMemberPaymentData) && !empty($journalVoucherMemberPaymentData)) {
            return $journalVoucherMemberPaymentData;
        }
        return array();
    }
}    